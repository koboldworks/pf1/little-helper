Please review and edit this template fully before posting your issue.

## Summary

Describe the problem.

### Reproduction steps

1. Describe the steps needed to cause the issue reliably.

## Expected result

What did you expect instead?

## Actual result

What did you expect to see instead?

## Additional Details

If you have anything else to add to explain the problem, do it here.

## Screenshots

If you have screenshots, put them here (drag&drop the file).

## Logs

```
Place any relevant log output here.
```

```
Use multiple code blocks like this if you have more than one distinct parts.
```

/label ~bug
/health_status needs_attention
