export class TooltipManager {
	static TOOLTIP_DIRECTIONS: string = Readonly<{
		UP: 'UP'
		DOWN: 'DOWN'
		LEFT: 'LEFT'
		RIGHT: 'RIGHT'
	}>
}
