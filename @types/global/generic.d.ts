type UniqueId = string;

type UUID = string;

type UserId = string;

type ActionId = string;

type ItemId = string;

type ActorId = string;
