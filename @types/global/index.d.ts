import './foundry/tooltip-manager';

import * as shared from '../../core/shared-data.mjs';

declare global {
	export import SharedData = shared.SharedData;

	
	interface LenientGlobalVariableTypes {
		game: never // the type doesn't matter
	}

}


import './generic';
import './feature';

export {}
