# Features

- Pathfinder 1e specific
  - Display caster level on spellbook title to ease with problems relating to CL alterations.
  - Display little notes for checks.
    - Skill checks, ability checks, saving throws, concentration, etc.
  - Various checks chatcards are more detailed.
  - Attack cards have more descriptive inline rolls for attacks.
  - Highlights overuse of spells if spontaneous.
  - Display spells known per level.
  - Display base spell DC per level.
  - Display feat, trait, etc. counts.
  - Dispaly natural reach near size and stature.
  - Attack button redesign
  - Quick roll button for chat cards – for quickly and easily assisting with checks other characters do or otherwise doing the same check.
  - Details tab for actors to provide some insight.
  - Datalist feeding to various formulas for common formulas or values.
  - Coin weight display.
  - Item theory dialog (magnifying glass in items sheet header).
  - Language selector search.
  - Active buffs and summaries (displayed near chat log for currently selected token).
  - Little info tags for compendium packs.
  - Chat card layout melding
  - Quick value input to many input fields
  - Formula validation
  - Compendium browser filter clarifications
  - Item sheet extra icons
  - Basic system troubleshooter guide
  - Ability scores on summary tab.
  - Actor List Tooltips
  - Combat Tracker Tooltips
- System agnostic features
  - Macro directory enrichment
  - Warn about scene lacking initial view.
  - Warn about scene lacking player tokens with vision when token vision is enabled.
  - Sender tag. Little tag on side of chat messages that identifies if the message is from yourself (green), GM (gold), or someone else (grey).
  - Token HUD conditions label box.
  - Items & Actor directory enrichment
  - Auto-expand folders (per folder; right click folder to configure).

## Screens

### Actors

Natural reach:  
![Natural Reach](./img/screencaps/actors/natural-reach.png)

Spells known and base DC:  
![Spells known and base DC](./img/screencaps/actors/spell-dc-and-known.png)

Coin weight:  
![Coin Weight](./img/screencaps/actors/coin-weight.png)

Details tab:  
![Details Tab](./img/screencaps/actors/details-tab.png)

Item Summary enricher:  
![Item Summary](./img/screencaps/actors/item-summary-enhancer.png)

Active Buffs & conditions:  
![Active Buffs](./img/screencaps/actors/active-buffs.png)

### Items

Datalist formula sources:  
![Datalists](./img/screencaps/items/datalists.png)

Dialog skip override:  
![Dialog skip](./img/screencaps/items/dialog-override.png)

Roll mode override:  
![Roll Mode Override](./img/screencaps/items/rollmode-override.png)

Roll override:  
![Roll Override](./img/screencaps/items/roll-override.png)

### Chat

Display raw dice and total modifier in rolls:  
![Newer Checks](./img/screencaps/chat/check-expanded.png)

Inline attack roll expansion:  
![Roll Expansion](./img/screencaps/chat/roll-expander.png)

Inline Roll Formula:  
![Inline Roll Formula](./img/screencaps/chat/inline-roll-formula.png)

Quick roll (easy button to roll the same check):  
![Quick Roller](./img/screencaps/chat/quick-roller.png)

Sender tag:  
![Sender tag](./img/screencaps/chat/sender-tag.png)

### Foundry

Token HUD condition label:  
![Condition label](./img/screencaps/foundry/tokenhud-conditions.png)

Actor List Tooltips:  
![Actor list tooltips](./img/screencaps/foundry/actor-list-tooltip.png)

### Utility

Item theory:  
![Item Theory](./img/screencaps/utility/item-theory.png)
