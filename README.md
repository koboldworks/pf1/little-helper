# Koboldworks – Little Helper 🦎 for Pathfinder 1e

![Supported Foundry VTT version](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fkoboldworks%2Fpf1%2Flittle-helper%2F-%2Freleases%2Fpermalink%2Flatest%2Fdownloads%2Fmodule.json)
![Supported Game System version](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fsystem%3FnameType%3Dfull%26showVersion%3D1%26style%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fkoboldworks%2Fpf1%2Flittle-helper%2F-%2Freleases%2Fpermalink%2Flatest%2Fdownloads%2Fmodule.json)

Bunch of little tweaks, notifications, opinionated improvements, etc. that are too small to exist on their own.

WARNING: Due to limited information available in PF1e system, some of this module's functionality is limited to English due to string matching.  
 PF1 0.78.14 or newer mitigates this problem greatly.

## Features

See [Feature Listing](./FEATURES.md) for listing of features and screenshots of them.

## Compatibility

### Alt Sheet

CSS fixes:

```css
.lil-box.natural-reach {
	display: none;
}
```

## API

See [API documentation](./API.md).

## Install

Manifest URL: <https://gitlab.com/koboldworks/pf1/little-helper/-/releases/permalink/latest/downloads/module.json>

**WARNING**: Above manifest gives you the repository master branch, not a proper versioned release.  
Currently the version is being incremented irregularly, so if you want to update the module, you need to do so manually by re-installing it.  
The version number currently includes date of the release (local to me) in format of YYYYMMDDHH.

**UPDATING**: You may need to do so manually by re-installing it. Update checks within Foundry will not work usually due to version number not being updated consistently.

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/mkahvi)

## License

This software is distributed under the [MIT License](./LICENSE).

## Credits

Active buffs feature was inspired by similar feature in PF2. Only the high concept was re-used (buff icons near chat log in column).
