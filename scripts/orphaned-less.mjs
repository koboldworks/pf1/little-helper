import fs from 'fs';
import path from 'path';

const lessSrcFolder = 'less';

const files = [];

function enumFolder(folder) {
	const folders = [];
	console.log('-', folder);

	fs.readdirSync(folder, { withFileTypes: true })
		.forEach((d) => {
			const dt = { name: d.name, path: path.join(folder, d.name) };
			d.isDirectory() ? folders.push(dt) : files.push(dt);
		});

	for (const f of folders)
		enumFolder(f.path);
}

console.log('Scanning folders:');
enumFolder(lessSrcFolder);

const includedFiles = [];

console.log('\nProcessing files:');
for (const f of files) {
	const cwd = path.dirname(f.path);
	console.log('-', f.path);
	const lines = fs.readFileSync(f.path, { encoding: 'utf-8' }).split(/\n/);
	for (const line of lines) {
		const re = line.match(/^@import '(?<import>.*)';/);
		if (re) includedFiles.push(path.join(cwd, re.groups.import));
	}
}

console.log('\nOrphaned files:');
files.forEach(f => {
	if (!includedFiles.includes(f.path))
		console.log('-', f.path);
});
