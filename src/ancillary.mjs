import { CFG } from './config.mjs';
import { getUserFromData } from './common.mjs';
import { SharedData } from '@core/shared-data.mjs';

class AncillaryCallback {
	/** @type {Function.<ItemSheet | ActorSheet, JQuery, object, SharedData>} */
	fn;
	/** @type {number} */
	priority;
	/** @type {number} */
	id;

	/**
	 * @param {Function} fn
	 * @param {number} priority
	 * @param {number} id
	 */
	constructor(fn, priority = 0, id = null) {
		this.fn = fn;
		this.priority = priority;
		this.id = id;
	}
}

/**
 * @callback AncillaryCallback
 * @param {Application|DocumentSheet|ChatMessage} app
 * @param {JQuery} jq
 * @param {object} options
 * @param {SharedData} shared
 */

/** @typedef {"item"|"actor"|"chat"|"action"|"compendium"} AncillaryContext */

/**
 * Hook proxy for providing shared data and hook handler ordering.
 */
class HookAncillary {
	#callbacks = {
		/** @type {AncillaryCallback[]} */
		item: [],
		/** @type {AncillaryCallback[]} */
		actor: [],
		/** @type {AncillaryCallback[]} */
		chat: [],
		/** @type {AncillaryCallback[]} */
		action: [],
		/** @type {AncillaryCallback[]} */
		compendium: [],
	};

	/**
	 * @param {AncillaryContext} context
	 * @param {AncillaryCallback} callback
	 * @param {object} options
	 * @param {number} options.priority Higher value ensures earlier calling.
	 * @param options.id
	 */
	register(context, callback, { priority = 0, id = null } = {}) {
		if (context === 'chat' && game?.ready) console.warn('Chat hook registered very late');
		this.#callbacks[context].push(new AncillaryCallback(callback, priority, id));
		this.#callbacks[context].sort((a, b) => b.priority - a.priority);
	}

	/**
	 * @param {AncillaryContext} context
	 * @param {AncillaryCallback} callback
	 */
	unregister(context, callback) {
		/** @type {AncillaryCallback[]} */
		const list = this.#callbacks[context];
		const i = list.findIndex((e) => e.fn == callback);
		if (i >= 0) list.splice(i, 1);
	}

	/**
	 * @param {string} context
	 * @param {Application|DocumentSheet|ChatMessage} app
	 * @param  {...any} args
	 */
	#call(context, app, ...args) {
		if (context === 'actor' && !['character', 'npc'].includes(app.actor.type)) return;

		const shared = new SharedData(game.user ?? getUserFromData(), app.object ?? app.document ?? app.action ?? app.item ?? app);
		if (['actor', 'item'].includes(context) && !shared.isObserver) return;
		if (context !== 'chat') app.__littleHelper ??= {};

		const list = this.#callbacks[context];
		for (const li of list) {
			try {
				li.fn(app, ...args, shared);
			}
			catch (err) {
				console.error(err);
				this.unregister(context, li);
			}
		}
	}

	constructor() {
		Hooks.once('init', this.#init.bind(this));
		Hooks.once('setup', this.#setup.bind(this));
		Hooks.once('ready', this.#ready.bind(this));
	}

	_oldMsgId;
	#init() {
		Hooks.on('renderCompendiumDirectory', this.#call.bind(this, 'compendium'));

		// Deal with chat messages
		const user = getUserFromData();
		if (user.role > 0) {
			// User info could be gained early.
			Hooks.on('renderChatMessage', this.#call.bind(this, 'chat'));
			Hooks.once('ready', () => this._startCleanup());
		}
		else {
			console.warn('Little Helper 🦎 | Could not retrieve user information; delaying processing.');
			this._oldMsgId = Hooks.on('renderChatMessage', this._collectOldMessages.bind(this));
			Hooks.once('ready', this._finishCollection.bind(this));
		}
	}

	#setup() {
		// NOP
	}

	#ready() {
		Hooks.on('renderActorSheet', this.#call.bind(this, 'actor'));
		Hooks.on('renderItemSheet', this.#call.bind(this, 'item'));
		Hooks.on('renderItemActionSheet', this.#call.bind(this, 'action'));
	}

	_initialCMs = [];

	_collectOldMessages(cm, jq) {
		this._initialCMs.push([cm, jq]);
	}

	_finishCollection() {
		Hooks.off('renderChatMessage', this._oldMsgId);
		Hooks.on('renderChatMessage', this.#call.bind(this, 'chat'));
		for (const msgArgs of this._initialCMs)
			this.#call('chat', ...msgArgs);
		this._startCleanup();
	}

	_startCleanup() {
		// Permanent cleanup
		delete this._initialCMs;
		delete this._finishCollection;
		delete this._collectOldMessages;
		delete this._oldMsgId;
		delete this._startCleanup;
	}
}

export const Ancillary = new HookAncillary();
