import { CFG } from './config.mjs';
import { FAIcons } from '@core/icons.mjs';

// HACK: Silence v12 deprecation warnings.
export const clampNum = Math.clamp ?? Math.clamped;

/**
 * @param {Event} event
 */
export function stopEventPropagation(event) {
	event.preventDefault();
	event.stopPropagation();
	return event;
}

class UserDataShim {
	id;
	role;
	name;
	get isGM() {
		return this.role >= CONST.USER_ROLES.ASSISTANT;
	}

	constructor({ name, role, id } = {}) {
		this.id = id;
		this.name = name;
		this.role = role;
	}
}

export function getUserFromData() {
	const data = game.data; // Same for v9 and onwards
	const id = data?.userId;
	const userData = id ? data.users?.find(u => u._id === id) ?? {} : {};
	return new UserDataShim({ id, name: userData.name, role: userData.role ?? 0 });
}

export const setSheetMarker = (sheet, flag, id, callback) => {
	sheet.___littleHelper ??= {};
	const h = sheet.___littleHelper;
	if (h[flag]?.fn) h[flag].fn(sheet);
	h[flag] = { fn: callback, id };
};

export const delSheetMarker = (sheet, flag, id) => {
	sheet.___littleHelper ??= {};
	const h = sheet.___littleHelper;
	if (h[flag]?.id === id) delete h[flag];
};

export function getTransparency() {
	return game.settings.get(CFG.id, 'transparency');
}

export function testTransparency(user, doc) {
	if (!doc) return true;
	switch (getTransparency()) {
		case 3:
			return doc.testUserPermission(user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER);
		case 2:
			return doc.testUserPermission(user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER);
		case 1:
			return doc.hasPlayerOwner ?? doc.document?.hasPlayerOwner;
		default:
			return true;
	}
}

/**
 * Scrubs details from a formula.
 *
 * @param {string} formula
 * @param stripDetail
 */
export function compressFormula(formula, stripDetail = true) {
	if (stripDetail) {
		formula = formula
			.replace(/\s+/g, '') // remove whitespace
			.replace(/\[[^\]]+]/g, ''); // remove flairs
	}
	return formula.replace(/\s+\+\s*-\s+/g, ' - '); // Combine + -
}

/**
 * @param {User} user
 * @returns {boolean} Returns true if GM content should be hidden regardless of transparency.
 */
export function hideFromNonGM(user) {
	if (user.isGM) return false; // is GM so not hidden
	return game.settings.get(CFG.id, CFG.SETTINGS.transparencySham);
}

export function enrichRoll(roll, formula, label) {
	const a = createNode('a', null, ['inline-roll', 'inline-result'], { tooltip: formula });
	a.dataset.roll = escape(JSON.stringify(roll));
	a.innerHTML = `<i class='${FAIcons.d20}'></i> ${label}`;
	return a;
}

export function unifyProficiency(prof) {
	if (prof == undefined) return [];

	const trimProfs = (profs) => {
		if (Array.isArray(profs)) return profs;
		else return profs?.split(';').map(p => p?.trim()).filter(p => p?.length > 0) ?? [];
	};

	if (prof.total) {
		return [
			...prof.total,
			...trimProfs(prof.customTotal),
		]; // weapon/armor
	}
	// languaegs
	return [
		...prof.value,
		...trimProfs(prof.custom),
		...trimProfs(prof.customTotal),
	];
}

export const signNum = new Intl.NumberFormat(undefined, { signDisplay: 'always' }).format;
export const formatNum = new Intl.NumberFormat(undefined).format;

export function warningSymbol() {
	const i = document.createElement('i');
	i.classList.add('fas', 'fa-exclamation-triangle');
	return i;
}

export function constructWarning(warning, css = []) {
	const symbol = warningSymbol();
	addCoreTooltip(symbol, warning);
	symbol.classList.add('lil-warning-symbol', ...css);
	return symbol;
}

export function addWarning(el, warning) {
	if (!el) return;
	el.classList.add('lil-warning');
	el.prepend(constructWarning(warning));
	return el;
}

/**
 * @param {string} node
 * @param {string | null} text
 * @param {string[]} classes
 * @param {object} options
 * @param {string | null} options.title
 * @param {boolean} options.isHTML
 * @param {Element[]} options.children
 * @param {object} [options.data] Object mapping for data-${key}=${value} values.
 * @param {object} [options.attr] Object mapping for arbitrary attributes.
 * @param options.tooltip
 * @returns {HTMLElement}
 */
export function createNode(node = 'span', text = null, classes = [], { tooltip = null, isHTML = false, children = [], data, attr } = {}) {
	const n = document.createElement(node);
	if (text) {
		if (isHTML) n.innerHTML = text;
		else n.textContent = text;
	}
	if (classes.length) n.classList.add(...classes.filter(c => !!c));
	if (children.length) n.append(...children.filter(c => !!c));
	if (data) {
		for (const [key, value] of Object.entries(data))
			n.setAttribute(`data-${key}`, value);
	}
	if (attr) {
		for (const [key, value] of Object.entries(attr)) {
			if (value !== undefined)
				n.setAttribute(key, value);
		}
	}
	if (tooltip) addCoreTooltip(n, tooltip);
	return n;
}

/**
 * Append elements in to el and return el.
 *
 * @param {Element} el Parent
 * @param {Element[]} els Children
 * @todo Remove the use of this
 */
export function appendElements(el, ...els) {
	el.append(...els);
	return el;
}

/**
 * Basic testing to see if the user is allowed to view a message.
 *
 * @param {ChatMessage} cm
 * @param {User|null} user
 */
export function canViewChatMessage(cm, user = game.user) {
	const whisper = cm.whisper;

	// GM sees all
	if (user.isGM) return true;

	const isAuthor = cm.isAuthor;

	if (whisper.length) {
		// Display if whisper target regardless of anything else
		if (!isAuthor) return whisper.includes(user.id);

		// Hide blind rolls from owner
		if (cm.blind) return false;
	}

	// Creator always sees as long as they pass above tests
	if (isAuthor) return true;

	// Hide GM rolls for non-GMs if transparency sham is enabled.
	if (cm.user?.role >= CONST.USER_ROLES.ASSISTANT && hideFromNonGM(user)) return false;

	// Show for all others.
	return true;
}

/**
 * @param {number} num
 * @param {number} decimalPlaces
 * @param {"round"|"ceil"|"floor"} type Rounding function
 */
export function maxPrecision(num, decimalPlaces = 0, type = 'round') {
	const p = Math.pow(10, decimalPlaces || 0),
		n = num * p * (1 + Number.EPSILON);
	return Math[type](n) / p;
}

/**
 * @param {Actor} actor
 * @param {object} options
 * @param {Item} options.item Item instance
 * @param {string} options.condition Condition key
 * @returns {ActiveEffect|undefined}
 */
export const getRelevantAE = (actor, { item, condition } = {}) => {
	if (item) {
		if (!condition) {
			const ae = item.effect;
			if (ae) return ae;
		}
	}

	const effects = [...actor.allApplicableEffects()].filter(ae => ae.active);
	if (item) {
		const options = { relative: actor };
		return effects.find(ae => ae.origin ? fromUuidSync(ae.origin || '', options) === item : false);
	}
	else if (condition) {
		const relevant = effects.filter(ae => ae.statuses.has(condition));
		// Attempt to establish order
		relevant.sort((a, b) => {
			if (a.origin && !b.origin) return -1;
			if (b.origin && !a.origin) return 1;
			return a.statuses.size - b.statuses.size;
		});
		return relevant[0];
	}
};

/**
 * @param {HTMLElement} element - Element to enrich
 * @param {string} content - Tooltip content
 * @param {"UP"|"DOWN"|"LEFT"|"RIGHT"} direction - Direction where to go
 * @param {string} css - Space deliminated list of CSS classes
 * @returns {HTMLElement} - Enriched element
 */
export function addCoreTooltip(element, content, direction = TooltipManager.TOOLTIP_DIRECTIONS.UP, css) {
	element.dataset.tooltip = content;
	if (direction) element.dataset.tooltipDirection = direction;
	if (css) element.dataset.tooltipClass = css;
	return element;
}
