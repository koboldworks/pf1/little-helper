import { maxPrecision, getRelevantAE } from './common.mjs';

/**
 * Turn buff time into seconds.
 *
 * @param {number} t
 * @param {"round"|"hour"|"turn"|"minute"} unit
 */
export const timeTransform = (t, unit) => {
	switch (unit) {
		case 'round':
		case 'rounds':
			return t * CONFIG.time.roundTime;
		case 'hour':
		case 'hours':
			return t * 60 * 60;
		case 'minute':
		case 'minutes':
			return t * 60;
		case 'turn':
		case 'turns':
			return t * CONFIG.time.roundTime;
		case 'second':
		case 'seconds':
		case 'sec': return t;
		default: return null;
	}
};

export const humanTimeUnit = {
	r: { short: 'rnd(s)', long: 'round(s)' },
	m: { short: 'min(s)', long: 'minute(s)' },
	h: { short: 'hr(s)', long: 'hour(s)' },
	d: { short: 'd(s)', long: 'day(s)' },
	w: { short: 'w(s)', long: 'week(s)' },
	mo: { short: 'mo(s)', long: 'month(s)' },
	y: { short: 'y(s)', long: 'year(s)' },
};

const TIME = /** @type {const} */ ({
	MINUTE: 60,
	HOUR: 3_600,
	DAY: 86_400,
	WEEK: 86_400 * 7,
	MONTH: 86_400 * 30,
	YEAR: 86_400 * 365,
});

/**
 * @param {number} t Seconds
 * @returns {{n:number, u:string}} N[umber] U[nits]
 */
export const getHumanTime = (t) => {
	let num, unit;
	const ta = Math.abs(t);
	if (ta < CONFIG.time.roundTime * 15) {
		num = Math.floor(t / CONFIG.time.roundTime);
		unit = 'r';
	}
	else if (ta < (TIME.HOUR * 2)) { // less than about 2 hours
		num = maxPrecision(t / TIME.MINUTE, 2);
		unit = 'm';
	}
	else if (ta < (TIME.DAY * 1.3)) { // less than slightly over a day
		num = maxPrecision(t / TIME.HOUR, 2);
		unit = 'h';
	}
	else if (ta < TIME.DAY * 14) {
		num = maxPrecision(t / TIME.DAY, 2);
		unit = 'd';
	}
	else if (ta < TIME.DAY * 60) {
		num = maxPrecision(t / TIME.WEEK, 2);
		unit = 'w';
	}
	else if (ta < TIME.YEAR) {
		num = maxPrecision(t / TIME.MONTH, 2);
		unit = 'mo';
	}
	else {
		num = maxPrecision(t / TIME.YEAR, 2);
		unit = 'y';
	}

	return { n: num, u: unit };
};

class Duration {
	/** @type {number} */
	start = 0;
	/** @type {number} */
	duration = 0;
	/** @type {number} */
	offset = 0;

	get world() { return game.time.worldTime + this.offset; }
	get remaining() { return (this.start + this.duration) - this.world; }
	get passed() { return this.world - this.start; }

	/**	@type {ActiveEffect} */
	effect;
	/** @type {Item} */
	item;

	/**
	 * @param {number} start
	 * @param {number} duration
	 * @param {object} options
	 * @param {ActiveEffect} options.activeEffect
	 * @param {Item} options.item
	 * @param options.offset
	 */
	constructor(start, duration, { activeEffect, item, offset } = {}) {
		this.start = start ?? 0;
		this.duration = duration ?? 0;
		this.offset = offset ?? 0;

		this.effect = activeEffect ?? null;
		this.item = item ?? null;
		if (!item && activeEffect.parent instanceof Item) this.item = activeEffect.parent;
	}
}

export const getAEDuration = (ae, { offset = 0 } = {}) => {
	const dur = ae?.duration;
	if (!dur) return;
	return new Duration(dur.startTime, dur.seconds, { activeEffect: ae, offset });
};

/**
 * Get buff time information.
 *
 * @param {Item} item
 * @param {object} rollData
 */
export const getTimeFromItem = (item, rollData) => {
	const actor = item?.actor;
	const ae = actor ? getRelevantAE(actor, { item }) : null;

	if (ae) return getAEDuration(ae);

	const dur = item.system.duration;
	const roll = RollPF.safeRollSync(dur.value, rollData ?? item.getRollData(), undefined, undefined, { minimize: true });
	let value;
	if (roll.isDeterministic) value = roll.total;
	else value = pf1.utils.formula.simplify(roll.formula);
	return new Duration(dur.start, timeTransform(value, dur.units), { item });
};
