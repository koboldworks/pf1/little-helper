export const CFG = {
	id: /** @type {const} */ ('koboldworks-pf1-little-helper'),
	SETTINGS: {
		generalWorldConfiguration: 'config-world',
		generalClientConfiguration: 'config-client',
		featureKey: 'features',
		inlineFormulaDetail: 'inline-formula-detail',
		chat: 'chat',
		transparencySham: 'gmtransparencysham',
		transparency: 'transparency',
	},
	ROLLMODES: {
		'': 'LittleHelper.Options.NoOverride',
		publicroll: 'CHAT.RollPublic',
		gmroll: 'CHAT.RollPrivate',
		selfroll: 'CHAT.RollSelf',
		blindroll: 'CHAT.RollBlind',
	},
	system: {
		get version() {
			return game.system.version;
		},
		atLeast: function (v) {
			const v0 = this.version;
			return foundry.utils.isNewerVersion(v0, v) || v == v0;
		},
		newerThan: function (v) {
			return foundry.utils.isNewerVersion(this.version, v);
		},
	},
	core: {
		get version() {
			return game.version;
		},
		atLeast: function (v) {
			const curVer = this.version;
			return foundry.utils.isNewerVersion(curVer, v) || v == curVer;
		},
		newerThan: function (v) {
			return foundry.utils.isNewerVersion(this.version, v);
		},
	},
	state: {
		alt: false,
	},
	API: {
		utility: {},
		formula: {},
		classes: {},
	},
	i18n: {

	},
	console: {
		colors: {
			main: 'color:yellowgreen',
			number: 'color:mediumpurple',
			string: 'color:mediumseagreen',
			label: 'color:goldenrod',
			id: 'color:darkseagreen',
			unset: 'color:unset',
			error: 'color:red',
		},
	},
	isStream: undefined,
	version: {
		feature: null,
		patch: null,
	},
};

Hooks.once('init', () => {
	CFG.isStream = game.view === 'stream';
	const version = game.system.version.split('.').map(Number);
	CFG.version.feature = version.shift();
	CFG.version.patch = version.shift();
	const C = CFG.console.colors;
	console.log('%cLittle Helper%c 🦎', C.main, C.unset, '| Detected System Version | Feature:', CFG.version.feature, '| Patch:', CFG.version.patch);
	Object.freeze(CFG);
});
