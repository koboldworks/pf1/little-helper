import { CFG } from '../config.mjs';
import { Ancillary } from '../ancillary.mjs';
import { stopEventPropagation } from '../common.mjs';
import { FAIcons } from '@core/icons.mjs';

const FLAGS = {
	keenFlag: 'keen',
	hideAttacksFlag: 'hideAttacks',
	autoPowerAttack: 'autoPowerAttack',
	skipDialogFlag: 'skipDialog',
	rollModeOverrideFlag: 'rollmodeOverride',
	rollOverrideFlag: 'rollOverride',
};

export const migrateItemFlags = async (item) => {
	if (item.flags?.lilhelper?.item) return;

	const flags = {
		[FLAGS.rollOverrideFlag]: item.getItemDictionaryFlag(FLAGS.rollOverrideFlag),
		[FLAGS.rollModeOverrideFlag]: item.getItemDictionaryFlag(FLAGS.rollModeOverrideFlag),
		[FLAGS.keenFlag]: item.hasItemBooleanFlag(FLAGS.keenFlag),
		[FLAGS.hideAttacksFlag]: item.hasItemBooleanFlag(FLAGS.hideAttacksFlag),
		[FLAGS.autoPowerAttack]: item.hasItemBooleanFlag(FLAGS.autoPowerAttack),
		[FLAGS.skipDialogFlag]: item.getItemDictionaryFlag(FLAGS.skipDialogFlag),
	};

	Object.entries(flags).forEach(([key, value]) => {
		if (value === undefined || value === '' || value === false || value === 0) delete flags[key];
	});

	if (Object.keys(flags).length == 0) return null;

	const updateData = {
		flags: {
			lilhelper: {
				item: flags,
			},
		},
		system: {
			flags: {
				boolean: {
					'-=keen': null,
					'-=autoPowerAttack': null,
					'-=hideAttacks': null,
				},
				dictionary: {
					'-=rollOverride': null,
					'-=rollmodeOverride': null,
					'-=skipDialog': null,
				},
			},
		},
	};

	const C = CFG.console.colors;
	console.log('%cLittle Helper%c 🦎 | Migrating Item Flags |', C.main, C.unset, item);
	return item.update(updateData);
};

const sheetTemplateSource = /** @type {const} */ (`modules/${CFG.id}/template/item-config.hbs`);
Hooks.once('setup', () => loadTemplates([sheetTemplateSource]));

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 */
function itemSheetConfig(sheet, [html]) {
	const tab = html.querySelector('.tab[data-tab="advanced"]');
	if (!tab) return;
	const item = sheet.item;

	migrateItemFlags(item);

	// Guard against weird modes
	const rollModeGuard = (mode) => {
		if (mode == undefined) return '';
		if (Object.keys(CFG.ROLLMODES).includes(mode)) return mode;
		if (mode == 'roll') return 'publicroll';
		return 'garbage';
	};

	const flags = sheet.item.flags?.lilhelper?.item ?? {};

	const rollModeOverride = rollModeGuard(flags[FLAGS.rollModeOverrideFlag]);
	const rollModes = foundry.utils.deepClone(CFG.ROLLMODES);
	if (rollModeOverride == 'garbage') rollModes.garbage = 'BAD VALUE';

	const hasAction = !['class', 'race', 'buff', 'container'].includes(item.type);

	const templateData = {
		...flags,
		uid: `lil-${item.id}`,
		type: item.type,
		hasAction,
		choices: {
			rollmodes: rollModes,
			skipDialog: {
				0: 'LittleHelper.Options.NoOverride',
				1: 'LittleHelper.Options.ForceDisplay',
				'-1': 'LittleHelper.Options.ForceSkip',
			},
		},
		libwrapper: game.modules.get('lib-wrapper')?.active ?? false,
		icons: FAIcons,
	};

	const sheetTemplate = Handlebars.partials[sheetTemplateSource];

	const fragment = document.createDocumentFragment();
	// fragments have no innerHTML
	const div = document.createElement('div');
	div.innerHTML = sheetTemplate(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
	fragment.append(...div.childNodes);

	fragment.querySelectorAll('[data-name]').forEach(el => {
		el.addEventListener('change', ev => {
			const path = ev.target.dataset.name;
			const value = ev.target.value;
			item.update({ [path]: value });
		});
	});

	if (!sheet.isEditable) {
		fragment.querySelectorAll('input,select').forEach(el => {
			el.readOnly = true;
			el.disabled = true;
		});
	}

	tab.append(fragment);
}

Ancillary.register('item', itemSheetConfig, { priority: 500 });
