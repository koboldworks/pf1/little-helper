import './chat/_init.mjs';

import './actor/_init.mjs';
import './item/_init.mjs';

import './dialog/_init.mjs';

import './platform/_init.mjs';

import './style/_init.mjs';

import './generic/_init.mjs';

import './hud/_init.mjs';

import './scene/_init.mjs';
