import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';
import { TabData } from '@core/tab-data.mjs';

const systemSheets = new Set();

function detectSheets() {
	for (const data of Object.values(CONFIG.Actor.sheetClasses.character)) {
		if (data.id.startsWith('pf1.'))
			systemSheets.add(data.cls.name);
	}
	for (const data of Object.values(CONFIG.Actor.sheetClasses.npc)) {
		if (data.id.startsWith('pf1.'))
			systemSheets.add(data.cls.name);
	}
	for (const types of Object.values(CONFIG.Item.sheetClasses)) {
		for (const data of Object.values(types)) {
			if (data.id.startsWith('pf1.'))
				systemSheets.add(data.cls.name);
		}
	}
	// Action sheet
	for (const sheet of Object.values(pf1.applications.component)) {
		systemSheets.add(sheet.name);
	}
}

/**
 * @param {ItemSheet|ActorSheet} sheet
 * @param {JQuery} html
 * @param _opts
 * @param {SharedData} shared
 */
function initSharedDocData(sheet, [html], _opts, shared) {
	const sheetClass = sheet.constructor.name;
	shared.sysSheet = systemSheets.has(sheetClass);
	shared.coreSheet = sheet.document instanceof Actor ? ['ActorSheetPFCharacter', 'ActorSheetPFNPC'].includes(sheetClass) : shared.sysSheet;

	if (!shared.sysSheet)
		shared.altSheetZenvy = sheet.options.classes.includes('pf1alt');

	html.querySelectorAll('section .tab[data-tab]')
		.forEach(tab => shared.tabs[tab.dataset.tab] = new TabData(tab));
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param _opts
 * @param {SharedData} shared
 */
function initSharedActorData(sheet, [html], _opts, shared) {
	// Add shared containers to spellbooks
	const spellbook = shared.tabs.spellbook;
	if (spellbook?.tab) {
		// Lazy spellbook getter
		Object.defineProperty(spellbook, 'tabs', {
			_spellbooks: undefined,
			get: function getSpellbooks() {
				if (!this._spellbooks) {
					this._spellbooks = this.tab.querySelectorAll('.tab[data-group="spellbooks"]');

					if (!this._spellbooks) return null;

					// Add spell detail holder
					this._spellbooks
						?.forEach(tab => tab.querySelectorAll('.spellbook-header')
							?.forEach(node => node.querySelector('.item-name')
								?.append(createNode('div', null, ['lil-spell-details', 'lil-right-justify']))),
						);
				}
				return this._spellbooks;
			},
		});
	}

	const summary = shared.tabs.summary;
	if (summary?.tab) {
		// Lazy attributes section getter
		Object.defineProperty(summary, 'attributes', {
			_attributes: undefined,
			get: function getAttributes() {
				this._attributes ??= this.tab?.querySelector('.attributes');
				return this._attributes;
			},
		});
	}
}

Ancillary.register('actor', initSharedDocData, { priority: 1100 });
Ancillary.register('item', initSharedDocData, { priority: 1100 });
Ancillary.register('action', initSharedDocData, { priority: 1100 });

Ancillary.register('actor', initSharedActorData, { priority: 1000 });

Hooks.once('ready', detectSheets);
