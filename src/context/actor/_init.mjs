import './_init-shared-data.mjs';

import './actor-config.mjs';

// Sub-folders

import './attributes/_init.mjs';
import './buffs/_init.mjs';
import './combat/_init.mjs';
import './details-tab/_init.mjs';
import './generic/_init.mjs';
import './inventory/_init.mjs';
import './items/_init.mjs';
import './skills/_init.mjs';
import './spells/_init.mjs';
import './summary/_init.mjs';

// Additional Info

import './quick-action-tooltips.mjs';

// UNSORTED

import './language-selection.mjs';
import './discourage-sourceless.mjs';

// Nagging
import './missing-bits.mjs';
import './missing-settings.mjs';

// New interfaces

import './tab-popout.mjs';
