import { CFG } from '@root/config.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { i18n } from '@root/utility/i18n.mjs';

class ActorConfig extends DocumentSheet {
	get title() {
		return i18n.get('LittleHelper.Settings.ActorOverridesTitle') + ` – ${this.actor.name}`;
	}

	get id() {
		const uuid = this.actor?.uuid.replaceAll('.', '-');
		return `app-${uuid}-lh-overrides`;
	}

	get actor() {
		return this.document;
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			template: `modules/${CFG.id}/template/actor-config.hbs`,
			classes: ['little-helper', 'lil-overrides-config'],
			submitOnChange: true,
			closeOnSubmit: false,
			submitOnClose: false,
			sheetConfig: false,
			height: 'auto',
			width: 'auto',
		};
	}

	/** @override */
	getData() {
		const api = CFG.API;
		const keys = [
			'new-actor-guide',
			'missing-bits',
			'excess-skill-ranks',
			'duplicate-slots',
		];

		return {
			id: CFG.id,
			flags: this.actor.getFlag(CFG.id, 'overrides'),
			features: Object.fromEntries(keys.map(key => [key, api.features.get(key).enabled])),
		};
	}

	static async open(ev, actor, options = {}) {
		ev.preventDefault();

		const el = ev.target;
		const { bottom, right } = el.getBoundingClientRect();

		let app = Object.values(ui.windows).find(app => app instanceof ActorConfig && app.document === actor);
		if (app) {
			app.bringToTop();
			app.setPosition({ top: bottom + 6, left: right - 320 });
		}
		else {
			app = new this(actor, options);
			await app._render(true, { top: bottom + 6, left: right - 320 });
			// Foundry bug(?), initial position persists
			delete app.options.top;
			delete app.options.left;
		}

		return app;
	}
}

/**
 *
 * @param {ActorSheet} sheet
 * @param {JQuery<HTMLElement>} html
 * @param {object} options
 * @param {SharedData} shared
 */
async function injectActorSetup(sheet, [html], options, shared) {
	const tab = shared.tabs.settings?.tab;
	if (!tab) return void console.debug('Little Helper | Actor Config | Settings tab not found');

	const buttons = tab.querySelector('.base-setup .pointbuy-calculator')?.parentElement;
	if (!buttons) return void console.debug('Little Helper | Actor Config | Buttons not found!');

	const acb = document.createElement('button');
	acb.classList.add('lil-overrides-config');
	acb.type = 'button';
	acb.innerHTML = i18n.get('LittleHelper.Settings.ActorOverridesButton');
	acb.addEventListener('click', ev => ActorConfig.open(ev, sheet.actor));
	buttons?.append(acb);
}

Ancillary.register('actor', injectActorSetup);
