// Display extra info pertaining to creature type.
// https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/merge_requests/494

import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { createNode } from '@root/common.mjs';
import { SharedData } from '@core/shared-data.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function enrichCreatureTypeInfo(sheet, [html], options, shared) {
	if (!shared.coreSheet) return;

	const tab = shared.tabs.attributes?.tab;
	if (!tab) return;

	/** @type {Element} */
	const qr = tab.querySelector('input[name="system.attributes.quadruped"]');

	const actor = sheet.actor;

	const race = actor.race;
	// if (!race) return;

	const humtoggle = createNode('input');
	humtoggle.type = 'checkbox';
	humtoggle.readOnly = true; // Not respected by checkbox
	humtoggle.disabled = true; // Only way to make it readonly
	humtoggle.checked = race?.system.creatureType === 'humanoid';
	if (!actor.race) humtoggle.indeterminate = true;

	const hum = createNode('div', null, ['form-group'], {
		children: [
			createNode('label', 'Humanoid'),
			createNode('label', null, ['checkbox'], { children: [humtoggle] }),
		],
	});

	qr.parentElement?.parentElement.after(hum);
}

function enable() {
	Ancillary.register('actor', enrichCreatureTypeInfo);
}

function disable() {
	Ancillary.unregister('actor', enrichCreatureTypeInfo);
}

new Feature({ setting: 'creature-type-info', category: 'actor-sheet', enable, disable, stage: 'ready' });
