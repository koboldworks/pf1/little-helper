import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';
import { getHumanTime, humanTimeUnit, getTimeFromItem } from '@root/common.time.mjs';

const getTimeStr = (sdur) => {
	const { n, u } = getHumanTime(sdur);
	return `${n} ${humanTimeUnit[u].long}`;
};

/**
 * @param {Element} el
 * @param {Item} buff
 */
const activateDurationTooltip = async (el, buff) => {
	const d = document.createDocumentFragment();

	const dur = getTimeFromItem(buff);

	const durStr = getTimeStr(dur?.duration ?? 0),
		remStr = getTimeStr(dur?.remaining ?? 0),
		pasStr = getTimeStr(dur?.passed ?? 0);

	d.append(
		createNode('label', 'Active:'), createNode('span', pasStr, ['active']),
		createNode('label', 'Remaining:'), createNode('span', remStr, ['remaining']),
		createNode('label', 'Max duration:'), createNode('span', durStr, ['duration']),
	);

	game.tooltip.activate(el, { content: d, cssClass: 'pf1 lil-duration-tooltip', direction: TooltipManager.TOOLTIP_DIRECTIONS.LEFT });
};

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function addBuffDurationTooltips(sheet, [html], options, shared) {
	const tab = shared.tabs.buffs?.tab;
	if (!tab) return;

	const actor = sheet.actor;
	const buffs = actor.itemTypes.buff?.filter(i => i.isActive) ?? [];
	if (buffs.length == 0) return;

	tab.querySelectorAll('li.item[data-item-id]').forEach(el => {
		const itemId = el.dataset.itemId;
		const buff = actor.items.get(itemId);
		if (!buff?.isActive) return;

		const durEl = el.querySelector('.item-detail.item-duration');
		if (!durEl) return;

		durEl.addEventListener('pointerenter', _ => activateDurationTooltip(durEl, buff), { passive: true });
		durEl.addEventListener('pointerleave', () => game.tooltip.deactivate());
	});
}

function enable() {
	Ancillary.register('actor', addBuffDurationTooltips);
}

function disable() {
	Ancillary.unregister('actor', addBuffDurationTooltips);
}

new Feature({ setting: 'buff-tooltips', category: 'actor-sheet', enable, disable, stage: 'ready' });
