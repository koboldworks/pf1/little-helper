// Display condition durations

import { createNode } from '@root/common.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { getHumanTime, humanTimeUnit } from '@root/common.time.mjs';

const openSheets = {};

function addDurationDisplay(el, ae, time) {
	const { n, u } = getHumanTime(time);
	const text = `${n} ${humanTimeUnit[u].short}`;
	el ??= createNode('i', null, ['lil-condition-duration']);
	el.innerHTML = text;
	return el;
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function initializeConditionDurations(sheet, [html], options, shared) {
	const actor = sheet.actor;

	// Active conditions?
	if (!actor.statuses.size) {
		delete openSheets[sheet.appId];
		return;
	}

	const wt = game.time.worldTime;
	openSheets[sheet.appId] = sheet;

	const statusAEs = {};
	for (const ae of actor.allApplicableEffects()) {
		if (!ae.active) continue;

		Array.from(ae.statuses).forEach(status => {
			statusAEs[status] ??= [];
			statusAEs[status].push(ae);
		});
	}

	shared.tabs.buffs?.tab?.querySelectorAll('.buffs-conditions .condition.active a[data-condition-id]')
		.forEach(el => {
			const statusId = el.dataset.conditionId;
			const aes = statusAEs[statusId];
			if (!(aes?.length > 0)) return;
			aes.sort((a, b) => (a.duration?.startTime ?? -Infinity) - (b.duration?.startTime ?? -Infinity));
			const ae = aes[0];
			const nel = addDurationDisplay(null, ae, Math.floor(wt - ae.duration.startTime));
			el.append(nel);
			el.classList.add('lil-stretched-condition');
		});
}

/**
 * @param {ActorSheet} sheet
 */
function closeActorSheet(sheet) {
	delete openSheets[sheet.appId];
}

/**
 * @param {number} wt New world time
 * @param {number} _delta
 */
function refreshSheetDurations(wt, _delta) {
	Object.values(openSheets).forEach(sheet => {
		const conds = sheet.element?.[0].querySelector('.tab.buffs[data-tab="buffs"] .buffs-conditions');
		if (!conds) return;
		const actor = sheet.actor;

		const statusAEs = {};
		for (const ae of actor.allApplicableEffects()) {
			Array.from(ae.statuses).forEach(status => {
				statusAEs[status] ??= [];
				statusAEs[status].push(ae);
			});
		}

		conds.querySelectorAll('.buffs-conditions .condition.active a[data-condition-id]').forEach(el => {
			const dEl = el.querySelector('.lil-condition-duration');
			if (!dEl) return;
			const statusId = el.dataset.conditionId;
			const aes = statusAEs[statusId];
			if (!(aes?.length > 0)) return;
			aes.sort((a, b) => (a.duration?.startTime ?? -Infinity) - (b.duration?.startTime ?? -Infinity));
			const ae = aes[0];
			addDurationDisplay(dEl, ae, wt - ae.duration.startTime);
		});
	});
}

function enable() {
	Ancillary.register('actor', initializeConditionDurations);
	Hooks.on('closeActorSheet', closeActorSheet);
	Hooks.on('updateWorldTime', refreshSheetDurations);
}

function disable() {
	Ancillary.unregister('actor', initializeConditionDurations);
	Hooks.off('closeActorSheet', closeActorSheet);
	Hooks.off('updateWorldTime', refreshSheetDurations);
	Object.keys(openSheets).forEach(k => delete openSheets[k]);
}

new Feature({ setting: 'actor-sheet-condition-durations', category: 'actor-sheet', enable, disable, stage: 'ready' });
