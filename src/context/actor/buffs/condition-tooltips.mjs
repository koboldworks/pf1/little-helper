import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';
import { setStaticToolTipListener } from '@core/tooltip.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {ShareData} shared
 */
function addConditionTooltips(sheet, [html], options, shared) {
	/** @type {Element} */
	const tab = shared.tabs.buffs?.tab;

	const conds = tab?.querySelector('.buffs-conditions');
	if (!conds) return;

	const ConditionHelp = CFG.i18n.conditions;

	const fillTooltip = async (tip, condition) => tip.innerHTML = ConditionHelp[condition] ?? i18n.get('LittleHelper.Conditions._missing');

	conds.querySelectorAll('.condition [data-condition-id]').forEach(el => {
		const condition = el.dataset.conditionId;
		const tip = createNode('div', null, ['lil-tooltip']);
		const p = el.parentElement;
		p.append(tip);
		p?.addEventListener('pointerenter', _ => fillTooltip(tip, condition), { passive: true, once: true });
		setStaticToolTipListener(p, tip, { aligned: true, boundedBy: conds });
	});
}

function enable() {
	Ancillary.register('actor', addConditionTooltips);
}

function disable() {
	Ancillary.unregister('actor', addConditionTooltips);
}

new Feature({ setting: 'actor-condition-tooltips', label: 'Condition Tooltips', hint: 'Display tooltips describing effects of a conditon in buffs tab.', category: 'actor-sheet', enable, disable, stage: 'ready' });
