/*
 * Add some extra details to attacks.
 */

import { Ancillary } from '@root/ancillary.mjs';
import { createNode, addCoreTooltip } from '@root/common.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} _options
 * @param {SharedData} shared
 */
function attackDetailsInjection(sheet, [html], _options, shared) {
	const actor = sheet.actor;

	const rollData = shared.rollData;

	if (rollData.shield.type <= 1) return;

	const tab = shared.tabs.combat?.tab;
	if (!tab) return; // BUG: alt sheet?
	const items = tab?.querySelectorAll('.item[data-item-id]');
	if (!items) return;
	items.forEach(node => {
		const item = actor.items.get(node.dataset.itemId);
		if (!item) return;

		if (item.system.held !== '2h') return;

		node.classList.add('lil-awkward-shield');
		const name = node.querySelector('.item-name');
		if (!name) return;

		const twohanded = createNode('i', null, ['fas', 'fa-sign-language', 'lil-icon', 'right-floater', 'hands-of-effort']);
		addCoreTooltip(twohanded, 'LittleHelper.Warning.NoShieldWithTwohanded');
		// hookCoreTooltip(twohanded);
		name.append(twohanded);
	});
}

Ancillary.register('actor', attackDetailsInjection);

// TODO: FEATURE
