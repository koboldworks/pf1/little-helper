// Replaces the MR that was never accepted.
// https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/merge_requests/531

import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';

function addAmmoDisplay(el, actor) {
	const id = el.dataset.itemId;
	const item = actor.items.get(id);

	const action = item.defaultAction;
	if (!action) return;

	const usesAmmo = action.usesAmmo ?? item.system.usesAmmo;
	if (!usesAmmo) return;

	const defaultAmmoId = item?.getFlag('pf1', 'defaultAmmo');

	const usesEl = el.querySelector('.item-uses');
	if (!usesEl) return;

	const ammo = actor.items.get(defaultAmmoId);
	const quantity = ammo?.system.quantity ?? 0;

	usesEl.textContent = `${quantity}`;
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function displayRemainingAmmo(sheet, [html], options, shared) {
	const actor = shared.actor;

	/** @type {Element} */
	const combatTab = shared.tabs.combat?.tab;
	combatTab?.querySelectorAll('.combat-attacks .item-list .item[data-item-id]')
		.forEach(el => addAmmoDisplay(el, actor));

	/** @type {Element} */
	const invTab = shared.tabs.inventory?.tab;
	const invSelector = '.item-list[data-type="weapon"] .item[data-item-id]';
	invTab?.querySelectorAll(invSelector)
		.forEach(el => addAmmoDisplay(el, actor));
}

function enable() {
	Ancillary.register('actor', displayRemainingAmmo);
}

function disable() {
	Ancillary.unregister('actor', displayRemainingAmmo);
}

new Feature({ setting: 'actor-combat-remaining-ammo', label: 'Remaining Ammo', hint: 'Display remaining ammunition in charges column.', category: 'actor-sheet', enable, disable, stage: 'ready' });
