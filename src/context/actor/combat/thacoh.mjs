import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { createNode, clampNum } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} _options
 * @param {SharedData} shared
 */
function showThacoh(sheet, [html], _options, shared) {
	const actor = shared.actor;
	if (!['character', 'npc'].includes(actor.type)) return;

	const acEl = html.querySelector('section > .tab.combat .combat-defenses .ac-normal');
	if (!acEl) return;

	// Make assumptions
	const isPC = actor.type == 'character';
	const bab = isPC ? actor.system.attributes?.hd?.total : Math.min(Math.floor(actor.system.details?.cr?.total * 0.9), 20);

	const abilities = actor.system.abilities;
	const abl = Math.max(...Object.values(abilities).map(a => a.mod));
	// Weapon focus and greater weapon focus (or any other arbitrary bonus)
	const feats = 1 + (bab > 6 ? 1 : 0);
	// Simulate arbitrary bonuses (fighter-ish features, rogue debilitating injury, etc.)
	const wt = clampNum(Math.floor((bab - 1) / 4), 0, 4);
	// Simulate enhancement bonus
	let enh = clampNum(Math.floor((bab - 2) / 3), 0, 5);
	if (bab > 1 && enh == 0) enh += 1;

	const ac = actor.system.attributes.ac.normal.total;

	let buffs = 0;
	// buffs += clampNum(Math.floor(bab / 4) * 2, 0, 6); // Heroism-isms
	buffs += clampNum(Math.floor((bab + 4) / 5), 0, 4); // Inspire courage

	const hitTotal = bab + abl + enh + wt + feats + buffs;
	const formula = `+${bab}[BAB] +${abl}[Abl] +${enh}[Enh]<br>+${wt}[WT] +${feats}[Feats] +${buffs}[Buffs]<br>= +${hitTotal} to-hit`;

	// Minimum needed roll to hit
	const minRoll = clampNum(ac - hitTotal, 1, 20);

	// console.log({ bab, abl, bonuses, ac, thacoh: minRoll });

	const el = createNode('span', ` ${minRoll}+`, ['lil-thacoh']);
	const die = createNode('i', null, ['fa-solid', 'fa-dice-d20']);
	el.prepend(die);
	el.dataset.tooltip = i18n.get('LittleHelper.Info.Thacoh') + '<br>' + formula;
	acEl.append(el);
}

function enable() {
	Ancillary.register('actor', showThacoh);
}
function disable() {
	Ancillary.unregister('actor', showThacoh);
}

new Feature({ setting: 'actor-thacoh', label: 'To Hit AC', hint: 'Display estimation of high likely you\'re to be hit on combat tab.', category: 'actor-sheet', enable, disable, stage: 'ready' });
