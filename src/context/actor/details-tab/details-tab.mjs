import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';
import { SharedData } from '@core/shared-data.mjs';

async function fillDetailsTab(content, actor) {
	const C = CFG.console.colors;
	console.log(`%cLittle Helper%c 🦎 | Filling details tab for %c${actor.name}%c [%c${actor.id}%c]`,
		C.main, C.unset, C.label, C.unset, C.id, C.unset);
	await import('./providers/_init.mjs');

	const shared = new SharedData(game.user, actor);
	Hooks.call('little-helper.details-tab.content', content, shared);
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 */
function insertDetailsTab(sheet, [html]) {
	if (/Loot/.test(sheet.constructor.name)) return;

	const content = createNode('div', null, ['tab', 'lil-details-content']);
	content.dataset.group = 'primary';
	content.dataset.tab = 'lil-details';

	const navbutton = createNode('a', null, ['item', 'lil-sheet-tab']);
	navbutton.dataset.tab = 'lil-details';

	const icon = createNode('i', null, ['far', 'fa-file-alt']);
	navbutton.append(icon);

	const shc = html.querySelector('section.primary-body');
	const nav = html.querySelector('nav.sheet-navigation.tabs[data-group="primary"]');
	if (!shc || !nav) return;

	shc.append(content);
	nav.append(navbutton);

	navbutton.addEventListener('click', () => fillDetailsTab(content, sheet.actor), { passive: true, once: true });
}

function enable() {
	Ancillary.register('actor', insertDetailsTab);
}

function disable() {
	Ancillary.unregister('actor', insertDetailsTab);
}

new Feature({ setting: 'details-tab', label: 'Details Tab', hint: 'Add extra tab with random extra details.', category: 'actor-sheet', enable, disable, stage: 'ready' });
