import { createNode, appendElements } from '@root/common.mjs';

async function openAESheet(ev, actor) {
	ev.preventDefault();

	const el = ev.target.closest('.active-effects .active-effect[data-effect-uuid]');
	if (!el) return;

	const aeId = el.dataset.effectUuid;
	const ae = await fromUuid(aeId);
	ae.sheet.render(true, { focus: true });
}

function detailActiveEffects(content, shared) {
	const actor = shared.actor;
	const c = createNode('div', null, ['flexcol']);

	const g = createNode('ul', null, ['active-effects']);
	c.append(
		createNode('h2', 'Active Effects', ['lil-details-active-effects']),
		appendElements(createNode('div', null, ['detail-content']), g),
	);

	g.append(createNode('li', null, ['header'], {
		children: [
			createNode('span', ' ', ['image']),
			createNode('span', 'Name', ['name']),
			createNode('span', ' ', ['image']),
			createNode('span', 'Origin', ['origin']),
			createNode('span', 'Duration', ['duration']),
		],
	}));

	const effects = [...actor.allApplicableEffects()];

	if (effects.length) {
		for (const ae of effects) {
			const icon = ae.icon,
				originLink = ae.origin;

			let origin = originLink ? fromUuidSync(originLink, { relative: actor }) : null;
			const parent = ae.parent;
			origin ??= parent;
			const itemImage = origin?.img ?? '',
				haveOrigin = !!origin;

			g.append(createNode('li', null, ['active-effect'], {
				data: { 'effect-uuid': ae.uuid },
				children: [
					createNode('span', icon ? null : ' ', ['image', 'image-ae'], { children: icon ? [createNode('img', null, [], { attr: { src: icon } })] : [] }),
					createNode('span', ae.name || ae.label, ['name', 'ae-label']),
					createNode('span', haveOrigin ? null : ' ', ['image', 'image-ae', haveOrigin ? 'present' : 'missing'], { children: haveOrigin ? [createNode('img', null, [], { attr: { src: itemImage } })] : [] }),
					createNode('span', origin?.name ?? (originLink ? '[missing]' : '[n/a]'), ['name', 'origin', haveOrigin ? 'present' : 'missing']),
					createNode('span', ae.duration.seconds, ['duration']),
				],
			}));
		}
	}
	else {
		g.append('', createNode('p', 'No active effects found.', ['hint', 'span-all']));
	}

	g.addEventListener('contextmenu', ev => openAESheet(ev, actor));

	content.append(c);
}

Hooks.on('little-helper.details-tab.content', detailActiveEffects);
