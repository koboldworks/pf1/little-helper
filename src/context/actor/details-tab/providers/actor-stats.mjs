import { createNode, appendElements, maxPrecision } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

const getPackedSize = (obj) => JSON.stringify(obj).length;

const actorStats = (content, shared) => {
	const actor = shared.actor;
	const c = createNode('div', '', ['actor-stats', 'flexcol']);

	// console.log(actor);

	const obj = actor.toObject();
	const actorSize = getPackedSize(obj);
	const itemsSize = getPackedSize(obj.items);
	const itemCount = obj.items.length;

	const itemTypeSizes = {};
	for (const item of actor.items) {
		const type = item.type;
		itemTypeSizes[type] ??= { size: 0, count: 0, biggest: { size: 0, name: null } };
		const t = itemTypeSizes[type];
		t.count += 1;
		const size = getPackedSize(item);
		t.size += size;
		if (size > t.biggest.size) {
			t.biggest.size = size;
			t.biggest.name = item.name;
		}
	}

	let itemTypeSizeArray = [];
	Object.entries(itemTypeSizes).forEach(([type, value]) => itemTypeSizeArray.push({ type, ...value }));
	itemTypeSizeArray = itemTypeSizeArray.sort((a, b) => b.size - a.size).slice(0, 3);

	const itemRows = [];
	for (const stack of itemTypeSizeArray) {
		if (stack.size == 0) continue;
		const typeSize = appendElements(
			createNode('div', '', ['flexrow']),
			createNode('h4', `– ${stack.type}`, ['header', 'width8']),
			createNode('label', `${maxPrecision(stack.size / 1_000, 2)} kB`, ['items-size', 'width8', 'value', 'number']),
			createNode('label', `[${stack.count} entries; ${maxPrecision(stack.size / itemsSize * 100, 2)}% of all items]`, ['count', 'percentage']),
		);
		/*
		const typeBiggestSize = appendElements(
			createNode('div', '', ['flexrow']),
			createNode('h4', `– ${stack.type}: ${stack.biggest.name}`, ['header', 'width8']),
			createNode('label', `${maxPrecision(stack.biggest.size / 1_000, 2)} kB`, ['items-size', 'width8', 'value', 'number']),
			createNode('label', `[${maxPrecision(stack.biggest.size / stack.size * 100, 2)}% of type]`, ['percentage']),
		);
		*/
		itemRows.push(typeSize /* , typeBiggestSize*/);
	}

	c.append(
		createNode('h2', 'Stats'),
		appendElements(
			createNode('div', null, ['detail-content']),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', 'Total Size', ['header', 'width8']),
				createNode('label', `${maxPrecision(actorSize / 1_000, 2)} kB`, ['size', 'width8', 'value', 'number']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', 'Items', ['header', 'width8']),
				createNode('label', `${(itemsSize / 1_000).toFixed(2)} kB`, ['items-size', 'width8', 'value', 'number']),
				createNode('label', `[${maxPrecision(itemsSize / actorSize * 100, 2)}% of total; ${itemCount} entries]`, ['count', 'percentage']),
			),
			...itemRows,
		),
	);

	content.append(c);
};

Hooks.on('little-helper.details-tab.content', actorStats);
