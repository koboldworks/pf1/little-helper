/* eslint-disable style/key-spacing */
/* eslint-disable style/array-bracket-spacing */
/* eslint-disable style/no-multi-spaces */

import { i18n } from '@root/utility/i18n.mjs';
import { createNode, signNum, appendElements, clampNum } from '@root/common.mjs';

const BP = {
	base: {
		level:   [ 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], // for reference
	},
	take2: {
		// Source: https://docs.google.com/spreadsheets/d/1CvlqyaockPeeL56je7y1Fba7npoJXeJoYNPUOtprBEs/edit#gid=0
		attack:  [ 9, 10, 11, 13, 14, 15, 17, 18, 19, 23, 24, 27, 28, 30, 34, 34, 35, 37, 42, 43],
		armor:   [18, 20, 21, 22, 24, 26, 28, 29, 31, 32, 34, 35, 37, 38, 39, 41, 42, 46, 47, 47],
		save:    [ 5,  6,  7,  8,  8,  9, 10, 11, 11, 12, 13, 14, 14, 15, 16, 17, 17, 18, 19, 20],
		// spellcasting
		baddc:   [19, 20, 21, 21, 22, 23, 24, 25, 25, 26, 26, 28, 28, 30, 31, 29, 31, 33, 32, 35],
		gooddc:  [20, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 38, 38, 40],
	},
	take4: {
		// Source: https://docs.google.com/spreadsheets/d/1YEH6soDFkmGYi-BJI625UkfOb_Wjjw2D6oUApj0XFjo/edit#gid=0
		attack:  [ 7,  8,  9, 10, 11, 12, 13, 14, 16, 17, 18, 20, 21, 22, 23, 24, 25, 27, 27, 30],
		armor:   [18, 20, 21, 23, 25, 27, 28, 30, 31, 32, 34, 36, 37, 38, 40, 42, 45, 45, 47, 48],
		save:    [ 5,  6,  7,  8,  9, 10, 11, 12, 13, 13, 15, 15, 15, 17, 18, 19, 20, 22, 24, 24],
		// spellcasting
		baddc:   [16, 17, 17, 18, 19, 20, 21, 22, 22, 24, 24, 25, 25, 27, 27, 27, 28, 29, 30, 31],
		gooddc:  [18, 19, 20, 20, 22, 23, 24, 25, 26, 25, 28, 29, 29, 31, 31, 32, 34, 35, 36, 38],
	},
};

function benchPressBasics(content, shared) {
	const actor = shared.actor;
	const c = createNode('div', '', ['benchpress', 'flexcol']);

	const actorData = actor.system;

	const press = BP.take4;

	const level = actorData.details.level.value,
		index = clampNum(level - 1, 0, 19);
	const saves = Object.values(actorData.attributes.savingThrows).map(s => signNum(s.total)).join('/');

	const coreAttack = actorData.attributes.attack.shared + actorData.attributes.attack.general,
		szMod = pf1.config.sizeMods[actorData.traits.size],
		meleeAtkAbl = foundry.utils.getProperty(actorData, `abilities.${actorData.attributes.attack.meleeAbility}.mod`) ?? 0,
		rangedAtkAbl = foundry.utils.getProperty(actorData, `abilities.${actorData.attributes.attack.rangedAbility}.mod`) ?? 0,
		rAtk = coreAttack + szMod + actorData.attributes.attack.ranged + rangedAtkAbl,
		mAtk = coreAttack + szMod + actorData.attributes.attack.melee + meleeAtkAbl,
		bestAtk = Math.max(rAtk, mAtk);

	c.append(
		createNode('h2', 'Bench-Pressing'),
		appendElements(createNode('div', null, ['detail-content']),
			appendElements(createNode('div', null, ['flexrow', 'header']),
				createNode('label', null, ['header', 'width6']),
				createNode('label', 'Armor', ['header', 'width4', 'number']),
				createNode('label', 'Attack', ['header', 'width4', 'number']),
				createNode('label', 'Saves', ['header', 'width6', 'number']),
			),
			appendElements(createNode('div', null, ['flexrow', 'have']),
				createNode('label', 'Possessed', ['header', 'width6']),
				createNode('label', `${actorData.attributes.ac.normal.total}`, ['have', 'value', 'number', 'width4']),
				createNode('label', `${signNum(bestAtk)}`, ['have', 'value', 'number', 'width4']),
				createNode('label', `${saves}`, ['have', 'value', 'number', 'width6']),
			),
			appendElements(createNode('div', null, ['flexrow']),
				createNode('label', 'Expected', ['header', 'width6', 'number']),
				createNode('label', `${press.armor[index]}`, ['expected', 'value', 'number', 'width4']),
				createNode('label', `${signNum(press.attack[index])}`, ['expected', 'value', 'number', 'width4']),
				createNode('label', `${signNum(press.save[index])}`, ['expected', 'value', 'number', 'width6']),
			),
			/*
			appendElements(createNode('div', null, ['flexrow']),
				createNode('label', 'Difference', ['header', 'width4', 'number'])
				createNode('label', `??`, ['diff', 'value', 'number','header', 'width4']),
				createNode('label', `??`, ['diff', 'value', 'number','header', 'width4']),
				createNode('label', `??`, ['diff', 'value', 'number','header', 'width4']),
			)
			*/
			createNode('p', i18n.get('LittleHelper.Details.BenchPressing.Disclaimer'), [], { isHTML: true }),
		),
	);

	content.append(c);
}

Hooks.on('little-helper.details-tab.content', benchPressBasics);
