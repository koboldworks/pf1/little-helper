import { createNode, appendElements } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

function benchPressBasics(content, shared) {
	const actor = shared.actor;
	const c = createNode('div', '', ['token-info', 'flexcol']);

	// console.log(actor);

	const protoToken = actor.prototypeToken,
		hasVision = protoToken.sight.enabled,
		linkData = protoToken.actorLink,
		disposition = `${protoToken.disposition}`,
		displayName = protoToken.displayName,
		displayBars = protoToken.displayBars;

	const dispositionLabel = {
		0: 'TOKEN.DISPOSITION.NEUTRAL',
		1: 'TOKEN.DISPOSITION.FRIENDLY',
		'-1': 'TOKEN.DISPOSITION.HOSTILE',
	};
	const dispositionStyle = {
		0: 'neutral',
		1: 'friendly',
		'-1': 'hostile',
	};

	const displayLabel = {
		0: 'TOKEN.DISPLAY_NONE',
		10: 'TOKEN.DISPLAY_CONTROL',
		20: 'TOKEN.DISPLAY_OWNER_HOVER',
		30: 'TOKEN.DISPLAY_HOVER',
		40: 'TOKEN.DISPLAY_OWNER',
		50: 'TOKEN.DISPLAY_ALWAYS',
	};

	c.append(
		createNode('h2', 'Token Info'),
		appendElements(
			createNode('div', null, ['detail-content']),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', i18n.get('LittleHelper.Details.Token.Disposition'), ['header', 'width6']),
				createNode('label', i18n.get(dispositionLabel[disposition]), ['disposition', dispositionStyle[disposition], 'width14', 'value', 'center-text']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', i18n.get('Vision'), ['header', 'width6']),
				createNode('label', i18n.get(hasVision ? 'LittleHelper.Details.Token.Sighted' : 'LittleHelper.Details.Token.Blind'), [hasVision ? 'sighted' : 'blind', 'vision', 'width14', 'value', 'center-text']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', i18n.get('LittleHelper.Details.Token.Linked'), ['header', 'width6']),
				createNode('label', linkData ? 'Linked' : 'Unlinked', [linkData ? 'linked' : 'unlinked', 'linkstate', 'width14', 'value', 'center-text']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', i18n.get('TOKEN.CharShowNameplate'), ['header', 'width6']),
				createNode('label', i18n.get(displayLabel[displayName]), ['name-display', 'width14', 'value', 'center-text']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', i18n.get('TOKEN.ResourceDisplay'), ['header', 'width6']),
				createNode('label', i18n.get(displayLabel[displayBars]), ['bar-display', 'width14', 'value', 'center-text']),
			),
		),
	);

	content.append(c);
}

Hooks.on('little-helper.details-tab.content', benchPressBasics);
