// Display Wealth by Level information

import { createNode, signNum, appendElements, maxPrecision } from '@root/common.mjs';

const wealthByLevel = {
	2: 1_000,
	3: 3_000,
	4: 6_000,
	5: 10_500,
	6: 16_000,
	7: 23_500,
	8: 33_000,
	9: 46_000,
	10: 62_000,
	11: 82_000,
	12: 108_000,
	13: 140_000,
	14: 185_000,
	15: 240_000,
	16: 315_000,
	17: 410_000,
	18: 530_000,
	19: 685_000,
	20: 880_000,
};

function wealthByLevelProvider(content, shared) {
	const actor = shared.actor;
	const actorData = actor.system;

	const lv = actorData.details.level.value;
	const tv = actor.sheet.calculateTotalItemValue({ inLowestDenomination: true }); // Total value, not counting coins

	const cp = actor.getTotalCurrency?.() ?? 0;

	const wbl = wealthByLevel[lv] ?? Number.NaN;
	const diff = tv + cp - wbl * 100;

	const itemsByType = shared.items.byType;

	const typedValue = {
		tradegoods: 0,
		consumables: 0,
		ammunition: 0,
		unusedWeapons: 0,
		unusedArmor: 0,
		unusedGear: 0,
	};

	let unused = 0;

	const getValue = (i) => i.getValue({ inLowestDenomination: true, sellValue: 1 });

	itemsByType.consumable?.all.forEach(i => typedValue.consumables += getValue(i));
	itemsByType.loot?.all.forEach(i => {
		const value = getValue(i);
		switch (i.system.subType) {
			case 'tradeGoods': typedValue.tradegoods += value; break;
			case 'ammo': typedValue.ammunition += value; break;
		}
	});
	itemsByType.equipment?.all.forEach(i => {
		const value = getValue(i);
		const notEquipped = !i.isActive;
		if (notEquipped) unused += value;
		else return;

		switch (i.system.equipmentType) {
			case 'armor': typedValue.unusedArmor += value; break;
			case 'shield': typedValue.unusedArmor += value; break;
			case 'misc': typedValue.unusedGear += value; break;
		}
	});
	itemsByType.weapon?.all.forEach(i => {
		const value = getValue(i);
		const notEquipped = !i.isActive;
		if (notEquipped) {
			unused += value;
			typedValue.unusedWeapons += value;
		}
	});

	const c = createNode('div', '', ['wealth-by-level', 'flexcol']);
	c.append(
		createNode('h2', 'Wealth by Level'),
		appendElements(
			createNode('div', null, ['detail-content']),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', 'Expected', ['header', 'width8']),
				createNode('label', `${wbl} gp`, ['expected', 'width8', 'value', 'number']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', 'Possessed', ['header', 'width8']),
				createNode('label', `${tv / 100} gp`, ['total-value', 'width8', 'value', 'number']),
				createNode('label', `[${maxPrecision((tv + cp) / (wbl * 100) * 100, 2)}% of WBL]`, ['percentage']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', '– Unused', ['header', 'width8']),
				createNode('label', `${unused / 100} gp`, ['unused', 'width8', 'value', 'number']),
				createNode('label', `[${tv > 0 ? maxPrecision(unused / tv * 100, 2) : 0}% wealth]`, ['unused', 'percentage']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', '– Consumables', ['header', 'width8']),
				createNode('label', `${typedValue.consumables / 100} gp`, ['consumables', 'width8', 'value', 'number']),
				createNode('label', `[${tv > 0 ? maxPrecision(typedValue.consumables / tv * 100, 2) : 0}% wealth]`, ['unused', 'percentage']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', '– Ammunition', ['header', 'width8']),
				createNode('label', `${typedValue.ammunition / 100} gp`, ['ammunition', 'width8', 'value', 'number']),
				createNode('label', `[${tv > 0 ? maxPrecision(typedValue.ammunition / tv * 100, 2) : 0}% wealth]`, ['unused', 'percentage']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', '– Trade goods', ['header', 'width8']),
				createNode('label', `${typedValue.tradegoods / 100} gp`, ['trade-goods', 'width8', 'value', 'number']),
				createNode('label', `[${tv > 0 ? maxPrecision(typedValue.tradegoods / tv * 100, 2) : 0}% wealth]`, ['unused', 'percentage']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', '– Coinage', ['header', 'width8']),
				createNode('label', `${cp / 100} gp`, ['total-coinage', 'width8', 'value', 'number']),
				createNode('label', `[${tv + cp > 0 ? maxPrecision(cp / (tv + cp) * 100, 2) : 0}% wealth]`, ['unused', 'percentage']),
			),
			appendElements(
				createNode('div', '', ['flexrow']),
				createNode('h4', 'Difference', ['header', 'width8']),
				createNode('label', `${signNum(diff / 100)} gp`, ['diff', 'width8', 'value', 'number']),
				createNode('label', diff >= 0 ? 'ahead' : 'behind'),
			),
			createNode('p', 'This does not take into account fine untrackable details, such as carrying items for other people, nor can it identify backup gear or similar reliably.'),
		),
	);

	content.append(c);
}

Hooks.on('little-helper.details-tab.content', wealthByLevelProvider);
