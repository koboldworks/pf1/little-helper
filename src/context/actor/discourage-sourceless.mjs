import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function discourageSourceless(sheet, [html], options, shared) {
	if (!shared.coreSheet) return;

	const actorData = shared.systemData;

	// Combat Tab
	const combatTab = shared.tabs.combat?.tab;
	if (combatTab) {
		const notes = combatTab.querySelector('.defense-notes');

		let hasValues = false;
		const inputs = notes?.querySelectorAll('textarea');
		if (inputs) {
			for (const input of inputs) {
				if (input.textContent.length) {
					hasValues = true;
					break;
				}
			}
		}

		if (!hasValues) {
			notes?.remove();
			const header = combatTab.querySelector('table')?.parentElement;
			if (header) header.style.flex = '0';
			const footer = combatTab.querySelector('.combat-defenses');
			if (footer) footer.style.flex = '0';
		}
	}

	// Skills Tab
	const skillsTab = shared.tabs.skills?.tab;
	if (skillsTab) {
		const bonusSkills = skillsTab.querySelector('.skill-rank-formula');
		if (actorData.details.bonusSkillRankFormula.length === 0) {
			bonusSkills?.remove();

			// Shuffle lock button to better position
			const lockbutton = skillsTab.querySelector('.skill-lock-button');
			const holder = lockbutton.parentElement;
			const backgroundSkills = game.settings.get('pf1', 'allowBackgroundSkills');
			if (backgroundSkills) {
				// Background skills layout
				skillsTab.querySelector('nav.tabs').append(lockbutton);
			}
			else {
				const superholder = holder?.parentElement;
				superholder.append(lockbutton);
				superholder.style.setProperty('flex-direction', 'row');
				lockbutton.style.padding = '0.3em';
				const prev = lockbutton.previousElementSibling;
				if (prev) prev.style.flex = '1';
			}
			lockbutton.classList.add('lil-moved-skill-lock');
			// Prevent event bubbling into tabs
			lockbutton.addEventListener('click', (ev) => ev.stopPropagation());
			holder.remove();
		}
	}

	// Spells Tab
	{
		const tab = shared.tabs.spellbook?.tab;
		tab?.querySelectorAll('.spellcasting-notes').forEach(el => {
			let emptyNotes = true;
			el.querySelectorAll('textarea')?.forEach(nel => {
				if (nel.textContent.length > 0) emptyNotes = false;
			});
			if (emptyNotes) el.remove();
		});
	}
}

function enable() {
	Ancillary.register('actor', discourageSourceless);
}

function disable() {
	Ancillary.unregister('actor', discourageSourceless);
}

new Feature({ setting: 'discourage-sourceless', label: 'Discourage Sourceless', hint: 'Removes input elements for sourceless notes or bonuses that can be added in their actual sources, and adjusts layout to compensate.', category: 'actor-sheet', enable, disable, stage: 'ready' });
