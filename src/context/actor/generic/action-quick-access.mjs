import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function enableActionQuickAccess(sheet, [html], options, shared) {
}

function enable() {
	Ancillary.register('actor', enableActionQuickAccess);
}

function disable() {
	Ancillary.unregister('actor', enableActionQuickAccess);
}
