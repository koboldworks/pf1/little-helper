// Display current CL at spellbook name.

import { CFG } from '@root/config.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';

/**
 * @param {ActorSheet} _sheet
 * @param {JQuery} _jq
 * @param {object} _opts
 * @param {SharedData} shared
 */
function itemCountInjector(_sheet, _jq, _opts, shared) {
	const typed = shared.items.byType,
		spells = typed.spell;

	function addSpellCount(parentEl, count, level) {
		const actualCount = Math.max(0, count),
			pageCount = level >= 0 ? actualCount * Math.max(1, level) : null,
			title = level >= 0 ? `Page Count: ${pageCount}` : undefined;

		parentEl.querySelector('.lil-spell-details')
			.append(createNode('label', `Known: ${actualCount}`, ['lil-box', 'lil-spells-known'], { title }));
	}

	/** @type {Element[]} */
	const spellTabs = shared.tabs.spellbook?.tabs;
	spellTabs?.forEach(tab => {
		if (!spells) return;
		if (spells.all.length == 0) return;

		const bookId = tab.dataset.tab;
		const book = spells._book?.[bookId];
		if (!book) return;

		tab.querySelectorAll('.item-list[data-level]')
			.forEach(ol => {
				const level = ol.dataset.level;
				const spellCount = book[level]?.length ?? 0;
				addSpellCount(ol, spellCount, level);
			});
	});

	function addFeatCount(parentEl, count) {
		const name = parentEl.querySelector('.item-list-header .item-name');
		const rjust = createNode('div', null, ['lil-right-justify']);
		const iCount = createNode('label', `Count: ${Math.max(0, count)}`, ['lil-box', 'lil-feats-possessed']);
		rjust.append(iCount);
		name.append(rjust);
	}

	shared.tabs.feats?.tab
		?.querySelectorAll('ol.item-list[data-type]').forEach(ol => {
			const type = ol.dataset.type,
				subtype = ol.dataset.subtype;
			const count = typed[type][subtype]?.all?.length ?? 0;
			addFeatCount(ol, count);
		});
}

Ancillary.register('actor', itemCountInjector);
