import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function newActorGuide(sheet, [html], options, shared) {
	if (!sheet.isEditable) return;

	if (sheet.actor.getFlag(CFG.id, 'overrides')?.ready) return;

	// Only support specific sheets
	if (!['ActorSheetPFCharacter', 'ActorSheetPFNPC'].includes(sheet.constructor.name)) return;

	const doc = shared.document;
	if (!['character', 'npc'].includes(doc.type)) return;
	if (doc._newActor === false) return;

	const data = shared.systemData;

	const abls = data.abilities;
	const undefinedAbilities = Object.values(abls).every(abl => abl.value === 10);

	const itemTypes = shared.items.byType;
	const noClass = itemTypes.class?.all.length == 0;
	const noRace = itemTypes.race?.all.length == 0;
	const noEquipment = itemTypes.weapon?.all.length == 0 && itemTypes.equipment?.all.length == 0 && itemTypes.loot?.all.length == 0 && itemTypes.container?.all.length == 0 && itemTypes.consumable?.all.length == 0;
	const noSkills = Object.values(data.skills).every(sk => !(sk.rank > 0));

	const classLevels = itemTypes.class?.all
		.filter(cls => ['base', 'npc', 'prestige', 'racial'].includes(cls.subType))
		.reduce((cur, o) => cur + o.hitDice, 0);
	const baseFeatCount = Math.ceil(classLevels / 2);
	const noFeats = baseFeatCount > 0 ? itemTypes.feat?.all.filter(f => f.subType === 'feat').length == 0 : false;

	if (!undefinedAbilities && !noClass && !noRace && !noFeats && !noEquipment && !noSkills) {
		console.debug('Marking', doc.name, doc.id, 'as finished.');
		doc._newActor = false;
		return;
	}

	const warningClass = 'lil-new-actor-warning',
		tabWarningClass = 'lil-new-actor-tab-warning';

	const tabs = html.querySelector('nav.tabs');

	const form = html.querySelector('form') ?? html;
	let haveWarning = form.querySelector('form > .warning') != null;

	function addWarning(text) {
		if (haveWarning) return;
		haveWarning = true;
		form.prepend(createNode('span', text, ['warning', 'charactermancer']));
	}

	const stab = shared.tabs.summary?.tab;
	if (noClass) {
		stab.querySelector('.classes-body')?.classList.add(warningClass);
		tabs.querySelector('.item[data-tab="summary"]')?.classList.add(tabWarningClass);
	}

	if (noRace) {
		stab.querySelector('.race-container')?.classList.add(warningClass);
		tabs.querySelector('.item[data-tab="summary"]')?.classList.add(tabWarningClass);
		addWarning('The character has no race. Go to Summary tab to fix this.');
	}

	if (undefinedAbilities) {
		const atab = shared.tabs.attributes?.tab;
		atab?.querySelector('button.pointbuy-calculator')
			?.classList.add(warningClass);

		atab?.querySelector('.ability-scores')
			?.querySelectorAll('tr.ability > th + td')
			.forEach(el => el.classList.add(warningClass));

		tabs.querySelector('.item[data-tab="attributes"]')
			?.classList.add(tabWarningClass);

		addWarning('The character has no ability scores set. Go to Attributes tab to fix this.');
	}

	if (noFeats) {
		const f = shared.tabs.feats.tab?.querySelector('ol.feats-feat');
		f?.classList.add(warningClass, 'unpadded');
		tabs.querySelector('.item[data-tab="feats"]')?.classList.add(tabWarningClass);
		addWarning('The character has no feats. Go to Features tab to fix this.');
	}

	if (noSkills) {
		tabs.querySelector('.item[data-tab="skills"]')?.classList.add(tabWarningClass);
		addWarning('The character has no skill ranks. Go to Skills tab to fix this.');
	}

	if (noEquipment) {
		tabs.querySelector('.item[data-tab="inventory"]')?.classList.add(tabWarningClass);
		addWarning('The character has no equipment. Go to Inventory tab to fix this.');
	}
}

function enable() {
	Ancillary.register('actor', newActorGuide);
}

function disable() {
	Ancillary.unregister('actor', newActorGuide);
}

new Feature({ setting: 'new-actor-guide', category: 'actor-sheet', enable, disable, stage: 'ready' });
