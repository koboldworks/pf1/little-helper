import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode, maxPrecision } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} opts
 * @param {SharedData} shared
 */
function insertCoinWeightDisplay(sheet, [html], opts, shared) {
	const weight = sheet.actor._calculateCoinWeight();
	if (weight < Number.EPSILON) return;

	const { light } = sheet.actor.getCarryCapacity();

	const ratio = maxPrecision(weight / light, 2);

	const weightCSS = ratio >= 0.5 ? 'heavy-weight' :
		ratio >= 0.35 ? 'moderate-weight' :
			ratio >= 0.2 ? 'light-weight' :
				ratio >= 0.1 ? 'feather-weight' : 'smart-weight';

	const label = shared.tabs.inventory?.tab?.querySelector('.currency h3');
	if (!label) return;

	const units = pf1.utils.getWeightSystem() === 'metric' ? i18n.get('PF1.Kgs') : i18n.get('PF1.Lbs');

	const weightLabel = createNode('label', `(${maxPrecision(weight, 1)} ${units})`, ['lil-currency-weight', weightCSS]);
	weightLabel.dataset.ratio = ratio;
	weightLabel.dataset.grade = Math.round(ratio * 10);
	label?.append(weightLabel);
}

function enable() {
	Ancillary.register('actor', insertCoinWeightDisplay);
}

function disable() {
	Ancillary.unregister('actor', insertCoinWeightDisplay);
}

new Feature({ setting: 'currency-weight-display', category: 'actor-sheet', enable, disable, stage: 'ready' });
