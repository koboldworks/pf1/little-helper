import { CFG } from '@root/config.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';
import { wondrousSlots, implantSlots, armorSlots } from '../items/item-slots.mjs';
import { Feature, SubSetting } from '@core/feature.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {Item[]} eq
 * @param {Function} getSlot
 * @returns {object}
 */
function groupItems(eq, getSlot) {
	return eq.reduce((all, item) => {
		const slot = getSlot(item);
		if (slot != undefined) {
			if (all[slot] === undefined) all[slot] = [];
			all[slot].push(item);
		}
		return all;
	}, {});
}

/**
 * @param {object} info
 * @param {object} grouped
 */
function countSlotUsage(info, grouped) {
	for (const key of Object.keys(info)) {
		info[key] = info[key].clone();
		const value = info[key];
		if (grouped[key] === undefined) continue;
		value.count = grouped[key].length;
	}
}

/**
 * @param {Element} section
 * @param {object} info
 * @param {Actor} actor
 * @param {object} grouped
 * @param {Function} getSlot
 */
function markItems(section, info, actor, grouped, getSlot) {
	const items = section.querySelectorAll('.item[data-item-id]');
	for (const el of items) {
		const item = actor.items.get(el.dataset.itemId);
		const slot = getSlot(item);
		if (info[slot]?.warn) {
			if (grouped[slot].find(i => i.id === item.id) === undefined) continue;
			const st = el.querySelector('.item-slot');
			const soft = info[slot].soft;
			st.classList.add(soft ? 'lil-warning' : 'lil-error');

			const sel = st.firstElementChild || st;
			// TODO: Improve messageing
			let msg = i18n.get('LittleHelper.Warning.TooManySlotItems', { used: info[slot].count, limit: info[slot].limit });
			if (soft) msg += '<br>' + i18n.get('LittleHelper.Clarify.NoActionNeeded');
			sel.dataset.tooltip += '<br>' + msg;
		}
	}
}

function notifyLackingUsage(el, info, labels) {
	const unusedSlots = [];
	for (const [slot, sInfo] of Object.entries(info)) {
		if (Number.isFinite(sInfo.limit) && sInfo.count < sInfo.limit) {
			// const label = sInfo.limit > 1 ? `${sInfo.limit - sInfo.count}× ${slot}` : slot;
			unusedSlots.push(slot);
		}
	}

	if (unusedSlots.length) {
		const unusedMsg = i18n.get('LittleHelper.Warning.UnusedSlots') + ': ' + unusedSlots.map(s => labels[s] || s).sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' })).join(', ');
		const holder = createNode('div', null, ['lil-unused-slots']);
		holder.append(createNode('i', null, ['fas', 'fa-info-circle', 'lil-info-symbol']), unusedMsg);

		el.append(holder);
	}
}

/**
 * @param {Element} tab
 * @param {Actor} actor
 */
function armorAndShieldConflicts(tab, actor) {
	const eq = actor.itemTypes.equipment?.filter(i => i.isActive && ['armor', 'shield'].includes(i.subType)) ?? [];

	// Group by slot
	const grouped = groupItems(eq, i => i.subType);

	const info = { ...armorSlots };
	countSlotUsage(info, grouped);

	// Check slotted items
	const eqSec = tab?.querySelector('.item-list[data-list="armor"]');
	if (!eqSec) return;

	markItems(eqSec, info, actor, grouped, i => i?.subType);

	// Unused slots
	if (feature.subsettings.unused.value) {
		if (info.armor.count == 0) {
			const unusedMsg = i18n.get('LittleHelper.Warning.NoArmor');
			const holder = createNode('div', null, ['lil-unused-slots']);
			holder.append(createNode('i', null, ['fas', 'fa-info-circle', 'lil-info-symbol']), unusedMsg);

			eqSec.append(holder);
		}
	}
}

/**
 * @param {Element} tab
 * @param {Actor} actor
 */
function magicItemConflicts(tab, actor) {
	const eq = actor.itemTypes.equipment?.filter(i => i.isActive && i.hasSlots) ?? [];
	if (eq.length == 0) return;

	// Group by slot
	const grouped = groupItems(eq, i => i.system.slot);

	const info = { ...wondrousSlots };
	countSlotUsage(info, grouped);

	// Check slotted items
	const eqSec = tab?.querySelector('.item-list[data-list="equipment"]');
	if (!eqSec) return;

	markItems(eqSec, info, actor, grouped, i => i?.system.slot);

	// Unused slots
	if (feature.subsettings.unused.value) {
		notifyLackingUsage(eqSec, info, pf1.config.equipmentSlots.wondrous);
	}
}

/**
 * @param {Element} tab
 * @param {Actor} actor
 */
function implantConflicts(tab, actor) {
	const eq = actor.itemTypes.implant?.filter(i => i.isActive) ?? [];
	if (eq.length == 0) return;

	// Group by slot
	const grouped = groupItems(eq, i => i.system.slot);

	const info = { ...implantSlots };
	countSlotUsage(info, grouped);

	// Check slotted items
	const eqSec = tab?.querySelector('.item-list[data-list="implants"]');
	if (!eqSec) return;

	markItems(eqSec, info, actor, grouped, i => i?.system.slot);

	// Unused slots
	if (feature.subsettings.unused.value) {
		// notifyLackingUsage(eqSec, info, pf1.config.implantSlots.cybertech);
	}
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {import('@core/shared-data.mjs').SharedData} shared
 */
function slotUsageConflict(sheet, [html], options, shared) {
	if (!sheet.isEditable) return;
	if (!shared.coreSheet) return;

	if (sheet.actor.getFlag(CFG.id, 'overrides')?.equipped) return;

	const tab = shared.tabs?.inventory?.tab;
	if (!tab) return;

	const actor = sheet.actor;

	armorAndShieldConflicts(tab, actor);
	magicItemConflicts(tab, actor);
	implantConflicts(tab, actor);
}

function enable() {
	Ancillary.register('actor', slotUsageConflict);
}

function disable() {
	Ancillary.unregister('actor', slotUsageConflict);
}

const feature = new Feature({
	setting: 'duplicate-slots',
	category: 'actor-sheet',
	enable,
	disable,
	stage: 'ready',
	subsettings: {
		unused: new SubSetting({
			label: 'Unused Slots',
			hint: 'Display summary of unused slots at end of the slotted item list.',
			type: Boolean,
			fallback: true,
		}),
	},
});
