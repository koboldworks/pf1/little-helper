import { CFG } from '@root/config.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { i18n } from '@root/utility/i18n.mjs';
import { SharedData } from '@core/shared-data.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {Element} html
 * @param {object} options
 * @param {SharedData} shared
 */
async function addSortingInterface(sheet, html, options, shared) {

}

function enable() {
	Ancillary.register('actor', addSortingInterface);
}

function disable() {
	Ancillary.unregister('actor', addSortingInterface);
}

const feature = new Feature({
	setting: 'item-sorting',
	label: 'Item Sorting',
	hint: 'Adds interface to easily sort inventory.',
	category: 'actor-sheet',
	enable,
	disable,
	stage: 'ready',
});
