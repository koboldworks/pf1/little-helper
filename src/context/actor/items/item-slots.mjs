import { CFG } from '@root/config.mjs';

// WIP

/**
 * Slot information
 */
export class Slot {
	/**
	 * @type {number} - Maximum number of items in this slot.
	 */
	limit = 1;

	/**
	 * @type {boolean} - Is the limit soft?
	 */
	soft = false;

	/**
	 * @type {number} - Number of matching items on actor.
	 */
	count = 0;

	/**
	 * @param {number} limit - Maximum number of items
	 * @param {object} [options] - Additional options
	 * @param {boolean} [options.soft] - Soft limit
	 * @param {number} [options.count] - Number of items
	 */
	constructor(limit = 1, { soft = false, count = 0 } = {}) {
		this.limit = limit;
		this.soft = soft;
		this.count = count;
	}

	get warn() {
		return this.count > this.limit;
	}

	clone() {
		return new Slot(this.limit, { soft: this.soft, count: this.count });
	}
}

/* Humanoid default */
export const wondrousSlots = {
	belt: new Slot(1),
	body: new Slot(1),
	chest: new Slot(1),
	eyes: new Slot(1),
	feet: new Slot(1),
	hands: new Slot(1),
	head: new Slot(1),
	headband: new Slot(1),
	neck: new Slot(1),
	ring: new Slot(2),
	shoulders: new Slot(1),
	slotless: new Slot(Number.POSITIVE_INFINITY),
	wrists: new Slot(1),
};

export const implantSlots = {
	arm: new Slot(1),
	body: new Slot(1, { soft: true }), // cybersoldier can equip 2
	brain: new Slot(1, { soft: true }), // cybersoldier can equip 2
	ears: new Slot(1),
	eyes: new Slot(1),
	head: new Slot(1),
	legs: new Slot(1),
};

export const armorSlots = {
	armor: new Slot(1, { soft: true }),
	shield: new Slot(1),
};

CFG.API.slots = wondrousSlots;
CFG.API.classes.slot = Slot;
