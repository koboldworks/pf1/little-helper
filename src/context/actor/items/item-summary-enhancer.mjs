import { createNode, signNum } from '@root/common.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { getTimeFromItem, getHumanTime, humanTimeUnit } from '@root/common.time.mjs';
import { i18n } from '@root/utility/i18n.mjs';

const addFASIcon = (el, iconCss) => {
	el.prepend(createNode('i', null, iconCss));
	return el;
};

function getBaseGearInfo(sheet, shared) {
	const cache = {};
	const types = shared.items.byType;

	cache['equipment'] = {};
	cache['weapon'] = [];
	types.equipment.active.forEach(i => {
		const subType = i.subType;
		if (['shield', 'armor'].includes(subType))
			cache[i.type][subType] = i;
	});
	types.weapon.active.forEach(i => {
		cache[i.type].push(i);
	});
	return cache;
}

function getItemInfo(item, rv = {}, { isIdentified = true } = {}) {
	const type = item.type,
		subType = item.subType,
		itemData = item.system;

	if (game.user.isGM)
		isIdentified = true;

	rv.weight = itemData.weight;
	rv.size = itemData.size;
	rv.resizing = itemData.resizing;
	rv.broken = itemData.broken ?? false;

	switch (type) {
		case 'equipment': {
			switch (subType) {
				case 'shield':
				case 'armor': {
					rv.armor = itemData.armor;
					rv.masterwork = itemData.masterwork ?? false;
					rv.enhancement = isIdentified ? (rv.armor.enh ?? 0) : 0;
					rv.ac = rv.armor.value + rv.enhancement;
					rv.maxDex = rv.armor.dex;
					rv.acp = Math.max(0, rv.armor.acp + (rv.masterwork ? -1 : 0));
					rv.asf = itemData.spellFailure;
				}
			}
			break;
		}
		case 'weapon': {
			rv.masterwork = itemData.masterwork;
			rv.enhancement = isIdentified ? rv.enh : 0;

			// console.log({ itemData });
			break;
		}
		case 'attack': {
			break;
		}
	}

	const act = item.defaultAction;
	rv.action ??= {};
	if (act?.hasAttack) {
		rv.action.critRange = act.critRange ?? 20;
		rv.action.critMult = act.ability?.critMult ?? 2;
	}

	return rv;
}

function artificialSubType(item) {
	switch (item.type) {
		case 'weapon': return 'any';
	}
}

const sizeMap = {
	fine: 0, dim: 1, tiny: 2, sm: 3, med: 4, lg: 5, huge: 6, grg: 7, col: 8,
};

/**
 * @param {Item} item
 * @param {Element} node
 * @param {Actor} actor
 */
async function enrichItemSummary(item, node, { actor, gear } = {}) {
	const props = node.querySelector('.item-properties');

	if (!props) {
		// synthetic properties?
		console.warn('--- property list not found:', item.name);
		return;
	}

	const itemData = item.system,
		actorData = actor.system,
		type = item.type,
		subType = item.subType ?? artificialSubType(item),
		// isEphemeral = ['attack', 'feat', 'race', 'class', 'spell', 'buff'].includes(type),
		isPhysical = ['weapon', 'equipment', 'consumable', 'loot', 'container'].includes(type),
		isIdentified = itemData.identified ?? true;

	const info = getItemInfo(item, undefined, { isIdentified });

	const acAbility = actorData.attributes.ac.normal.ability,
		dexMod = actorData.abilities[acAbility]?.mod;

	if (isPhysical) {
		const isWeapon = item.type === 'weapon',
			isArmor = !isWeapon && ['armor', 'shield'].includes(subType);

		if (isArmor) {
			const acText = i18n.get('LittleHelper.Info.AC', { ac: info.ac });
			props.append(createNode('span', acText, ['tag', 'ac', info.enhancement > 0 ? 'enhanced' : 'mundane']));

			if (Number.isFinite(info.maxDex)) {
				const emDex = Math.min(info.maxDex, dexMod);
				const mdText = i18n.get('LittleHelper.Info.MDexAC', { mdex: info.maxDex, ac: emDex });
				props.append(createNode('span', mdText, ['tag', 'mdex']));
			}

			const acpText = i18n.get('LittleHelper.Info.ACP', { acp: info.acp });
			props.append(createNode('span', acpText, ['tag', 'acp']));

			if (info.asf > 0) {
				const text = i18n.get('LittleHelper.Info.ASF', { asf: info.asf });
				props.append(createNode('span', text, ['tag', 'asf']));
			}
		}

		const price = item.getValue({ recurivse: false, sellValue: 1, inLowestDenomination: true });
		if (price !== undefined) {
			const valueText = i18n.get('LittleHelper.Info.Value', { gp: pf1.utils.limitPrecision(price / 100, 2) });
			props.append(createNode('span', valueText, ['tag', 'value']));
		}

		const hp = itemData.hp;
		if (hp?.value !== undefined) {
			const hpText = i18n.get('LittleHelper.Info.HP', { current: hp.value, max: hp.max });
			const hpTag = createNode('span', hpText, ['tag', 'hit-points']);
			if (info.broken || hp.value < hp.max / 2) addFASIcon(hpTag, ['fas', 'fa-heart-broken']);
			else addFASIcon(hpTag, ['fas', 'fa-heart']);
			props.append(hpTag);
		}

		const hardness = itemData.hardness;
		if (hardness !== undefined) {
			const hardText = i18n.get('LittleHelper.Info.Hardness', { hardness });
			props.append(createNode('span', hardText, ['tag', 'hardness']));
		}

		// TODO: Item size
		if (info.size !== undefined && !info.resizing) {
			const sizeNum = sizeMap[info.size],
				actorSize = sizeMap[actorData.traits.size || 'med'];

			if (sizeNum !== actorSize && !['consumable'].includes(item.type)) {
				const sizeLabel = pf1.config.actorSizes[info.size];
				props.append(createNode('span', sizeLabel.capitalize(), ['tag', 'size', 'bad-fit']));
			}
		}

		if (type === 'container') {
			const items = item.items;
			const countLabel = i18n.get('LittleHelper.Info.ItemCount', { count: items.contents.length });
			props.append(createNode('span', countLabel, ['tag', 'items']));
			if (items.find(i => i.type === 'container')) {
				const subcontainers = i18n.get('LittleHelper.Info.Subcontainers');
				props.append(addFASIcon(createNode('span', subcontainers, ['tag', 'sub-containers']), ['fas', 'fa-box']));
			}

			const cp = item.getTotalCurrency({ inLowestDenomination: true });
			if (cp != 0) {
				const currencyLabel = i18n.get('LittleHelper.Info.Currency', { gp: cp / 100 });
				props.append(addFASIcon(createNode('span', currencyLabel, ['tag', 'container-currency']), ['fas', 'fa-coins']));
			}
		}
	}

	if (info.action?.critRange !== undefined) {
		const text = i18n.get('LittleHelper.Info.Critical', { range: info.action.critRange, mult: info.action.critMult });
		props.append(addFASIcon(createNode('span', text, ['tag', 'critical']), ['fas', 'fa-bullseye']));
	}

	/*
	if (item.hasSave) {
		console.log('hasSave', item, itemInfo);
	}
	*/

	if (itemData.spellArea) {
		props.append(addFASIcon(createNode('span', itemData.spellArea, ['tag', 'area']), ['fas', 'fa-ruler-combined']));
	}

	if (itemData.duration?.value) {
		const ti = getTimeFromItem(item, item.getRollData())?.duration;
		const ht = getHumanTime(ti);
		props.append(addFASIcon(createNode('span', `${ht.n} ${humanTimeUnit[ht.u].long}`, ['tag', 'duration', 'scaling']), ['fas', 'fa-hourglass']));
	}

	if (itemData.spellDuration) {
		props.append(addFASIcon(createNode('span', itemData.spellDuration, ['tag', 'duration', 'descriptive']), ['fas', 'fa-hourglass']));
	}

	// ITEM COMPARISON

	/** @type {Item} */
	const comparedItem = gear[type]?.[subType]?.id !== item.id ? gear[type]?.[subType] : null;
	if (comparedItem) {
		const diffs = [],
			cItemData = comparedItem.system,
			itemInfoC = getItemInfo(comparedItem, undefined, { isIdentified: cItemData.identified ?? true });

		switch (type) {
			case 'equipment': {
				// ARMOR stats
				const acD = info.ac - itemInfoC.ac,
					acpD = info.acp - itemInfoC.acp,
					asfD = info.asf - itemInfoC.asf,
					mdexD = info.maxDex - itemInfoC.maxDex,
					eDexCurrent = Number.isFinite(info.maxDex) ? Math.min(dexMod, info.maxDex) : null,
					eDexOther = Number.isFinite(itemInfoC.maxDex) ? Math.min(dexMod, itemInfoC.maxDex) : null,
					eDexD = (eDexCurrent ?? Number.POSITIVE_INFINITY) - (eDexOther ?? Number.POSITIVE_INFINITY);

				if (acD !== 0) {
					const text = i18n.get('LittleHelper.Info.AC', { ac: signNum(acD) });
					diffs.push(createNode('span', text, ['tag', 'ac']));
				}
				if (mdexD !== null) {
					// console.log({ mdexD, eDexN: eDexCurrent, eDexO: eDexOther, eDexD });
					const text = Number.isFinite(eDexD) ? i18n.get('LittleHelper.Info.MDexAC', { mdex: signNum(mdexD), ac: signNum(eDexD) }) : i18n.get('LittleHelper.Info.MDex', { mdex: signNum(mdexD) });
					diffs.push(createNode('span', text, ['tag', 'mdex']));
				}
				if (acpD !== 0) {
					const text = i18n.get('LittleHelper.Info.ACP', { acp: signNum(acpD) });
					diffs.push(createNode('span', text, ['tag', 'acp']));
				}
				if (asfD !== 0) {
					const text = i18n.get('LittleHelper.Info.ASF', { asf: signNum(asfD) });
					diffs.push(createNode('span', text, ['tag', 'asf']));
				}

				{
					const text = i18n.get('LittleHelper.Info.ComparedAgainst', { name: comparedItem.name });
					diffs.push(createNode('span', text, ['lil-short-label']));
				}

				break;
			}
		}

		const cprops = createNode('div', null, ['item-comparison', 'item-properties', 'tag-list']);
		cprops.append(createNode('span', i18n.get('LittleHelper.Info.Comparison')), ': ', ...diffs);
		props.after(cprops);
	}

	// Weapon qualities
	if (item.type === 'weapon') {
		const qualities = Object
			.entries(itemData.properties ?? {})
			.filter(([_, enabled]) => enabled)
			.map(([key]) => createNode('div', pf1.config.weaponProperties[key], ['tag']));

		if (qualities.length) {
			const wprops = createNode('div', null, ['weapon-qualities', 'item-properties', 'tag-list']);
			wprops.append(createNode('span', i18n.get('Koboldworks.Missing.PF1.Qualities')), ': ', ...qualities);
			props.after(wprops);
		}
	}
}

const config = { subtree: false, childList: true, attributes: false };

const observers = {};

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function enrichOpenSummaries(sheet, [html], options, shared) {
	const actor = sheet.document,
		gearCache = getBaseGearInfo(sheet, shared);

	html.querySelectorAll('.item-list > .item[data-item-id] > .item-summary')
		.forEach(el => {
			const itemId = el.closest('.item[data-item-id]')?.dataset.itemId,
				item = actor.items.get(itemId);
			if (item)
				enrichItemSummary(item, el, { actor, gear: gearCache });
		});
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function observeItems(sheet, [html], options, shared) {
	const actor = sheet.actor;

	const gearCache = getBaseGearInfo(sheet, shared);

	const mutationCallback = (mutList) => {
		function mutHandler(mut) {
			const node = mut.addedNodes[0];
			if (!node.classList.contains('item-summary')) return;

			const item = actor?.items.get(mut.target.dataset.itemId);
			if (!item) return;
			enrichItemSummary(item, node, { actor, gear: gearCache });
		}

		Array.from(mutList).filter(m => m.addedNodes.length).forEach(mutHandler);
	};

	observers[sheet.appId]?.observer?.disconnect(); // kill old observer if it exists

	const observer = new MutationObserver(mutationCallback);
	observers[sheet.appId] = {
		sheet,
		actor: sheet.actor,
		observer,
	};

	html.querySelectorAll('.item-list .item[data-item-id]')
		?.forEach(el => observer.observe(el, config));
}

const endObserver = (appId) => {
	observers[appId]?.observer.disconnect();
	delete observers[appId];
};

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} jq
 */
const closeSheetEvent = (sheet, jq) => endObserver(sheet.appId);

function enable() {
	Ancillary.register('actor', observeItems);
	Ancillary.register('actor', enrichOpenSummaries);
	Hooks.on('closeActorSheet', closeSheetEvent);
}

function disable() {
	Object.keys(observers).forEach(appid => {
		observers[appid]?.observer?.disconnect();
		delete observers[appid];
	});

	Ancillary.unregister('actor', observeItems);
	Ancillary.unregister('actor', enrichOpenSummaries);
	Hooks.off('closeActorSheet', closeSheetEvent);
}

new Feature({
	setting: 'item-summary-enhancer', label: 'Item Summary Enricher', hint: 'Enriches item summary with additional info, such as item value, hit points, and hardness.', category: 'actor-sheet', enable, disable, stage: 'ready',
});
