// WIP

/**
 * Render item slot configuration on the character sheet.
 */

import { CFG } from '@root/config.mjs';
import { Ancillary } from '@root/ancillary.mjs';

import { wondrousSlots } from './item-slots.mjs';
import { FAIcons } from '@core/icons.mjs';

import { createNode } from '@root/common.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} opts
 * @param {object} shared
 */
function itemSlotConfig(sheet, [html], opts, shared) {
	if (!opts.editable) return;

	const tab = shared.tabs.settings?.tab;

	const actor = sheet.actor;

	const slots = actor.getFlag(CFG.id, 'itemSlots') ?? foundry.utils.deepClone(wondrousSlots);

	const d = createNode('div', null, ['flexcol']);
	d.append(
		createNode('div', null, ['form-group', 'lil-item-slot-config'], {
			children: [createNode('i', null, FAIcons.puzzlePiece,
				createNode('h2', 'Item Slot Configuration'))],
		}),
		createNode('div', null, ['form-group'], { children: [
			Object.entries(wondrousSlots).forEach((s) => {
				const [slot, data] = s;
			}),
		] }),
	);
}

Ancillary.register('actor', itemSlotConfig);
