/*
 * Make finding common easier.
 */

import { Feature } from '@core/feature.mjs';
import { createNode } from '@root/common.mjs';

const LangGroups = {
	secret: ['druidic', 'drowsign'],
	core: ['draconic', 'cyclops', 'azlanti'], // Langauges other languages base themselves on
	racial: ['elven', 'dwarven', 'gnome', 'orc', 'goblin', 'grippli', 'halfling', 'gnoll', 'tengu', 'strix', 'samsaran', 'kasatha', 'kuru', 'munavri', 'nagaji', 'caligni', 'dark'],
	monster: ['sphinx', 'sasquatch', 'aboleth', 'aklo', 'cyclops', 'necril', 'giant', 'dziriak', 'arboreal', 'treant', 'vegepygmy', 'boggard'], // Non-specific monstrous/NPC race languages.
	regional: ['varisian', 'skald', 'taldane', 'shoanti', 'vudrani', 'varki', 'shadowtongue', 'orvian', 'osiriani', 'kelish', 'minatan', 'hallit', 'polyglot', 'mwangi'], // Regional & Cultural
	elemental: ['terran', 'ignan', 'aquan', 'auran'],
	planar: ['celestial', 'infernal', 'abyssal', 'protean'],
	dead: ['azlanti', 'thassilonian', 'ancientosiriani', 'tekritanin', 'shory', 'jistka'],
	common: ['common', 'taldane', 'tien', 'undercommon'],
};

const animalIcons = ['fa-cat', 'fa-dog', 'fa-otter', 'fa-hippo', 'fa-crow', 'fa-dove', 'fa-dragon', 'fa-frog', 'fa-horse', 'fa-fish'];

/**
 * @param {ItemSheet} app
 * @param {JQuery} html
 */
function manipulateTraitSelector(app, [html]) {
	const isLang = app.options.subject === 'languages';
	if (!isLang) return;

	html.classList.add('lil-enhanced-language-selector');

	html.querySelectorAll('li input[type="checkbox"]')
		?.forEach(node => {
			if (node.type !== 'checkbox') return;
			const el = node.closest('li');
			const lang = el.dataset.choice;
			let icon, css;
			const label = createNode('label', null, ['lil-lang-type']);
			if (lang === 'draconic') {
				css = ['draconic'];
				icon = ['fa-dragon'];
				label.textContent = ' ';
			}
			else if (lang === 'sylvan') {
				css = ['sylvan'];
				icon = ['fa-cannabis'];
				label.textContent = ' ';
			}
			else if (LangGroups.common.includes(lang)) {
				css = ['lil-highlight', 'common'];
				icon = ['fa-globe'];
				label.textContent = 'Common';
			}
			else if (LangGroups.secret.includes(lang)) {
				css = ['secret'];
				icon = ['fa-mask'];
				label.textContent = 'Secret';
			}
			else if (LangGroups.racial.includes(lang)) {
				css = ['racial'];
				icon = ['fa-fish'];
				label.textContent = 'Racial';
			}
			else if (LangGroups.monster.includes(lang)) {
				css = ['monster', 'npc'];
				icon = ['fa-pastafarianism'];
				label.textContent = 'Monster/NPC';
			}
			else if (LangGroups.regional.includes(lang)) {
				css = ['regional'];
				icon = ['fa-map'];
				label.textContent = 'Regional';
			}
			else if (LangGroups.elemental.includes(lang)) {
				css = ['elemental'];
				icon = ['fa-fire'];
				label.textContent = 'Elemental';
			}
			else if (LangGroups.planar.includes(lang)) {
				css = ['planar'];
				icon = ['fa-solar-panel'];
				label.textContent = 'Planar';
			}
			else if (LangGroups.dead.includes(lang)) {
				css = ['rare'];
				icon = ['fa-monument'];
				label.textContent = 'Dead';
			}
			else {
				css = ['unknown'];
				icon = ['fa-question-circle'];
				label.textContent = 'Unidentified';
			}
			if (css) el.classList.add(...css);
			if (icon) node.parentElement.after(
				createNode('i', null, ['fas', ...icon, 'lil-icon', 'lil-faded']),
				label);
		});
}

function enable() {
	Hooks.on('renderActorTraitSelector', manipulateTraitSelector);
}

function disable() {
	Hooks.off('renderActorTraitSelector', manipulateTraitSelector);
}

new Feature({ setting: 'enhanced-language-selection', category: 'actor-sheet', enable, disable, stage: 'ready' });
