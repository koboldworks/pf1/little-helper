/*
 * Warn about various bits not being there.
 */

import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { addWarning, constructWarning, unifyProficiency, createNode } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} _data
 * @param {import('@root/core/shared-data.mjs').SharedData} shared
 */
function missingBitsWarnings(sheet, [html], _data, shared) {
	if (!sheet.isEditable) return;
	if (!shared.coreSheet) return;

	if (sheet.actor.getFlag(CFG.id, 'overrides')?.complete) return;

	const actor = sheet.actor,
		actorData = shared.systemData;

	// FEATS
	/** @type {Element} */
	const featTab = shared.tabs.feats?.tab;

	const feats = actor.getFeatCount();
	const active = feats.active;
	if (feats.max !== active) {
		const diff = active - feats.max;
		const featsUsed = featTab?.querySelector('.tab-footer table td');
		if (featsUsed) {
			addWarning(featsUsed, i18n.get('LittleHelper.Warning.FeatMismatch'));
			featsUsed.append(createNode('span', ` (${Math.abs(diff)} ${diff < 0 ? ' missing' : ' excess'})`, ['lil-diff']));
		}
	}

	// SKILLS
	// Made unnecessary with PF1v10
	/** @type {Element} */
	const skillTab = shared.tabs.skills?.tab;
	if (skillTab) {
		const srInfo = skillTab.querySelector('.skill-rank-info');
		if (srInfo) {
			const skillRanksMax = srInfo.querySelector('.ranks-max'),
				skillRanksUsed = srInfo.querySelector('.ranks-used');

			{
				const available = parseInt(skillRanksMax.innerText, 10), used = parseInt(skillRanksUsed.innerText, 10),
					diff = used - available;
				if (diff !== 0) {
					addWarning(skillRanksUsed, i18n.get('LittleHelper.Warning.SkillMismatch'));
					skillRanksUsed.append(createNode('span', ` (${Math.abs(diff)} ${diff < 0 ? ' missing' : ' excess'})`, ['lil-diff']));
				}
			}

			const bgRanksMax = srInfo.querySelector('.ranks-max-bg'),
				bgRanksUsed = bgRanksMax ? srInfo.querySelector('.ranks-used-bg') : null;

			// Background skills
			if (bgRanksUsed) {
				const available = parseInt(bgRanksMax.innerText, 10), used = parseInt(bgRanksUsed.innerText, 10),
					diff = used - available;
				if (diff !== 0) {
					addWarning(bgRanksUsed, i18n.get('LittleHelper.Warning.SkillMismatch'));
					bgRanksUsed.append(createNode('span', ` (${Math.abs(diff)} ${diff < 0 ? ' missing' : ' excess'})`, ['lil-diff']));
				}
			}

			// Unnamed skills
			skillTab?.querySelectorAll('[data-skill] .skill-name input[type="text"]')
				.forEach(el => {
					if (el.value.trim().length === 0)
						el.closest('[data-skill]')?.classList.add('lil-missing-skill-name');
				});
		}
	}

	// UNUSED SPELLS
	/** @type {Element} */
	const spellTab = shared.tabs.spellbook?.tab;
	spellTab?.querySelectorAll('.spell-uses .spell-slots')
		?.forEach(el => {
			const left = parseInt(el.querySelector('[data-dtype="Number"]')?.textContent.trim(), 10);
			if (left < 0 || !el.classList.contains('spontaneous') && left > 0)
				el.classList.add('lil-warning');
		});

	// FCB
	/** @type {Element} */
	const clss = shared.tabs.summary?.tab;
	if (clss) {
		const fcb = shared.items.byType.class?.all.reduce((total, cls) => {
			const { alt, hp, skill } = cls.system.fc;
			return total + alt.value + hp.value + skill.value;
		}, 0) ?? 0;

		if (fcb === 0) {
			clss.querySelector('.classes-body .item-list-header .item-name')
				.append(
					createNode('label', 'No FCB on any class.', ['lil-warning', 'lil-box', 'lil-fcb-notification']),
				);
		}
	}

	// LANGUAGES
	const languages = unifyProficiency(actorData.traits.languages);
	if (languages && !shared.isMindless) {
		// 1 (base) + 1 per Int Mod + 1 per linguistics rank
		const minLang = actorData.abilities.int.mod + (actorData.skills.lin.rank ?? 0) + 1;

		if (languages.length < minLang) {
			const list = shared.tabs.attributes?.tab
				?.querySelector('[data-options="languages"]')
				?.closest('.form-group')
				?.querySelector('.traits-list');

			if (list) {
				list.append(constructWarning(i18n.get('LittleHelper.Warning.TooFewLanguages', { granted: minLang, missing: minLang - languages.length })));
				list.classList.add('lil-warning');
				list.closest('.traits .form-group')?.classList.add('lil-warning');
			}
		}
	}

	// PROFICIENCIES
	const armorProficiencies = unifyProficiency(actorData.traits.armorProf);
	if (armorProficiencies.length == 0) {
		const aEl = shared.tabs.attributes?.tab
			?.querySelector('[for="system.traits.armorProf"]')
			?.closest('form-group')
			?.querySelector('.traits-list');

		if (aEl) {
			aEl.append(constructWarning(i18n.get('LittleHelper.Warning.NoArmorProf')));
			aEl.classList.add('lil-warning');
			aEl.closest('.traits .form-group')?.classList.add('lil-warning');
		}
	}
	const weaponProficiencies = unifyProficiency(actorData.traits.weaponProf);
	if (weaponProficiencies.length == 0) {
		const wEl = shared.tabs.attributes?.tab
			?.querySelector('[for="system.traits.weaponProf"]')
			?.closest('form-group')
			?.querySelector('.traits-list');

		if (wEl) {
			wEl.append(constructWarning(i18n.get('LittleHelper.Warning.NoWeaponProf')));
			wEl.classList.add('lil-warning');
			wEl.closest('.traits .form-group')?.classList.add('lil-warning');
		}
	}
}

function enable() {
	Ancillary.register('actor', missingBitsWarnings);
}

function disable() {
	Ancillary.unregister('actor', missingBitsWarnings);
}

new Feature({ setting: 'missing-bits', category: 'actor-sheet', enable, disable, stage: 'ready' });
