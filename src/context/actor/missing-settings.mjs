/**
 * Warn about missing ability score links.
 */

import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { addCoreTooltip } from '@root/common.mjs';

const checkForEmpty = [
	'attributes.hpAbility',
	'attributes.init.ability',
	'attributes.cmbAbility',
	'attributes.attack.meleeAbility', // non-critical
	'attributes.attack.rangedAbility', // non-critical
	'attributes.ac.normal.ability',
	'attributes.ac.touch.ability',
	'attributes.cmd.strAbility',
	'attributes.cmd.dexAbility',
	'attributes.savingThrows.fort.ability',
	'attributes.savingThrows.ref.ability',
	'attributes.savingThrows.will.ability',
];

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function warnMissingSettings(sheet, [html], options, shared) {
	const actor = sheet.actor,
		data = shared.systemData;
	const settings = html.querySelector('.sheet-navigation.tabs [data-tab="settings"]');
	/** @type {Element} */
	const tab = shared.tabs?.settings?.tab;
	if (!tab) return;
	let badAbls = false;

	const colors = CFG.console.colors;

	for (const attr of checkForEmpty) {
		const v = foundry.utils.getProperty(data, attr);
		if (!(v in data.abilities)) {
			// Not found
			console.log(`[%cERROR%c] %c${attr}`, colors.error, colors.unset, colors.label, { ability: v }, 'not found');
			const attrEl = tab.querySelector(`[name="system.${attr}"],[data-name="system.${attr}"]`);
			attrEl.classList.add('lil-select-value');
			badAbls = true;
		}
	}

	if (badAbls && !!settings) {
		settings.classList.add('lil-angry-warning-glow');
	}
}

function enable() {
	Ancillary.register('actor', warnMissingSettings);
}

function disable() {
	Ancillary.unregister('actor', warnMissingSettings);
}

new Feature({ setting: 'missing-ability-links', label: 'Unlinked Ability Scores', hint: 'Add warnings about missing ability score links.', category: 'actor-sheet', enable, disable, stage: 'ready' });
