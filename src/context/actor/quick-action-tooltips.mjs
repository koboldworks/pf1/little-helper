import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode, signNum } from '@root/common.mjs';
import { setStaticToolTipListener } from '@core/tooltip.mjs';
import { i18n } from '@root/utility/i18n.mjs';

function rangeLabel(value, units) {
	let label = pf1.utils.convertDistance(0)[1];
	label = label === 'ft' ? pf1.config.measureUnitsShort.ft : pf1.config.measureUnitsShort.m;
	label = `${value} ${label}`;

	switch (units) {
		case 'personal':
			return pf1.config.distanceUnits.personal;
		case 'melee':
			return pf1.config.distanceUnits.melee + ` (${label})`;
		case 'touch':
			return pf1.config.distanceUnits.touch + ` (${label})`;
		case 'reach':
			return pf1.config.distanceUnits.reach + ` (${label})`;
		default: {
			return label;
		}
	}
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {ShareData} shared
 */
function addQuickActionTooltips(sheet, [html], options, shared) {
	/** @type {Element} */
	const tab = shared.tabs.summary?.tab;

	/** @type {ActorPF} */
	const actor = shared.actor;

	const acts = tab?.querySelector('.quick-actions');
	if (!acts) return;

	/**
	 *
	 * @param {ItemAction} act
	 * @param {*} actData
	 * @param {ItemPF} item
	 * @param {*} ul
	 */
	const fillActionDetails = (act, actData, item, ul) => {
		// Damage
		const damage = Handlebars.helpers.actionDamage(act, shared.rollData, { hash: { combine: true } });
		if (damage) {
			const damagel = createNode('div', null, ['damage', 'value', 'nowrap']);
			damagel.append(damage);
			const label = act.isHealing ? 'PF1.Healing' : 'PF1.Damage';
			ul.append(createNode('li', null, ['damage'], { children: [createNode('label', i18n.get(label)), damagel] }));
		}

		// Attacks
		if (act.hasAttack) {
			const attacks = act.getAttacks({ full: true, resolve: true, bonuses: true, conditionals: true });
			if (attacks?.length) {
				const attacksl = createNode('div', null, ['attacks', 'value', 'nowrap']);
				attacksl.append(attacks.map(a => signNum(a.bonus)).join('/'));
				ul.append(createNode('li', null, ['attacks'], { children: [createNode('label', 'Attacks'), attacksl] }));
			}
		}

		// CHARGES
		if (act.isCharged) {
			const charges = createNode('div', null, ['charges', 'value']);
			const chargeCost = act.getChargeCost();
			charges.append(createNode('span', `${Math.floor(item.charges / chargeCost)}`, ['remaining']));
			const m = item.chargeMax;
			if (m > 0) charges.append(' / ', createNode('span', Math.floor(m / chargeCost), ['max']));
			ul.append(createNode('li', null, ['charges'], { children: [createNode('label', 'Charges'), charges] }));
		}

		if (actData.usesAmmo) {
			const ammoId = item.getFlag('pf1', 'defaultAmmo');
			const ammo = actor.items.get(ammoId);

			if (ammo) {
				const ammoLeft = ammo.system.quantity ?? 0;
				ul.append(
					createNode('li', null, ['ammo'], {
						children: [
							createNode('label', 'Ammo'),
							createNode('span', ammo.name, ['value']),
						],
					}),
					createNode('li', null, ['ammo'], {
						children: [
							createNode('label', ' – Quantity'),
							createNode('span', ammoLeft, ['value']),
						],
					}));
			}
			else {
				ul.append(createNode('li', null, ['ammo'], { children: [createNode('label', 'No Default Ammo', ['warning', 'span-2'])] }));
			}
		}

		const rUnits = actData.range?.units;
		if (act.hasRange && !!rUnits) {
			const r = act.getRange();
			const rl = rangeLabel(r, rUnits);

			ul.append(createNode('li', null, ['range'], {
				children: [
					createNode('label', 'Range'),
					createNode('span', rl, ['value']),
				],
			}));

			const minRange = act.minRange;
			if (minRange > 0) {
				const mrlabel = rangeLabel(minRange, rUnits);
				ul.append(createNode('li', null, ['range', 'min'], {
					children: [
						createNode('label', '– Min'),
						createNode('span', mrlabel, ['value']),
					],
				}));
			}

			const mi = actData.range.maxIncrements ?? 1;
			if (mi > 1 && rUnits === 'ft') {
				const maxRange = act.maxRange;
				const mrlabel = rangeLabel(maxRange, rUnits);

				ul.append(createNode('li', null, ['range', 'max'], {
					children: [
						createNode('label', '– Max'),
						createNode('span', mrlabel, ['value']),
					],
				}));
			}
		}
	};

	/**
	 *
	 * @param {Element} tip
	 * @param {Element} el
	 * @param {ItemPF} item
	 */
	const fillTooltip = async (tip, el, item) => {
		const itemData = item.system;
		const ul = createNode('ul', null, ['details']);

		const act = item.defaultAction;
		if (!act) return; // No action

		// TODO: Remove once PFv10 support is dropped
		const actData = act instanceof foundry.abstract.DataModel ? act : act.data;

		let skip = false;
		if (itemData.actions?.length === 0) {
			skip = true;
			ul.append(createNode('li', 'No Actions', ['warning', 'span-2']));
		}

		if (!skip) fillActionDetails(act, actData, item, ul);

		tip.append(createNode('h2', item.name, ['name']), ul);
	};

	acts.querySelectorAll('[data-item-id]').forEach(el => {
		const tip = createNode('div', null, ['lil-tooltip']);
		/** @type {ItemPF} */
		const item = actor.items.get(el.dataset.itemId);
		if (!item) return;
		el.removeAttribute('title'); // Remove normal tooltip
		el.append(tip);
		el.classList.add('lil-quick-action-tooltip-fix');
		el.querySelector('.icon-label')?.remove(); // Remove default label
		el.addEventListener('pointerenter', _ => fillTooltip(tip, el, item), { passive: true, once: true });
		setStaticToolTipListener(el, tip, { aligned: true, boundedBy: acts });
	});
}

function enable() {
	Ancillary.register('actor', addQuickActionTooltips);
}

function disable() {
	Ancillary.unregister('actor', addQuickActionTooltips);
}

new Feature({ setting: 'actor-quick-action-tooltips', label: 'Quick Action Tooltips', hint: 'Display tooltips for quick actions.', category: 'actor-sheet', enable, disable, stage: 'ready' });
