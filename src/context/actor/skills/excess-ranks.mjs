import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} data
 * @param {SharedData} shared
 */
function checkExcessSkillRanks(sheet, [html], data, shared) {
	const actor = sheet.actor,
		actorData = shared.systemData;

	if (actor.getFlag(CFG.id, 'overrides')?.excess) return;

	const hd = actorData.attributes.hd.total;

	shared.tabs.skills?.tab
		?.querySelectorAll('[data-skill]')
		.forEach(el => {
			const parentSkill = el.dataset.mainSkill,
				skill = el.dataset.skill,
				key = parentSkill ? `${parentSkill}.${skill}` : `${skill}`,
				sk = actor.getSkillInfo(key);

			if (sk.rank > hd) el.classList.add('lil-excess-skill-ranks');
		});
}

function enable() {
	Ancillary.register('actor', checkExcessSkillRanks);
}

function disable() {
	Ancillary.unregister('actor', checkExcessSkillRanks);
}

new Feature({ setting: 'excess-skill-ranks', category: 'actor-sheet', enable, disable, stage: 'ready' });
