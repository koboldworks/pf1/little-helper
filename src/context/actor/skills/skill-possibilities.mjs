// WIP
// Display possible results of your skills

import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} opts
 * @param {SharedData} shared
 */
function skillPossibilityInjector(sheet, [html], opts, shared) {
	const actor = sheet.actor,
		actorData = shared.systemData;

	const tab = shared.tabs.skills?.tab;
	tab?.querySelectorAll('li.skill').each(function () {
		const skill = this.dataset.skill;
		const ski = actor.getSkillInfo(skill);
		const mod = ski.bonus;
		switch (skill) {
			case 'acr': {
				const speedMod = Math.floor((actorData.attributes.speed.land.total - 30) / 10) * 4,
					dMin = 1 + mod + speedMod,
					dMax = 20 + mod + speedMod,
					hMin = Math.floor((1 + mod) / 4),
					hMax = Math.floor((20 + mod) / 4);

				const s = createNode('div', null, ['flexrow', 'lil-skill-possibility']);
				s.append(
					createNode('span', 'Distance:<br/>Height:<br/>', [], { isHTML: true }),
					createNode('span', `${dMin} – ${dMax}<br/>${hMin} – ${hMax}`, [], { isHTML: true }),
				);

				const name = this.querySelector('.skill-name');

				let spacer = name.querySelector('.lil-spacer');
				if (!spacer) {
					spacer = createNode('div', null, ['lil-spacer']);
					name.append(spacer);
				}

				spacer.after(s);
				break;
			}
		}
	});
}

Ancillary.register('actor', skillPossibilityInjector);
