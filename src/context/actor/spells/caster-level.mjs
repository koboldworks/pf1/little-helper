// Display current CL at spellbook name.
// OBSOLETE with PF1 v11

import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} _opts
 * @param {SharedData} shared
 */
function casterLevelInjection(sheet, [html], _opts, shared) {
	const spellbooks = html.querySelector('.tabs.spellbooks[data-group="spellbooks"]');
	if (!spellbooks) return;

	const tabs = spellbooks.querySelectorAll('nav.tabs.spellbooks [data-tab]');
	if (!tabs) return;

	// const rd = shared.rollData;

	const actorData = shared.systemData;
	const books = actorData.attributes.spells.spellbooks;

	for (const tab of tabs) {
		const bookId = tab.dataset.tab;
		// const cl = rd.spells[book].cl.total;

		const book = books[bookId];
		if (!book) return void console.warn('No book found for tab:', bookId);

		const cl = book.cl.total;
		tab.append(` [${cl}]`);

		// const conc = books[book].concentration.total;
		// jq.find('.spellcasting-concentration label').append($('<span/>').text(` +${concBonus}`));
		// CL + Ability + Modifiers
	}
}

function enable() {
	Ancillary.register('actor', casterLevelInjection);
}

function disable() {
	Ancillary.unregister('actor', casterLevelInjection);
}

new Feature({ setting: 'display-caster-level', label: 'Display Caster Level', hint: 'Display effective caster level in spellbook headers.', category: 'actor-sheet', enable, disable, stage: 'ready', conflict: { system: '11.0' } });
