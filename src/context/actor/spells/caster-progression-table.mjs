import { CFG } from '@root/config.mjs';
import { createNode } from '@root/common.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { FAIcons } from '@core/icons.mjs';
import { hookCoreTooltip } from '@core/tooltip.mjs';
import { i18n } from '@root/utility/i18n.mjs';

function fillLevels(list) {
	list.forEach(l => {
		const max = list[list.length - 1].length,
			ol = l.length;
		if (l.length < max) {
			l.length = list[list.length - 1].length;
			l.fill(null, ol, max);
		}
	});
}

function processCastingLevels(list) {
	return list.map((cl, i0) => {
		return {
			level: i0 + 1,
			slots: cl.map((sl, i1) => {
				return {
					level: i1,
					slots: sl,
					active: sl >= 0,
					infinite: Number.POSITIVE_INFINITY === sl,
				};
			}),
		};
	});
}

class CasterProgressionTable extends DocumentSheet {
	/** @type {Item|ItemClassPF} */
	casterClass;

	/** @type {string} */
	bookId;

	constructor(actor, bookId) {
		super(actor, { id: `${actor.uuid}-${bookId}-caster-progression` });
		this.bookId = bookId;
		const actorData = actor.system;

		this.book = actorData.attributes.spells.spellbooks[this.bookId];
		if (this.book.class && this.book.class !== '_hd')
			this.casterClass = actor.itemTypes.class.find(i => i.system.tag == this.book.class);
	}

	get title() {
		const bookName = this.book.name || this.casterClass?.name || i18n.get(`PF1.SpellBook${this.bookId.capitalize()}`);
		return i18n.get('LittleHelper.CasterProgressionTitle', { actor: this.actor.name, book: bookName });
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			// resizable: true,
			classes: [...options.classes, 'lil-caster-progression'],
			// width: 560,
			// height: 700,
			width: 'auto',
			submitOnClose: false,
			submitOnChange: false,
			sheetConfig: false,
			closeOnSubmit: true,
		};
	}

	get template() {
		return `modules/${CFG.id}/template/dialog/caster-progression.hbs`;
	}

	/** @type {Actor} */
	get actor() {
		return this.document;
	}

	getData() {
		const actor = this.actor,
			actorData = actor.system,
			book = actorData.attributes.spells.spellbooks[this.bookId],
			type = book.casterType,
			prepMode = book.spellPreparationMode,
			config = pf1.config.casterProgression,
			casts = config.castsPerDay[prepMode][type],
			prep = config.spellsPreparedPerDay[prepMode][type];

		fillLevels(casts);
		fillLevels(prep);

		const actualCasts = processCastingLevels(casts);
		const actualPrep = processCastingLevels(prep);

		// console.log({ book });

		/** @type {Item} */
		const casterClass = this.casterClass,
			casterClassData = casterClass?.system;

		// Get class level
		let classLevel = casterClassData?.level || actorData.attributes?.hd?.total || 0;
		const rollData = actor.getRollData();
		const bonusLevelFormula = book.cl?.autoSpellLevelCalculationFormula || '0';
		classLevel += RollPF.safeRollSync(bonusLevelFormula, rollData).total;

		return {
			book,
			type,
			prepMode,
			config,
			casts: actualCasts,
			prep: actualPrep,
			cl: classLevel,
			cantrips: true,
			isSpontaneous: book.spontaneous,
			castsLevels: Array.fromRange(casts[casts.length - 1].length),
			prepLevels: Array.fromRange(casts[prep.length - 1].length),
			maxLevel: casts[prep.length - 1].length,
			icons: FAIcons,
		};
	}
}

/**
 * @param {Actor} actor
 * @param {string} bookId
 */
function openProgressionSheet(actor, bookId) {
	const uid = `${actor.uuid}-${bookId}-caster-progression`;

	const window = Object.values(ui.windows).find(app => app.id == uid) ?? new CasterProgressionTable(actor, bookId);

	window?.render(true, { focus: true });
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {ShareData} shared
 */
function injectCasterProgressionTable(sheet, [html], options, shared) {
	const books = shared.tabs.spellbook;
	const tab = books?.tab;
	if (!tab) return;

	const actor = sheet.actor;

	books.tabs?.forEach(el => {
		const bookId = el.dataset.tab,
			r = el.querySelector(`[name="system.attributes.spells.spellbooks.${bookId}.spellPreparationMode"]`),
			rs = r?.closest('.radios');
		if (!rs) return;

		const b = createNode('i', null, ['fas', 'fa-table', 'lil-progression-table-button']);
		hookCoreTooltip(b, { fn: () => 'Display Progression Table' });

		b.addEventListener('click', _ => openProgressionSheet(actor, bookId));
		rs.prepend(b);
	});
}

function enable() {
	Ancillary.register('actor', injectCasterProgressionTable);
}

function disable() {
	Ancillary.unregister('actor', injectCasterProgressionTable);
}

new Feature({ setting: 'caster-progression-table', label: 'Caster Progression Table', hint: 'Dialog for displaying currently configured caster progression. Accessed near spellbook progression configuration.', category: 'actor-sheet', enable, disable, stage: 'ready' });
