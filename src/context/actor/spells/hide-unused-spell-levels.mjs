/**
 * Hide unused spell levels
 *
 * OBSOLETED by PF1 v11
 */

import { createNode } from '@root/common.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { SharedData } from '@core/shared-data.mjs';

/**
 * @param {Element} uf
 * @param {Actor} actor
 * @param {Element} book
 * @param {boolean} filtered
 */
const toggleUnused = (uf, actor, book, filtered = false) => {
	const b = uf.firstChild;
	b.classList.toggle('fa-folder-open', !filtered);
	b.classList.toggle('fa-folder', filtered);
	uf.classList.toggle('active', filtered);

	book.querySelectorAll('.item-list[data-level]').forEach(level => {
		const hasSpells = level.querySelector('li[data-item-id]');
		level.classList.toggle('lil-hidden-spell-level', !hasSpells && filtered);
	});
};

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function hideUnusedSpellLevels(sheet, [html], options, shared) {
	if (!shared.coreSheet) return;

	sheet.__littleHelper.unusedLevels ??= {};
	const afilters = sheet.__littleHelper.unusedLevels;
	const books = shared.items.byType.spell;

	const actor = sheet.actor;

	/** @type {Element[]|undefined} */
	const bookTabs = shared.tabs.spellbook?.tabs;
	bookTabs?.forEach(
		book => {
			const bookId = book.dataset.tab;
			const bookData = books?._book?.[bookId];
			const spellCount = bookData?.all?.length ?? 0;
			if (!bookData || spellCount == 0) return;

			// Book levels are hidden already with automatic spell levels
			if (bookData.book?.autoSpellLevelCalculation === true) return;

			// Initialize filter state.
			afilters[bookId] ??= spellCount != 0;

			const filters = book.querySelector('.filter-list');
			const uf = createNode('li', '', ['lil-unused-filter']);
			uf.dataset.tooltip = 'Unused Levels';

			uf.append(createNode('i', null, ['fas', 'fa-folder']));
			filters.append(uf);

			uf.addEventListener('click', ev => {
				ev.preventDefault();
				ev.stopPropagation();

				if (spellCount > 0) {
					afilters[bookId] = !afilters[bookId];
					toggleUnused(uf, actor, book, afilters[bookId]);
				}
				else {
					ui.notifications.warn('Unused spell levels filter toggle ignored with no spells');
				}
			});

			// Apply filter
			if (spellCount > 0)
				toggleUnused(uf, actor, book, afilters[bookId]);

			if (spellCount != 0) {
				// Hide filter categories
				filters.querySelectorAll('li[data-filter]').forEach(li => {
					const re = /^type-(?<level>\d+)$/.exec(li.dataset.filter);
					if (re) {
						const level = Number(re.groups.level);
						const lvlSpellCount = books._book[bookId]?.[level]?.length ?? 0;
						if (lvlSpellCount == 0) {
							li.style.display = 'none';
						}
					}
				});
			}
		},
	);
}

function enable() {
	Ancillary.register('actor', hideUnusedSpellLevels, { priority: -10 });
}

function disable() {
	Ancillary.unregister('actor', hideUnusedSpellLevels);
}

new Feature({ setting: 'hide-unused-spell-levels', label: 'Hide Unused Spell Levels', hint: 'Adds filter toggle in spellbooks that hides spell levels with no spells, initially hides them to reduce clutter. Also hides filter categories that have zero spells. Disabled if spellbook has zero spells.', category: 'actor-sheet', enable, disable, stage: 'ready', conflict: { system: '11.0' } });
