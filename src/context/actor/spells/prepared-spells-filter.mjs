/*
 * Toggle visibility of prepared or not spells.
 *
 * OBSOLETED BY PF1 v11
 */

import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';

const buttonSelector = 'lil-prepared-filter';

/**
 * @param {Element} button
 * @param {Actor} actor
 * @param {Element} book
 * @param {boolean} filtered
 */
function togglePrepared(button, actor, book, filtered = false) {
	setPreparedTooltip(button, filtered);

	button.classList.toggle('active', filtered);

	button.firstChild.classList.toggle('fa-feather-alt', filtered);
	button.firstChild.classList.toggle('fa-feather', !filtered);

	book.querySelectorAll('li.item[data-item-id]')
		?.forEach(el => {
			const item = actor.items.get(el.dataset.itemId),
				itemData = item.system,
				prep = itemData.preparation;

			const display = !filtered || prep.value > 0 || itemData.atWill || false;
			el.classList.toggle('lil-unprepared-spell-filter', !display);
		});
}

function setPreparedTooltip(el, filtered = false) {
	el.dataset.tooltip = !filtered ? 'LittleHelper.Info.SpellsShowPrepared' : 'LittleHelper.Info.SpellsShowAll';
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} _opts
 * @param {SharedData} shared
 */
function preparedSpellsFilter(sheet, [html], _opts, shared) {
	if (!shared.sysSheet) return;

	const tabs = shared.tabs?.spellbook?.tabs;
	if (!tabs) return;
	const actor = sheet.actor;

	// const books = shared.items.byType.spell;

	sheet.__littleHelper.preparedFilters ??= {};
	const afilters = sheet.__littleHelper.preparedFilters;

	for (const bookTab of tabs) {
		const filters = bookTab.querySelector('.filter-list');
		if (!filters) continue;
		const bookId = bookTab.dataset.tab;

		afilters[bookId] ??= false;

		const pf = createNode('li', '', [buttonSelector]);
		pf.append(createNode('i', null, ['fas', 'fa-feather']));

		filters.append(pf);
		pf.addEventListener('click', ev => {
			ev.preventDefault();
			ev.stopPropagation();

			afilters[bookId] = !afilters[bookId];
			togglePrepared(pf, actor, bookTab, afilters[bookId]);

			game.tooltip.activate(pf);
		});

		const afilter = afilters[bookId];
		if (afilter !== undefined)
			togglePrepared(pf, actor, bookTab, afilter);
	}
}

function enable() {
	Ancillary.register('actor', preparedSpellsFilter, { priority: 10 });
}

function disable() {
	Ancillary.unregister('actor', preparedSpellsFilter);
}

new Feature({ setting: 'prepared-spells-filter', label: 'Prepared Spells Filter', hint: 'Adds little feather to spell level tabs to filter prepared spells.', category: 'actor-sheet', enable, disable, stage: 'ready', conflict: { system: '11.0' } });
