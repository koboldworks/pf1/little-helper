import { Ancillary } from '@root/ancillary.mjs';
import { createNode, addCoreTooltip } from '@root/common.mjs';

// Display DC per spell level
function spellLevelDCInjector(sheet, _jq, _opts, shared) {
	const rd = shared.rollData;

	for (const tab of shared.tabs.spellbook?.tabs ?? []) {
		const bookId = tab.dataset.tab;
		const bookData = rd.spells[bookId];
		if (!bookData) continue;
		rd.ablMod = rd.abilities[bookData.ability]?.mod ?? 0;
		rd.cl = bookData.cl.total;

		for (const node of tab.querySelectorAll('.spellbook-header')) {
			rd.sl = Number(node.dataset.level);
			if (!Number.isFinite(rd.sl)) continue;
			const roll = RollPF.safeRollSync(bookData.baseDCFormula, rd);
			delete rd.sl;

			const holder = node.querySelector('.lil-spell-details');
			const b = createNode('label', null, ['lil-box', 'lil-spell-dc']);
			let tt;
			if (roll.err) {
				b.textContent = 'DC: Error';
				tt = roll.err.message;
			}
			else {
				b.textContent = `DC: ${roll.total}`;
				tt = roll.formula;
			}

			addCoreTooltip(b, tt);

			holder?.append(b);
		}

		delete rd.cl;
		delete rd.ablMod;
	}
}

Ancillary.register('actor', spellLevelDCInjector);
