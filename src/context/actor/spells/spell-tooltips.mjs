import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';
import { setStaticToolTipListener } from '@core/tooltip.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {ShareData} shared
 */
function addSpellTooltips(sheet, [html], options, shared) {
	const actor = shared.actor;
	/** @type {HTMLElement[]} */
	const bookTabs = shared.tabs.spellbook?.tabs;

	const listFormat = (str) => new Intl.ListFormat(game.i18n.lang, { style: 'short', type: 'unit' }).format(str);

	/**
	 * @param {Element} tip
	 * @param {Element} el
	 * @param {pf1.documents.item.ItemSpellPF} spell
	 */
	const fillTooltip = async (tip, el, spell) => {
		const itemData = spell.system,
			act = spell.defaultAction;

		const ul = createNode('ul', null, ['details']);
		tip.append(createNode('h2', spell.name, ['name']), ul);
		if (!act) {
			ul.append(createNode('li', 'No Actions', ['warning', 'span-2']));
			return;
		}

		const rollData = act.getRollData();

		// Actual details

		// School
		const schoolParts = [createNode('span', pf1.config.spellSchools[itemData.school], ['school', 'value', 'text'])];
		if (itemData.subschool) {
			let subschool;
			if (typeof itemData.subschool === 'string' && itemData.subschool)
				subschool = itemData.subschool ? [itemData.subschool] : [];
			else
				subschool = listFormat(itemData.subschool.names ?? []);

			if (subschool?.length)
				schoolParts.push(' (', createNode('span', subschool, ['subschool', 'value', 'text']), ')');
		}
		if (itemData.descriptors) {
			const descs = itemData.descriptors.names;
			if (descs.length)
				schoolParts.push(' [', createNode('span', listFormat(descs), ['descriptors', 'value', 'text']), ']');
		}
		ul.append(
			createNode('li', null, ['school'], {
				children: [
					createNode('label', 'School'),
					createNode('span', null, ['school', 'composite'], { children: schoolParts }),
				],
			}),
		);

		// Material
		if (itemData.components.material) {
			const mats = itemData.materials;
			const materialDetails = createNode('span', null, ['material', 'composite']);
			materialDetails.append(createNode('span', mats.value, ['detail', 'value', 'text']));
			if (mats.gpValue > 0) materialDetails.append(' [', createNode('span', `${mats.gpValue} gp`, ['cost', 'value', 'number']), ']');
			ul.append(
				createNode('li', null, ['material'], {
					children: [
						createNode('label', 'Material'),
						materialDetails,
					],
				}),
			);
		}

		// Focus
		if (itemData.components.focus) {
			const focus = itemData.materials.focus;
			ul.append(
				createNode('li', null, ['focus'], {
					children: [
						createNode('label', 'Focus'),
						createNode('span', focus, ['value', 'text', 'focus']),
					],
				}),
			);
		}

		// Range
		const rUnits = act.range.units; // Hack for 0.82.x bug in .hasRange
		if (act.hasRange && !!rUnits) {
			const rangeDetails = createNode('span', null, ['range', 'composite']);
			const isBasic = ['ft', 'km', 'm'].includes(rUnits);
			const isIndistinct = ['personal', 'seeText', 'special', 'unlimited', 'none'].includes(rUnits);
			if (!isBasic) rangeDetails.append(createNode('span', pf1.config.distanceUnits[rUnits], ['units', 'text']));
			if (!isIndistinct) {
				const range = act.getRange();
				const [_, unit] = pf1.utils.convertDistance(0, isBasic ? rUnits : undefined);
				const uLabel = pf1.config.measureUnitsShort[unit] ?? pf1.config.distanceUnits[unit];
				const rl = `${range} ${uLabel}`;
				const rd = createNode('span', rl, ['measured', 'value', 'number']);
				if (!isBasic) rangeDetails.append(' (', rd, ')');
				else rangeDetails.append(rd);
			}

			ul.append(createNode('li', null, ['range'], {
				children: [
					createNode('label', 'Range'),
					rangeDetails,
				],
			}));

			const mi = act.range.maxIncrements ?? 1;
			if (mi > 1) {
				ul.append(createNode('li', null, ['range'], {
					children: [
						createNode('label', 'Max Increments'),
						createNode('span', `${mi}`, ['value']),
					],
				}));
			}
		}

		// Area
		const area = act.spellArea;
		if (area?.length > 0) {
			ul.append(
				createNode('li', null, ['area'], {
					children: [
						createNode('label', 'Area'),
						createNode('span', area, ['value', 'text', 'area']),
					],
				}),
			);
		}

		// Targets
		const target = act.target?.value;
		if (target) {
			ul.append(
				createNode('li', null, ['target'], {
					children: [
						createNode('label', 'Target'),
						createNode('span', target, ['value', 'text', 'target']),
					],
				}),
			);
		}

		// Duration
		if (act.duration?.value) {
			let d = act.duration.value, enriched = false;
			if (/\[\[.*]]/.test(d)) {
				d = TextEditor.enrichHTML(d, { rolls: true, rollData, async: false });
				enriched = true;
			}
			ul.append(
				createNode('li', null, ['duration'], {
					children: [
						createNode('label', 'Duration'),
						createNode('span', d, ['value', 'text'], { isHTML: enriched }),
					],
				}),
			);
		}

		// Save
		if (act.hasSave) {
			const dc = act.getDC(rollData);
			const type = act.save.type;
			// const desc = actData.save.description;

			const saveDetails = createNode('span', null, ['save', 'composite']);
			saveDetails.append(
				'DC ',
				createNode('span', dc, ['value', 'number']),
				' ',
				pf1.config.savingThrows[type],
			);

			ul.append(
				createNode('li', null, ['save'], {
					children: [
						createNode('label', i18n.get('PF1.Save')),
						saveDetails,
					],
				}),
			);
		}
	};

	bookTabs?.forEach(bookTab => {
		const bookId = bookTab.dataset.tab;
		bookTab.querySelectorAll('.item[data-item-id]')
			.forEach(spellEl => {
				const tip = createNode('div', null, ['lil-tooltip']);
				const item = actor.items.get(spellEl.dataset.itemId);
				if (!item) return; // Shouldn't happen

				// const actEl = spellEl.querySelector('.item-actions');

				spellEl.append(tip);
				spellEl.addEventListener('pointerenter', () => fillTooltip(tip, spellEl, item), { passive: true, once: true });
				setStaticToolTipListener(spellEl, tip, { aligned: false, boundedBy: bookTab });
				/*
				// Move tooltip to right of actions
				spellEl.addEventListener('pointerenter', () => {
					const r = actEl?.getBoundingClientRect();
					spellEl.style.setProperty('--act-l', r?.left ?? (spellEl.style.getProperty('--p-left') + 24));
				});
				*/
				// Move tooltip with mouse
				spellEl.addEventListener('pointermove', ev => {
					spellEl.style.setProperty('--mx', ev.clientX);
					const tp = tip.getBoundingClientRect();
					const { left, width } = spellEl.getBoundingClientRect();
					// Toggle alignment. Condition to avoid alignment going wild near the threshold.
					// TODO: Make the positio instead smooth instead of flippy by maintaining offset based on left and right distance
					// - Mouse X to width percentage offset
					// - Width - tip width max left offset
					// - Adjust left offset percentage of max left offset equal to mouse offset precentage
					if (tip.classList.contains('right-aligned'))
						tip.classList.toggle('right-aligned', (tp.right + 10) > (left + width / 2));
					else
						tip.classList.toggle('right-aligned', tp.left > (left + width / 2));
				}, { passive: true });
			});
	});
}

function enable() {
	Ancillary.register('actor', addSpellTooltips);
}

function disable() {
	Ancillary.unregister('actor', addSpellTooltips);
}

new Feature({ setting: 'actor-spell-tooltips', label: 'Spell Tooltips', hint: 'Display tooltips describing some spell details.', category: 'actor-sheet', enable, disable, stage: 'ready' });
