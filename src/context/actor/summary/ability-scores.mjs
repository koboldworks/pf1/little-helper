/**
 * Display ability scores on summary tab.
 */
import { createNode, signNum, maxPrecision } from '@root/common.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { i18n } from '@root/utility/i18n.mjs';

const ablKeys = ['str', 'dex', 'con', 'int', 'wis', 'cha'];

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function summaryAbilityScores(sheet, [html], options, shared) {
	const actor = sheet.actor,
		actorData = shared.systemData;

	const summary = shared.tabs.summary;
	const attr = summary?.attributes;
	if (!attr) return;

	const abls = foundry.utils.deepClone(actorData.abilities);

	const ablsSorted = Object.entries(abls).reduce((arr, [key, abl]) => {
		const data = abl;
		data.key = key;
		arr.push(data);
		return arr;
	}, []).sort((a, b) => b.total - a.total);

	ablsSorted[0].grade = 5;
	const best = ablsSorted[0],
		worst = ablsSorted[5];

	ablsSorted.forEach(abl => {
		abl.percentile = maxPrecision((abl.total - worst.total) / (best.total - worst.total), 2);
		abl.grade = Math.round(abl.percentile * 5);
	});

	// console.log(ablsSorted);

	const ablDiv = createNode('div', null, ['lil-ability-scores']);
	const abilities = {};
	for (const ablKey of ablKeys) {
		const abl = abls[ablKey],
			aligner = createNode('div', null, ['ability-score-holder']),
			ablD = createNode('h3', null, ['ability-score']),
			l = createNode('label', i18n.get(`PF1.AbilityShort${ablKey.capitalize()}`)),
			t = createNode('span', abl.total, ['total']),
			m = createNode('span', `(${signNum(abl.mod)})`, ['modifier']);

		const isNull = abl.total === null;
		if (isNull) {
			t.textContent = '–';
			m.textContent = '';
			aligner.classList.add('null');
		}

		abilities[ablKey] = { d: ablD, l, t, m };
		ablD.append(l, t, m);
		aligner.append(ablD);
		ablDiv.append(aligner);

		ablD.dataset.ability = ablKey;
		ablD.dataset.grade = abl.grade;

		if (abl.grade == 5) {
			const icon = createNode('i', null, ['far', 'fa-star', 'lil-icon']);
			ablD.prepend(icon);
		}

		if (!isNull) ablD.addEventListener('click', function rollAbilityCheck(ev) {
			ev.preventDefault();
			ev.stopPropagation();
			actor.rollAbilityTest(ablKey);
		});
	}

	attr.before(ablDiv);

	// Add a fix for weird layout
	attr.querySelector('.maneuverability-details')
		?.classList.add('lil-abl-score-layout-fix');
}

function enable() {
	Ancillary.register('actor', summaryAbilityScores);
}

function disable() {
	Ancillary.unregister('actor', summaryAbilityScores);
}

new Feature({ setting: 'summary-ability-scores', label: 'Ability Scores in Summary Tab', hint: 'Displays ability scores on the summary tab just above health.', category: 'actor-sheet', enable, disable, stage: 'ready' });
