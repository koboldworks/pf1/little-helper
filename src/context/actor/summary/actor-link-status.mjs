/**
 * Clarify actor linking status.
 */

import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode, addCoreTooltip } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

const variants = {
	linked: {
		label: 'LittleHelper.LinkState.Linked.TitleTag',
		sub: 'LittleHelper.LinkState.Linked.Token',
		proto: 'LittleHelper.LinkState.Linked.Prototype',
		sproto: 'LittleHelper.LinkState.Linked.PrototypeShort',
		css: ['lil-token-linked', 'linked'],
		icon: ['fas', 'fa-link', 'fa-fw'],
	},
	unlinked: {
		label: 'LittleHelper.LinkState.Unlinked.TitleTag',
		sub: 'LittleHelper.LinkState.Unlinked.Token',
		proto: 'LittleHelper.LinkState.Unlinked.Prototype',
		sproto: 'LittleHelper.LinkState.Unlinked.PrototypeShort',
		css: ['lil-token-unlinked', 'unlinked'],
		icon: ['fas', 'fa-unlink', 'fa-fw'],
	},
};

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} _opts
 * @param {SharedData} shared
 * @returns
 */
function actorLinkStatus(sheet, [html], _opts, shared) {
	const actor = sheet.actor,
		token = sheet.token ?? actor.token,
		isToken = !!token,
		isLinked = isToken ? token.isLinked : actor.prototypeToken.actorLink;

	if (actor.pack) return; // Inside compendium, ignore

	/** @type {Element} */
	const tab = shared.tabs.summary?.tab;
	if (!tab) return;

	const baseActor = actor.token?.baseActor ?? actor;
	const isProtoLinked = baseActor.prototypeToken.actorLink;

	const vd = variants[isLinked ? 'linked' : 'unlinked'];

	if (game.user.isGM || !isLinked) {
		const tsheet = html.closest('.app.sheet.actor');
		const title = tsheet?.querySelector('.window-title');
		if (!title) return;
		const fc = title.firstChild; // Text node
		if (fc) {
			let extraLabel;
			if (isLinked) {
				if (isProtoLinked) extraLabel = 'full-link';
				else extraLabel = 'bad-link';
			}
			else {
				if (isProtoLinked) extraLabel = 'broken';
				else extraLabel = 'full-unlink';
			}

			// Delete already existing status
			fc.parentElement?.querySelector('.lil-link-status')?.remove();

			const linkLabel = createNode('i', null, ['lil-link-status', ...vd.icon, ...vd.css, extraLabel]);
			addCoreTooltip(linkLabel, isToken ? vd.sub : vd.proto, 'DOWN');
			fc.after(linkLabel);
		}
	}
}

function enable() {
	Ancillary.register('actor', actorLinkStatus);
}

function disable() {
	Ancillary.unregister('actor', actorLinkStatus);
}

new Feature({ setting: 'actor-unmistaken-linking', label: 'Display Linking Status', hint: 'Display if an actor is linked or not.', category: 'actor-sheet', enable, disable, stage: 'ready' });
