import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { evaluateDeathPoint } from '@root/helpers/wounds.mjs';
import { clampNum } from '@root/common.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param _opts
 * @param shared
 */
function considerDeath(sheet, [html], _opts, shared) {
	const tab = shared.tabs.summary?.tab;

	// As per 0.78.15 sheet design
	const hpEl = tab?.querySelector('[name="system.attributes.hp.value"]')?.parentElement;
	if (!hpEl) return;

	const actor = shared.actor,
		actorData = shared.systemData,
		hp = actorData.attributes.hp,
		eHP = hp.value + hp.temp;

	// https://aonprd.com/Rules.aspx?Name=Dying%20(Negative%20Hit%20Points)&Category=Injury%20and%20Death
	// > If your hit point total is negative, but not equal to or greater than your Constitution score, you’re dying.

	const cls = [];

	const hpPercentage = eHP / hp.max,
		woundThreshold = clampNum(4 - Math.ceil(hpPercentage * 4), 0, 3);
	switch (woundThreshold) {
		case 0: cls.push('healthy'); break;
		case 1: cls.push('grazed'); break;
		case 2: cls.push('wounded'); break;
		case 3: cls.push('critical'); break;
	}

	if (eHP <= evaluateDeathPoint(actor))
		cls.push('lil-dead'); // Dead, unaffected by NL
	else if (eHP >= 0) {
		cls.push('lil-alive'); // in relatively good health
		const eHPNL = eHP - hp.nonlethal;
		if (eHPNL >= 0) cls.push('lil-awake');
		if (eHPNL <= 0) {
			if (eHPNL == 0) cls.push('lil-staggered');
			if (eHPNL < 0) cls.push('lil-unconscious');
			if (hp.nonlethal > 0) {
				tab.querySelector('[name="system.attributes.hp.nonlethal"]')
					?.parentElement?.classList.add('lil-staggered');
			}
		}
	}
	else
		cls.push('lil-dying', 'lil-unconscious'); // Dying, unaffected by NL

	hpEl.classList.add(...cls);
}

function enable() {
	Ancillary.register('actor', considerDeath);
}

function disable() {
	Ancillary.unregister('actor', considerDeath);
}

new Feature({ setting: 'death-proximity', label: 'Death Proximity', hint: 'Color and iconize actor sheet health depending on health status.', category: 'actor-sheet', enable, disable, stage: 'ready' });
