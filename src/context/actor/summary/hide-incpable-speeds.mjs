/**
 * Hide missing speeds.
 *
 * OBSOLETE with PF1 v10
 */

import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function hideMissingSpeeds(sheet, [html], options, shared) {
	const actor = sheet.actor,
		actorData = shared.systemData;

	const attr = shared.tabs.summary?.attributes;
	if (!attr) return;

	/** @type {Element} */
	const speed = attr.querySelector('.speed-details');
	if (!speed) return;
	let hidden = 0;
	speed.classList.add('lil-collapsed-speeds');
	speed.querySelectorAll('ul li').forEach(el => {
		['land', 'climb', 'swim', 'burrow', 'fly'].forEach(mode => {
			if (el.classList.contains(mode)) {
				const speed = actorData.attributes.speed[mode].total;
				if (speed == 0) {
					el.classList.add('incapable');
					if (mode !== 'land') el.classList.add('hide');
					hidden++;
				}
			}
		});
	});

	speed.addEventListener('focusin', function onSpeedFocusIn() { speed.classList.add('lil-edit-focus'); }, { passive: true });
	speed.addEventListener('focusout', function onSpeedFocusOut() { speed.classList.remove('lil-edit-focus'); }, { passive: true });

	if (hidden > 0) {
		attr.querySelector('.maneuverability-details')
			?.classList.add('lil-fix-speed-collapse');

		attr.classList.add('lil-attributes-tweak');
	}

	// HACK: Enable animations at a delay
	// Without the delay they still get animated for unknown reasons
	setTimeout(() => speed.classList.add('animated'), 10);
}

function enable() {
	// Ancillary.register('actor', hideMissingSpeeds);
}

function disable() {
	// Ancillary.unregister('actor', hideMissingSpeeds);
}

new Feature({
	setting: 'collapse-incapable-speeds',
	label: 'Collapse Incapable Speeds',
	hint: 'Collapses speeds a character is incapable of. These are still accessible by hovering over the speed section-',
	category: 'actor-sheet',
	enable,
	disable,
	stage: 'ready',
	conflict: { system: '10.0' },
});
