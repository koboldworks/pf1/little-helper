import { CFG } from '@root/config.mjs';
import { createNode, signNum } from '@root/common.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { i18n } from '@root/utility/i18n.mjs';
import { hookCoreTooltip } from '@core/tooltip.mjs';
import { FAIcons } from '@root/core/icons.mjs';

const defaultSkills = ['per', 'dip', 'hea'];

/**
 * @returns {string[]}
 */
const getDefaultSkills = () => game.settings.get(CFG.id, 'quickSkills')?.split(',').filter(id => !!id) ?? [];

const getActorConfig = (actor) => new QuickSkillsModel(actor.getFlag(CFG.id, 'quickSkills') ?? {});

function getActorSkills(actor, options) {
	const skills = [];

	const addSkill = (sklId) => {
		const ski = actor.getSkillInfo(sklId) ?? {};
		if (ski.mod > options.maxMod) options.maxMod = ski.mod;
		const [parentId, subId] = ski.id.split('.');
		ski.parentId = parentId;
		ski.subId = subId;
		skills.push({
			...ski,
		});
	};

	for (const skillId of actor.allSkills ?? [])
		addSkill(skillId);

	skills.sort((a, b) => a.fullName.localeCompare(b.fullName, undefined, { sensitivity: 'base' }));

	const rv = {};
	skills.forEach(s => rv[s.id] = s);

	return rv;
}

class SkillEditor extends FormApplication {
	constructor(actor) {
		super();
		this.user = !!actor;
		actor ??= new Actor.implementation({ type: 'character', name: 'Quick Skills Temp' });
		this.actor = actor;

		this.skillsConfig = getActorConfig(actor);
	}

	get title() {
		const actor = this.actor;
		if (actor.id) return i18n.get('LittleHelper.QuickSkills.ActorTitle', { name: actor.name, id: actor.id });
		else return i18n.get('LittleHelper.QuickSkills.DefaultTitle');
	}

	get id() {
		if (this.actor.id)
			return `${this.actor.uuid}-quick-skills-editor`;
		else
			return 'default-quick-skills-editor';
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			classes: [_default.classes, 'lil-quick-skills-editor'],
			height: 700,
			width: 460,
			resizable: true,
			submitOnChange: false,
			closeOnSubmit: true,
			submitOnClose: false,
		};
	}

	get template() {
		return `modules/${CFG.id}/template/quick-skills-editor.hbs`;
	}

	getData() {
		const actor = this.actor,
			actorData = actor?.system;

		const options = { maxMod: Number.NEGATIVE_INFINITY };
		const skills = getActorSkills(actor, options);

		const qs = this.skillsConfig;
		qs.skills.forEach(skId => {
			const sklData = skills[skId];
			if (sklData) {
				sklData.selected = true;
			}
			else {
				console.warn('Invalid skill ID:', skId);
			}
		});

		getDefaultSkills().forEach(sk => {
			if (qs.skills.length === 0) skills[sk].selected = true;
			skills[sk].default = true;
		});

		const context = {
			actor,
			user: this.user,
			skills,
			icons: FAIcons,
			labels: {},
		};

		if (actor) {
			const hd = actorData.attributes.hd.total,
				maxRank = hd,
				minRank = qs.minRank ?? 1;

			context.threshold = qs.threshold;
			context.thresholdPct = Math.floor(qs.threshold * 100);
			context.useMod = qs.useMod;
			context.maxMod = options.maxMod;
			context.maxRank = maxRank;
			context.minRank = minRank;

			context.labels.maxMod = `<span class='maxmod-value'>${signNum(context.maxMod)}</span>`;
			context.labels.maxRank = `<span class='maxrank-value'>${context.maxRank}</span>`;
		}

		// console.log(data);

		return context;
	}

	/**
	 * @param {Actor} actor
	 */
	async resetSkills() {
		const actor = this.actor;

		if (actor.id) {
			await Dialog.confirm({
				title: i18n.get('LittleHelper.QuickSkills.Reset.Title'),
				content: '<p>' + i18n.get('LittleHelper.QuickSkills.Reset.Info'),
				yes: () => {
					actor.unsetFlag(CFG.id, 'quickSkills');
					this.close();
				},
				no: () => null,
			});

			this.skillsConfig.skills = getDefaultSkills();
		}
		else {
			this.skillsConfig.skills = defaultSkills;
			this.render();
			ui.notifications.warn(i18n.get('LittleHelper.QuickSkills.Reset.Soft'));
		}
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		const actor = this.actor;

		html.querySelector('button[data-action="reset"]').addEventListener('click', ev => {
			ev.preventDefault();
			ev.stopPropagation();
			this.resetSkills().then(_ => this.render());
		});

		if (!actor.id) return;

		const t = html.querySelector('.threshold'),
			r = t?.querySelector('[name="threshold"]'),
			mr = t?.querySelector('[name="minRank"]'),
			um = t?.querySelector('[name="useMod"]'),
			p = t?.querySelector('.percentage'),
			hus = html.querySelector('input.hide-unselected'),
			sl = html.querySelector('.skill-list');

		const actorData = actor.system,
			hd = actorData.attributes.hd.total,
			maxRank = hd;

		let maxMod = Number.NEGATIVE_INFINITY;

		const qs = getActorConfig(actor);
		let threshold = qs.threshold ?? 0,
			minRank = qs.minRank ?? 1,
			hideUnselected = false;

		const skills = html.querySelectorAll('[data-skill]');

		let useMod = qs.useMod;

		function hightlightSkills(percent, minRank = 1) {
			skills.forEach(el => {
				const rank = parseInt(el.dataset.rank) || 0,
					mod = parseInt(el.dataset.mod) || 0,
					val = useMod ? mod : rank;
				if (mod > maxMod) maxMod = mod;
				const tt = useMod ? maxMod : maxRank;
				const pcte = percent > 0 ? val >= Math.floor(tt * percent) : false,
					mre = minRank > 0 ? rank >= minRank : false,
					both = percent == 0 && minRank == 0 ? false : (percent > 0 ? pcte : true) && (minRank > 0 ? mre : true);
				el.classList.toggle('auto-select', both);
			});

			const thval = Math.floor((useMod ? maxMod : maxRank) * threshold);
			// console.log({ useMod, maxMod, maxRank, threshold });
			p.textContent = `${Math.floor(percent * 100)}% [${thval}]`;
		}

		hightlightSkills(qs.threshold);
		hus.addEventListener('change', ev0 => {
			hideUnselected = Boolean(ev0.target.checked);
			sl.classList.toggle('hide-unselected', hideUnselected);
			hightlightSkills(threshold, minRank);
		}, { passive: true });

		um.addEventListener('change', ev0 => {
			useMod = Boolean(ev0.target.checked);
			hightlightSkills(threshold, minRank);
		}, { passive: true });

		r.addEventListener('input', ev0 => hightlightSkills(threshold = parseFloat(ev0.target.value), minRank), { passive: true });
		r.addEventListener('change', ev0 => hightlightSkills(threshold = parseFloat(ev0.target.value), minRank), { passive: true });
		mr.addEventListener('input', ev0 => hightlightSkills(threshold, minRank = parseFloat(ev0.target.value)), { passive: true });
		mr.addEventListener('change', ev0 => hightlightSkills(threshold, minRank = parseFloat(ev0.target.value)), { passive: true });
	}

	_updateObject(event, formData) {
		const actor = this.actor;

		const qs = Object.entries(formData)
			.reduce((qs, [skId, enabled]) => {
				if (skId.startsWith('skill.')) {
					if (enabled) qs.push(skId.replace(/^skill\./, ''));
				}
				return qs;
			}, []);

		const C = CFG.console.colors;
		console.log(actor);
		if (actor.id) {
			const setup = new QuickSkillsModel({ ...formData, skills: qs });
			console.log('%cLittle Helper%c | Quick Skills | Saving actor config:',
				C.main, C.unset,
				setup);
			actor.setFlag(CFG.id, 'quickSkills', setup.toObject());
		}
		else {
			console.log('%cLittle Helper%c | Quick Skills | Saving defaults:',
				C.main, C.unset,
				qs);
			game.settings.set(CFG.id, 'quickSkills', qs.join(','));
		}
	}
}

Hooks.once('ready', () => {
	game.settings.register(
		CFG.id,
		'quickSkills',
		{
			type: String,
			default: defaultSkills.join(','),
			scope: 'world',
			config: false,
		},
	);

	game.settings.registerMenu(
		CFG.id,
		'quickSkills',
		{
			name: 'Quick Skills',
			label: 'Default Skills',
			type: SkillEditor,
			restricted: true,
		},
	);
});

class QuickSkillsModel extends foundry.abstract.DataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			skills: new fields.ArrayField(new fields.StringField(), { initial: getDefaultSkills() }),
			threshold: new fields.NumberField({ initial: 0.4 }),
			minRank: new fields.NumberField({ initial: 1 }),
			useMod: new fields.BooleanField({ initial: true }),
			sortMod: new fields.BooleanField({ initial: false }),
			defaults: new fields.BooleanField({ initial: true }),
		};
	}
}

const skipConfirm = () => game.settings.get('pf1', 'skipActionDialogs') && !pf1.skipConfirmPrompt ||
	!game.settings.get('pf1', 'skipActionDialogs') && pf1.skipConfirmPrompt;

async function onSkillEdit(ev, actor) {
	ev.preventDefault();
	ev.stopPropagation();

	const app = Object.values(ui.windows)
		.find(a => a instanceof SkillEditor && a.actor === actor) ?? new SkillEditor(actor);

	app.render(true, { focus: true });
}

function onSkillClick(ev, actor) {
	const el = ev.target.dataset.skill ? ev.target : ev.target.closest('.lil-quick-skills [data-skill-id]');
	if (!el) return;
	ev.preventDefault();
	ev.stopPropagation();

	const skillId = el.dataset.skillId;

	actor.rollSkill(skillId, { skipDialog: skipConfirm() });
}

const fillTooltip = async (event, el, { actor, skillId } = {}) => {
	const tip = document.createElement('div');

	const si = actor.getSkillInfo(skillId);

	tip.append(
		createNode('h2', si.fullName),
		createNode('div', null, ['row', 'rank'], { children: [createNode('label', i18n.get('PF1.Rank')), createNode('span', `${si.rank ?? 0}`, ['rank', 'value', si.rank == 0 ? 'null' : ''])] }),
		createNode('div', null, ['row', 'modifier'], { children: [createNode('label', i18n.get('PF1.Modifier')), createNode('span', signNum(si.mod), ['modifier', 'value', si.mod === 0 ? 'null' : ''])] }),
		createNode('h3', i18n.get('LittleHelper.QuickSkills.Sources')),
	);

	// Reproduce normal source details

	const sourceDetails = actor.getSourceDetails(`system.skills.${skillId}.mod`);

	for (const source of sourceDetails) {
		tip.append(createNode('div', null, ['row', 'source'], { children: [createNode('label', source.name), createNode('span', signNum(source.value), ['value', source.value == 0 ? 'null' : ''])] }));
	}

	return tip;
};

/**
 *
 * @param {Event} ev
 * @param {Element} tip
 * @param {object} options
 * @param {Actor} options.actor
 * @param {string} options.skillId
 */
function activateQuickSkillTooltip(ev, tip, { actor, skillId } = {}) {
	const content = document.createElement('div');

	fillTooltip(content, { actor, skillId });

	game.tooltip.activate(ev.target, {
		content,
		direction: TooltipManager.TOOLTIP_DIRECTIONS.LEFT,
		cssClass: 'lil-quick-skills-tooltip',
	});
}

/**
 * @param {Event} ev
 */
function deactivateQuickSkillTooltip(ev) {
	game.tooltip.deactivate();
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function addQuickSkills(sheet, [html], options, shared) {
	const tab = shared.tabs.summary?.tab,
		sd = tab?.querySelector('.subdetails-body');
	if (!sd) return;
	const qsEl = createNode('div', null, ['lil-quick-skills']);
	const actor = sheet.actor,
		actorData = actor.system;

	const setup = getActorConfig(actor);
	const qs = new QuickSkillsModel(setup ?? {});
	const hd = actorData.attributes.hd.total,
		maxRank = hd;

	const skopts = { maxMod: Number.NEGATIVE_INFINITY };
	const skills = getActorSkills(actor, skopts);

	getDefaultSkills();

	qs.skills.forEach(skId => {
		const sklData = skills[skId];
		if (sklData) {
			sklData.enabled = true;
		}
		else {
			console.warn('Invalid skill ID found:', skId);
		}
	});

	// Add default skills if none are set
	if (qs.skills.length == 0 && setup == undefined) {
		const isCaster = actorData.attributes.spells.usedSpellbooks?.length > 0;
		const dsq = getDefaultSkills({ isCaster });
		dsq.forEach(skId => skills[skId].autoSelect = true);
	}

	// Add skills by threshold
	if (qs.threshold > 0 || qs.minRank > 0) {
		Object.values(skills).forEach(sk => {
			let mVal = false, mRank = false;
			if (qs.threshold > 0) {
				const val = qs.useMod ? sk.mod : sk.rank,
					thval = qs.useMod ? skopts.maxMod : maxRank;
				if (val >= Math.floor(thval * qs.threshold))
					mVal = true;
			}
			else
				mVal = true;

			if (qs.minRank > 0) {
				if (sk.rank >= qs.minRank)
					mRank = true;
			}
			else
				mRank = true;

			if (mVal && mRank)
				sk.autoSelect = true;
		});
	}

	const ul = createNode('ul', null, ['skill-list']);

	const acp = actorData.attributes.acp.total;

	let quickSkillCount = 0;
	for (const skId of Object.keys(skills)) {
		const sk = skills[skId];
		if (!(sk.enabled || sk.autoSelect)) continue;
		quickSkillCount++;
		const li = createNode('li', null, ['skill']);
		const [parentId, subId] = skId.split('.');
		li.dataset.skillId = skId;
		li.dataset.skill = parentId;
		li.dataset.subSkill = subId;
		li.append(
			createNode('h4', sk.fullName, ['name']),
			createNode('span', signNum(sk.mod), ['mod']),
			createNode('i', null, ['acp', sk.acp ? 'affected' : 'unaffected', acp > 0 ? 'penalty' : 'no-penalty', 'fas', 'fa-shield-alt'], { tooltip: sk.acp ? (i18n.get('PF1.ACPLong') + `<br>-${acp}`) : null }),
		);

		// Delayed filling of tooltip
		const context = { actor, skillId: skId };
		hookCoreTooltip(li, { direction: TooltipManager.TOOLTIP_DIRECTIONS.LEFT, fn: fillTooltip, context, css: 'lil-quick-skills-tooltip' });
		ul.append(li);
	}

	// Add notification about lacking matching skills.
	if (quickSkillCount == 0)
		ul.append(createNode('span', i18n.get('LittleHelper.QuickSkills.NoneMatched'), ['lil-quick-skills-notification']));

	const skEdit = createNode('span', null, ['edit', 'action']);
	skEdit.dataset.action = 'edit';
	skEdit.append(createNode('i', null, ['fas', 'fa-cog']));
	skEdit.dataset.tooltip = 'LittleHelper.Action.Configure';

	const h = createNode('div', null, ['quick-skills-header']);
	h.append(createNode('h2', i18n.get('LittleHelper.QuickSkills.Title')), skEdit);

	qsEl.append(h, ul);

	skEdit.addEventListener('click', ev => onSkillEdit(ev, actor));
	qsEl.addEventListener('click', ev => onSkillClick(ev, actor));

	const sdc = createNode('div', null, ['flexrow', 'lil-subdetails-reorganizer']);
	sd.before(sdc);
	sdc.append(sd, qsEl);
}

function enable() {
	Ancillary.register('actor', addQuickSkills);
}

function disable() {
	Ancillary.unregister('actor', addQuickSkills);
}

new Feature({ setting: 'summary-quick-skills', label: 'Quick Skills', hint: 'Displays quick & configurable skill roll access in summary tab.', category: 'actor-sheet', enable, disable, stage: 'ready' });
