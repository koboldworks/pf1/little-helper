import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';
import { SharedData } from '@core/shared-data.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function addRaceName(sheet, [html], options, shared) {
	if (!shared.coreSheet) return;

	const actor = shared.actor,
		race = actor.race;

	if (!race) return;

	const tab = shared.tabs?.summary?.tab;

	const header = tab?.querySelector('.summary-header');

	const raceInfo = header?.querySelector(`[data-item-id="${race.id}"] .race-info`);

	raceInfo?.prepend(createNode('div', null, ['lil-race-name'],
		{
			children: [
				createNode('span', race.name, ['name']),
			],
		}));
}

function enable() {
	Ancillary.register('actor', addRaceName);
}

enable();
