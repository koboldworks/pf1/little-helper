// Token image mismatch
/**
 * Displays little marker if itoken image does not match portrait image.
 *
 * Useful in case you like to keep the two synced but don't always remember to update the token image.
 */

import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { addCoreTooltip } from '@root/common.mjs';

class TokenSyncDialog extends FormApplication {
	constructor(...args) {
		super(...args);
	}

	get document() {
		return this.object;
	}

	get title() {
		return `Image Sync: ${this.document.token?.name ?? this.document.name}`;
	}

	get template() {
		return `modules/${CFG.id}/template/dialog/token-image-sync-dialog.hbs`;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			classes: [..._default.classes, 'lil-token-image-sync-dialog'],
			id: 'lil-token-image-sync-dialog',
			height: 'auto',
			width: 'auto',
		};
	}

	getData() {
		const context = super.getData();

		context.actor = this.document;
		context.token = this.document.token ?? this.document.prototypeToken;

		return context;
	}

	/**
	 * @param {Event} ev
	 * @param {{source: string}} options
	 */
	async _syncImage(ev, { source } = {}) {
		ev.preventDefault();
		ev.stopPropagation();

		const actor = this.document,
			token = actor.token ?? actor.prototypeToken;

		switch (source) {
			case 'actor': {
				if (actor.token)
					token.update({ 'texture.src': actor.img });
				else
					actor.update({ 'prototypeToken.texture.src': actor.img });
				break;
			}
			case 'token':
				actor.update({ img: token.texture.src });
				break;
		}

		this.close();
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		const [html] = jq;

		const actorImg = html.querySelector('.actor-image');
		const tokenImg = html.querySelector('.token-image');

		actorImg.addEventListener('click', ev => this._syncImage(ev, { source: 'actor' }));
		tokenImg.addEventListener('click', ev => this._syncImage(ev, { source: 'token' }));
	}
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function tokenImageMismatch(sheet, [html], options, shared) {
	const actor = sheet.actor;

	const summary = shared.tabs.summary?.tab;
	const img = summary?.querySelector('section > .tab.summary img.profile[data-edit="img"]');

	if (!img) return;

	const pImg = actor.img,
		tImg = actor.token?.texture.src ?? actor.prototypeToken.texture.src;

	const mismatch = pImg !== tImg;
	if (!mismatch) return;
	// TODO: Add marker when it matches, in case there's people who want the portrait and token to always be different.

	const slant = document.createElement('span');
	slant.classList.add('lil-token-mismatch-marker');
	addCoreTooltip(slant, 'Token image does not match profile.<br>Click to show sync options.');

	slant.addEventListener('click', ev => {
		ev.preventDefault();
		ev.stopPropagation();

		new TokenSyncDialog(actor).render(true, { focus: true, left: ev.clientX + 20, top: ev.clientY + 20 });
	});

	img.before(slant);
}

function enable() {
	Ancillary.register('actor', tokenImageMismatch);
}

function disable() {
	Ancillary.unregister('actor', tokenImageMismatch);
}

new Feature({ setting: 'actor-token-mismatch', category: 'actor-sheet', enable, disable, enabledByDefault: false, stage: 'ready' });
