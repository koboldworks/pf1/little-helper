// Display token name with actor name

import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { createNode } from '@root/common.mjs';
import { hookCoreTooltip } from '@core/tooltip.mjs';

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function displayTokenName(sheet, [html], options, shared) {
	/** @type {Element} */
	const tab = shared.tabs.summary?.tab;

	const actor = sheet.actor;
	// sheet token > actor token > prototype token
	const token = sheet.token ?? actor.token ?? actor.prototypeToken;
	const tokenName = token.name;
	if (actor.name == tokenName) return;

	const nameEl = tab?.querySelector('input[name=name]');
	if (!nameEl) return;

	const tokenNameEl = createNode('span', tokenName, ['token-name']);
	tokenNameEl.dataset.tooltip = 'Token Name';

	nameEl.after(tokenNameEl);
}

function enable() {
	Ancillary.register('actor', displayTokenName);
}

function disable() {
	Ancillary.unregister('actor', displayTokenName);
}

new Feature({ setting: 'actor-token-name', category: 'actor-sheet', enable, disable, stage: 'ready' });
