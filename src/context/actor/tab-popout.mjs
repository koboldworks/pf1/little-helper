// Right click popout of bio and notes tabs

import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { addCoreTooltip } from '@root/common.mjs';

class BioNotesApp extends FormApplication {
	constructor(actor, options) {
		super(actor, options);

		// console.log({ actor });

		const bio = this.options.type === 'bio';
		this.options.id = actor.uuid + '-' + (bio ? 'bio-popout' : 'notes-popout');
		this.options.title = actor.name + ': ' + (bio ? 'Biography' : 'Notes');
		// console.log(this.options.title);

		actor.apps[this.appId] = this;
	}

	get template() {
		return `modules/${CFG.id}/template/bionotes-popout.hbs`;
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'koboldworks', 'bionotes'],
			resizable: true,
			width: 520,
			height: 580,
			closeOnSubmit: false,
			submitOnClose: true,
			submitOnChange: true,
		};
	}

	async getData() {
		const context = super.getData(),
			actor = this.object,
			actorData = actor.system;

		context.data = actorData;
		const bio = this.options.type === 'bio';
		context.enrichedContent = await TextEditor.enrichHTML(bio ? actorData.details.biography.value : actorData.details.notes.value, { async: true });
		context.target = bio ? 'system.details.biography.value' : 'system.details.notes.value';
		context.editable = actor.isOwner;
		context.owner = context.editable;

		return context;
	}

	_updateObject(event, formData) {
		// Strip TinyMCE cruft
		for (const key of Object.keys(formData))
			if (/^mce_\d+$/.test(key)) delete formData[key];

		if (!foundry.utils.isEmpty(formData))
			this.object.update(formData);
	}

	static open(actor, options) {
		const old = Object.values(actor.apps).find(a => a instanceof BioNotesApp && a.options.type === options.type);
		if (old) old.render(true, { focus: true });
		else new BioNotesApp(actor, options).render(true, { focus: true });
	}

	close(...args) {
		delete this.object.apps[this.appId];
		super.close(...args);
	}
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 */
function enableTabPopout(sheet, [html]) {
	const actor = sheet.actor;

	const tabs = html.querySelector('form nav.tabs[data-group="primary"]');

	const bio = tabs?.querySelector('[data-tab="biography"');
	const notes = tabs?.querySelector('[data-tab="notes"]');

	// console.log(sheet);

	const hint = 'LittleHelper.UI.PopTabHint';
	if (bio) {
		addCoreTooltip(bio, hint);
		bio.addEventListener('contextmenu', () => BioNotesApp.open(actor, { type: 'bio' }));
	}
	if (notes) {
		addCoreTooltip(notes, hint);
		notes.addEventListener('contextmenu', () => BioNotesApp.open(actor, { type: 'notes' }));
	}
}

function enable() {
	Ancillary.register('actor', enableTabPopout);
}

function disable() {
	Ancillary.unregister('actor', enableTabPopout);
}

new Feature({ setting: 'bionotes-popout', label: 'Bio & Notes Popout', hint: 'Allow popping out bio and notes with right click on tab header.', category: 'actor-sheet', enable, disable, stage: 'ready' });
