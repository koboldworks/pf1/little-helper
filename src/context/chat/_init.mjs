// Added functionality
import './quick-same-roll.mjs';
import './apply-buff.mjs';

// Expanded inline formula handler
import './inline-roll-expansion-hook.mjs';
import './inline-formula.mjs';

// Generic chat card
import './sender-marker.mjs';

import './card/_init.mjs';

// Roll enhancers
import './check-hint-tags.mjs';

import './roll-enhancement/_init.mjs';
// import './damage-types.mjs'; // Maybe revisit some other time?
import './inline-roll-minmax-enhancer.mjs';
import './meld-damage-rolls.mjs';

// Scroll to end
import './ensure-chat-log-end-position.mjs';

// Interaction
import './apply-damage-sanity-check.mjs';
