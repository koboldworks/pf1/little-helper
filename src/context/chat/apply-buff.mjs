import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';

/**
 * @param {Actor} actor
 * @param {Item} buff
 * @param {object} createData
 */
function applyBuff(actor, buff, createData) {
	const old = actor.items.find(i => i.type === 'buff' && i.name === createData.name);
	if (old) {
		// Update existing buff
		if (old.isActive) return null;
		const updateData = { 'system.active': true };
		const level = createData.system?.level;
		if (old.system.level !== level)
			updateData['system.level'] = level;
		return old.update(updateData);
	}
	else {
		// Add non-existent buff
		return actor.createEmbeddedDocuments('Item', [createData]);
	}
}

/**
 * @param {Event} ev
 * @param {Item} buff
 */
function applyBuffTriage(ev, buff) {
	ev.preventDefault();
	ev.stopPropagation();

	const actors = canvas.tokens.controlled.map(t => t.actor).filter(a => a?.isOwner);
	if (actors.length === 0)
		return ui.notifications?.warn('No valid actors selected.');

	const buffData = buff.toObject();

	// Delete some unnecessary data
	delete buffData.ownership;
	delete buffData.sort;
	delete buffData.folder;
	delete buffData.system.links;
	delete buffData.system.duration?.start;

	// Ensure buff is active
	buffData.system.active = true;
	// TODO: Add source identifier?

	actors.forEach(a => applyBuff(a, buff, buffData));
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function addApplyBuff(cm, [html]) {
	if (!cm.isContentVisible) return;

	const item = cm.itemSource;
	if (item?.type !== 'buff') return;

	const metadata = cm.getFlag('pf1', 'metadata');
	if (metadata?.action) return;

	const content = html.querySelector('.card-content');

	const d = createNode('div', null, ['lil-apply-buff', 'flexrow']);
	const button = createNode('button', 'Apply to Selected', ['apply-button']);
	d.append(button);

	button.addEventListener('click', ev => applyBuffTriage(ev, item));

	content?.after(d);
}

function enable() {
	Ancillary.register('chat', addApplyBuff);
}

function disable() {
	Ancillary.unregister('chat', addApplyBuff);
	return false;
}

new Feature({ setting: 'card-apply-buff', label: 'Apply Buff Button', hint: 'Adds button to apply a buff to selected tokens to buff chat cards.', category: 'chat-card', enable, disable, stage: 'init' });
