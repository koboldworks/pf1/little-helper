import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';

/* global libWrapper */

/**
 * @param wrapped
 * @param {string} action
 * @param {object} options
 * @param {Element} options.button
 * @param {Item} options.item
 */
const chatCardActionSanityCheck = async (wrapped, action, options) => {
	if (['applyDamage'].includes(action)) {
		const msgId = options.button.closest('.chat-message[data-message-id]').dataset.messageId;
		const msg = game.messages.get(msgId);
		const metadata = msg?.getFlag('pf1', 'metadata');
		const trueTargets = metadata?.targets?.map(id => canvas.scene?.tokens.get(id) ?? fromUuidSync(id));
		if (trueTargets?.length) {
			const selected = canvas.tokens.controlled.map(t => t.document);
			const notInTargets = selected.filter(t => !trueTargets.some(tgt => tgt === t));
			if (notInTargets.length) {
				const targets = notInTargets.map(t => ({ token: t, targeted: false, selected: true }));
				const actualTargets = trueTargets.map(t => ({ token: t, targeted: true, selected: selected.some(t2 => t2 === t) }));

				const templateData = {
					targets,
					included: actualTargets.filter(t => t.selected),
					excluded: actualTargets.filter(t => !t.selected),
				};

				const rv = await Dialog.confirm({
					title: 'Applying to untargeted tokens',
					content: await renderTemplate(`modules/${CFG.id}/template/dialog/target-validation.hbs`, templateData),
					options: {
						classes: ['dialog', 'lil-target-sanity-check'],
						jQuery: false,
					},
				});

				if (rv !== true) return;
			}
		}
	}

	return wrapped(action, options);
};

const enable = () => {
	if (pf1.documents.item.ItemPF._onChatCardAction)
		libWrapper.register(CFG.id, 'pf1.documents.item.ItemPF._onChatCardAction', chatCardActionSanityCheck, libWrapper.MIXED);

	// TODO: Override pf1.utils.chat.onButton instead for PFv11
};

const disable = () => {
	if (pf1.documents.item.ItemPF._onChatCardAction)
		libWrapper.unregister(CFG.id, 'pf1.documents.item.ItemPF._onChatCardAction');
};

new Feature({ setting: 'apply-damage-sanity', label: 'Apply Damage Sanity Checks', hint: 'Perform minor sanity check on applying damage by checking your selected tokens are among targets. If discrepancies are found, confirmation dialog is displayed.', category: 'chat-card', enable, disable, stage: 'init' });
