/*
 * Add additional properties to chat cards.
 */

import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode, canViewChatMessage, testTransparency, addCoreTooltip } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function additionalProperties(cm, [html], options, shared) {
	const item = cm.itemSource;
	if (!item) return; // Only item has the data needed

	if (!canViewChatMessage(cm, shared.user)) return;
	if (!testTransparency(game.user, item)) return;

	const cardContent = html.querySelector('.pf1.item-card');
	if (!cardContent) return;

	const extraProps = [];

	const props = html.querySelector('.property-group.gm-sensitive div');
	if (!props) return;

	// const { itemId, actionId, tokenUuid, actorId } = cardContent.dataset ?? {};
	const actionId = cardContent.dataset.actionId;
	const action = item.actions?.get(actionId);

	const newProp = (text, classes = [], tooltip) => createNode('span', text, ['tag', ...classes], { tooltip });

	// const itemData = item.system;

	if (action) {
		if (action.attackType == 'natural' && !action.naturalAttack.primaryAttack)
			extraProps.push(newProp(i18n.get('PF1.SecondaryAttack'), ['lil-potentially-invalid'], 'Matches only default choice.'));
	}

	if (extraProps.length > 0)
		props.append(...extraProps);
}

function enable() {
	Ancillary.register('chat', additionalProperties);
}

function disable() {
	Ancillary.unregister('chat', additionalProperties);
	return false;
}

new Feature({ setting: 'card-additional-properties', label: 'Additional Properties', hint: 'Adds additional properties to chat cards, such as secondary natural attack identifier.', category: 'chat-card', enable, disable, stage: 'init', stream: true });
