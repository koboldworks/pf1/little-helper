import { Feature, SubSetting } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode, canViewChatMessage } from '@root/common.mjs';
import { setStaticToolTipListener } from '@core/tooltip.mjs';
import { i18n } from '@root/utility/i18n.mjs';

function getRollModeName(mode) {
	switch (mode) {
		case 'roll':
		case 'publicroll': return 'Public';
		case 'gmroll': return 'GM';
		case 'selfroll': return 'Self';
	}
}

/**
 * @param {Element} tip
 * @param {ChatMessage} cm
 * @param {object} options
 * @param {Item} options.item
 * @param {Element} options.element
 * @param {object} options.metadata
 * @param {boolean} options.hasAttack
 * @param {boolean} options.hasDamage
 * @param options.attacks
 * @param options.tagGroups
 */
const fillCardTooltip = async (tip, cm, { item, element, metadata, attacks, hasAttack, hasDamage, tagGroups = [] } = {}) => {
	// const modeName = getRollModeName(cm);
	// const msgType = cmData.type;

	const speaker = ChatMessage.getSpeakerActor(cm.speaker),
		isVisible = cm.isContentVisible,
		whisper = cm.whisper,
		isWhisper = whisper.length > 0,
		isBlind = isWhisper && cm.blind,
		author = cm.author ?? cm.user, // TODO: remove .user once v11 support is removed
		authorId = author?.id,
		isSelfWhisper = whisper.length == 1 && whisper[0] == authorId;

	// Author
	const nameDetails = [];
	if (!authorId) nameDetails.push(' ', createNode('span', '(Missing)', ['hint']));
	else if (authorId === game.user.id) nameDetails.push(' ', createNode('span', '(You)', ['hint']));
	const ul = createNode('ul', null, ['details'], {
		children: [
			createNode('li', null, ['author'], {
				children: [
					createNode('label', 'Author'),
					createNode('span', `${author?.name}`, ['name', 'author-name', 'value'], { children: nameDetails }),
				],
			}),
		],
	});

	const addLine = () => ul.append(createNode('hr', null, ['span-2']));

	// Speaker
	if (speaker && isVisible) {
		addLine();

		const alias = cm.speaker.alias,
			tokenName = speaker.token?.name,
			actorName = speaker.name,
			primaryName = alias ?? tokenName ?? actorName;
		const lis = createNode('li', null, ['speaker'], {
			children: [
				createNode('label', 'Speaker'),
				createNode('span', primaryName, ['name', 'speaker-name', 'value']),
			],
		});
		ul.append(lis);

		if (speaker.testUserPermission(game.user, 'OBSERVER')) {
			if (!!tokenName && primaryName !== tokenName) {
				const lit = createNode('li', null, ['speaker'], {
					children: [
						createNode('label', 'Token'),
						createNode('span', tokenName, ['name', 'token-name', 'value']),
					],
				});
				ul.append(lit);
			}

			if (!!actorName && primaryName !== actorName && tokenName !== actorName) {
				const lia = createNode('li', null, ['speaker'], {
					children: [
						createNode('label', 'Actor'),
						createNode('span', actorName, ['name', 'actor-name', 'value']),
					],
				});
				ul.append(lia);
			}
		}
	}

	addLine();

	// Roll Mode
	{
		let modeLabel;
		if (!isWhisper) modeLabel = 'Public';
		else if (isBlind) modeLabel = 'Blind GM Whisper';
		else if (isSelfWhisper) modeLabel = 'Self';
		else if (whisper.every(u => game.users.get(u)?.isGM)) modeLabel = 'GM Whisper';
		else modeLabel = 'Whisper';

		const rlmode = createNode('li', null, ['roll-mode'], {
			children: [
				createNode('label', 'Visibility'),
				createNode('span', modeLabel, ['name', 'speaker-name', 'value']),
			],
		});
		ul.append(rlmode);
	}

	// Is Blind
	if (isBlind) {
		const li = createNode('li', null, ['blind'], {
			children: [
				createNode('label', 'Blind'),
				createNode('span', isBlind ? 'Yes' : 'No', ['blind-mode', isBlind ? 'enabled' : 'disabled', 'value']),
			],
		});
		ul.append(li);
	}

	// Is Self Roll
	if (isSelfWhisper) {
		const li = createNode('li', null, ['selfroll'], {
			children: [
				createNode('label', 'Self'),
				createNode('span', isSelfWhisper ? 'Yes' : 'No', ['self-roll-mode', isSelfWhisper ? 'enabled' : 'disabled', 'value']),
			],
		});
		ul.append(li);
	}
	// Is Whisper
	else if (isWhisper) {
		const lipre = createNode('li', null, ['whisper'], {
			children: [
				createNode('label', 'Whisper'),
				createNode('span', isWhisper ? 'Yes' : 'No', ['whisper-mode', isWhisper ? 'enabled' : 'disabled', 'value']),
			],
		});
		ul.append(lipre);

		if (isVisible) {
			const targets = cm.whisper.map(uid => game.users.get(uid) ?? { id: uid, missing: true });
			for (const t of targets) {
				const isSelf = game.user.id === t.id,
					isGM = t.isGM === true,
					label = t.name ?? `[${t.id}]`,
					targetNames = [];
				if (isSelf) targetNames.push(' ', createNode('span', '(You)', ['hint']));
				const li = createNode('li', null, ['whisper-target'], {
					children: [
						createNode('label', ' – ', ['preformatted']),
						createNode('span', label, ['name', 'whisper-target', isSelf ? 'self' : 'other', isGM ? 'gm' : 'player', 'value'], { data: { 'user-id': t.id }, children: targetNames }),
					],
				});
				ul.append(li);
			}
		}
	}

	addLine();

	{
		const timestamp = cm.timestamp;
		/*
		const litime = createNode('li', null, ['timestamp'], {
			children: [
				createNode('label', 'Timestamp'),
				createNode('time', timestamp, ['timestamp', 'value'])
			]
		});
		*/

		const date = new Date(timestamp);
		const dateLabel = date.toLocaleString();
		const lidate = createNode('li', null, ['date-time'], {
			children: [
				createNode('label', 'Date & Time'),
				createNode('time', dateLabel, ['date-time', 'value']),
			],
		});
		ul.append(lidate);

		if (feature.subsettings.stealTimestamp.value) {
			const lifuzzy = createNode('li', null, ['relative-time'], {
				children: [
					createNode('label', 'Relative'),
					createNode('time', tip.querySelector('.message-timestamp')?.textContent, ['message-timestamp', 'value']),
				],
			});
			ul.append(lifuzzy);
		}
	}

	if (isVisible && tagGroups.length) {
		addLine();
		const tagEl = createNode('div', null, ['property-group-tags', 'span-2']);
		for (const g of tagGroups) {
			const tagGEl = createNode('div', null, ['tags']);
			tagGEl.append(...g.tags);
			tagEl.append(
				createNode('h3', `Tags: ${g.label}`, ['group-title']),
				tagGEl,
			);
		}
		ul.append(tagEl);
	}

	const rv = Hooks.call('little-helper.chat.tooltip.meta', cm, ul);
	if (rv === false) return;

	tip.replaceChildren(ul);
};

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function injectChatTooltip(cm, [html], options, shared) {
	// const subject = cm.flags?.pf1?.subject;

	// if (!canViewChatMessage(cm)) return;

	const item = cm.itemSource,
		metadata = cm.system,
		attacks = metadata?.rolls?.attacks,
		hasAttack = attacks?.length > 0,
		hasDamage = attacks?.[0]?.damage?.length > 0;

	const tip = createNode('div', null, ['lil-tooltip', 'lil-card-tooltip']);

	if (feature.subsettings.stealRecipients.value)
		html.querySelector('.message-header .whisper-to')?.remove();
	if (feature.subsettings.stealTimestamp.value)
		tip.append(html.querySelector('.message-metadata .message-timestamp')); // Temporary location to avoid foundry crapping out
	const tagGroups = [];
	if (feature.subsettings.stealTags.value) {
		const footer = html.querySelector('footer.card-footer');
		footer?.querySelectorAll('.property-group.general-notes').forEach(el => {
			const label = el.firstElementChild?.textContent?.trim();
			const tags = Array.from(el.querySelectorAll('.tag'));
			if (tags.length) {
				tagGroups.push({ label, tags });
				el.remove();
			}
		});
	}

	html.addEventListener('pointerenter', ev => fillCardTooltip(tip, cm, { element: html, item, metadata, attacks, hasAttack, hasDamage, tagGroups }), { passive: true, once: true });

	setStaticToolTipListener(html, tip, { aligned: false });

	html.append(tip);
}

function enable() {
	Ancillary.register('chat', injectChatTooltip);
}

function disable() {
	Ancillary.unregister('chat', injectChatTooltip);
	return false;
}

const feature = new Feature({
	setting: 'card-tooltips',
	label: 'Metadata Tooltips',
	hint: 'Display card metadata in a tooltip.',
	category: 'chat-card',
	enable,
	disable,
	stage: 'init',
	subsettings: {
		stealRecipients: new SubSetting({
			label: 'Steal Recipients',
			hint: 'Remove recipient display from the main card, only displaying it in the tooltip.',
			fallback: true,
			type: Boolean,
		}),
		stealTimestamp: new SubSetting({
			label: 'Steal Timestamp',
			hint: 'Move timestamp display from the main card into the tooltip.',
			fallback: true,
			type: Boolean,
		}),
		stealTags: new SubSetting({
			label: 'Steal Property Tags',
			hint: 'Move property tags into the tooltip.',
			fallback: false,
			type: Boolean,
		}),
	},
});
