/*
 * Add more information to chat card header.
 */

import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode, canViewChatMessage, testTransparency } from '@root/common.mjs';

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function cardHeaderEnrichment(cm, [html]) {
	const item = cm.itemSource;
	if (!canViewChatMessage(cm)) return;
	if (!testTransparency(game.user, item)) return;

	const metadata = cm.system;
	if (!item && !metadata) return;

	const name = html.querySelector('.card-header .item-name');
	if (!name) return;

	const hd = createNode('div', '', ['lil-header-details', 'flexcol']);

	const cl = metadata?.spell?.cl ?? item?.casterLevel;
	if (cl != null) hd.append(createNode('span', `CL ${cl}`, ['lil-cl', 'lil-detail']));

	const sl = metadata?.spell?.sl ?? item?.spellLevel;
	if (sl != null) hd.append(createNode('span', `SL ${sl}`, ['lil-sl', 'lil-detail']));

	if (cl != null || sl != null)
		name.after(hd);
}

function enable() {
	Ancillary.register('chat', cardHeaderEnrichment);
}

function disable() {
	Ancillary.unregister('chat', cardHeaderEnrichment);
	return false;
}

new Feature({ setting: 'card-header-enrichment', label: 'Header Enrichment', hint: 'Adds additional details to card headers, such as spell level and caster level.', category: 'chat-card', enable, disable, stage: 'init', stream: true });
