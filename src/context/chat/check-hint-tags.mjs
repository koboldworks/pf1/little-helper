// Little hint tags on checks to speed up their use.

import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { enrichRoll, canViewChatMessage, createNode, addCoreTooltip } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

const getSkillName = (id) => pf1.config.skills[id] || id;

class Tag {
	check;
	success;
	failure;
	possible;
	content;
	hint;
	constructor(label, { hint = null, check = 0, success = false, possible = false, failure = false } = {}) {
		this.check = check;
		this.content = label;
		this.hint = hint;
		this.success = success;
		this.possible = possible;
		this.failure = failure;
	}
}

function appendCustomTags(html, tags = []) {
	const div = createNode('div', null, ['property-group', 'reminders', 'lil-check-notes']);

	const tagList = createNode('div', null, ['tag-list', 'flexrow']);
	div.append(tagList);

	tags.forEach(t => {
		const tag = createNode('span', null, ['tag', 'enriched']);
		tag.innerHTML = t.content;
		if (t.check) {
			tag.classList.add('check');
			if (t.success) tag.classList.add('success');
			if (t.possible) tag.classList.add('possible');
			if (t.failure) tag.classList.add('failure');
		}
		if (t.hint) {
			tag.classList.add('hint');
			addCoreTooltip(tag, t.hint);
		}
		tagList.append(tag);
	});

	html.querySelector('.message-content')?.append(div);
}

function handleSkillCheck({ skillId, tags, check, cm, actor } = {}) {
	const isMetric = pf1.utils.getDistanceSystem() === 'metric';

	const rank = null; // TODO: Read card metadata once available
	const trained = rank > 0;

	switch (skillId) {
		case 'apr':
			// Appraise
			tags.push(new Tag(i18n.get('LittleHelper.Checks.BaseDC', { dc: 20 }), { check, failure: check < 10, possible: check >= 10 && check < 20, success: check >= 20 }));
			break;
		case 'spl':
			// Spellcraft
			// Display what level of spell the roll is good for.
			{
				const formula = '@check[Result] - 15[Base DC]';
				const roll = RollPF.safeRollSync(formula, { check });
				const enriched = enrichRoll(roll, formula, roll.total).outerHTML;
				tags.push(new Tag(i18n.get('LittleHelper.CardHints.IdentifyMaxLevel', { level: enriched }), { hint: 'LittleHelper.CardHints.IdentifyMaxLevelHint', check, possible: check >= 15, success: check > 33, failure: check < 15 }));
			}
			break;
		case 'dip':
			// Diplomacy
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.IndifferentBaseDC', { dc: 15 }), { hint: '', check, failure: check < 5, possible: check >= 5 && check < 15, success: check > 15 }));
			break;
		case 'int':
			// Intimidate
			tags.push(
				new Tag(i18n.get('LittleHelper.CardHints.DemoralizeDC'), { check, failure: check < 10, possible: check >= 10 }),
				new Tag(i18n.get('LittleHelper.CardHints.DemoralizeSizeBonus'), { hint: 'LittleHelper.CardHints.DemoralizeSizeBonusHint' }),
			);
			break;
		case 'fly':
			// Fly
			tags.push(
				new Tag(i18n.get('LittleHelper.CardHints.FlySlowDC', { dc: 10 }), { check, success: check >= 10, failure: check < 10 }),
				new Tag(i18n.get('LittleHelper.CardHints.FlyHoverDC', { dc: 15 }), { check, success: check >= 15, failure: check < 15 }),
				new Tag(i18n.get('LittleHelper.CardHints.FlyOtherDC', { dc: '15/20' }), { check, success: check >= 20, possible: check >= 15 && check < 20, failure: check < 15 }),
			);
			break;
		case 'acr': {
			// Acrobatics
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.TumbleDC'), { hint: 'LittleHelper.CardHints.TumbleDCHint', check, failure: check < 0, possible: check >= 0 }));
			const actor = ChatMessage.getSpeakerActor(cm.speaker);
			// tags.push({ content: 'Through DC=5+CMD', failure: check < 15, possible: check >= 15 }); // it's just +5 DC
			const unit = isMetric ? pf1.config.measureUnitsShort.m : pf1.config.measureUnitsShort.ft;
			if (actor) {
				const speed = actor.system.attributes?.speed?.land?.total ?? 0;
				const hformula = 'floor((@check[Result] + floor(((@attributes.speed.land.total - 30) / 10)[Speed Modifier] * 4[Check Adjust])) / 4)';
				const hroll = RollPF.safeRollSync(hformula, { attributes: { speed: { land: { total: speed } } }, check });
				const hcv = pf1.utils.convertDistance(hroll.total)[0];
				const henriched = hroll.err ? '<i>NaN</i>' : enrichRoll(hroll, hformula, `${hcv} ${unit}`).outerHTML;
				tags.push(new Tag(i18n.get('LittleHelper.CardHints.JumpHeight', { value: henriched }), { check, failure: hroll.total <= 0, possible: hroll.total > 0, hint: hroll.err?.message }));

				const dformula = '@check[Base Distance] + floor((@attributes.speed.land.total - 30) / 10)[Speed Modifier] * 4[Check Adjust]';
				const droll = RollPF.safeRollSync(dformula, { attributes: { speed: { land: { total: speed } } }, check });
				const dcv = pf1.utils.convertDistance(droll.total)[0];
				const denriched = droll.err ? '<i>NaN</i>' : enrichRoll(droll, dformula, `${dcv} ${unit}`).outerHTML;
				tags.push(new Tag(i18n.get('LittleHelper.CardHints.JumpLength', { value: denriched }), { check, failure: droll.total <= 0, possible: droll.total > 0, hint: droll.err?.message }));
			}
			break;
		}
		case 'blf':
			// Bluff
			tags.push(
				new Tag(i18n.get('LittleHelper.CardHints.FeintDC', { dc: 10 }), { hint: 'LittleHelper.CardHints.FeintDCHint', check, failure: check < 10, possible: check >= 10 }),
				new Tag(i18n.get('LittleHelper.CardHints.OpposedBy', { skill: getSkillName('sen') })),
			);
			break;
		case 'clm': { // Climb
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.ClimbNaturalRockDC', { dc: 15 }), { check, possible: check >= 5, failure: check < 5 }));
			const clmb = actor?.system.attributes?.speed?.climb?.total ?? 0;
			let speed = clmb;
			if (clmb == 0) {
				const base = (actor?.system.attributes?.speed?.land?.total ?? 0) / 5;
				speed = Math.floor(base / 4) * 5; // Quarter normal speed
			}
			const [cspeed, unit] = pf1.utils.convertDistance(speed);
			const lspeed = i18n.get('Koboldworks.Missing.PF1.Distance', { distance: cspeed, unit: pf1.config.measureUnitsShort[unit] || unit });
			const hint = clmb > 0 ? 'LittleHelper.CardHints.NaturalSpeed' : 'LittleHelper.CardHints.ClimbSpeedHint';
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.ClimbSpeed', { speed: lspeed }), { hint }));
			if (speed > 0) {
				const aspeed = i18n.get('Koboldworks.Missing.PF1.Distance', { distance: cspeed * 2, unit: pf1.config.measureUnitsShort[unit] || unit });
				tags.push(new Tag(i18n.get('LittleHelper.CardHints.FastClimb', { speed: aspeed }), { hint: 'LittleHelper.CardHints.FastClimbHint' }));
			}
			break;
		}
		case 'dev': // Disable Device
			tags.push(
				new Tag(i18n.get('LittleHelper.CardHints.DisableDeviceBaseDC', { dc: 10 }), { check, failure: check < 10, possible: check >= 10 }),
				new Tag(i18n.get('LittleHelper.CardHints.DisableLockBaseDC', { dc: 20 }), { check, failure: check < 20, possible: check >= 20 }),
			);
			break;
		case 'dis': // Disguise
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.OpposedBy', { skill: getSkillName('per') })));
			break;
		case 'esc': // Escape Artist
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.EscapeArtistBaseDC', { dc: 20 }), { check, failure: check < 20, possible: check >= 20 }));
			break;
		case 'han': // Handle Animal
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.HandleAnimalBaseDC', { dc: 10 }), { check, failure: check < 10, possible: check >= 10 }));
			break;
		case 'hea':
			// Heal
			tags.push(
				new Tag(i18n.get('LittleHelper.CardHints.HealCommonDC', { dc: 15 }), { check, success: check >= 15, possible: check >= 13 && check < 15, failure: check < 13 }),
				new Tag(i18n.get('LittleHelper.CardHints.HealDeadlyDC', { dc: 20 }), { hint: 'LittleHelper.CardHints.HealDeadlyDCHint', check, success: check >= 24, possible: check >= 18 && check < 23, failure: check < 18 }),
			);
			break;
		case 'rid':
			// Ride
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.RideFastMountDC', { dc: 20 }), { check, failure: check < 20, success: check >= 20 }));
			break;
		case 'sen':
			// Sense Motive
			tags.push(
				new Tag(i18n.get('LittleHelper.Checks.BaseDC', { dc: 20 }), { check, failure: check < 20, possible: check >= 20 }),
				new Tag(i18n.get('LittleHelper.CardHints.OpposedBy', { skill: getSkillName('blf') })),
			);
			break;
		case 'slt':
			// Sleight of Hand
			tags.push(
				new Tag(i18n.get('LittleHelper.CardHints.PalmDC', { dc: 20 }), { check, failure: check < 10, possible: check >= 10 }),
				new Tag(i18n.get('LittleHelper.CardHints.StealDC', { dc: 20 }), { check, failure: check < 20, possible: check >= 20 }),
				new Tag(i18n.get('LittleHelper.CardHints.OpposedBy', { skill: getSkillName('per') })),
			);
			break;
		case 'per':
			// Perception
			tags.push(
				new Tag(i18n.get('LittleHelper.CardHints.OpposedBy', { skill: getSkillName('ste') })),
				new Tag(i18n.get('LittleHelper.CardHints.OpposedBy', { skill: getSkillName('slt') })),
			);
			break;
		case 'ste':
			// Stealth
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.OpposedBy', { skill: getSkillName('per') })));
			break;
		case 'sur': {
			// Survival
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.SubsistDC', { dc: 10 }), { hint: 'LittleHelper.CardHints.SubsistDCHint', check, failure: check < 10, success: check >= 10 }));
			// TODO: Simply display how many are fed?
			const count = Math.max(0, Math.floor((check - 8) / 2));
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.SubsistFed', { count })));
			break;
		}
		case 'swm': {
			tags.push(new Tag(i18n.get('LittleHelper.Checks.BaseDC', { dc: 10 }), { check, failure: check < 10, possible: check >= 10 }));
			const swm = actor?.system.attributes?.speed?.swim?.total ?? 0;
			let speed = swm;
			if (swm == 0) {
				const base = (actor?.system.attributes?.speed?.land?.total ?? 0) / 5;
				speed = Math.floor(base / 4) * 5; // Quarter normal speed
			}
			const [cspeed, unit] = pf1.utils.convertDistance(speed);
			const lspeed = i18n.get('Koboldworks.Missing.PF1.Distance', { distance: cspeed, unit: pf1.config.measureUnitsShort[unit] || unit });
			const hint = swm > 0 ? 'LittleHelper.CardHints.NaturalSpeed' : 'LittleHelper.CardHints.SwimSpeedHint';
			tags.push(new Tag(i18n.get('LittleHelper.CardHints.SwimSpeed', { speed: lspeed }), { hint }));

			break;
		}
		case 'umd':
			// Use Magic Device
			{
				const formula = '@check[Result] - 20[Base DC]';
				const roll = RollPF.safeRollSync(formula, { check });
				const enriched = enrichRoll(roll, formula, roll.total).outerHTML;
				tags.push(new Tag(i18n.get('LittleHelper.CardHints.UMDWandDC', { dc: 20 }), { check, failure: check < 20, possible: check >= 20 }));
				tags.push(new Tag(i18n.get('LittleHelper.CardHints.UMDScrollMaxLevel', { level: enriched }), { hint: 'LittleHelper.CardHints.UMDScrollMaxLevelHint', check, possible: check >= 20, failure: check < 20, success: check >= 29 }));
			}
			break;
		case 'kar':
		case 'kdu':
		case 'ken':
		case 'kge':
		case 'khi':
		case 'klo':
		case 'kna':
		case 'kno':
		case 'kpl':
		case 'kre':
			if (!trained)
				tags.push(new Tag(i18n.get('LittleHelper.CardHints.UntrainedKnowledgeMaxDC', { dc: 10 }), { hint: 'LittleHelper.CardHints.CommonKnowledgeHint', check, success: check >= 10, failure: check < 5, possible: check < 10 && check >= 5 }));
			if (rank !== 0)
				tags.push(new Tag(i18n.get('LittleHelper.CardHints.KnowledgeBaseDC', { dc: 15 }), { hint: 'LittleHelper.CardHints.KnowledgeBaseDCHint', check, failure: check < 15, possible: check >= 15 && check < 33, success: check >= 33 }));
			break;
		case undefined:
			break;
		default:
			/*
			// craft skills
			// The DCs are all over the place (0 to 35 with no clear base
			}
			*/
			break;
	}
}

function handleAbilityCheck({ abilityId, tags, check, cm, actor } = {}) {
	switch (abilityId) {
		case 'str':
			// Strength
			tags.push(
				new Tag(i18n.get('LittleHelper.Checks.BaseDC', { dc: 5 }), { check, failure: check < 5, possible: check >= 5 }),
				new Tag(i18n.get('LittleHelper.CardHints.DoorDC'), { hint: 'LittleHelper.CardHints.DoorDC', check, possible: check < 30, success: check >= 30, failure: check < 0 }),
			);
			break;
		case 'dex':
			// Dexterity
			tags.push(new Tag(i18n.get('LittleHelper.Checks.BaseDC', { dc: 5 }), { check, failure: check < 5, possible: check >= 5 }));
			break;
		case 'con':
			// Constitution
			tags.push(new Tag(i18n.get('LittleHelper.Checks.BaseDC', { dc: 5 }), { check, failure: check < 5, possible: check >= 5 }));
			{
				const actor = ChatMessage.getSpeakerActor(cm.speaker);
				if (!actor) return;

				const hp = actor.system.attributes?.hp;
				const eHP = hp.value + hp.temp;
				if (eHP < 0) {
					const formula = '10[Base] - (@hp.value + @hp.temp)[Effective HP]';
					const roll = RollPF.safeRollSync(formula, { hp });
					const dc = roll.total;
					const enriched = enrichRoll(roll, formula, roll.total).outerHTML;
					tags.push(new Tag(i18n.get('LittleHelper.CardHints.StabilizeDC') + ' ' + enriched, { check, failure: dc > check, success: dc <= check }));
				}
				else {
					const conditions = actor.system.conditions;
					const b = conditions.bleed;
					tags.push(new Tag(i18n.get('PF1.Condition.stable'), { check, success: !b, possible: b }));
				}
			}
			break;
		case 'int':
			tags.push(new Tag(i18n.get('LittleHelper.Checks.BaseDC', { dc: 5 }), { check, failure: check < 5, possible: check >= 5 }));
			break;
		case 'wis':
			tags.push(new Tag(i18n.get('LittleHelper.Checks.BaseDC', { dc: 5 }), { check, failure: check < 5, possible: check >= 5 }));
			break;
		case 'cha':
			tags.push(new Tag(i18n.get('LittleHelper.Checks.BaseDC', { dc: 10 }), { check, failure: check < 10, possible: check >= 10 }));
			break;
	}
}

function handleOtherCheck({ otherId, tags, check, cm, actor } = {}) {
	switch (otherId) {
		case 'concentration':
			{
				const formulaDefensiveCasting = 'floor((@check[Result] - 15[Base DC]) / 2)';
				const formulaEntangledCasting = 'floor(@check[Result] - 15[Base DC])';
				const defensiveRoll = RollPF.safeRollSync(formulaDefensiveCasting, { check });
				const entangledRoll = RollPF.safeRollSync(formulaEntangledCasting, { check });
				const enrichedDef = enrichRoll(defensiveRoll, formulaDefensiveCasting, defensiveRoll.total).outerHTML;
				const enrichedEtgl = enrichRoll(entangledRoll, formulaEntangledCasting, entangledRoll.total).outerHTML;

				tags.push(
					// new Tag('Defensive DC 15+SL×2', { hint: `Max spell level: ${defensiveRoll.total}`, check, failure: check < 15, possible: check >= 15, success: check > 33 }),
					new Tag(i18n.get('LittleHelper.Checks.Other.concentration.DefensiveMaxSL', { level: enrichedDef }), { hint: 'DC 15+SL×2', check, failure: check < 15, possible: check >= 15, success: check > 33 }),
					new Tag(i18n.get('LittleHelper.Checks.Other.concentration.DamagedDC'), { hint: 'LittleHelper.CardHints.ContinuousDamageHint', check, failure: check < 10, possible: check >= 10 }),
					new Tag(i18n.get('LittleHelper.Checks.Other.concentration.GrappledDC'), { check, failure: check < 10, possible: check >= 10 }),
					// new Tag('Entangled DC 15+SL', { hint: `Max spell level: ${entangledRoll.total}`, check, failure: check < 15, possible: check >= 15, success: check >= 15 + 9 }),
					new Tag(i18n.get('LittleHelper.Checks.Other.concentration.EntangledMaxSL', { level: enrichedEtgl }), { hint: 'LittleHelper.Checks.Other.concentration.EntangledMaxSLHint', check, failure: check < 15, possible: check >= 15, success: check >= 15 + 9 }),
				);
			}
			break;
		case undefined:
			break;
	}
}

function handleSaveCheck({ saveId, tags, check, cm, actor }) {
	switch (saveId) {
		case 'ref':
		case 'will':
		case 'fort': {
			tags.push(new Tag(i18n.get('LittleHelper.Checks.BaseDC', { dc: 10 }), { check, failure: check < 10, possible: check >= 10 }));
			break;
		}
		case undefined:
			break;
	}
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 * @param {string} subject
 * @param options
 * @param shared
 */
function handleCheckMessages(cm, [html], options, shared) {
	if (!canViewChatMessage(cm, shared.user)) return;

	const metadata = cm.system;
	const subject = metadata.subject;
	if (!subject) return;

	const actor = ChatMessage.getSpeakerActor(cm.speaker);

	const roll = cm.rolls[0],
		check = roll ? roll.total : 0,
		tags = [];

	const skillId = subject.skill,
		abilityId = subject.ability,
		saveId = subject.save,
		otherId = subject.core;

	// const chatCfg = game.settings.get(CFG.id, CFG.SETTINGS.chat);

	if (skillId) handleSkillCheck({ skillId, tags, check, cm, actor });
	if (abilityId) handleAbilityCheck({ abilityId, tags, check, cm, actor });
	if (saveId) handleSaveCheck({ saveId, tags, check, cm, actor });
	if (otherId) handleOtherCheck({ otherId, tags, check, cm, actor });

	Hooks.call('little-helper.checks.hints', tags, { subject, cm, roll, result: check, element: html, cls: Tag });

	if (tags.length > 0) appendCustomTags(html, tags);
}

function enable() {
	Ancillary.register('chat', handleCheckMessages);
	return false;
}

function disable() {
	Ancillary.unregister('chat', handleCheckMessages);
	return false;
}

new Feature({ setting: 'card-hints', category: 'chat-card', enable, disable, stage: 'init', stream: true });
