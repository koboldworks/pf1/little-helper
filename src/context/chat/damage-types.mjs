// DISABLED UNDER FARTHER NOTICE

import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

class Damage {
	key = null;
	label = null;
	match = null;
	icon = null;
	color = null;
	glow = null;

	constructor(key, label, match, icon = null, color = null, glow = null) {
		this.key = key;
		this.label = label;
		this.match = match;
		this.icon = icon;
		this.color = color;
		this.glow = glow;
	}
}

// TODO: i18n support... somehow
const DamageType = {
	fire: new Damage('fire', 'Fire', /\b(?:fire|flame|flaming)\b/i, ['ra', 'ra-fire-symbol'], '#efc178', 'orange'), // 'fas fa-fire-alt' // 'fas fa-meteor'
	cold: new Damage('cold', 'Cold', /\b(?:cold|frost|freezing)\b/i, ['ra', 'ra-snowflake'], '#65ffff', 'white'),
	acid: new Damage('acid', 'Acid', /\bacid\b/i, ['ra', 'ra-acid'], 'greenyellow', 'lime'),
	electric: new Damage('electric', 'Electric', /\b(?:electric|electricity|electrifying|lightning)\b/i, ['ra', 'ra-lightning-storm'], 'lightskyblue', '#6475ff'),
	sonic: new Damage('sonic', 'Sonic', /\bsonic\b/i, ['ra', 'ra-horn-call'], 'gold', '#64ffb3'), // 'ra ra-ringing-bell' // 'fas fa-music' // 'ra ra-horn-call'
	bludgeon: new Damage('bludgeon', 'Bludgeon', /\b(?:b|bludgeon|bludgeoning|blunt)\b/i, ['ra', 'ra-flat-hammer'], 'white', 'rgba(255,0,0,0.8)'),
	piercing: new Damage('piercing', 'Piercing', /\b(?:p|pierce|piercing)\b/i, ['ra', 'ra-spear-head'], 'white', 'rgba(255,0,0,0.8)'),
	slashing: new Damage('slashing', 'Slashing', /\b(?:s|slashing|slash|cutting)\b/i, ['ra', 'ra-sword'], 'white', 'rgba(255,0,0,0.8)'),
	precision: new Damage('precision', 'Precision', /\b(?:precision|sneak attack)\b/i, ['ra', 'ra-on-target'], '#ff0', 'white'),
	magic: new Damage('magic', 'Magic', /\bmagic\b/i, ['ra', 'ra-fairy-wand'], 'fuchsia', 'cyan'), // '#a155ff',
	force: new Damage('magic', 'Force', /\bforce\b/i, ['ra', 'ra-focused-lightning'], '#ff8ff1', 'cyan'),
	untyped: new Damage('untyped', 'Untyped', /\buntyped\b/i, ['fas', 'fa-question-circle'], 'white', 'yellow'),
	healing: new Damage('healing', 'Healing', /\b(?:heal|healing)\b/, ['ra', 'ra-health'], 'orangered', 'pink'), // 'fas fa-first-aid'
	positive: new Damage('positive', 'Positive Energy', /\bpositive\b/i, ['fas', 'fa-seedling'], 'white', 'lime'), // 'fas fa-seedling' // 'ra ra-regeneration'
	negative: new Damage('negative', 'Negative Energy', /\bnegative\b/i, ['fas', 'fa-skull'], 'white', 'lime'),
	bleed: new Damage('bleed', 'Bleed', /\b(?:bleed|bleeding)\b/i, ['ra', 'ra-droplet'], '#fb4a4a', 'white'),
	splash: new Damage('splash', 'Splash', /\bsplash\b/i, ['ra', 'ra-bomb-explosion'], 'orange', 'yellow'),
};

const typeDelim = /\b(?:\/|or|and)\b/gi;
const delimExact = /^(?:\/|or|and)$/i;
function identifyType(text) {
	const strings = text.trim().toLowerCase().split(typeDelim);
	const rv = [];
	// console.log(text, strings);
	for (const str of strings) {
		if (delimExact.test(str)) continue; // skip delimiters themselves.
		// console.log(str);
		for (const d of Object.values(DamageType)) {
			// console.log(str, "===", d.key, d.match);
			if (d.match.test(str)) rv.push(d);
		}
	}
	if (rv.length) return rv;
	return [DamageType.untyped];
}

/**
 * @param {Element} el
 */
function insertSymbol(el) {
	const types = identifyType(el.textContent);
	// console.log("Type:", types.map(t => t.label).join(', '));
	const f = new DocumentFragment();
	for (const type of types) {
		if (el.textContent.length === 1) el.textContent = type.label;
		if (type.icon) {
			const icon = document.createElement('i');
			icon.classList.add(...type.icon, 'lil-damage-type');
			if (type.color?.length > 0) icon.style.color = `${type.color}`;
			if (type.glow?.length > 0) icon.style.textShadow = `0 0 5px ${type.glow}`;
			f.append(icon);
		}
	}
	el.append(f);
}

/**
 * Discard bad things. Accept only every second TD.
 *
 * @param {number} index
 * @param {Element} el
 */
function acceptInstance(index, el) {
	// Identify table with [roll] | [type] format
	return (index + 1) % 2 === 0 && // Only every second cell
		el?.innerText?.trim().length > 0 && // Only cells with immediate text
		el.querySelector('.inline-roll') == null; // No inline roll
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function insertDamageTypes(cm, [html]) {
	const item = cm.itemSource;
	const hasDamage = cm.flags?.pf1?.metadata?.rolls?.attacks?.[0]?.damage?.[0] !== undefined;
	if (!(hasDamage || item?.hasDamage)) return;

	html.querySelectorAll('.chat-attack tr')
		.forEach(tr => tr.querySelectorAll('td')
			.forEach((el, index) => {
				if (el.colSpan > 1) return;
				if (acceptInstance(index, el))
					insertSymbol(el);
			}));
}

let preloadLink;
function preload(url) {
	if (preloadLink) return;
	const link = document.createElement('link');
	link.rel = 'prefetch';
	link.href = url;
	link.as = 'font';
	preloadLink = link;
	document.head.append(link);
}

function enable() {
	Ancillary.register('chat', insertDamageTypes);
	preload(`modules/${CFG.id}/css/ra/fonts/rpgawesome-webfont.woff2`);
}

function disable() {
	Ancillary.unregister('chat', insertDamageTypes);
	return false;
}

new Feature({ setting: 'card-damage-types', label: 'Damage Type Icons', hint: 'Display icons corresponding to damage types on damage.', category: 'chat-card', enable, disable, stage: 'init', conflict: { system: '0.81.0' } });
