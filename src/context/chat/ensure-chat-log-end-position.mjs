/*
 * Attempt to rectify chat card modifications twitching the chat log scroll position at start.
 */

import { CFG } from '@root/config.mjs';

function scrollToEnd() {
	// Scroll to end
	const log = document.getElementById('chat-log');
	if (!log) return;
	const position = log.clientHeight + log.scrollTop;
	// const distance = log.scrollHeight - position;
	const target = log.scrollHeight;
	if (position == target) return;
	const C = CFG.console.colors;
	console.log(`%cLittle Helper%c 🦎 | Scrolling from %c${position}%c to %c${target}%c`,
		C.main, C.unset, C.number, C.unset, C.number, C.unset);
	log.scrollTo(0, target);

	// Foundry method, works less reliably despite being almost identical.
	// ui.chat?.scrollBottom();
}

Hooks.once('ready', () => setTimeout(scrollToEnd, 1000)); // attempt to delay scrolling arbitrarily but not too much
