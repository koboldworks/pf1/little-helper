/**
 * Adds formula display on top of the inline roll expansion.
 */

import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { compressFormula, createNode } from '@root/common.mjs';

function inlineFormulaDetails(node, _, data) {
	const formula = compressFormula(data.roll._formula, !game.settings.get(CFG.id, CFG.SETTINGS.inlineFormulaDetail))
		.replace(/([)\d])\s*([+*\-/])\s*/g, (_, m1, m2) => `${m1} ${m2} <wbr>`); // Ensure line-breaks
	const formulaEl = createNode('div', formula, ['inline-formula', 'lil-dice-tooltip-source'], { isHTML: true });
	node.prepend(formulaEl, document.createElement('hr'));
}

function enable() {
	Hooks.on('little-helper.cm.dice-tooltip', inlineFormulaDetails);
}

function disable() {
	Hooks.off('little-helper.cm.dice-tooltip', inlineFormulaDetails);
}

new Feature({ setting: 'inline-roll-formula', label: 'Inline Roll Formula', hint: 'Display formula at top of inline roll expanded view in chat log.', category: 'chat-card', enable, disable, stage: 'init' });
