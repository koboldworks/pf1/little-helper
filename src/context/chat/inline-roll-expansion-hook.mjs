// Common mutation observer for chat message roll expanded view.

/**
 * @param {Node} node
 * @param {MutationRecord} mutation
 */
function mutationNodeHandler(node, mutation) {
	if (!node.classList?.contains('dice-tooltip')) return;

	const parentEl = mutation.target;
	const rollData = JSON.parse(unescape(parentEl.dataset.roll));
	const roll = RollPF.fromData(rollData);
	const formula = parentEl.title; // roll.formula instead maybe?
	const data = { roll, rollData, formula };

	// Call anything listening to us.
	Hooks.callAll('little-helper.cm.dice-tooltip', node, parentEl, data);
}

/**
 * @param {MutationRecord} mutation
 */
function mutationListHandler(mutation) {
	if (mutation.type !== 'childList') return;
	mutation.addedNodes.forEach(node => mutationNodeHandler(node, mutation));
}

/**
 * @param {MutationRecord[]} mutations
 */
function mutationCallback(mutations) {
	mutations.forEach(mutationListHandler);
}

const observerConfig = { subtree: true, childList: true, attributes: false };
const observer = new MutationObserver(mutationCallback);

function initMutationObserver() {
	const chatLog = document.getElementById('chat-log');
	// observer.disconnect(); // Would break with chat log popout
	observer.observe(chatLog, observerConfig);
}

Hooks.once('init', () => {
	Hooks.on('renderChatLog', initMutationObserver);
});
