/*
 * Makes inline rolls show how good they rolled like attack rolls.
 */

import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

/**
 * @param {Element} el
 * @param {Roll} roll
 */
function enhanceInlineRoll(el, roll) {
	el.classList.add('lil-roll-restyle');

	if (roll.dice.length === 0) {
		el.classList.add('diceless');
		return;
	}
	// console.log(roll);

	let tMin = 0, tMax = 0, tRatio = 0;
	// const tpMin = 0, tpMax = 0, tpAvg = 0;

	let dice = 0;

	for (const d of roll.dice) {
		let min = 0, max = 0, ratio = 0;
		// tpMin += pMin; tpMax += pMax; tpAvg += pAvg;
		for (const r of d.results) {
			if (!r.active) continue;
			dice++;
			if (r.result == 1) min++;
			else if (r.result == d.faces) max++;
			ratio += r.result;
		}
		const pMin = dice, pMax = d.number * d.faces, pAvg = Math.floor((pMax + pMin) / 2 * 10) / 10;
		ratio /= dice;
		ratio = Math.floor(ratio / d.faces * 1000) / 1000;
		// console.log(d.formula, d.total, { pMin, pMax, pAvg }, { min, max, ratio });
		tMin += min; tMax += max; tRatio += ratio;
	}

	// const _avg = (v) => Math.floor(v / dice * 100) / 100;

	// tpMin = _avg(tpMin);
	// tpMax = _avg(tpMax);
	// tpAvg = _avg(tpAvg);
	// tMin = _avg(tMin);
	// tMax = _avg(tMax);
	tRatio = Math.floor(tRatio / roll.dice.length * 100) / 100;
	// console.log({ tMin, tMax, tRatio }, { tpMin, tpMax, tpAvg }, { dice });

	// Enrich with classes
	if (tMin > 0) el.classList.add('has-min-roll');
	if (tMax > 0) el.classList.add('has-max-roll');
	if (tMin === dice) el.classList.add('minimum-roll' /* , 'failure'*/);
	if (tMax === dice) el.classList.add('maximum-roll' /* , 'success'*/);

	if (tRatio > 0.9) el.classList.add('great-roll');
	if (tRatio > 0.75) el.classList.add('good-roll');
	else if (tRatio > 0.5) el.classList.add('decent-roll');
	if (tRatio < 0.1) el.classList.add('horrible-roll');
	if (tRatio < 0.25) el.classList.add('bad-roll');
	else if (tRatio < 0.5) el.classList.add('poor-roll');
	if (tRatio < 0.6 && tRatio > 0.4) el.classList.add('middling-roll');
	// console.log(Array.from(el.classList));

	el.dataset.rollGrade = Math.round(tRatio * 10);

	// Record natural roll
	const firstTerm = roll.terms[0];
	if (firstTerm.constructor.name === 'Die') {
		const results = firstTerm.results.filter(r => r.active);
		if (results.length === 1) {
			const natural = results[0].result;
			el.dataset.rollNatural = natural;
		}
	}
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function inlineRollEnricher(cm, [html]) {
	html.querySelectorAll('a.inline-roll.inline-result')
		.forEach(el => {
			const json = JSON.parse(unescape(el.dataset.roll));
			if (!json) return; // void console.warn('Missing roll data:', { element: el, message: cm });
			try {
				enhanceInlineRoll(el, Roll.fromData(json));
			}
			catch (err) {
				console.error({ json, element: el, message: cm }, err);
			}
		});
}

function enable() {
	Ancillary.register('chat', inlineRollEnricher);
}

function disable() {
	Ancillary.unregister('chat', inlineRollEnricher);
}

new Feature({
	setting: 'inline-roll-grading', label: 'Inline Roll Grading', hint: 'Grades inline rolls with how well or poorly they went.', category: 'chat-card', enable, disable, stage: 'init', stream: true,
});
