// Meld damage inline-rolls

import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function meldDamageRolls(cm, [html]) {
	const item = cm.itemSource;

	const metadata = cm.system;
	if (!metadata) return;

	const hasDamage = metadata.rolls?.attacks?.[0]?.damage?.[0] !== undefined;
	if (!(hasDamage || item?.hasDamage)) return;

	// Rows
	html.querySelectorAll('.chat-attack tr')
		// Cells
		.forEach(tr => tr.querySelectorAll('td')
			.forEach((el, index) => {
				if ((index + 1) % 2 === 0) return;
				if (el.colSpan > 1) return; // skip attack rolls
				const ir = el.querySelector('.inline-roll,span');
				const next = el.nextElementSibling;
				if (ir) {
					ir.classList.add('lil-melded-roll');
					el.classList.add('lil-melded-type');
					if (next) {
						const noType = next.innerText.length === 0;
						el.append(' ', ...el.nextElementSibling.childNodes);
						el.colSpan = 2;
						if (noType) el.append(i18n.get('PF1.Unknown'));
					}
				}
				else {
					// Meld empty cell pairs
					if (el.innerText === '' && next.innerText === '')
						el.colSpan = 2;
				}
				next?.remove();
			}));
}

function enable() {
	Ancillary.register('chat', meldDamageRolls, { priority: -100 });
}

function disable() {
	Ancillary.unregister('chat', meldDamageRolls);
	return false;
}

new Feature({ setting: 'card-damage-melding', label: 'Meld Damage & Type', hint: 'Combines damage and type into single cell.', category: 'chat-card', enable, disable, stage: 'init', stream: true });
