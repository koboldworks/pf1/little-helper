import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { canViewChatMessage, createNode } from '@root/common.mjs';

/**
 * @param {MouseEvent} ev
 */
function rollAgain(ev) {
	ev.preventDefault();
	ev.stopPropagation();

	const el = this;

	const skillId = el.dataset.skillId,
		abilityId = el.dataset.abilityId,
		saveId = el.dataset.saveId;

	const roll = skillId ? 'skill' : abilityId ? 'ability' : saveId ? 'save' : null;
	if (!roll) {
		ui.notifications.warn('Undefined roll received.');
		console.warn('ROLL | Undefined roll received');
	}

	let actors = canvas.tokens.controlled.map(t => t.actor).filter(a => a != null);
	if (actors.length === 0 && game.user.character) actors = [game.user.character];
	if (actors.length == 0) return ui.notifications.warn('No valid actor selected for rolling.');

	const isShift = ev.shiftKey;

	const skipDialog = game.settings.get('pf1', 'skipActionDialogs') && !isShift ||
		!game.settings.get('pf1', 'skipActionDialogs') && isShift;

	for (const a of actors) {
		if (skillId)
			a.rollSkill(skillId, { skipDialog });
		else if (abilityId)
			a.rollAbilityTest(abilityId, { skipDialog });
		else if (saveId)
			a.rollSavingThrow(saveId, { skipDialog });
	}
}

/**
 * @param {ChatMessage} cm
 * @param {Element} html
 * @param {string} subject
 */
function handleCMbySubject(cm, html, subject) {
	if (!cm.isContentVisible) return;

	const skillId = subject.skill,
		abilityId = subject.ability,
		saveId = subject.save,
		otherId = subject.core;

	/** Add me-too roller */
	// TODO: Make the button listener global to the chatlog
	if (skillId || abilityId || saveId) {
		const button = createNode('span', 'Roll', ['lil-roll-button']);
		if (skillId) button.dataset.skillId = skillId;
		if (abilityId) button.dataset.abilityId = abilityId;
		if (saveId) button.dataset.saveId = saveId;
		if (otherId) button.dataset.coreId = otherId;
		button.prepend(createNode('i', null, ['fas', 'fa-dice']));
		button.addEventListener('click', rollAgain);

		html.querySelector('.flavor-text')
			?.after(button, createNode('div', null, ['kbwks-flow-break']));
	}
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 * @param {object} options
 * @param {object} shared
 */
function handleMessage(cm, [html], options, shared) {
	if (!canViewChatMessage(cm, shared.user)) return;

	const subject = cm.system?.subject;
	if (subject) handleCMbySubject(cm, html, subject);
}

function enable() {
	Ancillary.register('chat', handleMessage);
	return false;
}

function disable() {
	Ancillary.unregister('chat', handleMessage);
	return false;
}

new Feature({ setting: 'quick-roller', category: 'chat-card', enable, disable, stage: 'init' });
