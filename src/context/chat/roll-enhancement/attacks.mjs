/**
 * Expands inline rolls for attacks to show natural rolls.
 */

import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { signNum, createNode, canViewChatMessage } from '@root/common.mjs';

function enhanceAttackRoll(el, roll, total) {
	/** Crushes die rolls to dust: Tries to simplify the input. */
	const rolls = roll.dice.length !== 1 ?
		[`${roll.terms[0].total}`] :
		roll.dice.map(d => `${d.values.join(', ')}`);

	const diceTotal = roll.dice.length === 1 ? roll.dice.reduce((a, r) => a + r.total, 0) : roll.terms[0].total;
	const hasDice = roll.dice.length > 0;
	const bonus = roll.total - diceTotal;

	// Normal roll
	if (hasDice) {
		const rollEl = createNode('span', rolls.join('+'), ['lil-roll', 'roll', 'die', 'd20']);
		const bonusEl = createNode('span', signNum(bonus), ['lil-bonus']),
			totalEl = createNode('span', total, ['lil-total']);
		el.append(rollEl, bonusEl, createNode('span', '⇒', ['lil-arrow']), totalEl);
	}
	// Obfuscated roll
	else {
		const rollEl = createNode('span', '?', ['lil-roll', 'roll', 'die', 'd20']);
		const bonusEl = createNode('span', '+?', ['lil-bonus']),
			totalEl = createNode('span', total, ['lil-total']);
		el.append(rollEl, bonusEl, createNode('span', '⇒', ['lil-arrow']), totalEl);
	}

	el.classList.add('lil-enhanced-roll');
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function attackRollEnricher(cm, [html]) {
	if (!cm.isContentVisible) return;

	// const item = cm.itemSource;
	const metadata = cm.system;
	const attacks = metadata?.rolls?.attacks;
	const hasAttacks = attacks?.length > 0;
	// const hasDamage = attacks?.[0]?.damage?.[0] !== undefined;
	if (!hasAttacks) return;

	if (!canViewChatMessage(cm)) return;

	// Get only main attack rolls
	/** @type {Element[]} */
	const ir = html.querySelectorAll('.chat-attack td[colspan] .inline-roll.inline-result[data-roll]');

	for (const irEl of ir) {
		if (irEl.classList.contains('defense-dc')) return;

		try {
			irEl.classList.add('lil-expanded-inline-roll', 'lil-roll-restyle');

			const faIcon = irEl.firstElementChild;
			const faText = faIcon?.nextSibling; // text node after child <i>
			if (!faText) continue;

			enhanceAttackRoll(irEl, Roll.fromJSON(unescape(irEl.dataset.roll)), faText.textContent?.trim());
			faIcon.remove();
			faText.remove();
		}
		catch (err) {
			console.error(err);
		}
	}
}

function enable() {
	Ancillary.register('chat', attackRollEnricher);
}

function disable() {
	Ancillary.unregister('chat', attackRollEnricher);
}

new Feature({
	setting: 'attack-roll-enhancer', label: 'Enrich Attack Rolls', hint: 'Enriches display of attack rolls.', category: 'chat-card', enable, disable, stage: 'init', stream: true,
});
