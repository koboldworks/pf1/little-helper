import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { createNode } from '@root/common.mjs';

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function injectSenderTag(cm, [html]) {
	const user = cm.author ?? cm.user;

	const div = createNode('div', '', ['lil-sender-tag']);
	const label = createNode('label', ' ', ['marker']);
	div.append(label);
	html.prepend(div);

	if (user?.isGM) label.classList.add('gm');
	if (game.user.id === user?.id) label.classList.add('self');
}

function enable() {
	Ancillary.register('chat', injectSenderTag);
}

function disable() {
	Ancillary.unregister('chat', injectSenderTag);
}

new Feature({ setting: 'sender-marker', label: 'Sender Marker', hint: 'Displays little marker to identify chat card sender as yourself, GM, or other players.', category: 'chat-card', enable, disable, stage: 'init' });
