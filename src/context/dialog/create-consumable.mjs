import { Feature } from '@core/feature.mjs';
import { createNode, maxPrecision, clampNum } from '@root/common.mjs';
import { hookCoreTooltip } from '@core/tooltip.mjs';
import { i18n } from '@root/utility/i18n.mjs';

class Pricing {
	static forWand(sl, cl, mcost = 0, uses = 50) {
		return 750 * clampNum(sl, 0.5, 9) * clampNum(cl, 1, 100) + (mcost * uses);
	}

	static forWandFormula() {
		return '750 * @sl * @cl + (@materials * @uses)';
	}

	static forPotion(sl, cl, mcost = 0) {
		return 50 * clampNum(sl, 0.5, 9) * clampNum(cl, 1, 100) + mcost;
	}

	static forPotionFormula() {
		return '50 * @sl * @cl + @materials';
	}

	static forScroll(sl, cl, mcost = 0) {
		return 25 * clampNum(sl, 0.5, 9) * clampNum(cl, 1, 100) + mcost;
	}

	static forScrollFormula() {
		return '25 * @sl * @cl + @materials';
	}

	static forScribing(sl, _cl) {
		return Math.max(5, Math.pow(sl, 2) * 10);
	}

	static forScribingFormula() {
		return '@sl ^ 2 * 10';
	}
}

/**
 * @param {Dialog} app
 */
function enrichCreateConsumable(app) {
	if (!app.options.classes.includes('create-consumable')) return;

	const html = app.element.querySelector('form');

	const allowScribing = !!app.options.buttons.spell;

	const content = html.querySelector('.dialog-content'),
		buttons = html.querySelector('footer');

	const d = createNode('div', null, ['flexrow', 'lil-consumable-cost']);
	const scribeCost = createNode('span', '0', ['value']),
		potionCost = createNode('span', '0', ['value']),
		scrollCost = createNode('span', '0', ['value']),
		wandCost = createNode('span', '0', ['value']);

	const bPot = buttons.querySelector('button[data-action="potion"]'),
		bScrl = buttons.querySelector('button[data-action="scroll"]'),
		bWand = buttons.querySelector('button[data-action="wand"]'),
		bSpl = buttons.querySelector('button[data-action="spell"]');

	const inSL = content?.querySelector('input[name="sl"]'),
		inCL = content?.querySelector('input[name="cl"]');

	const itemData = app.options?.itemData ?? {};

	// Material cost
	const mcost = Number(itemData.system?.materials?.gpValue || 0);

	let cl = 0, sl = 0;
	function updateValues() {
		cl = parseInt(inCL.value) || 1;
		sl = parseInt(inSL.value);
		if (!Number.isFinite(sl)) sl = 1;

		potionCost.textContent = `${Pricing.forPotion(sl, cl, mcost)}`;
		scrollCost.textContent = `${Pricing.forScroll(sl, cl, mcost)}`;
		const wcost = Pricing.forWand(sl, cl, mcost);
		wandCost.textContent = `${wcost}`;
		scribeCost.textContent = `${Pricing.forScribing(sl, cl)}`;

		const notPot = sl > 3;
		bPot.classList.toggle('impossible', notPot);
	}

	const scribeCostL = createNode('h4', ' gp', ['scroll', 'cost', 'type']),
		potionCostL = createNode('h4', ' gp', ['potion', 'cost', 'type']),
		wandCostL = createNode('h4', ' gp', ['wand', 'cost', 'type']),
		scrollCostL = createNode('h4', ' gp', ['scribe', 'cost', 'type']);

	hookCoreTooltip(potionCostL, { fn: () => Pricing.forPotionFormula() });
	if (allowScribing) hookCoreTooltip(scribeCostL, { fn: () => i18n.get('LittleHelper.Scribing') + '<br>' + Pricing.forScribingFormula() });
	const wcostChL = i18n.get('PF1.PricePerCharge');
	hookCoreTooltip(wandCostL, {
		fn: () => {
			const wcost = Pricing.forWand(sl, cl, mcost);
			return Pricing.forWandFormula() + `<br>${wcostChL}: ${maxPrecision(wcost / 50, 2)}`;
		},
	});

	hookCoreTooltip(scrollCostL, { fn: () => Pricing.forScrollFormula() });
	scrollCostL.prepend(scrollCost);

	if (allowScribing) scribeCostL.prepend(scribeCost);
	potionCostL.prepend(potionCost);
	wandCostL.prepend(wandCost);

	if (mcost > 0) {
		const mcostEl = createNode('div', null, ['lil-material-cost']);
		mcostEl.classList.add('form-group');
		mcostEl.append(
			createNode('label', 'Material Cost'),
			createNode('span', `${mcost} gp`, ['value']),
		);
		content.append(mcostEl);
	}

	d.append(potionCostL, scrollCostL, wandCostL);
	if (allowScribing) d.append(scribeCostL);
	content.append(d);

	[inSL, inCL].forEach(el => el.addEventListener('change', updateValues, { passive: true }));

	updateValues();

	// Resize to fit modifications
	setTimeout(() => app.setPosition({ height: 'auto' }), 100);
}

function enable() {
	Hooks.on('renderDialogV2', enrichCreateConsumable);
}

function disable() {
	Hooks.off('renderDialogV2', enrichCreateConsumable);
}

new Feature({ setting: 'enrich-create-consumable', label: 'Enrich Create Consumable', hint: 'Displays expected item cost in the consumable creation dialog.', category: 'dialog', enable, disable, stage: 'ready' });
