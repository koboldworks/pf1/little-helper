import { Feature } from '@core/feature.mjs';
import { createNode, signNum } from '@root/common.mjs';

function getSources(actor, abl) {
	return pf1.documents.actor.changes
		.getHighestChanges(actor.changes.filter(c => c.subTarget === abl), { ignoreTarget: true })
		.sort((a, b) => a.priority - b.priority);
}

/**
 * @param {Application} app
 * @param {JQuery} html
 * @param {object} options
 */
function enrichPointBuy(app, [html], options) {
	const abilities = {};

	const actor = app.actor;

	const form = html.matches('form') ? html : html.querySelector('form');
	form.classList.add('lil-enriched-pointbuy');

	function updateTotal(d) {
		let bonus = 0;
		d.sources.forEach(c => {
			if (c.operator !== 'add') return;
			bonus += c.value;
		});
		const total = d.base + bonus;
		d.bonus.textContent = signNum(bonus);
		d.total.textContent = `${total}`;
		const mod = pf1.utils.getAbilityModifier(total);
		d.mod.textContent = signNum(mod);
	}

	function updateTotalDebounce(abl) {
		const d = abilities[abl];
		clearTimeout(d.t);
		d.t = setTimeout(() => updateTotal(d), 200);
	}

	html.querySelectorAll('li[data-ability]').forEach(el => {
		const abl = el.dataset.ability;
		const d = abilities[abl] = {
			key: abl,
			data: actor.system.abilities[abl],
			get base() {
				return app.abilities.find(i => i.key === abl).value;
			},
			li: el,
			bonus: createNode('span', '0', ['lil-ability-bonus'], { tooltip: 'Bonuses' }),
			total: createNode('span', '00', ['lil-ability-total'], { tooltip: 'Total' }),
			mod: createNode('span', '0', ['lil-ability-mod'], { tooltip: 'Modifier' }),
			t: null,
			get sources() {
				return getSources(actor, abl);
			},
		};

		const abs = createNode('div', null, ['ability-setup']);
		const v = el.querySelector('.value');

		v.previousElementSibling.before(abs);
		abs.append(
			v.previousElementSibling,
			v,
			v.nextElementSibling,
		);

		el.querySelectorAll('.ability-control')
			.forEach(c => c.addEventListener('click', ev => updateTotalDebounce(abl)), { passive: true });

		const final = createNode('div', null, ['lil-ability-total-holder']);
		final.append(d.total, d.mod);
		el.append(d.bonus, final);

		updateTotal(abilities[abl]);
	});
}

function enable() {
	Hooks.on('renderPointBuyCalculator', enrichPointBuy);
}

function disable() {
	Hooks.off('renderPointBuyCalculator', enrichPointBuy);
}

new Feature({ setting: 'pointbuy-enricher', label: 'Point Buy Enricher', hint: 'Enriches point buy calculator with currently active bonuses and displays final ability score and its bonus.', category: 'dialog', enable, disable, stage: 'ready' });
