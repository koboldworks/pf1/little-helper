import { CFG } from '@root/config.mjs';
import { Feature } from '@root/core/feature.mjs';
import { i18n } from '@root/utility/i18n.mjs';
import { formatNum } from '@root/common.mjs';

class XPChartDialog extends Application {
	constructor(app, options) {
		super(options);

		this.configDialog = app;

		this._onRender = this.render.bind(this);
		Hooks.on('renderExperienceConfig', this._onRender);
	}

	close(...args) {
		Hooks.off('renderExperienceConfig', this._onRender);
		super.close(...args);
	}

	get template() {
		return `modules/${CFG.id}/template/dialog/xp-chart.hbs`;
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			title: i18n.get('LittleHelper.XPTable.Short'),
			id: 'lil-xp-chart',
			classes: [...options.classes, 'pf1', 'lil-xp-chart'],
			height: 'auto',
			width: 'auto',
		};
	}

	getData() {
		let html = this.configDialog.element;
		if (html instanceof jQuery) html = html[0];

		const track = html.querySelector('select[name="track"]').value;
		const custom = html.querySelector('input[name="custom.formula"]')?.value || '';

		let trackXp = pf1.config.CHARACTER_EXP_LEVELS[track]?.map(i => ({ total: i }));

		if (trackXp) {
			// Calculate XP increment
			for (let i = 19; i >= 0; i--) {
				const entry = trackXp[i];
				const prev = trackXp[i - 1] ?? { total: 0, value: 0 };
				entry.value = entry.total - prev.total;
			}
		}
		// custom
		else {
			trackXp = Array.fromRange(20).map(idx => ({ value: RollPF.safeRollSync(custom || '0', { level: idx + 1 }).total }));
			trackXp[0].value = 0; // Level 1 does not require XP
			// Calculate cumulative XP
			for (let i = 0; i < 20; i++) {
				const prev = trackXp[i - 1] ?? { total: 0 };
				const cur = trackXp[i];
				cur.total = prev.total + cur.value;
			}
		}

		// Calculate percentage
		for (let i = 1; i < 20; i++) {
			const t = trackXp[i];
			t.level = i + 1;
			const p = trackXp[i - 1] ?? { value: 0 };
			t.pct = pf1.utils.limitPrecision((t.value / p.value) * 100 - 100, 1);
			if (!Number.isFinite(t.pct)) t.pct = null;
		}

		trackXp.splice(0, 1); // Remove level which has no interesting data

		// Pretty format numbers
		for (const t of trackXp) {
			t.total = formatNum(t.total);
			t.value = formatNum(t.value);
		}

		return {
			track: trackXp,
		};
	}
}

function injectXPChart(app, html) {
	if (html instanceof jQuery) html = html[0];

	const track = html.querySelector('select[name="track"]');

	const i = document.createElement('i');
	i.classList.add('fa-solid', 'fa-table', 'fa-fw', 'lil-xp-chart');
	i.dataset.action = 'open-xp-chart';
	i.dataset.tooltip = 'LittleHelper.XPTable.Title';
	track.before(i);

	i.addEventListener('click', (ev) => {
		ev.preventDefault();

		const xpcapp = new XPChartDialog(app);
		xpcapp.render(true);
	});
}

function enable() {
	Hooks.on('renderExperienceConfig', injectXPChart);
}

function disable() {
	Hooks.off('renderExperienceConfig', injectXPChart);
}

new Feature({ setting: 'xp-chart', category: 'dialog', enable, disable, stage: 'ready' });
