import { createNode } from '@root/common.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { i18n } from '@root/utility/i18n.mjs';

async function exportDocument(doc) {
	return doc.exportToJSON();
}

/**
 * @param {Element} el
 * @param {Item|Actor} doc
 */
function appendExportButton(el, doc) {
	const button = createNode('button', null, ['lil-export']);
	button.append(createNode('i', null, ['fas', 'fa-save']), ' ', i18n.get('LittleHelper.Action.ExportJSON'));
	button.style.whiteSpace = 'nowrap';
	button.type = 'button';

	button.addEventListener('click', function (event) {
		event.preventDefault();
		/** @type {Element} */
		const bel = this;
		bel.disable = true;
		exportDocument(doc)
			.then(_ => bel.disable = false);
	});

	el.append(button);
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function injectExportActorSheet(sheet, [html], options, shared) {
	const tab = shared.tabs.settings?.tab;
	if (!tab) return;

	const actor = sheet.document;

	const div = createNode('div', null, ['form-group']);
	div.style.flexDirection = 'column';
	const h2 = createNode('h2');
	h2.style.width = '100%';
	h2.append(createNode('i', null, ['fas', 'fa-puzzle-piece']), ' ', i18n.get('LittleHelper.Action.Actions'));

	div.append(h2);
	appendExportButton(div, actor);

	tab.append(div);
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function injectExportItemSheet(sheet, [html], options, shared) {
	const adv = html.querySelector('.tab[data-tab="advanced"] .lil-advanced-options');
	if (!adv) return;

	appendExportButton(adv, sheet.document);
}

function enable() {
	Ancillary.register('actor', injectExportActorSheet);
	Ancillary.register('item', injectExportItemSheet);
}

function disable() {
	Ancillary.unregister('actor', injectExportActorSheet);
	Ancillary.unregister('item', injectExportItemSheet);
}

new Feature({ setting: 'easy-export', label: 'Easy Export', hint: 'Actor and Item sheets have easy export button in settings or advanced tab.', category: 'dev', enable, disable, stage: 'ready', enabledByDefault: false });
