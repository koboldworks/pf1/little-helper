// Data lists for various inputs

import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

const addDatalistSource = (el, listId) => {
	if (!el.getAttribute('list'))
		el.setAttribute('list', listId);
};

/**
 * @param {Element[]} el
 * @param f_input
 * @param item
 */
function handleInput(el, item) {
	if (el.matches('[name="powerAttack.multiplier"]'))
		addDatalistSource(el, 'lil-power-attack-mult');
}

/**
 * @param {Element[]} el
 * @param item
 */
function handleFormulaInputs(el, item) {
	if (el.matches('[name="system.formulaicAttacks.count.formula"],[name="formulaicAttacks.count.formula"]'))
		addDatalistSource(el, 'lil-premade-fac-list');
	else if (el.matches('[name="system.formulaicAttacks.bonus.formula"],[name="formulaicAttacks.bonus.formula"]'))
		addDatalistSource(el, 'lil-premade-fab-list');
	else if (el.matches('[name="system.attackBonus"],[name="attackBonus"]'))
		addDatalistSource(el, 'lil-premade-ab-list');
	else if (el.matches('[name="system.critConfirmBonus"],[name="critConfirmBonus"]'))
		addDatalistSource(el, 'lil-premade-cf-list');
	else if (el.matches('.damage-formula'))
		addDatalistSource(el, 'lil-premade-damage-formula-list');
	else if (el.matches('[name="system.save.dc"],[name="save.dc"]'))
		addDatalistSource(el, item.type === 'spell' ? 'lil-premade-spell-dc-list' : 'lil-premade-dc-list');
	else if (el.matches('[name="system.customHD"]'))
		addDatalistSource(el, 'lil-class-hd');
}

/**
 * @param {ItemSheetPF} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function inputQuickAccessInjection(sheet, [html], options, shared) {
	const item = shared.document;
	/** @type {HTMLInputElement[]} */
	const inputs = html.querySelectorAll('input');
	for (const f_input of inputs) {
		if (f_input.classList.contains('formula'))
			handleFormulaInputs(f_input, item);
		else
			handleInput(f_input, item);
	}
}

/**
 * @param {Dialog} dialog
 * @param {JQuery} html
 * @param {object} options
 */
function inputDialogQuickAccessInjection(dialog, [html]) {
	const subject = dialog.options.subject;
	if (!subject && !(dialog instanceof pf1.applications.AttackDialog))
		return;

	if (!dialog.options.classes.includes('pf1')) return;

	const inputs = html.querySelectorAll('input[name]');

	for (const el of inputs) {
		switch (el.name) {
			case 'd20': addDatalistSource(el, 'lil-premade-roll-list'); break;
			case 'attack-bonus': addDatalistSource(el, 'lil-premade-attack-bonus-list'); break;
			case 'bonus': addDatalistSource(el, 'lil-premade-roll-bonus-list'); break;
		}
	}
}

const dataSourceTemplate = `modules/${CFG.id}/template/data-sources.hbs`;

async function injectDataSources() {
	const data = {
		config: pf1.config,
		registry: {
			damageTypes: pf1.registry.damageTypes.getLabels(),
		},
		extra: {
			damageTypes: ['Healing', 'Bleed'],
		},
	};

	const fragment = document.createElement('template');
	fragment.innerHTML = await renderTemplate(dataSourceTemplate, data);
	document.body.append(fragment.content);
}

Hooks.once('ready', () => injectDataSources());

function enable() {
	Ancillary.register('item', inputQuickAccessInjection);
	Ancillary.register('action', inputQuickAccessInjection);

	Hooks.on('renderAttackDialog', inputDialogQuickAccessInjection);
	Hooks.on('renderDialog', inputDialogQuickAccessInjection); // Matches all dialogs. May as such match unwanted dialogs.
}

function disable() {
	Ancillary.unregister('item', inputQuickAccessInjection);
	Ancillary.unregister('action', inputQuickAccessInjection);

	Hooks.off('renderAttackDialog', inputDialogQuickAccessInjection);
	Hooks.off('renderDialog', inputDialogQuickAccessInjection);
}

new Feature({ setting: 'quick-input', label: 'Quick Input', hint: 'Adds quick access to various common values and formulas to various actor, item and dialog inputs.', category: 'common', enable, disable, stage: 'ready' });
