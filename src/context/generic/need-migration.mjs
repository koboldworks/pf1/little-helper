import { CFG } from '@root/config.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { createNode } from '@root/common.mjs';

// Since PF1 doesn't migrate these
const complementMigration = (updateData, doc) => {
	const flags = doc.system.flags;
	if (flags) {
		if (Array.isArray(flags.boolean) && flags.boolean.length === 0) {
			updateData.system ??= {};
			updateData.system.flags ??= {};
			updateData.system.flags.boolean = {};
		}
		if (Array.isArray(flags.dictionary) && flags.dictionary.length === 0) {
			updateData.system ??= {};
			updateData.system.flags ??= {};
			updateData.system.flags.dictionary = {};
		}
	}
};

const migrateDocument = async (doc) => {
	const data = doc.toObject();
	const updateData = doc instanceof Actor ? pf1.migrations.migrateActorData(data) : pf1.migrations.migrateItemData(data);

	complementMigration(updateData, doc);

	if (!foundry.utils.isEmpty(updateData)) {
		console.log('Updating document:', doc.name, doc.id, '\nUpdate:', updateData);
		const update = doc.update(updateData);
		return update;
	}
	else {
		console.warn('Migration produced an empty update. What?');
	}
};

const baseMigrationMessage = (doc, sheet, messages) => {
	if (game.user.isGM)
		messages.push('Please run system migration!');
	else if (!sheet.isEditable)
		messages.push('GM needs to run system migration.');
	else
		messages.push('Document needs migration!');
};

const packEntryMessage = (doc, messages) => {
	const inPack = !!doc.pack;
	if (inPack && game.user.isGM && game.packs.get(doc.pack)?.locked)
		messages.push('Compendium needs to be unlocked for system migration to take effect.');
};

/**
 * @param {Element} html
 * @param {string[]} messages
 * @param {boolean} canMigrate
 */
function printMigrationWarnings(html, messages, { canMigrate, doc, isEditable }) {
	if (messages.length == 0) return;

	const form = html.matches('form') ? html : html.querySelector('form');
	const warnings = createNode('div', null, ['lil-migration-warning']);
	const list = createNode('ul', null, ['warning-list']);

	for (const msg of messages)
		list.append(createNode('li', msg, ['warning'], { isHTML: true }));

	if (doc.isOwner && isEditable) {
		const m = createNode('li', 'Migrate This Document', ['migrate-action']);
		const s = createNode('span', null, ['lil-spinny'], { children: [createNode('i', null, ['fas', 'fa-spinner'])] });
		m.prepend(s);
		m.append(s.cloneNode(true));
		let inProgress = false;
		m.addEventListener('click', ev => {
			if (inProgress) return;
			inProgress = true;
			ev.preventDefault();
			ev.stopPropagation();
			m.classList.add('in-progress');
			migrateDocument(doc, { redo: true })
				.then(_ => {
					// Sheet should auto-refresh and remove the warning here
					ui.notifications.info(`${doc.name} [${doc.id}] migrated!`);
					m.classList.remove('in-progress');
					inProgress = false;
				});
		});
		list.append(m);
	}

	warnings.append(
		createNode('div', '', ['warning-padding']),
		list,
		createNode('div', '', ['warning-padding']),
	);

	form.prepend(warnings);
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function detectActorMigrationNeed(sheet, [html], options, shared) {
	const doc = sheet.actor;
	if (doc.type === 'basic') return;

	let needMigration = false;
	const data = shared.systemData;
	const messages = [];

	if (CFG.system.atLeast('0.80.22')) {
		const senses = data.traits.senses;
		if (typeof senses === 'string') {
			messages.push('Senses data in oudated format, new format introduced with PF1 0.80.22');
			needMigration = true;
		}
	}

	if (needMigration) {
		baseMigrationMessage(doc, sheet, messages);
		packEntryMessage(doc, messages);
	}

	printMigrationWarnings(html, messages, { canMigrate: needMigration, doc, isEditable: sheet.isEditable });
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function detectItemMigrationNeed(sheet, [html], options, shared) {
	const doc = shared.document;
	const data = shared.systemData;
	const source = shared.sourceData;

	let needMigration = false;
	const messages = [];

	if (source.actions === undefined && ['feat', 'attack', 'weapon', 'consumable', 'equipment', 'spell'].includes(doc.type)) {
		messages.push('Actions structure introduced by PF1 0.81.0 is missing');
		needMigration = true;
	}

	if (Array.isArray(source.flags?.boolean) || Array.isArray(source.flags?.dictionary)) {
		messages.push('System flags in format obsoleted by PF1 0.80.11 discovered.');
		needMigration = true;
	}

	if (source.weaponData !== undefined) {
		messages.push('Weapon data obsoleted by PF1 0.79.5 discovered.');
		needMigration = true;
	}

	if (['weapon', 'equipment', 'consumable', 'loot', 'container'].includes(doc.type)) {
		if (typeof source.weight === 'number' || source.weight === null || source.baseWeight !== undefined) {
			messages.push('Weight in format obsoleted by PF1 0.81.1 discovered.');
			needMigration = true;
		}
	}

	if (source.activation !== undefined || source.measureTemplate !== undefined || source.actionType !== undefined) {
		console.log('Pre-multiaction data (e.g. activation, measureTemplate, actionType) obsoleted by PF1 0.81.0 discovered in Item:', doc.name, doc);
		// messages.push('Pre-multiaction data obsoleted by PF1 0.81.0 discovered.');
		// needMigration = true;
	}

	if (source.actions !== undefined) {
		if (CFG.system.atLeast('0.81.1')) {
			for (const act of source.actions) {
				if (act.enh?.override !== undefined) {
					messages.push('Enhancement bonus override in format obsoleted by PF1 0.81.1 discovered.');
					needMigration = true;
					break;
				}
			}
		}
	}

	if (needMigration && !sheet.isEditable) {
		baseMigrationMessage(doc, sheet, messages);
		packEntryMessage(doc, messages);
	}

	printMigrationWarnings(html, messages, { canMigrate: needMigration, doc, isEditable: sheet.isEditable });

	// TODO: Add per item/actor migration option
}

function enable() {
	Ancillary.register('actor', detectActorMigrationNeed);
	Ancillary.register('item', detectItemMigrationNeed);
}

function disable() {
	Ancillary.unregister('actor', detectActorMigrationNeed);
	Ancillary.unregister('item', detectItemMigrationNeed);
}

new Feature({
	setting: 'migration-need',
	label: 'Migration Need',
	hint: 'Display warning for some common markers of lacking migration and provide an interface to easily migrate singular documents.',
	category: 'system',
	enable,
	disable,
	stage: 'ready',
	enabledByDefault: false,
});
