// Resolve formulas

import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';
import { unflair, getVariables } from '@core/formula.mjs';
import { setStaticToolTipListener } from '@core/tooltip.mjs';
import { i18n } from '@root/utility/i18n.mjs';

const hideIdenticalResolution = true;

// Ignore for resolution
const ignorePaths = [
	// Has built-in total
	/^formulaicAttacks\.count\.formula$/,
];

// Resolve even if no formula is present
const alwaysResolve = [
	/^save\.dc$/,
];

/**
 * Paths that have limited roll data available.
 */
const limitedRollData = {
	class: {
		item: [
			'system.customHD',
			'system.babFormula',
			'system.savingThrows.fort.custom',
			'system.savingThrows.ref.custom',
			'system.savingThrows.will.custom',
		],
	},
};

function getSpecialRollData(path, shared) {
	const item = shared.item;
	switch (path) {
		case 'system.customHD':
			return { item: { level: item.system.level } };
		case 'system.babFormula':
		case 'system.savingThrows.fort.custom':
		case 'system.savingThrows.ref.custom':
		case 'system.savingThrows.will.custom': {
			const itemData = item.system ?? {};
			return { level: itemData.system.level, hitDice: item.hitDice };
		}
	}

	return null;
}

function addSpecialRollData(rollData, path, context) {
	context.bookId = /\.spellbooks\.(?<bookId>\w+)\./.exec(path)?.groups.bookId;

	if (context.actor && !!context.bookId) {
		const bookData = rollData.spells[context.bookId];
		if (bookData) {
			if (rollData.sl === undefined) {
				rollData.sl = context.item?.system.level ?? 0; // Filler
				context.temporary.add('sl');
			}
			if (rollData.cl === undefined) {
				rollData.cl = bookData.cl.total;
				context.temporary.add('cl');
			}
			if (rollData.ablMod === undefined) {
				rollData.ablMod = rollData.abilities[bookData.ability]?.mod;
				context.temporary.add('ablMod');
			}
		}
	}
};

const cleanSpecialRollData = (rollData, path, context) => {
	for (const p of Array.from(context.temporary)) {
		foundry.utils.setProperty(rollData, p, undefined);
		context.temporary.delete(p);
	}
};

const rules = {
	matchCount: {
		parenthesis: { open: '(', close: ')', warning: 'LittleHelper.Warning.MismatchedQuantity' },
		squareBrackets: { open: '[', close: ']', warning: 'LittleHelper.Warning.MismatchedQuantity' },
		curlyBrackets: { open: '{', close: '}', warning: 'LittleHelper.Warning.MismatchedQuantity' },
	},
	general: {
		badVariable: { match: /@[^_\w]/i, warning: 'LittleHelper.Warning.BadVariable' }, // @ followed by bad characters
		squareBrackets: { match: /[^)\w\d]\[/, warning: 'LittleHelper.Warning.OrphanBrackets' }, // brackets without connected value
		// nonFunction: { match: /[^@[][\w_.]+[^(]/, warning: 'LittleHelper.Warning.FreeLetters' }, // freefloating letters
		runIn: { match: /\w[*+\-/]/, warning: 'LittleHelper.Warning.RunInOperator' },
		dieParens: { match: /\)d/, warning: 'LittleHelper.Warning.DieFunctionPair' },
	},
};

/**
 * @param {string} formula Formula to inspect.
 * @param {RollTerm[]} [terms] Roll terms to inspect.
 * @param {object[]} [issues] Issues array to append to
 * @param {object[]} [errors] Errors array to append to
 * @returns {{issues: object[], errors: object[]}} Issues array
 */
function validateFormula(formula = '', issues = [], errors = []) {
	let terms = [];
	if (formula) {
		try {
			terms = RollPF.parse(formula);
		}
		catch (err) {
			errors.push({
				rule: 'parse',
				message: err.message,
				error: err,
			});
		}
	}
	else {
		errors.push({
			rule: 'empty',
			message: i18n.get('LittleHelper.Warning.EmptyFormula'),
		});
	}

	for (const [subrule, rule] of Object.entries(rules.matchCount)) {
		let open = 0, close = 0;
		for (const l of formula) {
			if (l === rule.open) open++;
			else if (l === rule.close) close++;
		}

		if (open !== close) // mismatch
			issues.push(
				{
					rule: 'matchCount',
					subrule: subrule,
					message: i18n.get(rule.warning, { details: `"${rule.open}" ×${open}, "${rule.close}" ×${close}` }),
				},
			);
	}

	for (const [subrule, rule] of Object.entries(rules.general)) {
		const re = rule.match.exec(formula);
		if (re) {
			issues.push({
				rule: 'general',
				subrule: subrule,
				offset: re.index,
				message: i18n.get(rule.warning),
			});
		}
	}

	// Missing rounding
	for (const term of terms ?? []) {
		const re = /(\d\.\d|\/)/.exec(term.expression);
		if (!!re && !/(?:ceil|floor|round)\(/.test(term.expression)) {
			issues.push({
				rule: 'terms',
				subrule: 'rounding',
				offset: re.index,
				message: i18n.get('LittleHelper.Warning.MissingRounding'),
			});
		}
	}

	/**
	 * TODO:
	 * - Formula has only string term: e.g. "blep"
	 * - Misplaced stringterm
	 */

	return { issues, errors };
};

// Ensure line-breaks
const spaceFormula = (iformula) => iformula
	.replace(/([)\d\w])\s*([+*\-/])\s*/g, (_, m1, m2) => `${m1} ${m2} <wbr>`);

/**
 * @param {Element} parentEl
 * @param {string} formula
 * @param {SharedData} shared
 * @param {object} options
 * @param {string} options.bookId
 * @param {boolean} options.broken
 * @param {Roll} options.roll
 * @param {string[]} options.vars
 * @param {object} options.rollData
 */
function addFormulaTooltip(parentEl, formula, shared, { bookId, broken = false, roll, vars, rollData } = {}) {
	const numformula = Roll.replaceFormulaData(formula, rollData, { missing: 0 });
	if (!broken && numformula == formula) return;

	const actor = shared.actor;
	const doc = shared.document;

	const tip = createNode('div', null, ['lil-tooltip']);

	/**
	 * @param {Element} tip
	 * @param {string} formula
	 * @param {SharedData} shared
	 */
	const fillTooltip = async (tip, formula) => {
		tip.append(
			createNode('h2', 'Base formula'),
			createNode('p', spaceFormula(formula), ['base-formula'], { isHTML: true }),
		);

		// Check for basic validation errors
		const issues = [], errors = [];

		const checkVar = (v) => {
			if (v.path.startsWith('dFlags')) {
				const [_, tag] = v.path.split('.', 2);
				const pi = actor.items.find(i => i.system.tag === tag);
				if (!pi?.isActive) {
					issues.push({
						rule: 'itemDisabled',
						message: `Item "${tag}" disabled, @dFlags.${tag}.* set to 0`,
					});
				}
			}
			else if (v.path.startsWith('item.dFlags')) {
				if (!doc.isActive) {
					issues.push({
						rule: 'itemDisabled',
						message: 'Item disabled, @item.dFlags.* set to 0',
					});
				}
			}
		};

		if (vars?.length) {
			tip.append(
				createNode('h2', 'Variables resolved'),
				createNode('p', spaceFormula(numformula), ['numeric-formula'], { isHTML: true }),
			);

			const ul = createNode('ul', null, ['formula-parts']);
			ul.append(
				...vars.map(vdata => {
					const { path, value, idents: extraIdents } = vdata;
					const li = createNode('li', null, ['formula-variable']);

					li.append(
						createNode('span', `@${path}`, ['data-path']),
						' = ',
						createNode('span', `${value}`, ['data-value', ...extraIdents]),
					);
					return li;
				}),
			);

			tip.append(createNode('h2', 'Parts'), ul);

			if (actor) {
				for (const v of vars)
					checkVar(v);
			}
		}

		validateFormula(formula, issues, errors);

		if (issues.length)
			tip.append(createNode('h2', '⚠️ Warnings'), ...issues.map(issue => createNode('p', issue.message)));

		if (errors.length)
			tip.append(createNode('h2', '⚠️ ERRORS'), ...errors.map(issue => createNode('p', issue.message)));
	};

	// Delayed filling of tooltip
	parentEl.addEventListener('pointerenter', _ => fillTooltip(tip, formula), { passive: true, once: true });

	parentEl.append(tip);

	setStaticToolTipListener(parentEl, tip);
}

/**
 * @param {DocumentSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function resolveDocumentFormulas(sheet, [html], options, shared) {
	if (!shared.coreSheet && !shared.altSheetZenvy) return; // Ignore most alt sheets

	const doc = shared.document,
		action = shared.action,
		item = shared.item,
		actor = shared.actor,
		_rollData = foundry.utils.deepClone(shared.rollData);

	// Fill in some basic data not present without actor
	if (!shared.actor) {
		_rollData.size = 4;
	}

	const handleInput = async (el, path, context) => {
		if (path && ignorePaths.some(ign => ign.test(path))) return;
		let sourceFormula = el.value?.trim();

		let rollData = _rollData;

		if (item) {
			let specialData = limitedRollData[item.type];
			let isSpecial = false;
			if (specialData) {
				specialData = action ? specialData.action : specialData.item;
				isSpecial = specialData?.includes(path) ?? false;
			}

			if (isSpecial)
				rollData = getSpecialRollData(path, shared);
		}

		// Save DC, needs special handling with spells, but do so only if actor is present
		if (/^save\.dc$/.test(path) && context.item?.type === 'spell' && context.actor) {
			const bookData = rollData.spells[context.item?.system.spellbook];
			sourceFormula = [bookData.baseDCFormula, sourceFormula].filter(i => i.length).join(' + ');
		}
		else if (sourceFormula.length == 0) return;

		let rf, broken = false;
		try {
			rf = pf1.utils.formula.compress(pf1.utils.formula.simplify(sourceFormula, rollData, { errors: false }));
		}
		catch (err) {
			console.error('Bad formula:', sourceFormula, '\n\nDocument:', doc.name, doc, '\n\nData path:', path, '\n\nError:', err);
			rf = 'NaN';
			broken = true;
		}

		if (hideIdenticalResolution && rf === pf1.utils.formula.compress(unflair(sourceFormula)) && !broken) return; // Or maybe show ΔF=0

		const rv = createNode('label', null, ['lil-resolved-formula']);
		const nr = await RollPF.safeRollSync(sourceFormula, rollData, undefined, undefined, { minimize: true });
		if (nr?.err) broken = true;
		const terms = nr?.terms ?? [];

		rv.append('= ', createNode('span', `${rf}`, ['lil-resolved-value']));
		if (terms.length == 0)
			rv.classList.add('lil-broken-formula', 'NaN');
		else if (terms.some(t => t.constructor.name === 'StringTerm')) // TODO: v12 needs foundry.dice.terms.StringTerm
			rv.classList.add('lil-broken-formula');

		const varNames = getVariables(sourceFormula);
		const vars = varNames.map(kpath => {
			const value = foundry.utils.getProperty(rollData, kpath);
			const idents = [];

			if (value === undefined) idents.push('undefined', 'nullish');
			else if (value === null) idents.push('null', 'nullish');
			else if (typeof value === 'object') idents.push('object');

			return {
				path: kpath,
				value,
				idents,
			};
		});

		if (broken) rv.classList.add('broken');

		addFormulaTooltip(rv, sourceFormula, shared, { bookId: context.bookId, broken, roll: nr, vars, rollData });

		if (el.classList.contains('conditional-formula'))
			el.closest('li[data-conditional]')?.classList.add('lil-conditional-style-correction');

		if (nr?.err) {
			rv.append(createNode('i', null, ['fas', 'fa-exclamation-triangle', 'lil-formula-warning']));
		}

		el.after(rv);
	};

	html.querySelectorAll('input.formula').forEach(async el => {
		const path = el.name;
		const context = { temporary: new Set(), actor, item, doc };
		context.allowEmpty = false;
		addSpecialRollData(_rollData, path, context);

		try {
			await handleInput(el, path, context);
		}
		finally {
			cleanSpecialRollData(_rollData, path, context);
		}
	});
}

function enable() {
	Ancillary.register('item', resolveDocumentFormulas);
	Ancillary.register('actor', resolveDocumentFormulas);
	Ancillary.register('action', resolveDocumentFormulas);
}

function disable() {
	Ancillary.unregister('item', resolveDocumentFormulas);
	Ancillary.unregister('actor', resolveDocumentFormulas);
	Ancillary.unregister('action', resolveDocumentFormulas);
}

new Feature({
	setting: 'resolve-formulas',
	label: 'Resolve Formulas',
	hint: 'Resolves and performs basic validation on formulas and displays partially resolved value (no rolls) with tooltip on details about it on actor and item sheets.',
	category: 'common',
	enable,
	disable,
	stage: 'ready',
});

CFG.API.formula.validate = validateFormula;
