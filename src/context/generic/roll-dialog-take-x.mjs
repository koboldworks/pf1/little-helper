import { Feature } from '@core/feature.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {Application} app
 * @param {JQuery<HTMLElement>} html
 * @param {object} options
 */
function injectTakeXHint(app, [html], options) {
	const subject = app.options.subject;
	if (!subject && !(app instanceof pf1.applications.AttackDialog))
		return;

	if (!app.options.classes.includes('pf1')) return;

	const d20 = html.querySelector('input[name="d20"]');
	if (!d20) return;

	const txEl = document.createElement('span');
	txEl.classList.add('lil-take-x');
	d20.after(txEl);

	/**
	 * @param {Element} el
	 */
	async function updateNote(el) {
		// Re-find the element due to refresh remaking it
		const txEl = el.closest('form').querySelector('.lil-take-x');

		if (el.value.trim().length == 0) {
			txEl.textContent = '';
			return;
		}

		let roll;
		try {
			roll = new Roll(el.value);
			delete txEl.dataset.tooltip;
		}
		catch (err) {
			txEl.textContent = 'ERROR!';
			txEl.dataset.tooltip = err.message;
			return;
		}

		let text = '';
		if (roll.isDeterministic) {
			try {
				roll.evaluate({ async: false });
				text = i18n.get('PF1.TakeX', { number: roll.total });
			}
			catch (err) {
				text = 'ERROR!';
				txEl.dataset.tooltip = err.message;
			}
		}
		txEl.textContent = text;
	}

	updateNote(d20);

	d20.addEventListener('change', (ev) => updateNote(ev.target), { passive: true });
	d20.addEventListener('input', (ev) => updateNote(ev.target), { passive: true });
}

function enable() {
	Hooks.on('renderAttackDialog', injectTakeXHint);
	Hooks.on('renderDialog', injectTakeXHint); // Matches all dialogs. May as such match unwanted dialogs.
}

function disable() {
	Hooks.off('renderAttackDialog', injectTakeXHint);
	Hooks.off('renderDialog', injectTakeXHint);
}

new Feature({ setting: 'roll-dialog-take-x', label: 'Roll Dialog Take X', hint: 'Simple reminder that you\'re doing Take X roll in some circumstances.', category: 'common', enable, disable, stage: 'ready' });
