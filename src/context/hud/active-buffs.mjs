// Display buffs near chatlog

import { CFG } from '@root/config.mjs';
import { createNode, stopEventPropagation, getRelevantAE } from '@root/common.mjs';
import { getHumanTime, humanTimeUnit, getAEDuration } from '@root/common.time.mjs';
import { Feature, SubSetting } from '@core/feature.mjs';
import { i18n } from '@root/utility/i18n.mjs';

// TODO: Add quick access to enabling buffs and conditions, too.

const missingDescription = 'n/a';

/** @type {Token} */
let currentToken,
	/** @type {HTMLElement} */
	activeBuffs,
	/** @type {HTMLElement} */
	conditionList,
	/** @type {HTMLElement} */
	buffList,
	/** @type {HTMLElement} */
	permList;

/**
 * @type {ActiveBuffsDisplay}
 */
let activeBuffsApp;

const clearActor = () => {
	currentToken = null;
	activeBuffsApp?.close();

	activeBuffs.classList.remove('active');

	buffList?.replaceChildren();
	permList?.replaceChildren();
	conditionList?.replaceChildren();
};

/**
 * App for listening on actor updates.
 */
class ActiveBuffsDisplay extends Application {
	/** @type {Actor} */
	actor;
	/** @type {TokenDocument} */
	token;

	setToken(token) {
		const actor = token.actor;
		currentToken = token;
		this.token = token;
		this.actor = actor;
		activeBuffs.classList.add('active');
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			baseApplication: '', // Parent apps are undesirable
			popOut: false,
		};
	}

	render(force, context = {}) {
		if (context.action === 'update' && !force) {
			const updateData = context.data;
			// console.trace('UpdateData:', foundry.utils.deepClone(updateData));
			const actorUpdate = updateData.actorData ?? updateData;
			if (actorUpdate?.system?.attributes?.conditions === undefined) return;
		}
		renderCurrentToken();
	}

	_render() { }
	_getHeaderButtons() { return []; }
	_activateCoreListeners() { }

	// Simplified close to remove animation and other undesired aux functionality.
	close() {
		activeBuffsApp = null;
		currentToken = null;

		// Clean up data
		delete ui.windows[this.appId];
		this._state = Application.RENDER_STATES.CLOSED;
	}
}

/**
 * @param {Event} ev
 * @param {Actor} actor
 * @param {object} options
 * @param {string} options.buffId
 * @param {string} options.conditionId
 */
function disableCondition(ev, actor, { buffId, conditionId } = {}) {
	if (buffId) {
		const buff = actor.items.get(buffId);
		if (buff.isActive && buff.type === 'buff') {
			if (buff.subType === 'perm') return ui.notifications.warn('Will not disable permanent buff.');
			if ('setActive' in buff)
				buff.setActive(false);
			else
				buff.update({ 'system.active': false });
		}
	}
	else if (conditionId) {
		const name = pf1.registry.conditions.get(conditionId)?.name || conditionId;

		const onActor = [...actor.effects].some(ae => ae.statuses.has(conditionId));
		if (!onActor) {
			const items = [...actor.allApplicableEffects().filter(ae => ae.statuses.has(conditionId))].map(ae => ae.parent);
			const list = new Intl.ListFormat(game.i18n.lang, { style: 'long' }).format(items.map(i => i.name));
			ui.notifications.warn(`Unable to remove ${name}; inherited from ${list}`);
			return;
		}

		// Guard against value desync and future proofing against conditions being moved
		const oldValue = actor.hasCondition(conditionId);
		if (oldValue === true) {
			actor.setCondition(conditionId, false);
		}
		else {
			ui.notifications.warn(`Can't disable condition "${name}" as it is not active.`);
		}
	}
}

/**
 * @param {Event} ev
 * @param {Actor} actor
 * @param {object} options
 * @param {string} options.buffId
 * @param {string} options.conditionId
 */
function printDescription(ev, actor, { buffId, conditionId } = {}) {
	actor.items.get(buffId)?.displayCard();
}

/**
 * @param {boolean} isLeftClick
 * @param {Event} ev
 * @param event
 */
function clickHandler(isLeftClick, event) {
	stopEventPropagation(event);

	const rightClick = feature.subsettings.rightClick.value ?? false;

	/** @type {HTMLElement} */
	const el = event.target;

	const uuid = el.dataset.uuid,
		conditionId = el.dataset.conditionId,
		buffId = el.dataset.buffId;

	let actor = fromUuidSync(uuid);
	if (actor instanceof TokenDocument) actor = actor.actor; // TODO: Remove once v10 support is removed

	if (!actor) return void ui.notifications.warn('Actor not found');

	if (event.shiftKey && isLeftClick)
		printDescription(event, actor, { buffId, conditionId });
	else if (rightClick !== isLeftClick)
		disableCondition(event, actor, { buffId, conditionId });
}

function addTooltipTimeInfo(el, { tooltip, actor, item, condition } = {}) {
	const ae = getRelevantAE(actor, { item, condition });
	el.dataset.effectUuid = ae?.getRelativeUUID(actor);

	const dur = ae ? getAEDuration(ae) : {};

	const longFormat = feature.subsettings.longTimeFormat.value;

	if (dur.start !== null) {
		el.dataset.start = dur.start;

		const time = createNode('div', null, ['time']);
		time.dataset.start = `${dur.start}`;

		const activeTime = dur.passed;
		time.dataset.active = `${activeTime}`;

		const hActive = getHumanTime(activeTime);
		const activeTimeBlock = createNode('div', null, ['active-time', 'time-block'], {
			children: [
				'Active: ',
				createNode('span', `${hActive.n ?? 0}`, ['active', 'number']),
				' ',
				createNode('span', humanTimeUnit[hActive.u][longFormat ? 'long' : 'short'], ['remaining', 'unit']),
			],
		});
		time.append(activeTimeBlock);

		if (dur.duration > 0) {
			time.dataset.duration = `${dur.duration}`;

			const hRemaining = getHumanTime(dur.remaining),
				hDuration = getHumanTime(dur.duration);

			const remainingTimeBlock = createNode('div', null, ['remaining-time', 'time-block'], {
					children: [
						'Remaining: ',
						createNode('span', hRemaining.n, ['remaining', 'number', hRemaining.n <= 0 ? 'expired' : 'ongoing']),
						' ',
						createNode('span', humanTimeUnit[hRemaining.u][longFormat ? 'long' : 'short'], ['remaining', 'unit']),
					],
				}),
				maxTimeBlock = createNode('div', null, ['max-time', 'time-block'], {
					children: [
						'Max duration: ',
						createNode('span', hDuration.n, ['duration', 'number']),
						' ',
						createNode('span', humanTimeUnit[hDuration.u][longFormat ? 'long' : 'short'], ['duration', 'unit']),
					],
				});

			time.append(remainingTimeBlock, maxTimeBlock);
		}

		tooltip.append(time);
	}
}

async function fillTooltipDescription({ item, condition } = {}) {
	const ConditionHelp = CFG.i18n.conditions;

	let desc;
	if (item) {
		const rollData = item?.getRollData() ?? {};
		desc = item?.fullDescription ?? missingDescription;
		const secrets = feature.subsettings.secrets.value;
		desc = await TextEditor.enrichHTML(desc, { rollData, documents: true, rolls: true, secrets, async: true });
	}
	else {
		let valid = false;
		if (pf1.registry.conditions) {
			valid = pf1.registry.conditions.has(condition);
		}
		else {
			valid = condition in pf1.config.conditions;
		}
		desc = '<p>' + (valid ? ConditionHelp[condition] : i18n.get('LittleHelper.Conditions._invalid'));
	}

	return desc;
}

async function initTooltip(el, { label, actor, item = null, condition = null, event } = {}) {
	const tooltip = createNode('div', null, ['tooltip'], { children: [createNode('h4', label, ['name'])] });

	const level = item?.system.level;
	if (level != 0 && Number.isFinite(level)) {
		const levelEl = createNode('div', null, ['level']);
		const ll = createNode('label', i18n.get('PF1.Level'));
		const lv = createNode('span', level, ['value']);
		levelEl.append(ll, ' ', lv);
		tooltip.append(levelEl);
	}

	addTooltipTimeInfo(el, { tooltip, label, actor, item, condition, event });

	const descEl = createNode('div', null, ['description']);
	tooltip.append(descEl);

	fillTooltipDescription({ item, condition })
		.then(desc => descEl.innerHTML = desc);

	// Control help
	tooltip.append(createNode('hr'));
	if (item && item.system.buffType === 'perm')
		tooltip.append(createNode('p', i18n.get('PF1.Permanent'), ['buff-type']));
	else {
		const rightClick = feature.subsettings.rightClick.value;
		const clickLabel = !rightClick ? i18n.get('LittleHelper.UI.ClickToEndEffect') : i18n.get('LittleHelper.UI.ClickToEndEffectAlt');
		tooltip.append(createNode('p', clickLabel, ['buff-action', 'help']));
	}

	Hooks.callAll('little-helper.hud.tooltip.active', tooltip, { actor, item, condition, event });

	el.append(tooltip);

	// Set width so it can be aligned to the left correctly
	el.style.setProperty('--tt-width', tooltip.clientWidth);
}

function generateIcon({ label, image, css = [], callback, item, condition, token } = {}) {
	const d = createNode('li', null, [...css, 'buff']);
	const img = createNode('img');
	if (image) img.src = image;
	d.append(img);

	const labelEl = createNode('h4', label, ['fast-label']);
	d.append(labelEl);

	const actor = token?.actor;

	if (condition) {
		d.dataset.conditionId = condition;
		const valid = pf1.registry.conditions.has(condition);

		if (!valid) d.classList.add('invalid');
	}
	if (item) d.dataset.itemId = item.id;
	d.dataset.uuid = actor.uuid;

	// Lazy description addition
	d.addEventListener('pointerenter', ev => initTooltip(d, { label, actor, item, condition, event: ev }), { passive: true, once: true });

	return { element: d, label: labelEl };
}

function renderConditions(token, control) {
	const conditionIcons = [];

	for (const conditionId of token.actor.statuses) {
		const condition = pf1.registry.conditions.get(conditionId);
		const image = condition?.texture;
		const label = condition?.name || conditionId;

		const icon = generateIcon({ label, image, css: ['condition', conditionId], token, condition: conditionId });

		if (!icon) {
			console.warn('Failed to generate icon for:', conditionId);
			continue;
		}

		icon.element.dataset.conditionId = conditionId;
		conditionIcons.push(icon);
	}

	conditionList.classList.toggle('collapse', conditionIcons.length == 0);
	if (conditionIcons.length > 0)
		conditionList.replaceChildren(...conditionIcons.map(i => i.element));
}

function renderBuffs(token, control) {
	const showHidden = feature.subsettings.showHidden.value;

	const buffIcons = [], permIcons = [];
	const allBuffs = token.actor.itemTypes.buff ?? [];
	const buffs = allBuffs.filter(i => i.isActive)
		.sort((a, b) => b.sort - a.sort);

	for (const item of buffs) {
		if (!showHidden && item.system.hideFromToken) continue;
		const icon = generateIcon({
			label: item.name, image: item.img, css: ['buff'], token, item,
		});

		if (icon) {
			icon.element.dataset.buffId = item.id;
			if (item.subType === 'perm') {
				icon.element.classList.add('permanent');
				icon.element.dataset.permanent = true;
				permIcons.push(icon);
			}
			else buffIcons.push(icon);
		}
	}

	buffList.classList.toggle('collapse', buffIcons.length == 0);
	if (buffIcons.length > 0)
		buffList.replaceChildren(...buffIcons.map(i => i.element));

	permList.classList.toggle('collapse', permIcons.length == 0);
	if (permIcons.length > 0)
		permList.replaceChildren(...permIcons.map(i => i.element));
}

function renderActorIcons(token, control) {
	renderConditions(token, control);
	renderBuffs(token, control);
}

async function tokenControlEvent(_token, _isControlled) {
	const tokens = canvas.tokens?.controlled ?? [];
	const token = tokens[0];
	if (tokens.length === 1 && token.actor && activeBuffsApp?.token !== token) {
		activeBuffsApp ??= new ActiveBuffsDisplay();
		activeBuffsApp.setToken(token);
		activeBuffsApp.render();
	}
	else
		clearActor();
}

function initializeBuffDisplay() {
	const h = createNode('div', null, ['lil-active-buffs']);
	h.id = 'little-helper-active-buffs';
	const list = createNode('div', null, ['buff-container']);
	h.append(list);

	buffList = createNode('ul', null, ['buff-list', 'buffs', 'temporary']);
	permList = createNode('ul', null, ['buff-list', 'buffs', 'permanent']);
	conditionList = createNode('ul', null, ['buff-list', 'conditions']);

	list.append(conditionList, buffList, permList);

	activeBuffs = h;

	document.body.append(h);

	setTimeout(() => {
		list.addEventListener('click', clickHandler.bind(null, true));
		list.addEventListener('contextmenu', clickHandler.bind(null, false));
	}, 50);
}

function refreshBuff(buff, created = false) {
	// TODO: Inject the buff into the list instead when created, otherwise update times
	renderCurrentToken();
}

function removeBuff(buff) {
	buffList?.querySelector(`li.buff[data-item-id="${buff.id}"]`)?.remove();
}

const refreshCondition = (conditionId) => {
	//
};

const renderControl = { rendering: null, queued: null };
function renderCurrentToken() {
	const rc = renderControl;
	const token = currentToken;
	if (rc.rendering) {
		if (rc.rendering !== token) {
			rc.queued = token;
		}
	}
	else {
		rc.rendering = token;
		renderActorIcons(token, rc);
		rc.rendering = null;
		if (rc.queued && rc.queued !== token) {
			rc.queued = null;
			renderCurrentToken();
		}
		else rc.queued = null;
	}
}

/**
 * BUG: this doesn't work currently
 *
 * @param {number} world - World time
 * @param {number} offset - Offset from known world time
 */
function updateTimes(world, offset = 0) {
	world ??= game.time.worldTime;

	const longFormat = feature.subsettings.longTimeFormat.value;

	const actor = currentToken?.actor;
	if (!actor) return;

	buffList.querySelectorAll('.tooltip > .time[data-duration]')
		.forEach(el => {
			const aeUuid = el.closest('.buff-list li[data-effect-uuid]')?.dataset.effectUuid,
				ae = fromUuidSync(aeUuid, { relative: actor });

			if (!ae) return;

			if (!(Number.isFinite(ae.duration.seconds))) return;

			const dur = getAEDuration(ae, { offset });

			const n = el.querySelector('.remaining.number');
			if (n) {
				const hr = getHumanTime(dur?.remaining ?? 0);
				n.textContent = hr.n;
				n.nextElementSibling.textContent = humanTimeUnit[hr.u][longFormat ? 'long' : 'short'];
				if (hr.n <= 0) n?.classList.add('expired');
			}

			const a = el.querySelector('.active.number');
			if (a) {
				const ar = getHumanTime(dur?.passed ?? 0);
				a.textContent = ar.n;
				a.nextElementSibling.textContent = humanTimeUnit[ar.u][longFormat ? 'long' : 'short'];
			}
		});

	conditionList;
}

function onUpdateCombat(_combat, _data, options) {
	if (!currentToken?.combatant) return;

	const world = game.time.worldTime;
	updateTimes(world, options.advanceTime ?? 0);
	// renderCurrentToken();
}

/**
 * @param {number} world - New world time
 */
function onUpdateTime(world) {
	if (!currentToken) return;
	updateTimes(world);
	// renderCurrentToken();
}

async function onAEUpdate(ae, update) {
	if (!currentToken) return;
	const item = ae.parent;
	const isItem = item instanceof Item;
	const actor = isItem ? item.actor : item;
	if (actor !== currentToken?.actor) return;

	if (item?.isActive) refreshBuff(item);
	else removeBuff(item);

	// TODO: Update existing
	if (ae.statuses.size) renderCurrentToken();
}

async function onAECreate(ae, data) {
	if (!currentToken) return;
	const item = ae.parent;
	const isItem = item instanceof Item;
	const actor = isItem ? item.actor : item;
	if (actor !== currentToken?.actor) return;

	if (isItem) refreshBuff(item, true);

	// TODO: Update existing
	if (ae.statuses.size) renderCurrentToken();
}

async function onAEDelete(ae) {
	if (!currentToken) return;
	const item = ae.parent;
	const isItem = item instanceof Item;
	const actor = isItem ? item.actor : item;
	if (actor !== currentToken?.actor) return;

	buffList?.querySelector(`li.buff[data-item-id="${item.id}"]`)?.remove();

	// TODO: Update existing
	renderCurrentToken();
}

async function onItemUpdate(item, update, options, userId) {
	if (item.type !== 'buff') return;
	if (item.actor !== currentToken?.actor) return;
	if (update.system?.active === undefined) return;

	if (item?.isActive) refreshBuff(item);
	else removeBuff(item);
}

async function onItemCreate(item, data, options, userId) {
	if (item.type !== 'buff') return;
	if (item.actor !== currentToken?.actor) return;

	if (item.isActive) refreshBuff(item, true);
}

async function onItemDelete(item, userId) {
	if (item.type !== 'buff') return;
	if (item.actor !== currentToken?.actor) return;

	buffList?.querySelector(`li.buff[data-item-id="${item.id}"]`)?.remove();
}

async function onActorUpdate(actor, update, options, userId) {
	if (currentToken?.actor === actor)
		renderCurrentToken();
}

const displayExtraInfo = (enabled) => {
	activeBuffs.querySelectorAll('.fast-label').forEach(el => {
		el.style.setProperty('--width', el.clientWidth);
	});

	activeBuffs?.classList.toggle('extraInfo', enabled);
};

/**
 * @param {Sidebar} sidebar
 * @param {boolean} collapsed
 */
function collapseSidebar(sidebar, collapsed) {
	activeBuffs?.classList.toggle('sidebar-collapsed', collapsed);
}

let wthid, uchid;
function enable() {
	initializeBuffDisplay();
	Hooks.on('controlToken', tokenControlEvent);
	tokenControlEvent();

	wthid = Hooks.on('updateWorldTime', onUpdateTime);
	// uchid = Hooks.on('updateCombat', onUpdateCombat);

	Hooks.on('updateActiveEffect', onAEUpdate);
	Hooks.on('deleteActiveEffect', onAEDelete);
	Hooks.on('createActiveEffect', onAECreate);
	Hooks.on('updateItem', onItemUpdate);
	Hooks.on('deleteItem', onItemDelete);
	Hooks.on('createItem', onItemCreate);
	Hooks.on('updateActor', onActorUpdate);

	Hooks.on('little-helper.extraInfo', displayExtraInfo);

	Hooks.on('collapseSidebar', collapseSidebar);
}

function disable() {
	Hooks.off('controlToken', tokenControlEvent);

	Hooks.off('updateWorldTime', wthid);
	// Hooks.off('updateCombat', uchid);

	Hooks.off('updateActiveEffect', onAEUpdate);
	Hooks.off('deleteActiveEffect', onAEDelete);
	Hooks.off('createActiveEffect', onAECreate);
	Hooks.off('updateItem', onItemUpdate);
	Hooks.off('deleteItem', onItemDelete);
	Hooks.off('createItem', onItemCreate);
	Hooks.off('updateActor', onActorUpdate);

	activeBuffsApp?.close();
	activeBuffs?.remove();
	activeBuffs = null;

	Hooks.off('little-helper.extraInfo', displayExtraInfo);
	Hooks.off('collapseSidebar', collapseSidebar);
}

// TODO: Allow displaying the bar for configured character when no token is selected.
// TODO: Hold alt to display names of all buffs.

const feature = new Feature({
	setting: 'active-buffs',
	label: 'Active Buffs',
	hint: 'Display active buffs and conditions on the side of the chat log, providing quick access to some information and allowing disabling them with a click.',
	category: 'hud',
	enable,
	disable,
	stage: 'ready',
	subsettings: {
		showHidden: new SubSetting({ label: 'Show Hidden', hint: 'Show buffs with Hide from Token enabled.', type: Boolean, fallback: true }),
		longTimeFormat: new SubSetting({ label: 'Long Time Format', hint: 'Show time units in long format (hours(s) instead of h(s)).', type: Boolean, fallback: true }),
		secrets: new SubSetting({ label: 'Secrets', hint: 'Display contents of secret sections.', type: Boolean, fallback: true }),
		rightClick: new SubSetting({ label: 'Right Click to End', hint: 'End buffs with right click instead of left click.', type: Boolean, fallback: false }),
	},
});
