import { Ancillary } from '@root/ancillary.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function sharedItemData(sheet, [html], options, shared) {
	// NOP
}

Ancillary.register('item', sharedItemData, { priority: 1000 });
