// Wrapper around useAttack to allow multiple functions to hook into it.

import { CFG } from '@root/config.mjs';

function useAttackWrapper(wrapped, options = {}) {
	Hooks.call('little-helper.useAttackWrapper', this, options);
	return wrapped.call(this, options);
}

Hooks.once('setup', () => {
	const C = CFG.console.colors;
	console.log('%cLittle Helper%c 🦎 | Overriding Item#use', C.main, C.unset);
	/* global libWrapper */
	libWrapper.register(CFG.id, 'pf1.documents.item.ItemPF.prototype.use', useAttackWrapper, libWrapper.WRAPPER);
});
