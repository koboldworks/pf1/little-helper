// WIP

import { createNode, signNum } from '@root/common.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

class SubAction {
	/** @type {object} */
	instance;

	#fmAtkCount;
	get attackCount() {
		const hardExtras = this.instance.data.attack.parts.length;
		if (this.#fmAtkCount === undefined) {
			const fmAtk = this.instance.data.formulaicAttacks?.count?.formula?.trim();
			this.#fmAtkCount = fmAtk ? RollPF.safeRollSync(fmAtk, this.#rollData).total : 0;
		}
		return hardExtras + this.#fmAtkCount;
	}

	get attackArray() {
		const attacks = [0];
		if (this.attackCount == 0) return attacks;

		const atkData = this.instance.data;

		const addAttack = (formula) => {
			const bonus = RollPF.safeRollSync(formula, this.#rollData).total;
			if (Number.isFinite(bonus)) attacks.push(bonus);
		};

		// Static extra attacks
		atkData.attackParts
			.map(([n, _]) => n?.toString().trim())
			.filter(n => !!n)
			.forEach(addAttack);

		// Formula-based extra attacks
		if (this.#fmAtkCount > 0) {
			const fmAtkBonus = atkData.formulaicAttacks.bonus?.formula?.trim() || '0';
			for (let i = 0; i < this.#fmAtkCount; i++) {
				this.#rollData.formulaicAttack = i + 1;
				addAttack(fmAtkBonus);
			}
			delete this.#rollData.formulaicAttack;
		}

		// Conditional modifiers
		const condBonuses = new Array(attacks.length).fill(0);
		atkData.conditionals
			.filter((c) => c.default && c.modifiers.find((sc) => sc.target === 'attack'))
			.forEach((c) => {
				c.modifiers.forEach((cc) => {
					const bonusRoll = RollPF.safeRollSync(cc.formula, this.#rollData, undefined, undefined, { minimize: true }).total;
					if (bonusRoll == 0) return;
					const re = /^attack\.(?<iter>\d+)$/.exec(cc.subTarget);
					if (re) {
						const atk = parseInt(re.groups.iter);
						if (atk in condBonuses) condBonuses[atk] += bonusRoll;
					}
				});
			});

		const totalBonus = this.item.attackSources.reduce((f, s) => f + s.value, 0);

		return attacks.map((a, i) => a + totalBonus + condBonuses[i]);
	}

	get nonlethal() {
		return this.instance.data.nonlethal;
	}

	get critLabel() {
		const c = this.instance.data.ability;
		return `${c.critRange ?? 20}/×${c.critMult ?? 2}`;
	}

	get saveLabel() {
		const s = this.instance.data.save;
		return `DC ${s.dc} ${s.type} (${s.description})`;
	}

	/** @type {Item} */
	item;
	/** @type {object} */
	#rollData;

	constructor(action, item, rollData) {
		this.instance = action;
		this.item = item;
		this.#rollData = rollData;
	}
}

/**
 * @param {Element} parent
 * @param {ItemAction} action
 */
async function createActionTooltip(parent, action) {
	const rollData = action.getRollData();
	const act = new SubAction(action, action.item, rollData);

	// TODO: RANGE
	// TODO: AREA
	// TODO: AMMO / USES

	// Delayed filling of tooltip

	const template = document.createDocumentFragment();

	// template.append(createNode('h2', action.name));

	// ATTACK
	if (act.instance.hasAttack) {
		template.append(
			createNode('label', 'Attacks'),
			createNode('span', act.attackArray.map(n => signNum(n)).join('/'), ['value']),
			createNode('label', 'Critical'),
			createNode('span', act.critLabel, ['value']),
		);
	}

	// DAMAGE
	if (act.instance.hasDamage) {
		template.append(
			createNode('label', 'Damage'),
			createNode('span', '???', ['value']),
		);
	}

	// SAVE
	if (act.instance.hasSave) {
		template.append(
			createNode('label', 'Save'),
			createNode('span', act.saveLabel, ['value']),
		);
	}

	game.tooltip.activate(parent, { content: template, cssClass: 'pf1 lil-action-selector' });
};

/**
 * @param {*} app
 * @param {HTMLElement} html
 */
function actionSelectorEnrichment(app, html) {
	const item = app.item;

	const aEls = html.querySelectorAll('.action[data-action]');
	for (const el of aEls) {
		const action = item.actions.get(el.dataset.action);

		el.addEventListener('pointerenter', () => createActionTooltip(el, action), { passive: true });
		el.addEventListener('pointerleave', () => game.tooltip.deactivate(), { passive: true });
	}
}

Hooks.on('renderActionSelector', actionSelectorEnrichment);
