import { Feature } from '@core/feature.mjs';
import { createNode } from '@root/common.mjs';
import { getHumanTime, humanTimeUnit } from '@root/common.time.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {Element} props
 */
async function addBuffDuration(sheet, props) {
	const item = sheet.item;

	if (!item.isActive) return;
	if (item.type !== 'buff') return;

	const line = (selectors) => createNode('li', null, ['item-detail', ...selectors]);

	const durLine = line(['lil-property', 'duration']);

	const ae = item.effect;
	if (!ae) return;

	const s = ae.duration?.seconds ?? 0;

	const ht = getHumanTime(s);
	const tt = s > 0 ? ht.n : 'Infinite';
	const tu = s > 0 ? humanTimeUnit[ht.u]?.long : '';
	durLine.append(i18n.get('PF1.EnrichedText.Condition.Duration', { period: tt, unit: tu }));

	props.append(durLine);

	if (s <= 0) return;

	const { n, u } = getHumanTime(ae.duration.remaining);
	const remLine = line(['lil-property', 'remaining']);
	remLine.append(i18n.get('Koboldworks.Missing.PF1.DurationLeft', { time: n, unit: humanTimeUnit[u].long }));
	props.append(remLine);
	// TODO: Some kind of autoupdate?
}

function enable() {
	Hooks.on('little-helper.item.header.properties', addBuffDuration);
}

function disable() {
	Hooks.off('little-helper.item.header.properties', addBuffDuration);
}

new Feature({ setting: 'buff-item-duration-display', label: 'Buff Duration Display', hint: 'Displays remaining duration in buff item sheet. This is NOT updated except on sheet re-open. Requires header enrichment to be enabled.', category: 'items', enable, disable, stage: 'ready' });
