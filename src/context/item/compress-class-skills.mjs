// Compress class skills list

import { createNode } from '@root/common.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { i18n } from '@root/utility/i18n.mjs';

const allKnowledgeSkills = ['kar', 'kdu', 'ken', 'kge', 'khi', 'klo', 'kna', 'kno', 'kpl', 'kre'];

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function compressClassSkills(sheet, [html], options, shared) {
	const item = sheet.document;
	const actor = item.actor;
	if (!actor) return;

	if (sheet.__lilEditingClassList === true) return;

	// if (item.type !== 'class') return;

	const classSkills = Object.entries(item.system.classSkills ?? {})
		.reduce((total, [key, enabled]) => {
			if (enabled) total.push(key);
			return total;
		}, []);

	if (classSkills.length == 0) {
		// No class skills, assume this is new class and needs to be in editing mode. But only with classes. Compress with everything else anyway.
		if (item.type === 'class') {
			sheet.__lilEditingClassList = true;
			return;
		}
	}

	const ol = html.querySelector('li[data-skill]')?.parentElement;
	const container = ol?.parentElement;
	if (!container?.classList.contains('form-group')) return;

	let skillData = classSkills.map(skl => actor.getSkillInfo(skl));

	ol.classList.add('lil-class-skills');
	container.classList.add('lil-class-skill-container', 'collapsed');

	const minified = createNode('div', null, ['lil-class-skills-minified']);
	const prettylist = createNode('p');

	// Combine Knowledge skills to Knowledge (All) where appropriate
	if (i18n.lang === 'en') { // FLAW: There is no "Knowledge (All)" translation
		const knSkills = skillData.filter(sk => allKnowledgeSkills.includes(sk.id));
		if (knSkills.length === allKnowledgeSkills.length) {
			const fki = skillData.findIndex(skl => allKnowledgeSkills.includes(skl.id));
			skillData.splice(fki, 1, { name: 'Knowledge (All)', id: 'kn-all' });
			skillData = skillData.filter(sk => !allKnowledgeSkills.includes(sk.id));
		}
	}

	skillData.forEach((skl, i, skls) => {
		prettylist.append(createNode('b', skl.name));
		if (i == skls.length - 2) prettylist.append(' and ');
		else if (i < skls.length - 2) prettylist.append(', ');
	});

	if (prettylist.childNodes.length) minified.append(prettylist);
	minified.append(createNode('a', 'Click to edit'));

	function toggleClassList(event) {
		event.currentTarget.classList.toggle('collapsed');
		sheet.__lilEditingClassList = true;
		ol?.scrollIntoView();
	}

	container.addEventListener('click', toggleClassList, { passive: true, once: true });

	container.prepend(minified);
}

// Remove temporary flagging
function cleanCSFlags(sheet) {
	delete sheet.__lilEditingClassList;
}

function enable() {
	Ancillary.register('item', compressClassSkills);
	Hooks.on('closeItemSheet', cleanCSFlags);
}

function disable() {
	Ancillary.unregister('item', compressClassSkills);
	Hooks.off('closeItemSheet', cleanCSFlags);
}

new Feature({
	setting: 'compress-class-skill-list', label: 'Compress Class Skills', hint: 'Display simpler list of class skills until clicked.', category: 'items', enable, disable, stage: 'ready',
});
