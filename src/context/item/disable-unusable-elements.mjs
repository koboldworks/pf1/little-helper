import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {ShareData} shared
 */
function disableUnusuableDetails(sheet, [html], options, shared) {
	const item = sheet.document;

	const inner = html.querySelector('section');

	const disable = [];
	if (item.spellbook) {
		const spellPoints = item.spellbook.spellPoints?.useSystem ?? false;
		const isPrepared = item.spellbook.spellPreparationMode !== 'spontaneous';
		disable.push({
			enabled: spellPoints,
			selectors: [
				'[name="system.preparation.preparedAmount"]',
				'[name="system.preparation.maxAmount"]',
				'[name="system.slotCost"]',
			],
		});

		disable.push({
			enabled: !spellPoints && isPrepared,
			selectors: ['[name="system.spellPoints.cost"]'],
		});
	}

	disable.push({
		enabled: !!item.links.charges,
		selectors: [
			'[name="system.uses.maxFormula"]',
			'[name="system.uses.per"]',
			'[name="system.uses.value"]',
		],
	});

	disable.forEach(r => {
		if (!r.enabled) return;
		r.selectors.forEach(s => {
			const sel = inner.querySelector(s);
			if (sel) {
				sel.disabled = true;
				sel.readOnly = true;
				const p = sel.parentElement;
				p.classList.add('lil-disabled');
			}
		});
	});
}

function enable() {
	Ancillary.register('item', disableUnusuableDetails);
}

function disable() {
	Ancillary.unregister('item', disableUnusuableDetails);
}

new Feature({ setting: 'item-disable-unusable-details', label: 'Disable unusable details', hint: 'Disables/removes details from item sheet that are not usable with their current configuration.', category: 'items', enable, disable, stage: 'ready' });
