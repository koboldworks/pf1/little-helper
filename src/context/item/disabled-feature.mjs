// Improve visibility of an item being disabled

import { createNode } from '@root/common.mjs';
import { Ancillary } from '@root/ancillary.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} opts
 * @param {SharedData} shared
 */
function clarifyDisabledFeature(sheet, [html], opts, shared) {
	const item = sheet.document;

	if (item.isActive) return;

	const disabled = html.querySelector('[name="system.disabled"]');
	const warning = createNode('i', null, ['fas', 'fa-exclamation-triangle', 'warning-symbol']);
	const p = disabled?.parentElement;
	if (!p) return;
	p.append(warning);
	p.classList.add('lil-warning-glow');
}

Ancillary.register('item', clarifyDisabledFeature);

// TODO: FEATURE
