import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

const nameToIconMap = {
	// Enhancement
	'system.enh': ['ra', 'ra-fairy-wand', 'lil-icon-enhancement'],
	'system.armor.enh': ['ra', 'ra-fairy-wand', 'lil-icon-enhancement'],
	// Armor
	'system.armor.value': ['ra', 'ra-shield', 'lil-icon-armor'],
	'system.armor.acp': ['fas', 'fa-mitten', 'lil-icon-acp'],
	// Targeting
	'system.target.value': ['fas', 'fa-bullseye', 'lil-icon-target'],
	'system.range.units': ['fas', 'fa-ruler', 'lil-icon-distance'],
	'system.spellArea': ['fas', 'fa-ruler-combined', 'lil-icon-area'],
	// Duration
	'system.duration.units': ['fas', 'fa-hourglass-start', 'lil-icon-time'],
	'system.spellDuration': ['fas', 'fa-hourglass-start', 'lil-icon-time'],
	// Uses
	'system.uses.per': ['fas', 'fa-battery-half', 'lil-icon-armor'],
	// Attacks
	'system.attackBonus': ['ra', 'ra-axe-swing', 'lil-icon-attack'],
	'system.ability.critRange': ['ra', 'ra-decapitation', 'lil-icon-critical'],
};

function compatiblePathName(path) {
	return /^system\./.test(path) ? path : `system.${path}`;
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {object} shared
 */
function extraIcons(sheet, [html], options, shared) {
	// const item = shared.document;

	const inputs = html.querySelectorAll('input[name],select[name]');
	for (const input of inputs) {
		const iconCss = nameToIconMap[compatiblePathName(input.name)];
		if (!iconCss) continue;
		const label = input.closest('.form-group')?.querySelector('label');
		if (!label) continue;

		const icon = document.createElement('i');
		icon.classList.add(...iconCss, 'lil-extra-icon');

		label.append(icon);
	}
}

function enable() {
	Ancillary.register('item', extraIcons);
	Ancillary.register('action', extraIcons);
}

function disable() {
	Ancillary.unregister('item', extraIcons);
	Ancillary.unregister('action', extraIcons);
}

new Feature({ setting: 'extra-icons', label: 'Extra Icons', hint: 'Add extra icons to help find relevant info faster.', category: 'items', enable, disable, stage: 'ready' });
