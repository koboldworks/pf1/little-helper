import { createNode } from '@root/common.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { i18n } from '@utility/i18n.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {ShareData} shared
 */
function hitPointRetrainCapacity(sheet, [html], options, shared) {
	const item = sheet.document,
		actor = item.actor;
	if (!actor || item.type !== 'class') return;

	const hd = item.hitDice,
		itemData = item.system,
		hp = itemData.hp,
		hdSize = itemData.hd,
		maxHp = hdSize * hd,
		missing = maxHp - hp;

	// console.log({ hd, hp, hdSize, missing });
	if (missing <= 0) return;

	const hpEl = html.querySelector('input[name="system.hp"]');
	if (!hpEl) return;

	hpEl.after(createNode('label', i18n.get('LittleHelper.Info.RetrainPotential', { missing, max: maxHp }), ['lil-retrain-capacity', 'hit-points']));
}

function enable() {
	Ancillary.register('item', hitPointRetrainCapacity);
}

function disable() {
	Ancillary.unregister('item', hitPointRetrainCapacity);
}

new Feature({ setting: 'class-hitpoint-retrain-capacity', label: 'Hit Point Retrain Potential', hint: 'Display how much hit points could be retrained.', category: 'items', enable, disable, stage: 'ready' });
