// Adds little display of what action an item uses and if it counts as action within the system

import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { createNode } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 */
function injectActionDisplay(sheet, [html], options) {
	const item = sheet.item;
	if (!item) return;

	const h = html.querySelector('.header-details .summary');
	if (!h) return; // shouldn't happen

	const list = createNode('ul', null, ['lil-item-details', 'summary']);

	Hooks.callAll('little-helper.item.header.properties', sheet, list);

	if (list.childNodes.length) {
		h.after(list);
	}
}

function enable() {
	Ancillary.register('item', injectActionDisplay);
}

function disable() {
	Ancillary.unregister('item', injectActionDisplay);
}

new Feature({ setting: 'item-header-enrichment', label: 'Header Enrichment', hint: 'Display more info in the item sheet header, such as used action and identified status.', category: 'items', enable, disable, stage: 'ready' });
