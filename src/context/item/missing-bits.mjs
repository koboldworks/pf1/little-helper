import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';
import { hookCoreTooltip } from '@core/tooltip.mjs';

function addWarning(el, text) {
	const n = createNode('i', null, ['fas', 'fa-exclamation-triangle', 'lil-warning-icon']);
	if (text) hookCoreTooltip(n, { fn: () => text });
	el.before(n);
}

function missingActionBits(doc, html, shared) {
	const data = shared.systemData;

	// Ammo type missing with uses ammuniton selected
	if (data.usesAmmo && !data.ammoType) {
		const type = html.querySelector('select[name="ammoType"],select[name="system.ammoType"]');
		if (type) {
			addWarning(type, 'Missing selection with ammo usage enabled.');
			type.classList.add('lil-missing-input');
		}
	}

	if (!data.range?.units) {
		const rng = html.querySelector('select[name="range.units"],select[name="system.range.units"]');
		if (rng) {
			addWarning(rng, 'No range defined.');
			rng.classList.add('lil-missing-input');
		}
	}

	if (data.actionType === 'save' && data.save.dc == 0) {
		const dc = html.querySelector('input[name="save.dc"],input[name="system.save.dc"]');
		if (dc) {
			addWarning(dc, 'Missing saving throw DC.');
			dc.classList.add('lil-missing-input');
		}
	}

	const sdc = data.save?.dc;
	if (['save', 'spellsave'].includes(data.actionType) || sdc !== undefined && sdc != 0) {
		const type = data.save?.type;
		if (type !== undefined && type == 0) {
			const st = html.querySelector('select[name="save.type"],select[name="system.save.type"]');
			if (st) {
				addWarning(st, 'No save type selected.');
				st.classList.add('lil-missing-input');
			}
		}
		const desc = data.save?.description;
		if (desc !== undefined && desc == 0) {
			const sd = html.querySelector('input[name="save.description"],input[name="system.save.description"]');
			if (sd) {
				addWarning(sd, 'No save effect defined.');
				sd.classList.add('lil-missing-input');
			}
		}
	}

	// Measure Templates
	if (data.measureTemplate !== undefined && data.measureTemplate.type != 0) {
		if (data.measureTemplate.size == 0) {
			const mts = html.querySelector('input[name="measureTemplate.size"],input[name="system.measureTemplate.size"]');
			if (mts) {
				addWarning(mts, 'No template size defined.');
				mts.classList.add('lil-missing-input');
			}
		}

		if (data.measureTemplate.customColor == 0) {
			const mcc = html.querySelector('input[name="measureTemplate.customColor"],input[name="system.measureTemplate.customColor"]');
			if (mcc) {
				addWarning(mcc, 'No override color defined.');
				mcc.classList.add('lil-missing-input');
			}
		}

		if (data.measureTemplate.customTexture == 0) {
			const mct = html.querySelector('input[name="measureTemplate.customTexture"],input[name="system.measureTemplate.customTexture"]');
			if (mct) {
				addWarning(mct, 'No override texture defined.');
				mct.classList.add('lil-missing-input');
			}
		}
	}
}

/**
 * @param {ItemSheet|ActionSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function missingItemBits(sheet, [html], options, shared) {
	const data = shared.systemData,
		doc = shared.document;

	const isRealAction = doc instanceof pf1.components.ItemAction;
	const isAction = doc instanceof pf1.components.ItemAction;
	if (isAction) missingActionBits(doc, html, shared);

	if (!isRealAction) {
		// Unidentified item stuff
		const docData = shared.systemData;

		/** @type {HTMLElement} */
		const tab = shared.tabs.description?.tab;
		if (!tab) return;

		if (docData.identified === false && game.user.isGM) {
			const unIdentDesc = docData.description?.unidentified || '';

			if (unIdentDesc.length == 0) {
				const uin = tab.querySelector('.sheet-navigation.subtabs [data-tab="unidentified"]');
				uin?.append(' ', createNode('span', '(n/a)', ['lil-not-described', 'lil-unidentified-warning']));
			}

			const unIdentPrice = docData.unidentified?.price || 0;
			if (unIdentPrice == 0) {
				const uip = tab.querySelector('[data-attr-name="system.unidentified.price"]')?.firstElementChild;
				uip?.classList.add('lil-not-priced', 'lil-unidentified-warning');
			}
		}
	}
}

function enable() {
	Ancillary.register('item', missingItemBits);
	Ancillary.register('action', missingItemBits);
}

function disable() {
	Ancillary.unregister('item', missingItemBits);
	Ancillary.unregister('action', missingItemBits);
}

new Feature({ setting: 'missing-item-bits', label: 'Missing Bits', hint: 'Add warnings about missing bits.', category: 'items', enable, disable, stage: 'ready' });
