import { CFG } from '@root/config.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { createNode } from '@root/common.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function addNoActionWarning(sheet, [html], options, shared) {
	const item = shared.document,
		itemData = shared.systemData;

	if (itemData.actions === undefined) return;
	if (itemData.actions.length > 0) return;

	const actions = html.querySelector('.actions .action-parts');
	actions?.append(createNode('div', 'No Actions', ['warning', 'no-actions']));
}

function enable() {
	Ancillary.register('item', addNoActionWarning);
}

enable();
