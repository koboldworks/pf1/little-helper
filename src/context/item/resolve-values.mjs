import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { createNode, signNum } from '@root/common.mjs';
import { SharedData } from '@core/shared-data.mjs';

function createValue(value) {
	const c = createNode('label', null, ['lil-resolved-value']);
	c.append('=', createNode('span', value, ['value']));
	return c;
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function resolveActorValues(sheet, [html], options, shared) {
	const actor = sheet.actor;
	if (!['character', 'npc'].includes(actor.type)) return;
	if (!shared.coreSheet && !shared.altSheetZenvy) return; // Ignore most alt sheets
	const rollData = shared.rollData;

	/** @type {Element} */
	const bookTab = shared.tabs.spellbook?.tab;
	if (bookTab) {
		bookTab.querySelectorAll('.spellcasting-ability select[name]').forEach(el => {
			const abl = el.value;
			const mod = rollData.abilities[abl]?.mod;
			if (Number.isFinite(mod)) {
				const value = createValue(`${signNum(mod)}`);
				el.after(value);
			}
		});
	}
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function resolveItemValues(sheet, [html], options, shared) {
	if (!shared.systemDocument) return;
	const data = shared.systemData;
	const actor = shared.actor;
	if (!actor) return;
	const rollData = shared.rollData;

	// ATTACK ABILITY
	if (data.ability?.attack) {
		const ablAtk = html.querySelector('[name="ability.attack"],[name="system.ability.attack"]');
		const mod = rollData.abilities[data.ability?.attack].mod;
		const value = createValue(signNum(mod));
		ablAtk?.after(value);
	}

	// ABILITY DAMAGE
	if (data.ability?.damage) {
		const ablMult = html.querySelector('[name="ability.damageMult"],[name="system.ability.damageMult"]');
		const abl = data.ability?.damage,
			mult = data.ability?.damageMult,
			mod = rollData.abilities[abl].mod;
		const value = createValue(signNum(Math.floor(mod * mult)));
		ablMult?.after(value);
	}

	// RANGE
	const rngUnits = rollData.action?.range?.units ?? rollData.item.range?.units;
	if (['melee', 'reach'].includes(rngUnits)) {
		const rng = html.querySelector('[name="range.units"],[name="system.range.units"]');
		rng?.after(createValue(rollData.range[rngUnits]));
	}

	const rngMinUnits = rollData.action?.range?.minUnits ?? rollData.item.range?.minUnits;
	if (['melee', 'reach'].includes(rngMinUnits)) {
		const rng = html.querySelector('[name="range.minUnits"],[name="system.range.minUnits"]');
		rng?.after(createValue(rollData.range[rngMinUnits]));
	}

	// const pwAtk = html.querySelector('[name="powerAttack.multiplier"]');
}

function enable() {
	Ancillary.register('action', resolveItemValues);
	Ancillary.register('item', resolveItemValues);
	Ancillary.register('actor', resolveActorValues);
}

function disable() {
	Ancillary.unregister('action', resolveItemValues);
	Ancillary.unregister('item', resolveItemValues);
	Ancillary.unregister('actor', resolveActorValues);
}

new Feature({ setting: 'resolve-values', label: 'Resolve Values', hint: 'Resolves some indirect values to their actual numbers.', category: 'common', enable, disable, stage: 'ready' });
