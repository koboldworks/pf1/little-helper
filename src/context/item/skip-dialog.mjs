function skipDialogOnUse(item, options) {
	const skip = item.flags?.lilhelper?.item?.skipDialog ?? 0; // 0 = default, 1 = show, -1 = hide
	if (skip < 0) options.skipDialog = true;
	else if (skip > 0) options.skipDialog = false;
}

Hooks.on('little-helper.useAttackWrapper', skipDialogOnUse);
