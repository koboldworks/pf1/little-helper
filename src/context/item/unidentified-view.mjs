import { CFG } from '@root/config.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { FAIcons } from '@core/icons.mjs';
import { addCoreTooltip } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function renderItemSheet(sheet, [html], options, shared) {
	// const pv = sheet.__littleHelper?.unidentifiedView ?? false;
	const app = html.closest('.app[data-appid]');
	const button = app?.querySelector('header > .little-helper-unidentified-view');
	if (button) addCoreTooltip(button, 'Unidentified View');
}

/**
 * @param {ItemSheet} sheet
 * @param {object[]} buttons
 */
function addViewToggle(sheet, buttons) {
	if (sheet.item.system.identified) return;
	if (sheet.item.system.identified === undefined) return;

	const C = CFG.console.colors;

	buttons.unshift({
		class: 'little-helper-unidentified-view',
		icon: FAIcons.eyeSlash,
		// label: i18n.get('LittleHelper.UI.RefreshButton'),
		onclick: () => {
			const pv = sheet.__littleHelper?.unidentifiedView ?? false;
			if (pv) {
				console.log('%cLittle Helper%c 🦎 | Returning to Normal View', C.main, C.unset);
				delete sheet.__littleHelper.unidentifiedView;
			}
			else {
				console.log('%cLittle Helper%c 🦎 | Swapping to unidentified View', C.main, C.unset);
				sheet.__littleHelper ??= {};
				sheet.__littleHelper.unidentifiedView = true;
			}
			sheet.render(false, { __lilUnidentifiedView: !pv });
		},
	});
}

async function getDataWrapper(wrapped, ...args) {
	const rv = await wrapped.call(this, ...args);
	const pv = this.__littleHelper?.unidentifiedView ?? false;

	if (pv) {
		rv.isGM = false;

		const isIdentified = rv.system.identified;

		rv.showUnidentifiedData = true;
		rv.showIdentifyDescription = false;

		// Mess with the data since it is not reliant on the above toggles always
		rv.itemName = rv.system.unidentified.name;
		rv.descriptionAttributes = rv.descriptionAttributes.filter(d => {
			switch (d.name) {
				case 'system.unidentified.price':
					d.label = i18n.get('PF1.Price');
					break;
				case 'system.price':
					return false;
			}
			return true;
		});
	}

	return rv;
}

function renderWrapper(wrapped, force, options = {}) {
	if (game.user.isGM) {
		this.__littleHelper ??= {};
		this.__littleHelper.unidentifiedView = options.__lilUnidentifiedView ?? false;
	}
	return wrapped.call(this, force, options);
}

/* global libWrapper */

function enable() {
	if (!game.user.isGM) return;
	Ancillary.register('item', renderItemSheet);

	Hooks.on('getItemSheetHeaderButtons', addViewToggle);

	libWrapper.register(CFG.id, 'pf1.applications.item.ItemSheetPF.prototype.render', renderWrapper, libWrapper.WRAPPER);
	libWrapper.register(CFG.id, 'pf1.applications.item.ItemSheetPF.prototype.getData', getDataWrapper, libWrapper.WRAPPER);
}

function disable() {
	if (!game.user.isGM) return;
	Ancillary.unregister('item', renderItemSheet);

	Hooks.off('getItemSheetHeaderButtons', addViewToggle);

	libWrapper.unregister(CFG.id, 'pf1.applications.item.ItemSheetPF.prototype.render', renderWrapper);
	libWrapper.unregister(CFG.id, 'pf1.applications.item.ItemSheetPF.prototype.getData', getDataWrapper);
}

new Feature({ setting: 'item-unidentified-view', label: 'Simulated Unidentified View', hint: 'Adds header button to items to toggle partially simulated unidentified view of the item, as player would see it.', category: 'items', enable, disable, stage: 'ready', gm: true });
