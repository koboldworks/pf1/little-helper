import { CFG } from '@root/config.mjs';
import { Feature } from '@core/feature.mjs';

const POWER_ATTACK_FLAG = 'autoPowerAttack';

/**
 * @param {Dialog} dialog
 * @param {JQuery} html
 * @param {object} options
 */
const enableDialogPowerAttack = (dialog, [html], options) => {
	const action = dialog.action;
	const item = action?.item;
	const apa = item?.flags.lilhelper?.item?.[POWER_ATTACK_FLAG] ?? false;
	if (!apa) return;

	const pa = html.querySelector('input[name="power-attack"]');
	if (!pa) return;

	// change event fires after the dialog has already been re-rendered
	pa.addEventListener('click', ev => dialog._setPowerAttack = ev.target.checked, { passive: true });

	pa.checked = dialog._setPowerAttack ?? true;
};

/**
 * @this ActionUse
 * @param wrapped
 * @param {object} formData
 */
function autoPowerAttack(wrapped, formData) {
	// Don't touch if already there (e.g. due to dialog)
	const apa = this.item.flags.lilhelper?.item?.[POWER_ATTACK_FLAG] ?? false;
	if (apa) {
		formData ??= {};
		formData['power-attack'] ??= apa;
	}
	return wrapped.call(this, formData);
}

/* global libWrapper */
const enable = () => {
	Hooks.on('renderAttackDialog', enableDialogPowerAttack);

	const C = CFG.console.colors;

	// how to tell the dialog was skipped so power attack isn't enforced?
	console.log('%cLittle Helper%c 🦎 | Overriding alterRollData', C.main, C.unset);
	libWrapper.register(CFG.id, 'pf1.actionUse.ActionUse.prototype.alterRollData', autoPowerAttack, libWrapper.WRAPPER);
};

function disable() {
	Hooks.off('renderAttackDialog', enableDialogPowerAttack);

	libWrapper.unregister(CFG.id, 'pf1.actionUse.ActionUse.prototype.alterRollData');
}

Hooks.once('setup', () => enable());
