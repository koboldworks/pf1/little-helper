// Configuration is set up in item config.

const forceRoll = (item, options) => {
	if (!options.dice) {
		const roll = item.flags?.lilhelper?.item?.rollOverride ?? null;
		if (roll) options.dice = roll;
	}
};

Hooks.on('little-helper.useAttackWrapper', forceRoll);
