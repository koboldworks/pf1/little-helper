// Configuration is set up in item config.

import { CFG } from '@root/config.mjs';

const fetchForceModeSetting = (item) => {
	const rawRollMode = item.flags?.lilhelper?.item?.rollmodeOverride;
	if (rawRollMode) {
		let rollMode = Object.keys(CFG.ROLLMODES).includes(rawRollMode) ? rawRollMode : null;
		if (rollMode === 'roll') rollMode = 'publicroll';
		return rollMode ?? null;
	}
	return null;
};

const forceRollMode = (item, options) => {
	if (!options.rollMode) {
		const mode = fetchForceModeSetting(item);
		if (mode && options.rollMode !== mode) {
			options.rollMode = mode;
		}
	}
};

Hooks.on('little-helper.useAttackWrapper', forceRollMode);
