/**
 * Hide attacks from items with `hideAttacks` boolean item flag set.
 */

import { Ancillary } from '@root/ancillary.mjs';

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
const hideAttacks = (cm, [html]) => {
	const item = cm.itemSource;
	if (!item?.hasAttack) return; // Only the item has the required info

	const ha = item.flags?.lilhelper?.item?.hideAttacks ?? false;
	if (!ha) return;

	html.querySelectorAll('.chat-attack td[colspan] .inline-roll.inline-result')
		.forEach(el => el.closest('tr')?.remove());
};

Ancillary.register('chat', hideAttacks);
