import { CFG } from '@root/config.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/* global libWrapper */

const doKeen = (cr) => 21 - (21 - cr) * 2;

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
const sheetKeen = (sheet, [html], options, shared) => {
	const item = shared.item;
	const action = shared.action;

	const keen = item?.flags?.lilhelper?.item?.keen ?? false;

	if (!keen) return;
	const crEl = html.querySelector('[name="ability.critRange"],[name="system.ability.critRange"]');
	if (crEl) {
		if (item.system.broken) {
			crEl.after(createNode('span', `${i18n.get('PF1.Broken')}: 20`, ['lil-critrange-display', 'broken']));
			return;
		}
		const cr = action.ability?.critRange ?? 20;
		crEl.after(createNode('span', `Keen: ${doKeen(cr)} – 20`, ['lil-critrange-display', 'keen']));
	}
};

function keenCritRange(wrapped) {
	const cr = wrapped.call(this),
		item = this.item ?? this.action?.item,
		isBroken = item.system.broken ?? false,
		keen = item?.flags?.lilhelper?.item?.keen ?? false;
	// console.log('Keen:', !isBroken && keen ? doKeen(cr) : cr, { original: cr, isBroken, keen });
	return !isBroken && keen ? doKeen(cr) : cr;
}

function enable() {
	// how to tell the dialog was skipped so power attack isn't enforced?
	const C = CFG.console.colors;
	console.log('%cLittle Helper%c 🦎 | Overriding ChatAttack.critRange', C.main, C.unset);
	libWrapper.register(CFG.id, 'pf1.components.ItemAction.prototype.critRange', keenCritRange, libWrapper.WRAPPER);

	Ancillary.register('action', sheetKeen);
}

function disable() {
	libWrapper.unregister(CFG.id, 'pf1.components.ItemAction.prototype.critRange');
}

Hooks.once('setup', enable);
