import './compendiums/_init.mjs';

import './tokenhud-status-effects.mjs';
// import './actor-list-portraits.mjs';

import './macro/_init.mjs';

import './id-tag.mjs';

import './token-vision-hints.mjs';

import './world-identity.mjs';

// Sidebar

import './sidebar/_init.mjs';

import './wider-sidebar.mjs';

import './actors/_init.mjs';
import './items/_init.mjs';
import './rolltables/_init.mjs';
import './scenes/_init.mjs';
import './journals/_init.mjs';

import './chat/_init.mjs';

// Sheet headers

import './refresh-button.mjs';

// Combat

import './combat/_init.mjs';

// Other

import './boost.mjs';
