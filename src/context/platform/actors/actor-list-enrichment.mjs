import { Feature } from '@root/core/feature.mjs';
import { createNode, addCoreTooltip } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

function getActorType(actor) {
	switch (actor.type) {
		case 'character':
			return 'PC';
		case 'npc':
			return 'NPC';
		case 'basic':
			return 'Basic';
		default:
			return i18n.get(CONFIG.Actor.typeLabels[actor.type]);
	}
}

function CRtoString(value) {
	switch (value) {
		case 0.125: return '1/8';
		case 0.1625: return '1/6';
		case 0.25: return '1/4';
		case 0.3375: return '1/3';
		case 0.5: return '1/2';
		default: return `${value}`;
	}
}

function generateContent(actor) {
	const t = document.createDocumentFragment();

	const sysData = actor.system;

	// Display actor type
	const actorType = createNode('span', getActorType(actor), ['lil-tag', 'type', actor.type]);
	addCoreTooltip(actorType, i18n.get('LittleHelper.Sidebar.ActorType', { type: actor.type }));
	t.append(actorType);

	// Display Link status
	const isPC = actor.hasPlayerOwner,
		isLinked = actor.prototypeToken.actorLink;
	if ((isPC && !isLinked) || (!isPC && isLinked)) {
		const linkState = createNode('span', isLinked ? i18n.get('LittleHelper.Linked') : i18n.get('LittleHelper.Unlinked'), ['lil-tag', 'link-state', isPC ? 'pc' : 'npc', isLinked ? 'linked' : 'unlinked']);
		const playerOwned = i18n.get(isPC ? 'Yes' : 'No');
		const tokenLinked = i18n.get(isLinked ? 'Yes' : 'No');
		addCoreTooltip(linkState, i18n.get('LittleHelper.Sidebar.ActorType', { type: actor.type }) + `<br>Player Owned: ${playerOwned}<br>Linked Token: ${tokenLinked}`);
		t.append(linkState);
	}

	// Display player owner(s) for GM
	if (isPC && game.user.isGM) {
		const ownership = actor.ownership;
		const owners = Object.entries(ownership).reduce((total, [uid, perm]) => {
			if (uid === 'default') return total; // skip default permission
			if (perm >= CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER) total.push(uid);
			return total;
		}, []).map(uid => game.users.get(uid)).filter(u => u && !u.isGM);

		if (owners.length === 1)
			t.append(createNode('span', `Owner: ${owners[0].name}`, ['lil-tag', 'owner', 'singular']));
		else if (owners.length > 1 || ownership.default >= CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER)
			t.append(createNode('span', 'Multiple Owners', ['lil-tag', 'owner', 'multiple']));
	}

	// Display actual ownership level for player
	if (!game.user.isGM) {
		const perm = actor.permission;
		const permStr = foundry.utils.invertObject(CONST.DOCUMENT_OWNERSHIP_LEVELS)[perm];
		const label = i18n.get(`OWNERSHIP.${permStr}`);
		t.append(createNode('span', label, ['lil-tag', 'permission', permStr.toLowerCase()]));
	}

	// Display level/CR
	// presence of getCR function says the actor type cares about CR, so we don't display CR for others even if it's present.
	if (typeof actor.getCR === 'function') {
		const cr = actor.getCR();
		t.append(createNode('span', `CR ${pf1.utils.CR.fromNumber(cr)}`, ['lil-tag', 'cr']));
	}

	return t;
}

/**
 * @param {ActorDirectory} app
 * @param {JQuery} html
 * @param {object} options
 */
function onRenderActorDirectory(app, [html], options) {
	html.classList.add('lil-enriched-directory');

	html.querySelectorAll('li.document.actor')
		.forEach(el => {
			const actorId = el.dataset.documentId;
			const actor = game.actors.get(actorId);

			if (actor.permission < CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER) return;

			const name = el.querySelector('.document-name');
			if (!name) return;

			const holder = createNode('div', null, ['lil-item-info']);
			const content = generateContent(actor);
			if (content) holder.replaceChildren(content);

			el.classList.add('lil-enriched-document');

			name.append(holder);
		});
}

/**
 * updateActor event handler.
 *
 * @param {ActorPF} actor
 * @param {object} update
 */
function updateActorEvent(actor, update) {
	// TODO: Check if anything actually of interest was updated
	// ^ May be impossible with item Changes or similar modifying the derived values.

	const updateElements = [];

	const sidetab = document.getElementById('actors');
	if (sidetab) updateElements.push(sidetab);
	const popout = document.getElementById('actors-popout');
	if (popout) updateElements.push(popout);

	if (updateElements.length) {
		const content = generateContent(actor);
		updateElements.forEach((el, i, arr) => {
			const ii = el.querySelector(`li.document.actor[data-document-id="${actor.id}"] .lil-item-info`);
			if (!ii) return;
			const sc = i + 1 !== arr.length ? content.cloneNode(true) : content;
			ii.replaceChildren(sc);
		});
	}
}

function enable() {
	Hooks.on('renderActorDirectory', onRenderActorDirectory);
	Hooks.on('updateActor', updateActorEvent);
}

function disable() {
	Hooks.off('renderActorDirectory', onRenderActorDirectory);
	Hooks.off('updateActor', updateActorEvent);
	return false;
}

new Feature({ setting: 'enrich-actors-directory', label: 'Enrich Actors Directory', hint: 'Add additional information to actors directory.', category: 'foundry-ui', enable, disable, stage: 'setup' });
