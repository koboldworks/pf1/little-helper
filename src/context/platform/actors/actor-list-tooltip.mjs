import { Feature } from '@root/core/feature.mjs';
import { createNode, clampNum } from '@root/common.mjs';
import { healthLabels } from '@root/helpers/wounds.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * Plans:
 * - Experience
 */

// TODO: Sort types first, level second.
const classLevelSorter = (a, b) => b.system.level - a.system.level;

let ownership;

/**
 * @param {Element} el Parent element that prompts the tooltip.
 * @param {Element} tt Tooltip element.
 */
function hookTooltip(el, tt) {
	el.addEventListener('mousemove', ev => {
		const displayOnLeft = ev.clientX > Math.floor(window.innerWidth / 2);

		tt.classList.toggle('left-aligned', displayOnLeft);
		tt.classList.toggle('right-aligned', !displayOnLeft);

		const rect = el.getBoundingClientRect();
		const ttB = tt.clientHeight + rect.top;
		const bOff = ttB > window.innerHeight - 20 ? -(window.innerHeight - 20 - ttB) : 0;
		tt.style.cssText += `--left:${rect.left};--right:${rect.right};--top:${rect.top};--width:${tt.clientWidth};--bottom-offset:${bOff};`;
	}, { passive: true });
}

const havePerm = (c) => c.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER);

/**
 * @param {Actor} actor
 * @param {Element} tt
 */
const updateActorTooltip = async (actor, tt) => {
	const d = document.createDocumentFragment();

	const isNormal = ['character', 'npc'].includes(actor.type),
		hasPlayerOwner = actor.hasPlayerOwner;

	// const isOwner = actor.isOwner;
	const isObserver = actor.testUserPermission(game.user, 'OBSERVER');
	const isLimited = actor.testUserPermission(game.user, 'LIMITED', { exact: true });

	if (!isLimited && !isObserver) return;

	const hpCfg = game.settings.get('pf1', 'healthConfig');
	const variantHpRules = hpCfg.getActorConfig(actor).rules;

	const actorData = actor.system,
		attributes = actorData.attributes;

	// Token Name
	const protoToken = actor.prototypeToken;
	if (actor.name !== protoToken.name && isObserver) {
		const tEl = createNode('div', null, ['token']);
		tEl.append(
			createNode('label', 'Alias', ['label']),
			createNode('span', protoToken.name, ['simple-value', 'value', 'text', 'span-3']),
		);
		d.append(tEl);
	}

	if (!isNormal) {
		// Actor Type
		const typeEl = createNode('div', null, ['type']);
		typeEl.append(
			createNode('label', i18n.get('PF1.Type'), ['label']),
			createNode('span', actor.type, ['simple-value', 'value', 'text', 'span-3']),
		);
		d.append(typeEl);
	}
	// Normal actor
	else {
		const woundsAndVigor = variantHpRules.useWoundsAndVigor === true;

		// HD
		const hd = attributes.hd.total;
		if (hd > 0) {
			const hdEl = createNode('div', null, ['hd']);
			hdEl.append(
				createNode('label', i18n.get('PF1.HitDieShort'), ['label']),
				createNode('span', `${hd}`, ['value', 'span-3']),
			);
			d.append(hdEl);
		}

		// CR
		if (actorData.details.cr > 0) {
			// CR
		}

		if (['character', 'npc'].includes(actor.type)) {
			// Core Health Rules
			if (!woundsAndVigor) {
				const health = attributes.hp,
					ehp = health.value + health.temp,
					hpPercentage = ehp / health.max,
					woundThreshold = clampNum(4 - Math.ceil(hpPercentage * 4), 0, 3);

				const hpEl = createNode('div', null, ['health']);
				// Health
				const hpValueEl = createNode('span', null, ['health-data', 'simple-value']);
				hpValueEl.append(
					createNode('span', `${ehp}`, ['health', 'effective', 'value']),
					' / ',
					createNode('span', `${health.max}`, ['health', 'max', 'value']),
					' (',
					createNode('span', `${pf1.utils.limitPrecision(hpPercentage * 100, 1)}`, ['health', 'percentage', 'value']),
					'%)',
				);
				hpEl.append(createNode('label', 'HP', ['label']), hpValueEl);

				// Wound threshold
				const hpLabel = createNode('div', null, ['health']);
				hpLabel.append(
					createNode('label', 'Status', ['label']),
					createNode('span', `${healthLabels[woundThreshold]}`, ['simple-value', 'value', 'text', 'span-3']),
				);

				d.append(hpEl, hpLabel);
			}
			// Wounds & Vigor
			else {
				const { wounds, vigor } = attributes;
				const evig = vigor.value + (vigor.temp || 0),
					vigPct = evig / vigor.max,
					wndPCt = wounds.value / wounds.max;

				const hpEl = createNode('div', null, ['health']);

				// Wounds
				const wndValueEl = createNode('span', null, ['health-data', 'simple-value']);
				wndValueEl.append(
					createNode('span', `${wounds.value}`, ['wounds', 'effective', 'value']),
					' / ',
					createNode('span', `${wounds.max}`, ['wounds', 'max', 'value']),
					' (',
					createNode('span', `${pf1.utils.limitPrecision(wndPCt * 100, 1)}`, ['wounds', 'percentage', 'value']),
					'%)',
				);
				hpEl.append(createNode('label', i18n.get('PF1.Wounds'), ['label']), wndValueEl);

				// Vigor
				const vigValueEl = createNode('span', null, ['health-data', 'simple-value']);
				vigValueEl.append(
					createNode('span', `${evig}`, ['vigor', 'effective', 'value']),
					' / ',
					createNode('span', `${vigor.max}`, ['vigor', 'max', 'value']),
					' (',
					createNode('span', `${pf1.utils.limitPrecision(vigPct * 100, 1)}`, ['vigor', 'percentage', 'value']),
					'%)',
				);
				hpEl.append(createNode('label', i18n.get('PF1.Vigor'), ['label']), vigValueEl);

				d.append(hpEl);
			}
		}

		// Race
		const race = actor.race;
		if (race) {
			const rEl = createNode('div', null, ['race']);
			rEl.append(
				createNode('label', i18n.get(CONFIG.Item.typeLabels.race), ['label']),
				createNode('span', race.name, ['simple-value', 'text', 'value']),
			);
			d.append(rEl);
		}

		// Classes
		const classes = actor.itemTypes.class
			.filter(i => i.system.level > 0)
			.sort(classLevelSorter);
		if (classes.length) {
			const clsEl = createNode('div', null, ['classes']);
			const clsList = createNode('ul', null, ['class-list', 'span-4']);
			classes.forEach(cls => {
				const clsLiEl = createNode('li', null, ['class']);
				clsLiEl.append(
					createNode('span', cls.name, ['text', 'value']),
					createNode('span', cls.system.level, ['number', 'value']),
				);
				clsList.append(clsLiEl);
			});
			clsEl.append(createNode('label', 'Classes', ['label', 'span-4']), clsList);
			d.append(clsEl);
		}
	}

	ownership ??= {
		default: i18n.get('OWNERSHIP.DEFAULT'),
		1: i18n.get('OWNERSHIP.LIMITED'),
		2: i18n.get('OWNERSHIP.OBSERVER'),
		3: i18n.get('OWNERSHIP.OWNER'),
	};

	// Users with permissions
	const owners = game.users.players.filter(u => actor.testUserPermission(u, 'LIMITED'));

	const defaultOwnership = actor.ownership.default;

	if (defaultOwnership > 0 || owners.length) {
		const ownerList = createNode('ul', null, ['owner-list', 'span-4']);
		const addOwner = (name, level) => {
			const ownerLiEl = createNode('li', null, ['owner']);
			level ??= 'default';
			ownerLiEl.append(
				createNode('span', name, ['text', 'value']),
				createNode('span', ownership[level], ['value', 'text']),
			);
			ownerList.append(ownerLiEl);
		};
		if (defaultOwnership > 0)
			addOwner(ownership.default, defaultOwnership);
		owners.forEach(user => addOwner(user.name, actor.ownership[user.id]));
		const ownerEl = createNode('div', null, ['ownership']);
		ownerEl.append(createNode('label', 'Permissions', ['label', 'span-4']), ownerList);
		d.append(ownerEl);
	}

	// Link Status
	const linkStatus = protoToken.actorLink,
		linkStatusExpected = linkStatus ? !!hasPlayerOwner : !hasPlayerOwner;
	const linkEl = createNode('div', null, ['link-status']);
	const linkStateLabel = linkStatus ? i18n.get('LittleHelper.LinkState.Linked.Label') : i18n.get('LittleHelper.LinkState.Unlinked.Label');
	linkEl.append(
		createNode('label', 'Link State'),
		createNode('span', linkStateLabel, ['value', 'boolean', 'link-state', linkStatusExpected ? 'expected' : 'unexpected', linkStatus ? 'true' : 'false', 'span-3']),
	);

	d.append(linkEl);

	tt.replaceChildren(d);
};

/**
 * @param {Actor} actor
 * @param {Element} dom
 */
function generateActorTooltip(actor, dom) {
	if (!havePerm(actor)) return;

	const tt = createNode('span', null, ['lil-directory-tooltip']);
	dom.classList.add('has-little-helper-tooltip');
	dom.append(tt);

	// Passive on-demand filling
	dom.addEventListener('mouseenter', ev => updateActorTooltip(actor, tt), { passive: true });

	hookTooltip(dom, tt);
}

/**
 * @param {ActorDirectory} app
 * @param {JQuery} html
 */
function hookActorDirectoryTooltips(app, [html]) {
	const listing = html.querySelector('ol.directory-list');
	if (!listing) return;

	listing.querySelectorAll('.actor[data-document-id]')
		.forEach(el => generateActorTooltip(game.actors.get(el.dataset.documentId), el));
}

function enable() {
	Hooks.on('renderActorDirectory', hookActorDirectoryTooltips);
}

function disable() {
	Hooks.off('renderActorDirectory', hookActorDirectoryTooltips);
	return false;
}

new Feature({ setting: 'actor-list-tooltip', label: 'Actors Directory Tooltips', hint: 'Actor tooltips for the sidebar directory, providing basic info.', category: 'foundry-ui', enable, disable, stage: 'init' });
