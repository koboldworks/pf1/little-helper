import { Feature } from '@root/core/feature.mjs';

// Everything this does is done in CSS for now

function enable() {
	document.body.classList.add('lil-boost');
}

function disable() {
	document.body.classList.remove('lil-boost');
}

new Feature({ setting: 'boost', label: 'Performance Boost', hint: 'Add content hints for browser.', category: 'experiment', enable, disable, stage: 'ready', enabledByDefault: false });
