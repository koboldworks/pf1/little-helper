import { Feature } from '@root/core/feature.mjs';
import { FAIcons } from '@root/core/icons.mjs';

/**
 * @param _
 * @param {object[]} entries
 */
function insertOpenActorSheetContextOption(_, entries) {
	entries.unshift({
		name: 'LittleHelper.UI.OpenActorSheet',
		icon: `<i class="${FAIcons.fileAlt}"></i>`,
		condition: ([html]) => {
			const msgId = html.dataset.messageId;
			const cm = game.messages.get(msgId);
			const actor = ChatMessage.getSpeakerActor(cm?.speaker);
			return actor && actor.permission >= CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER;
		},
		callback: ([html]) => {
			const msgId = html.dataset.messageId;
			const cm = game.messages.get(msgId);
			const actor = ChatMessage.getSpeakerActor(cm?.speaker);
			actor?.sheet?.render(true, { focus: true, token: actor.token });
		},
	});
}

function enable() {
	Hooks.on('getChatLogEntryContext', insertOpenActorSheetContextOption);
}

function disable() {
	Hooks.off('getChatLogEntryContext', insertOpenActorSheetContextOption);
}

new Feature({ setting: 'chatlog-open-actor', label: 'Context Menu Actor Sheet', hint: 'Adds context menu option to open the relevant actor sheet directly.', category: 'chat-card', enable, disable, stage: 'init' });
