import { Feature } from '@root/core/feature.mjs';
import { FAIcons } from '@root/core/icons.mjs';

/**
 * @param {JQuery|Application} _app
 * @param {object[]} entries
 */
function insertOpenItemSheetContextOption(_app, entries) {
	entries.unshift({
		name: 'LittleHelper.UI.OpenItemSheet',
		icon: `<i class="${FAIcons.fileAlt}"></i>`,
		condition: function ([html]) {
			const msgId = html.dataset.messageId;
			const cm = game.messages.get(msgId);
			return cm.itemSource?.testUserPermission(game.user, 'OBSERVER') ?? false;
		},
		callback: function([html]) {
			const msgId = html.dataset.messageId;
			const cm = game.messages.get(msgId);
			cm.itemSource?.sheet.render(true, { focus: true });
		},
	});
}

function enable() {
	Hooks.on('getChatLogEntryContext', insertOpenItemSheetContextOption);
}

function disable() {
	Hooks.off('getChatLogEntryContext', insertOpenItemSheetContextOption);
}

new Feature({ setting: 'chatlog-open-item', label: 'Context Menu Item Sheet', hint: 'Adds context menu option to open the relevant item sheet directly.', category: 'chat-card', enable, disable, stage: 'init' });
