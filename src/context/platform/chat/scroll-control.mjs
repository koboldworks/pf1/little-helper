// Prevent automatic scroll to bottom when scrolled up.

// TODO: Add new message counter to scroll to bottom button and highlight it.

import { CFG } from '@root/config.mjs';
import { createNode } from '@root/common.mjs';
import { Feature } from '@root/core/feature.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * For v11, new and improved.
 *
 * @param {Function} wrapped
 * @param {object} options
 */
function scrollToBottom(wrapped, options) {
	const C = CFG.console.colors;
	if (options?.force) {
		console.debug('%cLittle Helper%c 🦎 | Log scrolling forced.',
			C.main, C.unset, C.number, C.unset);
	}
	else {
		if (this._lilIgnoreDistance || this._lilIgnoreTabSwitch) {
			delete this._lilIgnoreDistance;
			delete this._lilIgnoreTabSwitch;
			return;
		}
	}

	return wrapped(options);
}

function tabChange(wrapped, event, tabs, active) {
	const app = ui[active];
	if (active === 'chat' && app) {
		const C = CFG.console.colors;
		console.debug('%cLittle Helper%c 🦎 | Log scrolling prevented for tab switch.', C.main, C.unset);
		app._lilIgnoreTabSwitch = true;
	}
	return wrapped(event, tabs, active);
}

function preNewMessage(wrapped, message, ...args) {
	const log = this.element[0].querySelector('#chat-log');
	if (log) {
		const distance = log.scrollHeight - (log.scrollTop + log.clientHeight);
		if (distance > 20) {
			this._lilIgnoreDistance = true;
			const C = CFG.console.colors;
			console.debug(`%cLittle Helper%c 🦎 | Log scrolling prevented. Current position %c${distance}%c pixels away.`,
				C.main, C.unset, C.number, C.unset);
		}
	}

	return wrapped(message, ...args);
}

function enable() {
	/* global libWrapper */

	libWrapper.register(CFG.id, 'ChatLog.prototype.scrollBottom', scrollToBottom, libWrapper.MIXED);
	libWrapper.register(CFG.id, 'ChatLog.prototype.postOne', preNewMessage, libWrapper.WRAPPER);
	libWrapper.register(CFG.id, 'Sidebar.prototype._onChangeTab', tabChange, libWrapper.WRAPPER);
}

function disable() {
	libWrapper.unregister(CFG.id, 'ChatLog.prototype.scrollBottom');
	libWrapper.unregister(CFG.id, 'ChatLog.prototype.postOne');
	libWrapper.unregister(CFG.id, 'Sidebar.prototype._onChangeTab');

	return false;
}

new Feature({ setting: 'chatlog-scroll-control', label: 'Scroll Control', hint: 'Prevents chat log from scrolling to bottom on new messages or tab switch if you\'ve scrolled up. And adds an easy button to scroll to bottom (v10 only).', category: 'foundry-ui', enable, disable, stage: 'init', conflict: { module: ['df-chat-enhance', 'tabbed-chatlog'] }, core: 12 });
