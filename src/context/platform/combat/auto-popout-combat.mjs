// Automatically pop out combat tracker if there is active combat.

// TODO: Add option to change default position?

import { Feature, SubSetting } from '@root/core/feature.mjs';

function popOutCombatTracker() {
	if (game.combat?.started) {
		ui.combat?.createPopout()?.render(true);
	}
}

/**
 * @param {Combat} combat
 * @param _update
 * @param {object} context
 */
function preUpdateCombatEvent(combat, _update, context) {
	context.__littleHelper ??= {};
	context.__littleHelper.started = combat.started;
}

/**
 * @param {Combat} combat
 * @param update
 * @param {object} context
 */
function updateCombatEvent(combat, update, context) {
	if (update.round === 1 || !context.__littleHelper?.started && combat.started)
		popOutCombatTracker();
}

function enable() {
	Hooks.on('preUpdateCombat', preUpdateCombatEvent);
	Hooks.on('updateCombat', updateCombatEvent);
	Hooks.once('ready', () => setTimeout(popOutCombatTracker, 200));
}

function disable() {
	Hooks.off('preUpdateCombat', preUpdateCombatEvent);
	Hooks.off('updateCombat', updateCombatEvent);
}

new Feature({
	setting: 'combat-auto-popout',
	label: 'Combat Auto-Popout',
	hint: 'Automatically pops out the combat tracker when combat starts and on login if combat has already started.',
	category: 'foundry-ui',
	enable,
	disable,
	stage: 'init',
	enabledByDefault: false,
	/*
	subsettings: {
		position: new SubSetting({
			label: 'Set Position',
			hint: 'Move combat tracker to specific position.',
			type: Object,
			fallback: null,
			display: (v) => {
				if (v) return `x(${v.x}), y(${v.y})`;
				else return 'undefined';
			},
			callback: {
				label: 'Set Position',
				fn: (_ev, _f, _ss) => {
					const ct = Object.values(ui.windows).find(win => win instanceof CombatTracker);
					if (ct) return { x: ct.position.left, y: ct.position.top };
					else return null;
				}
			}
		}),
	}
	*/
});
