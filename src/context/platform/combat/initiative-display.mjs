import { CFG } from '@root/config.mjs';
import { Feature } from '@root/core/feature.mjs';
import { createNode } from '@root/common.mjs';

/**
 * @param {CombatTracker} app
 * @param {JQuery<HTMLElement} html
 * @param {object} options
 */
function renderCombat(app, [html], options) {
	for (const el of html.querySelectorAll('.token-initiative .initiative')) {
		const [main, tiebreaker] = el.textContent.split('.', 2);
		if (tiebreaker === undefined) continue;
		const tbEl = createNode('span', ` ${tiebreaker}`, ['lil-tiebreaker']);
		el.replaceChildren(main, tbEl);
		el.classList.add('lil-tiebreaker-restyle');
	}
}

function enable() {
	Hooks.on('renderCombatTracker', renderCombat);
}

function disable() {
	Hooks.off('renderCombatTracker', renderCombat);
	return false;
}

new Feature({ setting: 'combat-tracker-init-tiebreaker', label: 'Combat Tracker Initiative Tiebreaker', hint: 'Alters display of initiative tiebreaker.', category: 'foundry-ui', enable, disable, stage: 'init' });
