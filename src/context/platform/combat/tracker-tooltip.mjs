import { CFG } from '@root/config.mjs';
import { Feature } from '@root/core/feature.mjs';
import { createNode, clampNum } from '@root/common.mjs';
import { healthLabels, evaluateDeathPoint } from '@root/helpers/wounds.mjs';

const tooltipTemplateSource = `modules/${CFG.id}/template/tooltip/combat-tracker.hbs`;

/**
 * @param {Element} parentEl Parent element that prompts the tooltip.
 * @param {Element} tt Tooltip element.
 */
function hookTooltip(parentEl, tt) {
	parentEl.addEventListener('mousemove', ev => {
		const displayOnLeft = ev.clientX > Math.floor(window.innerWidth / 2);

		tt.classList.toggle('left-aligned', displayOnLeft);
		tt.classList.toggle('right-aligned', !displayOnLeft);

		// BUG: This jitters when the combat tracker refreshes (add 20-50ms debounce?)

		const rect = parentEl.getBoundingClientRect();
		tt.style.cssText += `--left:${rect.left};--right:${rect.width};--top:${parentEl.offsetTop};--width:${tt.clientWidth};`;
	}, { passive: true });
}

const havePerm = (c) => c.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER);

/**
 * @param {Combatant} combatant
 */
function collectCombatantData(combatant) {
	const actor = combatant.actor;
	if (!actor) return;

	const actorData = actor.system,
		attributes = actorData.attributes,
		hp = attributes.hp,
		ac = attributes.ac,
		cmd = attributes.cmd;

	const context = {
		actor,
		combatant,
		health: {
			value: hp.value,
			max: hp.max,
			temp: hp.temp,
			get effective() { return this.value + this.temp; },
			get percentage() { return this.effective / this.max; },
			get threshold() { return clampNum(4 - Math.ceil(this.percentage * 4), 0, 3); },
			get label() {
				const ehp = this.effective;
				if (ehp < 0) return ehp <= evaluateDeathPoint(actor) ? healthLabels.dead : healthLabels.dying;
				return healthLabels[this.threshold];
			},
		},
		defenses: {
			ac: {
				value: ac.normal.total,
				ff: ac.flatFooted?.total,
			},
			touch: {
				value: ac.touch.total,
				ff: ac.touch.total - (ac?.normal.total - ac?.flatFooted.total),
			},
			cmd: {
				value: cmd.total,
				ff: cmd.flatFootedTotal,
			},
		},
		effects: actor.effects.filter(ae => ae.icon != null && ae.active)
			.map(ae => ({ name: ae.name, id: ae.id, img: ae.img })),
	};

	return context;
}

/**
 * @param {Combatant} combatant
 * @param {Element} tt
 */
function updateCombatantTooltip(combatant, tt) {
	const templateData = collectCombatantData(combatant);
	if (!templateData) return false;

	const tooltipTemplate = Handlebars.partials[tooltipTemplateSource];

	const content = tooltipTemplate(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

	tt.innerHTML = content;
}

/**
 * TooltipManager based tooltip handling.
 * BLOCK: Align to top option missing.
 *
 * @param {Combatant} combatant
 * @param {Element} dom
 */
function generateCombatantTooltipCore(combatant, dom) {
	if (!dom) return;
	if (!combatant?.actor || !havePerm(combatant)) return;

	dom.addEventListener('mouseover', async event => {
		const templateData = collectCombatantData(combatant);
		if (!templateData) return;

		const tooltipTemplate = Handlebars.partials[tooltipTemplateSource];

		const content = tooltipTemplate(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

		game.tooltip.activate(dom, { text: content, direction: TooltipManager.TOOLTIP_DIRECTIONS.LEFT, cssClass: 'lil-combatant-tooltip' });
	}, { passive: true });

	dom.addEventListener('mouseout', event => game.tooltip.deactivate(), { passive: true });
}

/**
 * @param {Combatant} combatant
 * @param {Element} dom
 */
function generateCombatantTooltip(combatant, dom) {
	if (!dom) return;
	if (!combatant?.actor || !havePerm(combatant)) return;

	if (!['character', 'npc'].includes(combatant.actor.type)) return;

	const tt = createNode('span', null, ['lil-combatant-tooltip']);
	// updateCombatantTooltip(combatant, tt);
	// Delayed tooltip filling
	dom.addEventListener('mouseenter', () => updateCombatantTooltip(combatant, tt), { passive: true });
	dom.classList.add('has-little-helper-tooltip');
	dom.append(tt);

	hookTooltip(dom, tt);
}

/**
 * @param {CombatTracker} app
 * @param {JQuery} html
 */
function hookCombatTrackerTooltips(app, [html]) {
	const listing = html.querySelector('ol.directory-list');
	if (!listing) return;

	const combat = game.combats.active;
	if (!combat) return;

	listing.querySelectorAll('.combatant[data-combatant-id]')
		.forEach(el => generateCombatantTooltip(combat.combatants.get(el.dataset.combatantId), el));
}

function enable() {
	Hooks.on('renderCombatTracker', hookCombatTrackerTooltips);
	loadTemplates([`modules/${CFG.id}/template/tooltip/combat-tracker.hbs`]);
}

function disable() {
	Hooks.off('renderCombatTracker', hookCombatTrackerTooltips);
	return false;
}

new Feature({ setting: 'combat-tracker-tooltip', label: 'Combat Tracker Tooltip', hint: 'Basic info tooltip for combat tracker.', category: 'foundry-ui', enable, disable, stage: 'init' });
