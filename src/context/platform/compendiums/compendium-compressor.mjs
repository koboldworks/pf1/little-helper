import { Feature } from '@root/core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { createNode, addCoreTooltip } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

const iconCss = {
	world: ['fas', 'fa-globe'],
	system: ['fas', 'fa-book'],
	module: ['fas', 'fa-puzzle-piece'],
};

/**
 * @param {Compendium} dir
 * @param {JQuery} html
 */
function compendiumCompressor(dir, [html]) {
	// Handle sidepanel and popout
	const main = html.dataset.tab ? html : html.querySelector('[data-tab]');
	main.classList.add('lil-compressed-compendium');

	main.querySelectorAll('.directory-list .compendium-pack[data-pack]').forEach(node => {
		node.querySelector('span.document-type')?.remove(); // remove type re-statement

		const sourceNode = node.querySelector('.compendium-footer span');

		const pack = game.packs.get(node.dataset.pack),
			{ packageType, packageName } = pack.metadata,
			isSystem = packageType === 'system',
			isModule = packageType === 'module',
			isWorld = packageType === 'world';

		const type = isSystem ? 'system' : isWorld ? 'world' : 'module';
		const icon = createNode('i', null, iconCss[type]);
		icon.classList.add('pack', type);

		const sourceId = packageName,
			sourceName = isSystem ? game.system.title : isWorld ? game.world.title : game.modules.get(packageName).title;

		const labelParts = [`${i18n.get(type.capitalize())}`];
		if (sourceName !== sourceId) labelParts.push(sourceName);
		labelParts.push(sourceId);
		const label = labelParts.join('<br>');

		addCoreTooltip(icon, label, TooltipManager.TOOLTIP_DIRECTIONS.LEFT);

		// Add module ID display under pack name
		if (isModule)node.append(createNode('label', packageName, ['source']));

		sourceNode.after(icon);
		sourceNode.remove();

		// Add item count
		const count = createNode('span', `${pack.index.size}`, ['lil-document-count']);
		node.querySelector('.pack-title a')?.append(count);
	});
}

/**
 * Update compendium item count.
 *
 * @param {Compendium} pack
 */
const updateItemCount = (pack) => {
	[ui.compendium, ...Object.values(ui.windows).filter(app => app instanceof CompendiumDirectory)]
		.forEach(app => app.element[0]
			.querySelector(`.directory-item[data-pack="${pack.metadata.id}"] .lil-document-count`)
			.textContent = `${pack.index.size}`);
};

function enable() {
	Ancillary.register('compendium', compendiumCompressor);
	Hooks.on('updateCompendium', updateItemCount);
}

function disable() {
	Ancillary.unregister('compendium', compendiumCompressor);
	Hooks.off('updateCompendium', updateItemCount);
	return false;
}

new Feature({ setting: 'compendium-compression', label: 'Compendium Directory Compression', hint: 'Compresses the layout of the side panel compendium directory, reducing wasted space.', category: 'foundry-ui', enable, disable, conflict: { core: '11' }, stage: 'init' });
