import { CFG } from '@root/config.mjs';
import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@root/core/feature.mjs';
import { FAIcons } from '@root/core/icons.mjs';
import { i18n } from '@root/utility/i18n.mjs';

const STATUS = {
	error: -1,
	inprogress: 1,
	unlocking: 11,
	locking: 12,
	finished: 2,
	default: 0,
};

const MSG = {
	[STATUS.error]: 'LittleHelper.Action.Migration.Status.Error',
	[STATUS.inprogress]: 'LittleHelper.Action.Migration.Status.InProgress',
	[STATUS.locking]: 'LittleHelper.Action.Migration.Status.Locking',
	[STATUS.unlocking]: 'LittleHelper.Action.Migration.Status.Unlocking',
	[STATUS.finished]: 'LittleHelper.Action.Migration.Status.Finished',
};

const ICON = {
	[STATUS.error]: '✘',
	[STATUS.inprogress]: '⏲',
	[STATUS.locking]: '🔒',
	[STATUS.unlocking]: '🔓',
	[STATUS.finished]: '✔',
};

class MigrationDialog extends Application {
	/** @type {Compendium} */
	pack;

	constructor(pack) {
		super();
		this.pack = pack;
		this.options.title = `Migrating Compendium: ${pack.title}`;
	}

	inProgress = false;
	status = 'Initializing...';
	state = '';

	get template() {
		return `modules/${CFG.id}/template/compendium-migration.hbs`;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			classes: [..._default.classes, 'lil-compendium-migration'],
		};
	}

	getData() {
		const status = this.status;

		const type = this.pack.metadata.packageType;
		let packageName;
		if (type === 'system') packageName = game.system.title;
		else if (type === 'module') packageName = game.modules.get(this.pack.metadata.packageName)?.title;
		else if (type === 'world') packageName = 'World';

		return {
			...super.getData(),
			pack: this.pack,
			packageName,
			state: {
				id: foundry.utils.invertObject(STATUS)[status],
				label: MSG[status],
				icon: ICON[status],
			},
			indexSize: Math.floor(JSON.stringify(this.pack.index).length / 100) / 10,
			docTotal: this.docTotal ?? 'n/a',
			docAvg: this.docAvg ?? 'n/a',
		};
	}

	statusUpdate(state) {
		const html = this.element[0];
		if (!html) return; // Dialog was closed
		const status = html.querySelector('.value[name="status"]');

		this.status = state;
		status.textContent = ICON[state] + ' ' + i18n.get(MSG[state]);

		switch (state) {
			case STATUS.error: status?.classList.add('error'); this.state = 'error'; break;
			case STATUS.locking:
			case STATUS.unlocking:
			case STATUS.inprogress: status?.classList.add('inprogress'); this.state = 'inprogress'; break;
			case STATUS.finished: status?.classList.add('finished'); this.state = 'finished'; break;
			default: break;
		}
	}

	/**
	 * @param {Element} html
	 */
	async doMigration() {
		if (this.inProgress) return;
		this.inProgress = true;

		const html = this.element[0];

		const pack = this.pack;
		const wasLocked = pack.locked;
		if (wasLocked) {
			this.statusUpdate(STATUS.unlocking);
			await pack.configure({ locked: false });
		}

		this.statusUpdate(STATUS.inprogress);

		// This is called by migrateCompendium
		if (!['Actor', 'Item', 'Scene'].includes(this.pack.metadata.type)) {
			await this.pack.migrate()
				.catch(_ => this.statusUpdate(STATUS.error))
				.then(_ => this.statusUpdate(STATUS.finished));
		}
		else {
			await pf1.migrations.migrateCompendium(this.pack)
				.catch(_ => this.statusUpdate(STATUS.error))
				.then(_ => this.statusUpdate(STATUS.finished));
		}

		const docs = await pack.getDocuments();
		const totalDocData = docs
			.map(d => JSON.stringify(d.toObject()).length)
			.reduce((t, o) => t + o, 0);
		const doMB = totalDocData >= 700_000;
		const divisor = doMB ? 100_000 : 100;
		this.docTotal = Math.floor(totalDocData / divisor) / 10 + (' ' + (doMB ? 'MB' : 'kB'));
		this.docAvg = docs.length > 0 ? Math.floor(totalDocData / docs.length / 100) / 10 + ' kB' : 0;
		const dt = html.querySelector('.value[name="doc-total"]');
		const da = html.querySelector('.value[name="doc-avg"]');
		dt.textContent = this.docTotal;
		da.textContent = this.docAvg;

		if (wasLocked) {
			this.statusUpdate(STATUS.locking);
			await pack.configure({ locked: true });
			this.statusUpdate(STATUS.finished);
		}

		this.inProgress = false;
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		this.doMigration();
	}

	close(...args) {
		delete this.pack.__lilMD;
		super.close(...args);
	}
}

/**
 * @param {JQuery} html
 */
async function migrateCompendium([html]) {
	if (pf1.migrations.isMigrating) {
		ui.notifications.warn('Cancelled due to ongoing migration.');
		return;
	}
	const packId = html.dataset.pack;
	const pack = game.packs.get(packId);
	if (pack.__lilMD) return pack.__lilMD?.render(false, { focus: true });

	pack.__lilMD = new MigrationDialog(pack).render(true, { focus: true });
}

/**
 * @param {JQuery<HTMLElement>|CompendiumDirectory} apphtml - TODO: this is app in v12
 * @param {object[]} entries
 */
function addCompendiumMigrationContextOption(apphtml, entries) {
	const exportEntry = {
		name: 'LittleHelper.Action.Migrate',
		icon: `<i class='${FAIcons.fileExport}'></i>`,
		callback: migrateCompendium,
	};
	entries.push(exportEntry);
}

function enable() {
	Hooks.on('getCompendiumDirectoryEntryContext', addCompendiumMigrationContextOption);
}

function disable() {
	Hooks.off('getCompendiumDirectoryEntryContext', addCompendiumMigrationContextOption);
	return false;
}

new Feature({ setting: 'compendium-migration', label: 'Compendium Migration', hint: 'Allows easy migration of specific compendiums.', category: 'foundry-ui', enable, disable, stage: 'init', gm: true });
