// Hide private compendiums

import { createNode, stopEventPropagation, addCoreTooltip } from '@root/common.mjs';
import { Feature } from '@root/core/feature.mjs';
import { Ancillary } from '@root/ancillary.mjs';

/** @type {Element} */
let button;

/**
 * @param {CompendiumDirectory} compendium
 * @param {Element} html
 * @param {Event} ev
 */
function toggleVisibility(compendium, html, ev) {
	stopEventPropagation(ev);

	compendium._lh_trulyHidden = !compendium._lh_trulyHidden;
	html.classList.toggle('lil-visibility-obscured', compendium._lh_trulyHidden);
	button.classList.toggle('visibility-obscured', compendium._lh_trulyHidden);
}

/**
 * @param {CompendiumDirectory} compendium
 * @param {JQuery} html
 */
function compendiumVisibilityToggle(compendium, [html]) {
	if (!html.classList.contains('directory'))
		html = html.querySelector('.directory');
	html.classList.add('lil-private-packs');

	const buttons = html.querySelector('.header-actions.action-buttons');

	const dirs = html.querySelector('.directory-list');

	if (compendium._lh_trulyHidden === undefined)
		compendium._lh_trulyHidden = true;

	button = createNode('button', null, ['lil-toggle-visibility']);
	button.classList.toggle('visibility-obscured', compendium._lh_trulyHidden);
	button.append(createNode('i', null, ['fas', 'fa-eye-slash', 'hidden-state']), createNode('i', null, ['fas', 'fa-eye', 'visible-state']));
	button.addEventListener('click', ev => toggleVisibility(compendium, dirs, ev));
	buttons.append(button);

	addCoreTooltip(button, 'Toggle visibility of hidden compendiums.', TooltipManager.TOOLTIP_DIRECTIONS.LEFT);

	dirs.classList.toggle('lil-visibility-obscured', compendium._lh_trulyHidden);

	// Mark compendiums
	dirs.querySelectorAll('li.directory-item[data-pack]').forEach(el => {
		if (game.packs.get(el.dataset.pack)?.config.private)
			el.classList.add('private');
	});
}

function enable() {
	Ancillary.register('compendium', compendiumVisibilityToggle);
	return false;
}

function disable() {
	Ancillary.unregister('compendium', compendiumVisibilityToggle);
	return false;
}

new Feature({ setting: 'hide-private-compendiums', label: 'Hide Private Compendiums', hint: 'Hides private compendiums and adds a button to toggle this.', category: 'foundry-ui', enable, disable, conflict: { core: '11' }, stage: 'init', gm: true });
