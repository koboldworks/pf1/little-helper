/**
 * Adds little ID button to sheets for copying the underlying ID to clipboard.
 */

import { Feature } from '@root/core/feature.mjs';

const getSheetId = (sheet) => {
	if (sheet instanceof Compendium)
		return sheet.metadata.id;
	const doc = sheet.document;
	if (doc) return doc.id;
	return sheet.object.id;
};

// Mimic Foundry ID button
function injectIDButtonToTitle(app, html) {
	const v12 = game.release.generation >= 12;
	if (html instanceof jQuery) html = html[0];
	if (!html.dataset.appid) return;

	let type;
	if (app.object instanceof pf1.components.ItemAction) type = 'action';
	if (app instanceof Compendium) type = 'pack';

	const title = html.querySelector('.window-title');
	if (!title) return;
	const idLink = document.createElement('a');
	idLink.classList.add('document-id-link', 'lil-helper');

	let id, label;
	switch (type) {
		case 'action':
			id = app.object.id;
			label = 'Action';
			idLink.dataset.tooltip = `Copy Action ID: ${id}`;
			break;
		case 'pack':
			id = app.metadata.id;
			label = 'Compendium';
			idLink.dataset.tooltip = `Copy Compendum ID: ${id}`;
			break;
	}

	if (!id) return;

	idLink.dataset.tooltipDirection = 'UP';
	idLink.innerHTML = '<i class="fa-solid fa-passport"></i>';
	idLink.addEventListener('click', event => {
		event.preventDefault();
		game.clipboard.copyPlainText(id);
		ui.notifications.info(game.i18n.format('DOCUMENT.IdCopiedClipboard', { label, type: 'id', id }));
	});
	title.append(idLink);
}

const renderEvents = [
	'renderCompendium',
];

function enable() {
	for (const ev of renderEvents)
		Hooks.on(ev, injectIDButtonToTitle);
}

function disable() {
	for (const ev of renderEvents)
		Hooks.off(ev, injectIDButtonToTitle);
}

new Feature({ setting: 'sheet-id', label: 'Sheet ID', hint: 'Display button on action sheet and compendium header to copy relevant ID to clipboard.', category: 'dev', enable, disable, stage: 'ready' });
