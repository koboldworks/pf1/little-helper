import { Feature } from '@root/core/feature.mjs';
import { createNode } from '@root/common.mjs';
import { getFullItemTypeLabel } from '@root/helpers/item.mjs';

/**
 * @param {Item} item
 */
function getItemTags(item) {
	const tags = {};
	const type = item.type;
	const itemData = item.system;
	const subType = itemData.subType;

	tags.type = getFullItemTypeLabel(item, type, subType);

	switch (item.type) {
		case 'weapon': {
			const actionType = item.defaultAction?.actionType;
			switch (actionType) {
				case 'mwak':
				case 'msak':
				case 'mcman':
					tags.range = 'Melee';
					break;
				case 'rwak':
				case 'rsak':
				case 'rcman':
					tags.range = 'Ranged';
					break;
			}
			break;
		}
	}

	return tags;
}

/**
 * @param {Element} el
 * @param {Item} item
 */
function addDetails(el, item) {
	const tags = getItemTags(item);

	const t = document.createDocumentFragment();
	if (tags.type) t.append(createNode('span', tags.type, ['lil-tag', 'type']));
	if (tags.range) t.append(createNode('span', tags.range, ['lil-tag', 'range']));

	el.replaceChildren(t);
}

/**
 * @param {ItemDirectory} app
 * @param {JQuery} html
 * @param {object} options
 */
function onRenderItemDirectory(app, [html], options) {
	html.classList.add('lil-enriched-directory');

	html.querySelectorAll('li.document.item[data-document-id]')
		.forEach(el => {
			const itemId = el.dataset.documentId;
			const item = game.items.get(itemId);

			const holder = createNode('div', null, ['lil-item-info']);

			addDetails(holder, item);

			const name = el.querySelector('.document-name');
			el.classList.add('lil-enriched-document');

			name.append(holder);
		});
}

async function onUpdateItem(item) {
	if (item.actor) return;
	if (item.pack) return;

	const inItemsDir = item.collectionName === 'items';

	for (const app of item.collection?.apps ?? []) {
		if (inItemsDir && app instanceof ItemDirectory) {
			const html = app.element[0];
			const el = html.querySelector(`.item[data-document-id="${item.id}"] .lil-item-info`);
			if (el) addDetails(el, item);
		}
	}
}

function enable() {
	Hooks.on('updateItem', onUpdateItem);
	Hooks.on('renderItemDirectory', onRenderItemDirectory);
}

function disable() {
	Hooks.off('updateItem', onUpdateItem);
	Hooks.off('renderItemDirectory', onRenderItemDirectory);
	return false;
}

new Feature({ setting: 'enrich-items-directory', label: 'Enrich Items Directory', hint: 'Add additional information to items directory.', category: 'foundry-ui', enable, disable, stage: 'setup' });
