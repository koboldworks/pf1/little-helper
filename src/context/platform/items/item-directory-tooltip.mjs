import { Feature } from '@root/core/feature.mjs';
import { createNode, formatNum } from '@root/common.mjs';
import { getItemSubTypeLabel, getItemMainTypeLabel } from '@root/helpers/item.mjs';
import { i18n } from '@root/utility/i18n.mjs';

let ownership;

/**
 * @param {Element} el Parent element that prompts the tooltip.
 * @param {Element} tt Tooltip element.
 */
function hookTooltip(el, tt) {
	el.addEventListener('mousemove', ev => {
		const displayOnLeft = ev.clientX > Math.floor(window.innerWidth / 2);

		tt.classList.toggle('left-aligned', displayOnLeft);
		tt.classList.toggle('right-aligned', !displayOnLeft);

		const rect = el.getBoundingClientRect();
		const ttB = tt.clientHeight + rect.top;
		const bOff = ttB > window.innerHeight - 20 ? -(window.innerHeight - 20 - ttB) : 0;
		tt.style.cssText += `--left:${rect.left};--right:${rect.right};--top:${rect.top};--width:${tt.clientWidth};--bottom-offset:${bOff};`;
	}, { passive: true });
}

/**
 * @param {Item} item
 * @param {Element} d
 */
function addRaceInfo(item, d) {
	const itemData = item.system;
	const type = itemData.creatureType;
	if (type) {
		const typeEl = createNode('div', null, ['creature-type']);
		typeEl.append(
			createNode('label', i18n.get('PF1.CreatureType'), ['label']),
			createNode('span', `${pf1.config.creatureTypes[type]}`, ['value', 'text', 'span-3']),
		);
		d.append(typeEl);
	}

	const size = itemData.size;
	if (itemData.size) {
		const typeEl = createNode('div', null, ['size']);
		typeEl.append(
			createNode('label', i18n.get('PF1.Size'), ['label']),
			createNode('span', `${pf1.config.actorSizes[size]}`, ['value', 'text', 'span-3']),
		);
		d.append(typeEl);
	}

	if (itemData.subTypes?.length) {
		const subtypeList = createNode('ul', null, ['subtypes-list', 'span-4']);
		for (const type of itemData.subTypes) {
			const subtypeLiEl = createNode('li', null, ['owner']);
			subtypeLiEl.append(
				createNode('span', type, ['value', 'text']),
			);
			subtypeList.append(subtypeLiEl);
		}
		const subtypesEl = createNode('div', null, ['subtypes']);
		subtypesEl.append(
			createNode('label', i18n.get('PF1.RaceSubtypePlural'), ['label', 'span-4']),
			subtypeList,
		);
		d.append(subtypesEl);
	}
}

/**
 * @param {Item} item
 * @param {Element} tt
 */
const updateItemTooltip = async (item, tt) => {
	const d = document.createDocumentFragment();

	const permLevel = item.permission;

	const isGM = game.user.isGM;

	const itemData = item.system;

	const isIdentified = game.user.isGM || (itemData.identified ?? true);

	const typeLabel = getItemMainTypeLabel(item.type);
	const typeEl = createNode('div', null, ['type']);
	typeEl.append(
		createNode('label', i18n.get('PF1.Type'), ['label']),
		createNode('span', i18n.get(typeLabel), ['value', 'text', 'span-3']),
	);
	d.append(typeEl);

	// Display subtype for everything else except consumables unless they're identified
	if (!(!isIdentified && item.type === 'consumable')) {
		const subtype = item.subType;
		const sublabel = getItemSubTypeLabel(item, item.type, subtype);
		if (sublabel) {
			const subtypeEl = createNode('div', null, ['subtype']);

			subtypeEl.append(
				createNode('label', i18n.get('PF1.Subtype'), ['label']),
				createNode('span', sublabel, ['value', 'text', 'span-3']),
			);
			d.append(subtypeEl);
		}
	}

	if (item.type === 'race')
		addRaceInfo(item, d);

	if (Number.isFinite(itemData.quantity) && itemData.quantity !== 1) {
		const gpEl = createNode('div', null, ['quantity']);
		gpEl.append(
			createNode('label', i18n.get('PF1.Quantity'), ['label']),
			createNode('span', `${itemData.quantity}`, ['value', 'number', 'span-3']),
		);
		d.append(gpEl);
	}

	const value = item.isPhysical ? item.getValue({ inLowestDenomination: true, sellValue: 1, single: true }) : NaN;
	if (!Number.isNaN(value)) {
		const gpEl = createNode('div', null, ['gold-value']);
		const gpValueEl = createNode('span', null, ['simple-value']);
		gpValueEl.append(createNode('span', `${formatNum(value / 100)}`, ['value', 'number']), ' 🜚');
		gpEl.append(createNode('label', i18n.get('PF1.Value'), ['label']), gpValueEl);
		d.append(gpEl);
	}

	const weight = itemData.weight;
	if (Number.isFinite(weight?.total)) {
		const weightEl = createNode('div', null, ['weight-value']);
		const weightValueEl = createNode('span', null, ['simple-value']);
		const ws = pf1.utils.getWeightSystem();
		const units = ws === 'metric' ? 'PF1.Kgs' : 'PF1.Lbs';

		weightValueEl.append(createNode('span', `${pf1.utils.limitPrecision(weight.converted.total, 2)}`, ['value', 'number']), ` ${i18n.get(units)}`);
		weightEl.append(createNode('label', i18n.get('PF1.Weight'), ['label']), weightValueEl);
		d.append(weightEl);
	}

	// Spell info
	if (item.type === 'spell') {
		const school = createNode('div', null, ['school']);
		const label = pf1.config.spellSchools[itemData.school] || i18n.get('PF1.Undefined');
		school.append(
			createNode('label', i18n.get('PF1.School'), ['label']),
			createNode('span', label, ['value', 'string', 'span-3']),
		);
		d.append(school);
	}

	/*
	if (Number.isFinite(itemData.hardness)) {
		const hardnessEl = createNode('div', null, ['gold-value']);
		hardnessEl.append(
			createNode('label', 'Hardness', ['label']),
			createNode('span', `${itemData.hardness}`, ['value', 'number', 'span-3']),
		);
		d.append(hardnessEl);
	}
	*/

	ownership ??= {
		default: i18n.get('OWNERSHIP.DEFAULT'),
		1: i18n.get('OWNERSHIP.LIMITED'),
		2: i18n.get('OWNERSHIP.OBSERVER'),
		3: i18n.get('OWNERSHIP.OWNER'),
	};

	// Owners
	const owners = Object.entries(item.ownership)
		.map(([uid, level]) => [game.users?.get(uid), level])
		.filter(([user, _]) => !!user && !user?.isGM);
	const defaultOwnership = item.ownership.default;
	if (defaultOwnership > 0 || owners.length) {
		const ownerList = createNode('ul', null, ['owner-list', 'span-4']);
		const addOwner = (name, level) => {
			const ownerLiEl = createNode('li', null, ['owner']);
			ownerLiEl.append(
				createNode('span', name, ['text', 'value']),
				createNode('span', ownership[level], ['value', 'text']),
			);
			ownerList.append(ownerLiEl);
		};
		if (defaultOwnership > 0)
			addOwner(ownership.default, defaultOwnership);
		owners.forEach(([user, perm]) => addOwner(user.name, perm));
		const ownerEl = createNode('div', null, ['ownership']);
		ownerEl.append(
			createNode('label', i18n.get('Permissions'), ['label', 'span-4']),
			ownerList,
		);
		d.append(ownerEl);
	}

	// Aura is not visible with limited permission items
	if (permLevel > CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED) {
		if (isIdentified && itemData.cl > 0) {
			const school = itemData.aura.school,
				schoolLabel = itemData.aura.custom ? school : pf1.config.spellSchools[school],
				auraStrength = pf1.config.auraStrengths[item.auraStrength],
				auraLabel = i18n.get('LittleHelper.Aura', { school: schoolLabel, strength: auraStrength }),
				auraEl = createNode('div', null, ['aura-value']);
			auraEl.append(
				createNode('label', 'Aura', ['label']),
				createNode('span', auraLabel, ['value', 'text', 'span-3']),
			);
			d.append(auraEl);
		}
	}

	if (isGM) {
		const ident = itemData.identified;
		if (ident !== undefined) {
			const identEl = createNode('div', null, ['identified']),
				identLabel = i18n.get(ident ? 'Yes' : 'No');
			identEl.append(
				createNode('label', i18n.get('PF1.Identified'), ['label']),
				createNode('span', identLabel, ['value', 'boolean', 'identified-state', ident ? 'identified' : 'unidentified', ident ? 'true' : 'false', 'span-3']),
			);
			d.append(identEl);
		}
	}

	tt.replaceChildren(d);
};

/**
 * @param {Item} item
 * @param {Element} dom
 */
function generateItemTooltip(item, dom) {
	if (!item.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED)) return;

	const tt = createNode('span', null, ['lil-directory-tooltip']);
	dom.classList.add('has-little-helper-tooltip');
	dom.append(tt);

	// Passive on-demand filling
	dom.addEventListener('mouseenter', ev => updateItemTooltip(item, tt), { passive: true });

	hookTooltip(dom, tt);
}

/**
 * @param {ItemDirectory} app
 * @param {JQuery} html
 */
function hookItemDirectoryTooltips(app, [html]) {
	const listing = html.querySelector('ol.directory-list');
	if (!listing) return;

	listing.querySelectorAll('.item[data-document-id]')
		.forEach(el => generateItemTooltip(game.items.get(el.dataset.documentId), el));
}

function enable() {
	Hooks.on('renderItemDirectory', hookItemDirectoryTooltips);
}

function disable() {
	Hooks.off('renderItemDirectory', hookItemDirectoryTooltips);
	return false;
}

new Feature({ setting: 'item-list-tooltip', label: 'Items Directory Tooltips', hint: 'Item tooltips for the sidebar directory, providing basic info.', category: 'foundry-ui', enable, disable, stage: 'init' });
