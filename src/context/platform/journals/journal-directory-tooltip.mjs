import { Feature } from '@root/core/feature.mjs';
import { createNode } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

let ownership;

/**
 * @param {Element} el Parent element that prompts the tooltip.
 * @param {Element} tt Tooltip element.
 */
function hookTooltip(el, tt) {
	el.addEventListener('mousemove', ev => {
		const displayOnLeft = ev.clientX > Math.floor(window.innerWidth / 2);

		tt.classList.toggle('left-aligned', displayOnLeft);
		tt.classList.toggle('right-aligned', !displayOnLeft);

		const rect = el.getBoundingClientRect();
		const ttB = tt.clientHeight + rect.top;
		const bOff = ttB > window.innerHeight - 20 ? -(window.innerHeight - 20 - ttB) : 0;
		tt.style.cssText += `--left:${rect.left};--right:${rect.right};--top:${rect.top};--width:${tt.clientWidth};--bottom-offset:${bOff};`;
	}, { passive: true });
}

/**
 * @param {JournalEntry} journal
 * @param {Element} tt
 */
const updateJournalTooltip = async (journal, tt) => {
	const d = document.createDocumentFragment();

	ownership ??= {
		default: i18n.get('OWNERSHIP.DEFAULT'),
		0: i18n.get('OWNERSHIP.NONE'),
		1: i18n.get('OWNERSHIP.LIMITED'),
		2: i18n.get('OWNERSHIP.OBSERVER'),
		3: i18n.get('OWNERSHIP.OWNER'),
	};

	// Pages
	{
		const pages = journal.pages.size ?? 0;
		const pgEl = createNode('div', null, ['page-count']);
		const pgValueEl = createNode('span', null, ['simple-value']);
		pgValueEl.append(createNode('span', `${pages}`, ['value', 'number', 'pages', pages > 0 ? 'finite' : 'empty']), ' 🗐');
		pgEl.append(
			createNode('label', i18n.get('Pages'), ['label']),
			pgValueEl,
		);
		d.append(pgEl);
	}

	// Owners
	{
		const owners = Object.entries(journal.ownership)
			.map(([uid, level]) => [game.users?.get(uid), level])
			.filter(([user, _]) => !!user && !user?.isGM);
		const defaultOwnership = journal.ownership.default;
		const ownerList = createNode('ul', null, ['owner-list', 'span-4']);
		const addOwner = (name, level) => {
			const ownerLiEl = createNode('li', null, ['owner']);
			ownerLiEl.append(
				createNode('span', name, ['text', 'value']),
				createNode('span', ownership[level], ['value', 'text', 'ownership', `level-${level}`]),
			);
			ownerList.append(ownerLiEl);
		};
		addOwner(ownership.default, defaultOwnership);
		owners.forEach(([user, perm]) => addOwner(user.name, perm));
		const ownerEl = createNode('div', null, ['ownership']);
		ownerEl.append(
			createNode('label', i18n.get('Permissions'), ['label', 'span-4']),
			ownerList,
		);
		d.append(ownerEl);
	}

	tt.replaceChildren(d);
};

/**
 * @param {RollTable} table
 * @param {Element} dom
 */
function generateJournalTooltip(table, dom) {
	// Pointless
	if (!table.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED)) return;

	const tt = createNode('span', null, ['lil-directory-tooltip']);
	dom.classList.add('has-little-helper-tooltip');
	dom.append(tt);

	// Passive on-demand filling
	dom.addEventListener('mouseenter', ev => updateJournalTooltip(table, tt), { passive: true });

	hookTooltip(dom, tt);
}

/**
 * @param {SceneDirectory} app
 * @param {JQuery} html
 */
const hookJournalDirectoryTooltips = (app, [html]) => {
	const listing = html.querySelector('ol.directory-list');
	if (!listing) return;

	listing.querySelectorAll('.document[data-document-id]')
		.forEach(el => generateJournalTooltip(game.journal.get(el.dataset.documentId), el));
};

const enable = () => {
	Hooks.on('renderJournalDirectory', hookJournalDirectoryTooltips);
};

const disable = () => {
	Hooks.off('renderJournalDirectory', hookJournalDirectoryTooltips);
	return false;
};

new Feature({ setting: 'journal-list-tooltip', label: 'Journal List Tooltips', hint: 'Journal tooltips for the sidebar directory, providing basic info.', category: 'foundry-ui', enable, disable, stage: 'init' });
