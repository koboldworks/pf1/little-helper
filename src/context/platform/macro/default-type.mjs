import { Feature } from '@root/core/feature.mjs';

function defaultMacroType(macro, data, options, userId) {
	if (data.command == undefined)
		macro.updateSource({ type: 'script' });
}

function enable() {
	Hooks.on('preCreateMacro', defaultMacroType);
}

function disable() {
	Hooks.off('preCreateMacro', defaultMacroType);
}

new Feature({ setting: 'macro-default-script', label: 'Default Script Type', hint: 'Forces macros to script type by default.', category: 'macro', enable, disable, stage: 'ready' });
