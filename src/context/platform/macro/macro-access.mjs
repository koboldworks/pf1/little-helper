import { createNode } from '@root/common.mjs';
import { Feature } from '@root/core/feature.mjs';
import { hookCoreTooltip } from '@root/core/tooltip.mjs';

/**
 * @param {MacroConfig} app
 * @param {JQuery} html
 */
function displayMacroOwners(app, [html]) {
	if (!game.user.isGM) return;

	const doc = app.document;
	const header = html.querySelector('.sheet-header');
	if (!header) return;

	const perms = doc.ownership;
	const users = [];
	for (const uid of Object.keys(perms)) {
		if (uid === 'default') continue;
		const p = perms[uid];
		if (p >= CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER) {
			const user = game.users.get(uid);
			if (user?.isGM) continue;
			if (user) users.push(user);
		}
	}

	if (users.length) {
		const uls = createNode('ul', null, ['lil-user-access']);
		for (const u of users) {
			const owner = doc.testUserPermission(u, 'OWNER');
			const uel = createNode('li', u.name, [owner ? 'owner' : 'observer']);
			hookCoreTooltip(uel, { content: () => `ID: ${u.id}<br>Permission: ${owner ? 'Owner' : 'Observer'}` });
			uls.append(uel);
		}
		header.after(uls);
	}

	if (perms.default < CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER) {
		// NOT VISIBLE TO PLAYERS
		const noPlayers = createNode('h3', 'No players have access to this macro by default.', ['lil-user-warning']);
		noPlayers.prepend(createNode('i', ' ', ['fas', 'fa-exclamation-triangle']));
		header.after(noPlayers);
	}
}

function enable() {
	Hooks.on('renderMacroConfig', displayMacroOwners);
}

function disable() {
	Hooks.off('renderMacroConfig', displayMacroOwners);
}

new Feature({ setting: 'macro-access', label: 'User Access', hint: 'Display user access to macros in their sheet.', category: 'macro', enable, disable, stage: 'ready', gm: true });
