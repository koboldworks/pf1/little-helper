import { Feature } from '@root/core/feature.mjs';
import { createNode } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {MacroDirectory} app
 * @param {JQuery} html
 */
function macroDirectory(app, [html]) {
	const list = html.querySelector('.directory-list');
	if (!list) return;
	list.classList.add('lil-enhanced-macro-directory');

	const isGM = game.user.isGM,
		userId = game.user.id;

	const permlabels = {
		0: i18n.get('LittleHelper.Macro.None'),
		1: i18n.get('LittleHelper.Macro.Visible'),
		2: i18n.get('LittleHelper.Macro.Executable'),
		3: i18n.get('LittleHelper.Macro.Editable'),
	};

	for (const el of list.querySelectorAll('.directory-item')) {
		const docId = el.dataset.documentId;
		if (!docId) continue;

		const m = game.macros.get(docId),
			author = m.author,
			name = author ? author.name : `[${m._source.author}]`;

		// Find relevant element
		const nel = el.querySelector('.document-name a');
		nel.classList.add('macro-name');

		const md = createNode('div', null, ['lil-macro-details']);

		// Author
		md.append(createNode('label', i18n.get('LittleHelper.Macro.Author') + ': '), createNode('span', name, ['author-name']));

		// Access
		const defaultPerm = m.ownership?.default ?? 0;
		const perm = isGM ? defaultPerm : m.ownership[userId] ?? defaultPerm;
		md.append(createNode('label', i18n.get('LittleHelper.Macro.Permission') + ': '), createNode('span', permlabels[perm], ['permission']));

		nel.after(md);
	}
}

function enable() {
	Hooks.on('renderMacroDirectory', macroDirectory);
}

function disable() {
	Hooks.off('renderMacroDirectory', macroDirectory);
}

new Feature({ setting: 'macro-directory', label: 'Directory Enrichment', hint: 'Enhance information available in macro directory.', category: 'macro', enable, disable, stage: 'ready' });
