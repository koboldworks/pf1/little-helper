/**
 * Adds little refresh button to sheets to force re-render.
 */

import { Feature } from '@root/core/feature.mjs';
import { FAIcons } from '@root/core/icons.mjs';

function injectIDButton(sheet, buttons) {
	buttons.unshift({
		class: 'little-helper-sheet-refresh',
		icon: FAIcons.syncAlt,
		// label: i18n.get('LittleHelper.UI.RefreshButton'),
		onclick: _ => sheet.render(),
	});
}

const events = [
	'getDocumentSheetHeaderButtons',
	'getActorSheetHeaderButtons', // weirdly not documentSheet
	'getItemSheetHeaderButtons', // weirdly not documentSheet
	// 'getJournalSheetHeaderButtons', // documentSheet
	// 'getRollTableConfigHeaderButtons', // documentSheet
	'getCompendiumHeaderButtons',
	// 'getSceneConfigHeaderButtons', // documentSheet
	// 'getCombatantConfigHeaderButtons', // documentSheet
	'getTokenConfigHeaderButtons',
	// 'getWallConfigHeaderButtons' // documentSheet
];

function enable() {
	setTimeout(() => {
		for (const ev of events)
			Hooks.on(ev, injectIDButton);
	}, this.ready ? 0 : 2_000);
	this.ready = true;
}

function disable() {
	for (const ev of events)
		Hooks.off(ev, injectIDButton);
}

new Feature({ setting: 'sheet-refresh', label: 'Sheet Refresh', hint: 'Display button on sheet header to force refresh.', category: 'dev', enable, disable, stage: 'ready', enabledByDefault: false });
