import { Feature } from '@root/core/feature.mjs';
import { createNode } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

let ownership;

/**
 * @param {Element} el Parent element that prompts the tooltip.
 * @param {Element} tt Tooltip element.
 */
function hookTooltip(el, tt) {
	el.addEventListener('mousemove', ev => {
		const displayOnLeft = ev.clientX > Math.floor(window.innerWidth / 2);

		tt.classList.toggle('left-aligned', displayOnLeft);
		tt.classList.toggle('right-aligned', !displayOnLeft);

		const rect = el.getBoundingClientRect();
		const ttB = tt.clientHeight + rect.top;
		const bOff = ttB > window.innerHeight - 20 ? -(window.innerHeight - 20 - ttB) : 0;
		tt.style.cssText += `--left:${rect.left};--right:${rect.right};--top:${rect.top};--width:${tt.clientWidth};--bottom-offset:${bOff};`;
	}, { passive: true });
}

/**
 * @param {RollTable} table
 * @param {Element} tt
 */
const updateRollTableTooltip = async (table, tt) => {
	const d = document.createDocumentFragment();

	ownership ??= {
		default: i18n.get('OWNERSHIP.DEFAULT'),
		0: i18n.get('OWNERSHIP.NONE'),
		1: i18n.get('OWNERSHIP.LIMITED'),
		2: i18n.get('OWNERSHIP.OBSERVER'),
		3: i18n.get('OWNERSHIP.OWNER'),
	};

	const rollFormula = createNode('div', null, ['formula']);
	rollFormula.append(
		createNode('label', 'Formula'),
		createNode('span', table.formula, ['value', 'string', 'formula', 'span-3']),
	);
	const results = createNode('div', null, ['results']);
	results.append(
		createNode('label', 'Results'),
		createNode('span', `${table.results.size}`, ['value', 'number', 'result-count', 'span-3']),
	);
	d.append(rollFormula, results);

	// Owners
	{
		const owners = Object.entries(table.ownership)
			.map(([uid, level]) => [game.users?.get(uid), level])
			.filter(([user, _]) => !!user && !user?.isGM);
		const defaultOwnership = table.ownership.default;
		const ownerList = createNode('ul', null, ['owner-list', 'span-4']);
		const addOwner = (name, level) => {
			const ownerLiEl = createNode('li', null, ['owner']);
			ownerLiEl.append(
				createNode('span', name, ['text', 'value']),
				createNode('span', ownership[level], ['value', 'text', 'ownership', `level-${level}`]),
			);
			ownerList.append(ownerLiEl);
		};
		addOwner(ownership.default, defaultOwnership);
		owners.forEach(([user, perm]) => addOwner(user.name, perm));
		const ownerEl = createNode('div', null, ['ownership']);
		ownerEl.append(
			createNode('label', i18n.get('Permissions'), ['label', 'span-4']),
			ownerList,
		);
		d.append(ownerEl);
	}

	tt.replaceChildren(d);
};

/**
 * @param {RollTable} table
 * @param {Element} dom
 */
function generateRollTableTooltip(table, dom) {
	// Pointless
	if (!table.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED)) return;

	const tt = createNode('span', null, ['lil-directory-tooltip']);
	dom.classList.add('has-little-helper-tooltip');
	dom.append(tt);

	// Passive on-demand filling
	dom.addEventListener('mouseenter', ev => updateRollTableTooltip(table, tt), { passive: true });

	hookTooltip(dom, tt);
}

/**
 * @param {SceneDirectory} app
 * @param {JQuery} html
 */
const hookRollTableDirectoryTooltips = (app, [html]) => {
	const listing = html.querySelector('ol.directory-list');
	if (!listing) return;

	listing.querySelectorAll('.document[data-document-id]')
		.forEach(el => generateRollTableTooltip(game.tables.get(el.dataset.documentId), el));
};

const enable = () => {
	Hooks.on('renderRollTableDirectory', hookRollTableDirectoryTooltips);
};

const disable = () => {
	Hooks.off('renderRollTableDirectory', hookRollTableDirectoryTooltips);
	return false;
};

new Feature({ setting: 'rolltable-list-tooltip', label: 'RollTable List Tooltips', hint: 'RollTable tooltips for the sidebar directory, providing basic info.', category: 'foundry-ui', enable, disable, stage: 'init' });
