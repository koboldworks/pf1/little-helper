import { Feature } from '@root/core/feature.mjs';

/**
 * @param {SceneDirectory} app
 * @param {JQuery} html
 * @param {object} options
 */
function compressSceneDirectory(app, [html], options) {
	html.style.setProperty('--sidebar-scene-height', '60px');
}

function enable() {
	Hooks.on('renderSceneDirectory', compressSceneDirectory);
}

function disable() {
	return false;
}

new Feature({ setting: 'compress-scene-directory', label: 'Compress Scene Directory', hint: 'Reduces the space each scene takes.', category: 'foundry-ui', enable, disable, stage: 'init', enabledByDefault: false });
