import { Feature } from '@root/core/feature.mjs';
import { createNode } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

let ownership;

/**
 * @param {Element} el Parent element that prompts the tooltip.
 * @param {Element} tt Tooltip element.
 */
function hookTooltip(el, tt) {
	el.addEventListener('mousemove', ev => {
		const displayOnLeft = ev.clientX > Math.floor(window.innerWidth / 2);

		tt.classList.toggle('left-aligned', displayOnLeft);
		tt.classList.toggle('right-aligned', !displayOnLeft);

		const rect = el.getBoundingClientRect();
		const ttB = tt.clientHeight + rect.top;
		const bOff = ttB > window.innerHeight - 20 ? -(window.innerHeight - 20 - ttB) : 0;
		tt.style.cssText += `--left:${rect.left};--right:${rect.right};--top:${rect.top};--width:${tt.clientWidth};--bottom-offset:${bOff};`;
	}, { passive: true });
}

/**
 * @param {Scene} scene
 * @param {Element} tt
 */
const updateSceneTooltip = async (scene, tt) => {
	const d = document.createDocumentFragment();

	const displayName = scene.navName;
	if (displayName) {
		const nameEl = createNode('div', null, ['navigation-name']);
		nameEl.append(
			createNode('h3', displayName, ['value', 'text', 'span-4']),
		);
		d.append(nameEl);
	}

	const navEl = createNode('div', null, ['navigation']),
		navLabel = i18n.get(scene.navigation ? 'Yes' : 'No');
	navEl.append(
		createNode('label', i18n.get('Navigation'), ['label']),
		createNode('span', navLabel, ['value', 'boolean', 'navigation-state', scene.navigation ? 'navigatable' : 'unnavigatable', scene.navigation ? 'true' : 'false', 'span-3']),
	);
	d.append(navEl);

	const visionEl = createNode('div', null, ['fog']),
		vision = scene.tokenVision,
		visionLabel = i18n.get(vision ? 'Yes' : 'No');
	visionEl.append(
		createNode('label', i18n.get('Vision'), ['label']),
		createNode('span', visionLabel, ['value', 'boolean', 'vision-state', vision ? 'token' : 'universal', vision ? 'true' : 'false', 'span-3']),
	);
	d.append(visionEl);

	const fogEl = createNode('div', null, ['fog']),
		fog = scene.fog?.exploration ?? scene.fogExploration, // TODO: remove fogExploration when v11 support is removed
		fogLabel = i18n.get(fog ? 'Yes' : 'No');
	fogEl.append(
		createNode('label', i18n.get('Fog'), ['label']),
		createNode('span', fogLabel, ['value', 'boolean', 'fog-state', fog ? 'fogged' : 'fogless', fog ? 'true' : 'false', 'span-3']),
	);
	d.append(fogEl);

	tt.replaceChildren(d);
};

/**
 * @param {Scene} scene
 * @param {Element} dom
 */
function generateSceneTooltip(scene, dom) {
	// Pointless
	if (!scene.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED)) return;

	const tt = createNode('span', null, ['lil-directory-tooltip']);
	dom.classList.add('has-little-helper-tooltip');
	dom.append(tt);

	// Passive on-demand filling
	dom.addEventListener('mouseenter', ev => updateSceneTooltip(scene, tt), { passive: true });

	hookTooltip(dom, tt);
}

/**
 * @param {SceneDirectory} app
 * @param {JQuery} html
 */
function hookSceneDirectoryTooltips(app, [html]) {
	const listing = html.querySelector('ol.directory-list');
	if (!listing) return;

	listing.querySelectorAll('.document[data-document-id]')
		.forEach(el => generateSceneTooltip(game.scenes.get(el.dataset.documentId), el));
}

function enable() {
	Hooks.on('renderSceneDirectory', hookSceneDirectoryTooltips);
}

function disable() {
	Hooks.off('renderSceneDirectory', hookSceneDirectoryTooltips);
	return false;
}

new Feature({ setting: 'scene-list-tooltip', label: 'Scene List Tooltips', hint: 'Scene tooltips for the sidebar directory, providing basic info.', category: 'foundry-ui', enable, disable, stage: 'init' });
