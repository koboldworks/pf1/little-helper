import { CFG } from '@root/config.mjs';
import { Feature } from '@root/core/feature.mjs';
import { FAIcons } from '@root/core/icons.mjs';

const autoExpand = 'autoExpandFolders';

Hooks.once('init', () => {
	// Storage for expanded folders
	game.settings.register(CFG.id, autoExpand, {
		config: false,
		default: {},
		type: Object,
		scope: 'client',
	});
});

function expandFolders(folders) {
	console.group('Auto-Expand Folders');
	try {
		const foldersByType = Object.entries(folders)
			.filter(([_, open]) => open)
			.map(([fid, _]) => game.folders?.get(fid))
			.reduce((typed, f) => {
				typed[f.type] ??= [];
				typed[f.type].push(f);
				return typed;
			}, {});

		const types = Object.keys(foldersByType);

		if (types.length == 0)
			return void console.log('No folders configured');

		for (const type of types) {
			console.group(type);
			try {
				for (const folder of foldersByType[type])
					console.log(folder.name, `[${folder.id}]`);
			}
			finally {
				console.groupEnd();
			}
		}
	}
	finally {
		console.groupEnd();
	}
}

Hooks.once('ready', () => {
	// Cleanup missing folders
	const folders = game.settings.get(CFG.id, autoExpand);
	let update = false;

	for (const [uid, _] of Object.entries(folders)) {
		if (!game.folders.get(uid)) {
			delete folders[uid];
			update = true;
		}
	}

	if (update) game.settings.set(CFG.id, autoExpand, folders);

	expandFolders(folders);
});

/**
 * @param {Element} el
 * @param {boolean} expand
 */
function toggleFolder(el, expand) {
	// TODO: Swap to data-uuid usage for v11
	const folderId = el.closest('[data-folder-id]')?.dataset.folderId;
	if (!folderId) return;

	const cfg = game.settings.get(CFG.id, autoExpand);

	if (expand) cfg[folderId] = true;
	else delete cfg[folderId];

	return game.settings.set(CFG.id, autoExpand, foundry.utils.deepClone(cfg));
}

/**
 * @param {Element} el
 */
function getToggleState(el) {
	// TODO: Swap to data-uuid usage for v11
	const folderId = el.closest('[data-folder-id]')?.dataset.folderId;
	if (!folderId) return;
	const cfg = game.settings.get(CFG.id, autoExpand);
	return cfg[folderId] ?? false;
}

/**
 * @param _jq
 * @param {object[]} contextmenu
 */
function folderContext(_jq, contextmenu) {
	contextmenu.unshift({
		callback: ([html]) => toggleFolder(html, false),
		condition: ([html]) => getToggleState(html),
		icon: `<i class='${FAIcons.folder}'></i>`,
		name: 'Undo auto-expand',
	},
	{
		callback: ([html]) => toggleFolder(html, true),
		condition: ([html]) => !getToggleState(html),
		icon: `<i class='${FAIcons.folderOpen}'></i>`,
		name: 'Auto-expand folder',
	});
}

/**
 * @param {Sidebar} app
 * @param {JQuery} html
 * @param {object} options
 */
function onRenderSidebar(app, [html], options) {
	const cfg = game.settings.get(CFG.id, autoExpand);

	// TODO: Swap to data-uuid usage for v11
	html.querySelectorAll('li[data-folder-id]').forEach(el => {
		const folderId = el.dataset.folderId;
		const open = cfg[folderId] ?? false;
		if (open && el.classList.contains('collapsed')) el.querySelector('header')?.click();
	});
}

function enable() {
	Hooks.on('getDocumentDirectoryFolderContext', folderContext);
	Hooks.on('renderDocumentDirectory', onRenderSidebar);

	return true;
}

function disable() {
	Hooks.off('getDocumentDirectoryFolderContext', folderContext);
	Hooks.off('renderDocumentDirectory', onRenderSidebar);

	return true;
}

new Feature({ setting: 'auto-expand-sidebar-folders', label: 'Auto-expand Sidebar Folders', hint: 'Allows automatically expanding sidebar folders. Toggle the expansion with right click context menu.', category: 'foundry-ui', enable, disable, stage: 'init' });
