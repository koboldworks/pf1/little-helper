import { CFG } from '@root/config.mjs';
import { Feature } from '@root/core/feature.mjs';
import { createNode } from '@root/common.mjs';
import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {*} tokenConfig
 * @param {JQuery} html
 * @param {object} options
 */
function clarifyTokenSettings(tokenConfig, [html], options) {
	const iconEye = (title) => createNode('i', null, ['far', 'fa-eye', 'lil-icon', 'lil-vision-hint'], { title });
	const iconLamp = (title) => createNode('i', null, ['far', 'fa-lightbulb', 'lil-icon', 'lil-vision-hint'], { title });

	const inputs = [];
	html.querySelectorAll('[data-tab="vision"],[data-tab="light"]')
		?.forEach(tab => tab.querySelectorAll('input')?.forEach(lvin => inputs.push(lvin)));

	inputs.forEach(node => {
		/**
		 * @param {HTMLElement} el
		 */
		const prev = (el) => {
			const prevEl = el.previousElementSibling;
			prevEl?.classList.add('lil-vision-hint-label');
			return prevEl;
		};

		let icon;
		switch (node.name) {
			case 'light.dim': {
				icon = iconLamp(i18n.get('LittleHelper.Foundry.Light.Dim.Hint'));
				break;
			}
			case 'light.bright': {
				icon = iconLamp(i18n.get('LittleHelper.Foundry.Light.Bright.Hint'));
				break;
			}
		}
		if (icon) prev(node)?.after(icon);
	});
}

function enable() {
	Hooks.on('renderTokenConfig', clarifyTokenSettings);
}

function disable() {
	Hooks.off('renderTokenConfig', clarifyTokenSettings);
}

new Feature({ setting: 'token-vision-hints', label: 'Vision Hints', hint: 'Add extra clarity to token vision settings. With PF1 0.80.22 and newer disables brightvision editing in token settings.', category: 'token', enable, disable, stage: 'ready' });
