import { Feature } from '@root/core/feature.mjs';

const nullText = 'n/a';

/**
 * @param {TokenHUD} hud
 * @param {JQuery} html
 */
function renderTokenHUD(hud, [html]) {
	// const sefx = Object.values(data.statusEffects);

	const sEl = html.querySelector('div.status-effects');
	if (!sEl) return;

	sEl.classList.add('lil-enhanced');

	const d = document.createElement('div');
	d.classList.add('status-effects-label');
	d.textContent = nullText;

	const cs = getComputedStyle(sEl);
	// sEl.style.setProperty('--lil-width', cs.width);
	d.style.cssText += `--lil-width:${cs.width};width:calc(var(--lil-width) + 6px + 2px);`; // 6 for padding, 2 for border

	sEl.before(d);

	const mouseOver = (ev) => {
		const el = ev.target, hovering = el.matches('img.effect-control');
		d.textContent = hovering ? el.title : nullText;
		d.classList.toggle('none', !hovering);
	};

	sEl.addEventListener('mouseover', mouseOver, { passive: true });
}

function enable() {
	Hooks.on('renderTokenHUD', renderTokenHUD);
}

function disable() {
	Hooks.off('renderTokenHUD', renderTokenHUD);
}

new Feature({ setting: 'tokenhud-effects', label: 'Effects Menu Enhancements', hint: 'Display labels for token HUD status options, improve layout, and make active effects more obvious.', category: 'token-hud', enable, disable, stage: 'ready' });
