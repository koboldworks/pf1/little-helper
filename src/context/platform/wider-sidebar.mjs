import { Feature } from '@root/core/feature.mjs';

function widenSidebar() {
	document.body.style.setProperty('--sidebar-width', '360px');
}

function enable() {
	widenSidebar();
}

function disable() {
	return false;
}

new Feature({ setting: 'widen-sidebar', label: 'Widen Sidebar', hint: 'Increase sidebar width.', category: 'foundry-ui', enable, disable, stage: 'setup', enabledByDefault: false });
