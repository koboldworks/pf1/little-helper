/**
 * Display world name.
 */

import { Feature } from '@root/core/feature.mjs';
import { createNode } from '@root/common.mjs';

/**
 * @param {*} settings
 * @param {JQuery} html
 * @param {*} opts
 */
function renderSettingsEvent(settings, [html], opts) {
	const gd = html.querySelector('ul#game-details');
	const world = createNode('li', 'World', ['world']);
	world.append(createNode('span', game.world.title));
	gd.append(world);
}

function enable() {
	Hooks.on('renderSettings', renderSettingsEvent);
}

function disable() {
	Hooks.off('renderSettings', renderSettingsEvent);
	return false;
}

new Feature({ setting: 'world-identity', label: 'World Identity', hint: 'Display world name in settings tab.', category: 'foundry-ui', enable, disable, stage: 'setup' });
