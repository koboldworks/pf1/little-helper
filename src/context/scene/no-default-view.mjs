// NO DEFAULT VIEW ON SCENE

let lastSceneWarning = undefined;

function canvasReadyEvent(canvas) {
	if (!game.user.isGM) {
		Hooks.off('canvasReady', canvasReadyEvent);
		return;
	}

	if (canvas.scene.initial === null) {
		if (lastSceneWarning === canvas.scene.id) return;
		lastSceneWarning = canvas.scene.id;
		ui.notifications.warn(`This scene "${canvas.scene.name}" lacks initial view. Please consider setting it at bottom of scene settings.`);
	}
}

Hooks.on('canvasReady', canvasReadyEvent);
