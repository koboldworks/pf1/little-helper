import { i18n } from '@root/utility/i18n.mjs';

// Warns about scene lacking tokens with vision
// TODO: Warn about specific player owned tokens lacking vision.
// TODO: Warn about specific player lacking tokens.

function canvasReadyEvent(canvas) {
	if (!canvas.scene.tokenVision) return;

	// No tokens what-so-ever, hope it isn't decorative scene
	if (canvas.tokens.placeables.length == 0) return;

	// Player owned tokens that aren't NPC type
	const allPlayerTokens = canvas.tokens.placeables
		.filter(t => t.actor?.hasPlayerOwner && t.actor.type === 'character');
	const ownedTokens = allPlayerTokens.filter(t => t.isOwner);
	const unsightedTokens = ownedTokens.filter(t => !t.hasSight);

	if (unsightedTokens.length > 0) {
		if (game.user.isGM)
			ui.notifications.warn(i18n.get('LittleHelper.Scene.LackingVisionGM', { count: unsightedTokens.length }));
		else {
			if (unsightedTokens.length == 1)
				ui.notifications.warn(i18n.get('LittleHelper.Scene.NoVisionOnToken', { name: unsightedTokens[0].name }));
			else
				ui.notifications.warn(i18n.get('LittleHelper.Scene.LackingVision', { count: unsightedTokens.length }));
		}

		console.log('Unsighted Tokens:', unsightedTokens.map(t => t.name));
	}
}

/**
 * Scene update
 *
 * @param {Scene} scene
 * @param {object} update
 */
function sceneUpdateEvent(scene, update) {
	// Not viewed?
	if (scene.id !== canvas.scene?.id) return;

	// Ignore flag changes for notifications
	const scrubbedUpdate = foundry.utils.deepClone(update);
	delete scrubbedUpdate._id;
	delete scrubbedUpdate.flags; // e.g. Stairways module uses scene flags to store the teleporter points
	if (foundry.utils.isEmpty(scrubbedUpdate)) return;

	canvasReadyEvent(canvas);
}

/**
 * Warn about newly placed player owned token lacking vision.
 *
 * @param {TokenDocument} token
 * @param {object} options
 * @param {string} userId
 */
function newTokenOnScene(token, options, userId) {
	if (!canvas.scene.tokenVision) return;
	if (token.sight.enabled) return;
	const actor = token.actor;
	if (!actor?.hasPlayerOwner) return;
	if (actor.type !== 'character') return; // No NPC tokens, or basic actors
	if (!actor.isOwner) return;

	ui.notifications.warn(i18n.get('LittleHelper.Scene.NoVisionOnToken', { name: token.name }));
}

Hooks.on('createToken', newTokenOnScene);
Hooks.on('canvasReady', canvasReadyEvent);
Hooks.on('updateScene', sceneUpdateEvent);
