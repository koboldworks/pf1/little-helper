// WIP
// Sanity Check Tokens

import { createNode, getSystemData } from '@root/common.mjs';

/**
 * @param {TokenDocument} token
 * @param {object} options
 * @param {string} user
 */
function sanityCheckToken(token, options, user) {
	if (user !== game.user.id) return;

	if (options.temporary) return;

	const actor = token.actor;
	if (!actor) return;

	if (!['character', 'npc'].includes(actor.type)) return;

	const actorData = getSystemData(actor);

	let needMigration = false;

	const messages = [];

	// Old Senses Data
	if (typeof actorData.traits.senses === 'string') {
		messages.push('Actor has old & defunct senses data, migration needed.');
		needMigration = true;
	}

	if (messages.length) {
		const div = createNode('div', null, ['lil-sanity-check-dialog'], {
			children: [
				createNode('h3', 'Issues'),
				createNode('ul', null, ['messages'], {
					children: [...messages.map(m => createNode('li', m, ['lil-warning']))],
				}),
			],
		});

		if (needMigration) div.append(createNode('p', 'Do you wish to run migration on this token and its actor?'));

		const dialogData = {
			title: `${token.name} has issues...`,
			content: div.outerHTML,
			yes: async () => {
				ui.notifications?.info(`Migrating "${token.name}"`);
				const source = game.actors.get(token.actorId);
				if (source) {
					const sourceUpdate = pf1.migrations.migrateActorData(source);
					if (!foundry.utils.isEmpty(sourceUpdate)) await source.update(sourceUpdate);
					// console.log(sourceUpdate);
				}
				const actorUpdate = pf1.migrations.migrateActorData(actor, token);
				// if (!isEmpty(actorUpdate)) await actor.update(actorUpdate);
				// console.log(actorUpdate);
				ui.notifications?.info('Migration complete');
			},
			no: () => { },
		};

		if (needMigration)
			Dialog.confirm(dialogData);
		else
			Dialog.prompt(dialogData);
	}
}

Hooks.on('createToken', sanityCheckToken);
