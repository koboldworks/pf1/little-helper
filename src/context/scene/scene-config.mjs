/**
 * @param {*} app
 * @param {JQuery} html
 */
function sceneConfigEvent(app, [html]) {
	/** Another warning for no initial view */
	if (app.object.initial == null)
		html.querySelector('.initial-position')?.classList.add('lil-warning');
}

Hooks.on('renderSceneConfig', sceneConfigEvent);
