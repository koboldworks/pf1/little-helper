// Check if scene scale is ft in metric or m in imperial and report the mismatch

import { i18n } from '@root/utility/i18n.mjs';

const sceneScaleMismatch = (canvas) => {
	const grid = canvas.scene?.grid;
	if (!grid) return;

	const sunits = pf1.utils.getDistanceSystem();

	const { units, distance } = grid;
	const badUnits = sunits === 'metric' ? ['ft', 'mi'] : ['m', 'km'];
	const badUnitsi18n = badUnits.map(u => pf1.config.measureUnits[u]);
	if (badUnits.includes(units) || badUnitsi18n.some(ul => ul.toLowerCase() == units.toLowerCase()))
		ui.notifications.warn(i18n.get('LittleHelper.Warning.GM.SceneScaleMismatch', { units, scale: distance }));
};

Hooks.on('canvasReady', sceneScaleMismatch);
