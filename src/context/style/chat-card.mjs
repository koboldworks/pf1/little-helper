// Chat card restyle

import { Feature } from '@core/feature.mjs';

function enable() {
	document.body.classList.add('lil-chatcard-restyle');
}

function disable() {
	document.body.classList.remove('lil-chatcard-restyle');
}

new Feature({ setting: 'chat-restyle', label: 'Restyle', hint: 'Adds minor restyling to chat cards.', category: 'chat-card', enable, disable, stage: 'init', stream: true });
