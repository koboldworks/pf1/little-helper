import { CFG } from '../config.mjs';
import { getUserFromData } from '../common.mjs';
import { Feature, ClientSettingsData } from './feature.mjs';
import { NewsDialog } from './news.mjs';
import { i18n } from '@root/utility/i18n.mjs';

// Feature initialization

const init = () => {
	const C = CFG.console.colors;
	console.log('%cLittle Helper%c 🦎 | Initializing Features', C.main, C.unset);

	const user = game.user ?? getUserFromData();

	const settings = foundry.utils.mergeObject(ClientSettingsData.default(), game.settings.get(CFG.id, CFG.SETTINGS.generalClientConfiguration), { inplace: false });

	Feature.collection
		.forEach(f => f.init(user, settings));

	CFG.API.features = Feature.collection;
	CFG.API.utility.deleteClientSettings = () => game.settings.set(CFG.id, CFG.SETTINGS.generalClientConfiguration, null);
};

function load(stage) {
	Feature.stage[stage]
		.forEach(
			(/** @type {Feature} */ f) => {
				try {
					f.load();
				}
				catch (error) {
					f.error = error;
					console.error(error);
				}
			});
}

Hooks.once('init', init);

Hooks.once('i18nInit', () => {
	for (const feature of Feature.collection) {
		const { label, hint } = feature;
		feature.label = i18n.has(label) ? i18n.get(label) : label;
		feature.hint = i18n.has(hint) ? i18n.get(hint) : hint;
	}
});

Hooks.once('init', function initStageSubFeatures() {
	load('init');
});

Hooks.once('setup', function setupStageSubFeatures() {
	load('setup');
});

Hooks.once('ready', function readyStageSubFeatures() {
	load('ready');
	delete Feature.stage;

	const firstTime = Feature.collection.filter(f => f.firstTimeLaunch);
	if (firstTime.length > 0) NewsDialog.display(firstTime);
});
