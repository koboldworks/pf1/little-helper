import { CFG } from '../config.mjs';
import { i18n } from '@root/utility/i18n.mjs';

export class SubSetting {
	label;
	hint;
	type;
	value;
	fallback;

	callback;
	get display() { return this.#displayFn(this.value); }
	#displayFn = () => 'undefined';

	constructor({ setting, label, value, hint, type = Boolean, fallback = undefined, display, callback } = {}, key) {
		this.key = key;
		this.label = label || `LittleHelper.Feature.${setting}.${key}.Label`;
		this.hint = hint || `LittleHelper.Feature.${setting}.${key}.Hint`;
		this.type = type;
		this.fallback = fallback;
		this.value = value ?? fallback;
		if (typeof display === 'function')
			this.#displayFn = display;
		this.callback = callback;
	}
}

export class ClientSettingsData {
	static default() {
		return Feature.collection.reduce((settings, feature) => {
			const ssettings = { enabled: feature.defaultState };
			if (feature.subsettings !== undefined && !foundry.utils.isEmpty(feature.subsettings)) {
				ssettings.config = {};
				Object.entries(feature.subsettings).forEach(([key, cfg]) => {
					ssettings.config[key] = cfg.fallback;
				});
			}
			settings[feature.setting] = ssettings;
			return settings;
		}, new this);
	}
}

class Conflict {
	system;
	core;
	module;

	get obsolete() {
		const d = this.details;
		if (d.system.active) return true;
		if (d.core.active) return true;
		if (d.module.active) return true;
		return false;
	}

	/**
	 * @param {object} options
	 * @param {string | undefined} options.system System version number
	 * @param {string | undefined} options.core Core version number
	 * @param {string | undefined} options.module Module name
	 */
	constructor({ system, core, module } = {}) {
		this.system = system;
		this.core = core;
		this.module = module;
	}

	#details;
	get details() {
		this.#details ??= {
			system: {
				get active() { return this.obsolete; },
				_active: this.system != null,
				obsolete: this.system ? CFG.system.atLeast(this.system) : false,
				version: {
					threshold: this.system,
					current: CFG.system.version,
				},
			},
			core: {
				get active() { return this.obsolete; },
				_active: this.core != null,
				obsolete: this.core ? CFG.core.atLeast(this.core) : false,
				version: {
					threshold: this.core,
					current: CFG.core.version,
				},
			},
			module: {
				get active() { return this.obsolete; },
				_active: this.module?.length > 0,
				obsolete: this.module?.some(m => game.modules.get(m)?.active) ?? false,
				cause: this.module?.filter(m => game.modules.get(m)?.active),
			},
		};

		return this.#details;
	}
}

class Requirements {
	core;
	system;
	module;

	get satisfied() {
		if (this.module && !game.modules.get(this.module)?.active)
			return false;
		if (this.system && !CFG.system.atLeast(this.system))
			return false;
		if (this.core && !CFG.core.atLeast(this.core))
			return false;
		return true;
	}

	constructor(data) {
		this.module = data.module;
		this.system = data.system;
		this.core = data.core;
	}

	#details;
	get details() {
		const mod = game.modules.get(this.module);
		this.#details ??= {
			core: {
				get active() { return this._active; },
				_active: this.core != null,
				version: {
					threshold: this.system,
					current: CFG.core.version,
				},
				satisfied: this.core != null ? CFG.core.atLeast(this.core) : true,
			},
			system: {
				get active() { return this._active; },
				_active: this.system != null,
				version: {
					threshold: this.system,
					current: CFG.system.version,
				},
				satisfied: this.system != null ? CFG.system.atLeast(this.system) : true,
			},
			module: {
				get active() { return this._active; },
				_active: this.module != null,
				id: this.module,
				present: !!mod,
				enabled: mod?.active,
				satisfied: mod?.active ?? false,
			},
		};

		return this.#details;
	}
}

export class Feature {
	/**
	 * Enabled by user configuration.
	 *
	 * @private
	 */
	enabled = false;

	get active() {
		if (CFG.isStream && !this.stream) return false;
		return this.enabled && !this.disabled;
	}

	/** @private */
	defaultState = true;

	/**
	 * Disabled by some rule.
	 *
	 * @private
	 */
	disabled = false;

	/** Need restart if toggled */
	restart = false;

	/** Is the setting only useful for GMs?  */
	gmOnly = false;

	/** Scope */
	scope;

	get world() {
		return this.scope === 'world';
	}

	/** Label */
	label;
	/** Description */
	hint;
	/** Generic notification */
	notification;
	/** Generic warning */
	warning;
	/** Setting name */
	setting;
	/** Category for organizing */
	category;

	/** Related requirements, if any. */
	requirements; // { module: "id", system: "version", core: "version" }

	/** Obsoleted by versions. */
	conflict;

	/** What if anything the feature impacts heavily. */
	impact;
	/** Should this feature be enabled in stream view? */
	stream;

	subsettings;

	enable() { }
	disable() { }

	/**
	 * @type {Collection<Feature>}
	 * @private
	 */
	static collection = new Collection();

	/**
	 * Feature initialization stages.
	 *
	 * @type {{init: Feature[], setup: Feature[], ready: Feature[]}}
	 */
	static stage = {
		init: [],
		setup: [],
		ready: [],
	};

	/**
	 * @param {string} setting setting key
	 * @param {string} label Display name key
	 * @param {string} hint Explanation for the setting.
	 * @param {string} notification Notification associated with the setting.
	 * @param {string} warning Warning associated with the setting.
	 * @param {string} category Grouping ID.
	 * @param {boolean} restart Requires restart to take effect.
	 * @param {boolean} gm Meaningful for GM only
	 * @param {boolean} disabled
	 * @param {Function} enable Callback for enabling
	 * @param {Function} disable Callback for disabling
	 * @param {boolean} enabledByDefault Initial state when this feature is first discovered.
	 * @param {"init"|"setup"|"ready"} stage Stage at which to enable
	 * @param requirements
	 * @param conflict
	 * @param subsettings
	 * @param impact Structure for feature's impact on overall experience.
	 */
	constructor({ setting, label, hint, notification, warning, category, enable, disable, stage = 'init', scope = 'client', restart = false, gm = false, disabled = false, requirements, conflict, enabledByDefault = true, subsettings, impact, stream = false } = {}) {
		this.setting = setting;
		this.label = label || `LittleHelper.Feature.${setting}.Label`;
		this.hint = hint || `LittleHelper.Feature.${setting}.Hint`;
		this.notification = notification;
		this.warning = warning;
		this.category = category;
		this.enable = enable;
		this.disable = disable;
		this.scope = scope;
		this.restart = restart;
		this.gmOnly = gm ?? false;
		this.disabled = disabled;
		this.defaultState = enabledByDefault;
		this.subsettings = subsettings;
		this.impact = impact;
		this.stream = stream;

		if (conflict) this.conflict = new Conflict(conflict);
		if (requirements) this.requirements = new Requirements(requirements);

		if (Feature.collection.has(setting)) throw new Error(`Little Helper 🦎 | Attempting to re-define "${setting}"`);
		Feature.collection.set(setting, this);
		if (!Feature.stage[stage]) throw new Error(`Little Helper 🦎 | Registered "${setting}" for invalid or already passed stage: "${stage}".`);
		Feature.stage[stage].push(this);
	}

	static create({ setting, label, hint, notification, warning, category, enableCallback, disableCallback, stage = 'init', gm = false, disabled = false, conflict, requirements, enabledByDefault = true, impact } = {}) {
		new Feature({ setting, label, hint, notification, warning, category, enableCallback, disableCallback, stage, gm, disabled, conflict, requirements, enabledByDefault, impact });
	}

	toggle() {
		this.enabled = !this.enabled;
		this.enabled ? this.enable() : this.disable();
	}

	testToggle() {
		const scope = this.world ? CFG.SETTINGS.generalWorldConfiguration : CFG.SETTINGS.generalClientConfiguration;
		const settings = game.settings.get(CFG.id, scope);
		const doToggle = this.enabled !== (settings[this.setting]?.enabled ?? true);
		if (doToggle) this.toggle();
		return doToggle;
	}

	firstTimeLaunch = false;
	get firstTime() {
		const scope = this.world ? CFG.SETTINGS.generalWorldConfiguration : CFG.SETTINGS.generalClientConfiguration;
		// console.log(CFG.id, scope);
		const settings = game.settings.get(CFG.id, scope);
		return settings[this.setting]?.enabled === undefined;
	}

	init(user, settings) {
		this.firstTimeLaunch = this.firstTime;

		const scope = this.world ? CFG.SETTINGS.generalWorldConfiguration : CFG.SETTINGS.generalClientConfiguration;
		settings ??= foundry.utils.mergeObject(ClientSettingsData.default(), game.settings.get(CFG.id, scope), { inplace: false });

		const cfg = settings[this.setting];
		this.enabled = cfg?.enabled ?? this.defaultState;

		if (this.subsettings !== undefined)
			Object.entries(this.subsettings).forEach(([key, ss]) => {
				ss.value = cfg.config[key] ?? ss.fallback;
				// console.log(ss);
				this.subsettings[key] = new SubSetting(ss, key);
			});

		if (this.conflict?.obsolete) {
			const d = this.conflict.details;
			const o = ['⚠ Little Helper 🦎 | Feature:', this.setting, '| Obsoleted |'];
			const coreConflict = d.core.active,
				systemConflict = d.system.active,
				moduleConflict = d.module.active;
			if (coreConflict) o.push('Foundry', d.core.version);
			if (systemConflict) {
				if (coreConflict) o.push('|');
				o.push('Game System', d.system.version);
			}
			if (moduleConflict) {
				if (coreConflict || systemConflict) o.push('|');
				o.push('Module(s)', d.module.cause);
			}
			console.log(...o);
			this.disabled = true;
		}

		if (!this.disabled && this.requirements?.satisfied === false) {
			const d = this.requirements.details;
			const o = ['⚠ Little Helper 🦎 | Feature:', this.setting, '| Requirements not met |'];
			const coreReq = d.core.active,
				systemReq = d.system.active,
				moduleReq = d.module.active,
				coreAndSysReq = coreReq && systemReq,
				versionAndMod = (coreReq || systemReq) && moduleReq;
			if (coreReq) o.push('Foundry', d.core.version.threshold);
			if (coreAndSysReq) o.push('|');
			if (systemReq) o.push('Game System', d.system.version.threshold);
			if (versionAndMod) o.push('|');
			if (moduleReq) o.push('Module', { id: d.module.id });
			console.log(...o);
			this.disabled = true;
		}

		if (this.gmOnly && !user.isGM) {
			// console.log('GM ONLY', user);
			this.disabled = true;
		}
	}

	load() {
		if (this.active) this.enable();
	}

	save() {
		const scope = this.world ? CFG.SETTINGS.generalWorldConfiguration : CFG.SETTINGS.generalClientConfiguration;
		const settings = game.user.getFlag(CFG.id, scope);
		settings[this.setting].enabled = this.enabled;
		game.user.setFlag(CFG.id, scope, settings);
	}

	static saveAll(settings) {
		const oldSettings = foundry.utils.deepClone(game.settings.get(CFG.id, CFG.SETTINGS.generalClientConfiguration));
		this.settings = foundry.utils.mergeObject(ClientSettingsData.default(), oldSettings, { inplace: false });
		this.settings = foundry.utils.mergeObject(this.settings, settings, { inplace: false });

		return game.settings.set(CFG.id, CFG.SETTINGS.generalClientConfiguration, this.settings)
			.then(_ => {
				let refreshRequired = false;
				for (const [id, cfg] of Object.entries(settings)) {
					/** @type {Feature} */
					const feature = Feature.collection.get(id);

					if (cfg.config !== undefined)
						Object.entries(cfg.config).forEach(([key, ss]) => feature.subsettings[key].value = ss);

					// Toggle only if the setting changed
					if (cfg.enabled !== feature.enabled) {
						const noOldSettings = oldSettings[id]?.enabled === undefined;
						const rv = cfg.enabled ? feature.enable() : feature.disable();
						if (!noOldSettings && rv === false) refreshRequired = true;
						feature.enabled = cfg.enabled;
					}
				}

				if (refreshRequired) {
					ui.notifications?.warn(i18n.get('LittleHelper.Warning.RefreshNeeded'));
				}
			});
	}
}
