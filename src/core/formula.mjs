// Formula Helpers

import { CFG } from '../config.mjs';

/**
 * Remove flairs, remove excessive spacing, simplify math
 *
 * @param {string} formula
 */
export const unflair = (formula) => formula
	.replace(/\[.*?]/g, '').replace(/\s\s+/g, ' ').replace(/\+ -/g, '-').replace(/\s*\+\s*/g, '+').trim();

/**
 * @param {string} formula
 * @returns {string[]}
 */
export function getVariables(formula) {
	const vars = formula.match(/(@\w[\w.]*)\b/g);
	if (vars) return Array.from(vars.map(v => v.slice(1)).reduce((set, v) => set.add(v), new Set()));
	return [];
};

CFG.API.formula = {
	...CFG.API.formula,
	getVariables,
};
