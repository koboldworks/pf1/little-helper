class Icon {
	#cls;
	#string;

	/**
	 * @param {string} icon
	 * @param {string} type
	 */
	constructor(icon, type = 'fa-solid') {
		this.#cls = new Set([type, icon]);
		this.#string = [...this.#cls].join(' ');
	}

	toString() {
		return this.#string;
	}

	get string() {
		return this.toString();
	}
}

export const FAIcons = {
	link: new Icon('fa-link'),
	unlink: new Icon('fa-unlink'),
	heart: new Icon('fa-heart'),
	heartBroken: new Icon('fa-heart-broken'),
	box: new Icon('fa-box'),
	coins: new Icon('fa-coins'),
	hourglass: new Icon('fa-hourglass'),
	hourglassStart: new Icon('fa-hourglass-start'),
	seedling: new Icon('fa-seedling'),
	skull: new Icon('fa-skull'),
	dice: new Icon('fa-dice'),
	d20: new Icon('fa-dice-d20'),
	questionCircle: new Icon('fa-question-circle'),
	infoCircle: new Icon('fa-info-circle'),
	exclamationTriangle: new Icon('fa-exclamation-triangle'),
	cog: new Icon('fa-cog'),
	shieldAlt: new Icon('fa-shield-alt'),
	ruler: new Icon('fa-ruler-combined'),
	rulerCombined: new Icon('fa-ruler-combined'),
	bullseye: new Icon('fa-bullseye'),
	table: new Icon('fa-table'),
	signLanguage: new Icon('fa-sign-language'),
	puzzlePiece: new Icon('fa-puzzle-piece'),
	save: new Icon('fa-floppy-disk'),
	mitten: new Icon('fa-mitten'),
	spinner: new Icon('fa-spinner'),
	batteryHalf: new Icon('fa-battery-half'),
	eye: new Icon('fa-eye'),
	eyeSlash: new Icon('fa-eye-slash'),
	search: new Icon('fa-search'),
	book: new Icon('fa-book'),
	globe: new Icon('fa-globe'),
	filter: new Icon('fa-filter'),
	user: new Icon('fa-user'),
	infinity: new Icon('fa-infinity'),
	star: new Icon('fa-star'),
	crown: new Icon('fa-crown'),
	recycle: new Icon('fa-recycle'),
	reset: new Icon('fa-rotate-left'),
	sync: new Icon('fa-sync'),
	syncAlt: new Icon('fa-sync-alt'),
	file: new Icon('fa-file'),
	fileAlt: new Icon('fa-file-alt'),
	fileExport: new Icon('fa-file-export'),
	folder: new Icon('fa-folder'),
	folderOpen: new Icon('fa-folder-open'),
};
