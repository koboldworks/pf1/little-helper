import { CFG } from '../config.mjs';

Hooks.once('setup', () => {
	game.keybindings.register(CFG.id, 'extraInfo', {
		name: 'LittleHelper.Keybindings.ExtraInfo.Hold',
		editable: [{ key: 'AltRight' }, { key: 'AltLeft' }],
		onDown: () => {
			CFG.state.alt = true;
			Hooks.callAll('little-helper.extraInfo', true);
		},
		onUp: () => {
			CFG.state.alt = false;
			Hooks.callAll('little-helper.extraInfo', false);
		},
	});
});
