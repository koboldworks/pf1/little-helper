import { CFG } from '../config.mjs';
import { Feature } from './feature.mjs';
import { FAIcons } from './icons.mjs';

export class NewsDialog extends FormApplication {
	constructor(features) {
		super();

		this.features = features;
		this.groups = this.features.reduce((groups, feature) => {
			const cat = feature.category;
			groups[cat] ??= [];
			groups[cat].push(feature);
			return groups;
		}, {});
	}

	get template() {
		return `modules/${CFG.id}/template/news.hbs`;
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			title: 'Little Helper 🦎 new features',
			width: 460,
			classes: [...options.classes, 'koboldworks', 'news'],
			closeOnSubmit: true,
			submitOnClose: false,
			submitOnChange: false,
		};
	}

	getData() {
		const data = super.getData();
		data.features = this.features;
		data.groups = this.groups;
		data.sorted = Object.entries(this.groups)
			.map(([id, group]) => ({ id, group, label: `LittleHelper.Settings.Category.${id}` }))
			.sort((a, b) => a.id.localeCompare(b.id));
		data.icons = FAIcons;
		return data;
	}

	_updateObject(event, formData) {
		Feature.saveAll(foundry.utils.expandObject(formData));
	}

	static display(newfeatures) {
		new NewsDialog(newfeatures).render(true, { focus: false });
	}
}
