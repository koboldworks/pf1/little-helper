import { CFG } from '../config.mjs';
import { stopEventPropagation } from '../common.mjs';
import { Feature, ClientSettingsData } from './feature.mjs';
import { FAIcons } from './icons.mjs';
import { i18n } from '@root/utility/i18n.mjs';

export class ClientSettingsDialog extends FormApplication {
	settings;

	constructor() {
		super();

		this.settings = foundry.utils.mergeObject(ClientSettingsData.default(), foundry.utils.deepClone(game.settings.get(CFG.id, CFG.SETTINGS.generalClientConfiguration)));

		this.features = Feature.collection.filter(f => !f.world);
		// if (!game.user.isGM) this.features = this.features.filter(f => !f.gmOnly);
		this.groups = this.features.reduce((groups, feature) => {
			const cat = feature.category;
			if (!groups[cat]) groups[cat] = [];
			groups[cat].push(feature);
			return groups;
		}, {});
	}

	get template() {
		return `modules/${CFG.id}/template/client-settings.hbs`;
	}

	getData() {
		const data = super.getData();
		data.features = this.features;
		data.groups = this.groups;
		data.sorted = Object.entries(this.groups)
			.map(([id, group]) => ({ id, group, label: i18n.get(`LittleHelper.Settings.Category.${id}`) }))
			.sort((a, b) => a.label.localeCompare(b.label, undefined, { sensitivity: 'base' }));
		data.icons = FAIcons;
		// console.log(data);
		return data;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			title: i18n.get('LittleHelper.Settings.Client', { module: 'Little Helper 🦎' }),
			classes: [..._default.classes, 'koboldworks', 'settings'],
			width: 460,
			height: 720,
			id: 'little-helper-client-settings',
			scrollY: ['section.content'],
			closeOnSubmit: true,
			submitOnChange: false,
			submitOnClose: false,
			resizable: true,
		};
	}

	_updateObject(event, data) {
		Feature.saveAll(foundry.utils.expandObject(data));
	}

	_reset() {
		this.features.forEach(f => {
			if (!f.disabled) f.enabled = f.defaultState;
		});
		this.render();
	}

	_clear() {
		this.features.forEach(f => {
			if (!f.disabled) f.enabled = false;
		});
		this.render();
	}

	searchTimer;
	lastTerm;
	/**
	 * @param {Element} area
	 * @param {Event} event
	 * @param term
	 */
	doSearch(area, term) {
		// Clear all search related classes
		area.querySelectorAll('.search-mismatch,.search-match,.search-match-inner')
			.forEach(el => el.classList.remove('search-mismatch', 'search-match', 'search-match-inner'));

		if (term.length === 0) return;

		// Hide unmatched results
		for (const group of area.querySelectorAll('.group ul')) {
			let matches = 0;
			for (const setting of group.querySelectorAll('.setting[data-setting-id]')) {
				let match = false;
				if (setting.querySelector('label')?.textContent.trim().toLocaleLowerCase().includes(term)) {
					match = true;
					setting.classList.add('search-match');
				}
				else if (setting.querySelector('.hint')?.textContent.trim().toLocaleLowerCase().includes(term)) {
					match = true;
					setting.classList.add('search-match');
				}

				if (match) matches++;
				else setting.classList.add('search-mismatch');
			}

			if (group.querySelector('.title')?.textContent.trim().toLocaleLowerCase().includes(term)) {
				matches++;
				group.classList.add('search-match');
			}

			if (matches === 0)
				group.classList.add('search-mismatch');
			else
				group.classList.add('search-match-inner');
		}
	}

	/**
	 * @param {HTMLElement} html
	 * @param {Event} event
	 */
	debounceSearch(html, event) {
		stopEventPropagation(event);
		clearTimeout(this.searchTimer);

		const term = event.target.value.trim();
		if (term === this.lastTerm) return;
		this.lastTerm = term;

		setTimeout(() => this.doSearch(html, term.toLocaleLowerCase()), 250);
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = jq[0];

		html.querySelector('button[type="reset"]')
			.addEventListener('click', event => this._reset(stopEventPropagation(event)));

		html.querySelector('button[type="clear"]')
			.addEventListener('click', event => this._clear(stopEventPropagation(event)));

		const searchArea = html.querySelector('section.content');
		const search = html.querySelector('input.search-input');
		search.addEventListener('change', this.debounceSearch.bind(this, searchArea));
		search.addEventListener('input', this.debounceSearch.bind(this, searchArea));

		html.querySelectorAll('button[data-callback]').forEach(el => {
			const p = el.dataset.callback;
			const [fId, _, fsId] = p.split('.');
			const f = Feature.collection.get(fId);
			const ss = f?.subsettings[fsId];
			if (!ss) return;
			el.addEventListener('click', ev => {
				ev.preventDefault();
				ev.stopPropagation();
				ss.callback.fn(ev, f, ss);
			});
		});
	}
}
