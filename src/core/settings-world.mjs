// WIP

import { CFG } from '../config.mjs';
import { i18n } from '@root/utility/i18n.mjs';

export class WorldSettingsData {
	static default() {
		return new this;
	}
}

export class WorldSettingsDialog extends FormApplication {
	settings;

	constructor() {
		super();

		this.settings = game.settings.get(CFG.id, CFG.SETTINGS.generalWorldConfiguration);
	}

	get template() {
		return `modules/${CFG.id}/template/world-settings.hbs`;
	}

	getData() {
		return this.settings;
	}

	static get defaultOptions() {
		return {
			...super.defaultOptions,
			title: i18n.get('LittleHelper.Settings.World', { module: 'Little Helper 🦎' }),
		};
	}

	_updateObject(event, data) {

	}

	activateListeners(jq) {
		super.activateListeners(jq);
	}
}
