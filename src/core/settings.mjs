import { CFG } from '../config.mjs';

import { ClientSettingsData } from './feature.mjs';
import { ClientSettingsDialog } from './settings-client.mjs';
// import { WorldSettingsData, WorldSettingsDialog } from './settings-world.mjs';

import { FAIcons } from './icons.mjs';

Hooks.once('init', function registerSettings() {
	const C = CFG.console.colors;
	console.debug('%cLittle Helper%c 🦎 | Registering Settings', C.main, C.unset);
	/*
	game.settings.register(
		CFG.id,
		CFG.SETTINGS.generalWorldConfiguration,
		{
			type: Object,
			default: WorldSettingsData.default(),
			scope: 'world',
			config: false,
		}
	);
	*/

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.generalClientConfiguration,
		{
			type: Object,
			default: ClientSettingsData.default(),
			scope: 'client',
			config: false,
			// onChange: () => debouncedReload()
		},
	);

	// game.settings.get(CFG.id, CFG.SETTINGS.generalClientConfiguration);
	// game.settings.get(CFG.id, CFG.SETTINGS.generalWorldConfiguration);

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.transparency,
		{
			name: 'Transparency',
			hint: 'What permission level is required to display certain potentially sensitive information.',
			config: true,
			scope: 'world',
			type: Number,
			default: 3,
			choices: {
				3: 'Owner',
				2: 'Observer',
				1: 'Player-owned',
				0: 'Any (full transparency)',
			},
			requiresReload: true,
		},
	);

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.transparencySham,
		{
			name: 'GM special case for transparency',
			hint: 'GM is treated as special case for transparency. If enabled, various information enhancements are not applied to GM generated content regardless of the general transparency setting. This exists mostly as compatibility shim with other modules that want to obfuscate information.',
			type: Boolean,
			default: false,
			config: true,
			scope: 'world',
			requiresReload: true,
		},
	);

	/*
	game.settings.register(
		CFG.id,
		CFG.SETTINGS.chat,
		{
			type: Object,
			default: {},
			config: false,
			scope: 'client',
			requiresReload: true,
		}
	);
	*/

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.inlineFormulaDetail,
		{
			name: 'Detailed inline formulas',
			hint: 'Display greater detail for inline formulas. If disabled, flairs and excess details are scrubbed.',
			config: true,
			scope: 'client',
			type: Boolean,
			default: true,
			requiresReload: true,
		},
	);

	// delete game.settings.register(CFG.id, 'tokenhud-label', { ... });

	/*
	game.settings.registerMenu(
		CFG.id,
		CFG.SETTINGS.generalWorldConfiguration,
		{
			id: `${CFG.id}-config-world`,
			// name: 'World config',
			label: 'World Configuration',
			icon: FAIcons.globe, // button icon
			type: WorldSettingsDialog,
			restricted: true,
		}
	);
	*/

	game.settings.registerMenu(
		CFG.id,
		CFG.SETTINGS.generalClientConfiguration,
		{
			id: `${CFG.id}-config-client`,
			name: 'Client-side settings',
			label: 'Client Configuration',
			icon: FAIcons.user, // button icon
			type: ClientSettingsDialog,
			restricted: false,
		},
	);

	game.settings.register(CFG.id, 'migration', {
		type: String,
		default: '0.6.0', // Oldest supported pre-migration version.
		scope: 'world',
		config: false,
	});
});
