import { TabData } from './tab-data.mjs';

export class SharedData {
	/** @type {User} */
	user;
	/** @type {Actor|Item} */
	document;

	/** @type {boolean} - Is one of the system sheets? */
	sysSheet = false;
	/** @type {boolean} - Is one of the core large sheets?  */
	coreSheet = false;
	/** @type {boolean} - Is module introduced sheet? */
	altSheet = false;

	/** @returns {boolean} True if user has at least observer permission. */
	get isObserver() {
		return this.document.permission >= CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER;
	}

	/** @type {boolean} - Is system defined documen type */
	get systemDocument() {
		const doc = this.document;
		if (doc instanceof Actor) return Object.keys(game.system.documentTypes.Actor).includes(doc.type);
		else if (doc instanceof Item) return Object.keys(game.system.documentTypes.Item).includes(doc.type);
		else return false;
	}

	/**
	 * Return system data for this.document or .data for action.
	 *
	 * @type {object}
	 */
	get systemData() {
		const doc = this.document;
		if (doc instanceof Actor || doc instanceof Item) return doc.system;
		if (doc instanceof pf1.components.ItemAction) return doc;
		return undefined;
	}

	get sourceData() {
		return this.document.toObject().system;
	}

	/** @type {object} */
	#rollData;
	/**
	 * @returns {object}
	 */
	get rollData() {
		if (!this.#rollData) {
			if (!this.document.actor && !(this.document instanceof Actor))
				// Fill in game.user.character roll data if no actor is otherwise present
				this.#rollData = { ...(game.user.character?.getRollData() ?? {}), ...this.document.getRollData() };
			else
				this.#rollData = { ...this.document.getRollData() };
		}
		return this.#rollData;
	}

	#dc;
	/**
	 * @returns {number | undefined}
	 */
	get dc() {
		this.#dc ??= this.document.getDC(this.rollData);
		return this.#dc;
	}

	/** @type {Record<string, TabData>} */
	tabs = {};

	#items;
	get items() {
		if (!this.#items) {
			const actor = this.document,
				books = actor.system.attributes?.spells?.spellbooks;

			this.#items = { byType: {} };

			const itemTypes = this.document.itemTypes;
			Object.entries(itemTypes).forEach(([key, list]) => {
				this.#items.byType[key] ??= {};
				Object.defineProperty(this.#items.byType[key], 'all', {
					value: list,
					enumerable: true,
				});
			});

			const typed = this.#items.byType;
			for (const type of Object.keys(typed)) {
				const activeCache = [], subTypes = {};

				const items = typed[type];

				items.all.forEach(item => {
					items.active = true;
					if (item.isActive) activeCache.push(item);

					if (item.type === 'spell') {
						const itemData = item.system;
						const bookId = itemData.spellbook;
						if (!bookId) return;
						items._book ??= {};
						items._book[bookId] ??= {
							all: [],
							book: books?.[bookId],
						};
						const book = items._book[bookId];
						book.all.push(item);
						book[itemData.level] ??= [];
						book[itemData.level].push(item);
					}
					else {
						const subType = item.subType;
						items[subType] ??= { all: [] };
						items[subType].all.push(item);
					}
				});

				const props = {
					active: {
						value: activeCache,
					},
				};

				Object.entries(subTypes).forEach(([key, data]) => {
					props[key] = {
						value: data,
					};
				});

				Object.defineProperties(items, props);
			}
		}
		return this.#items;
	}

	/** @returns {Actor|undefined} */
	get actor() {
		const doc = this.document;
		if (doc instanceof Actor) return doc;
		if (doc instanceof Item) return doc.actor ?? game.user.character;
		if (doc instanceof pf1.components.ItemAction) return doc.item?.actor ?? game.user.character;
		return undefined;
	}

	get actorData() {
		return this.actor.system;
	}

	/** @type {Item|undefined} */
	get item() {
		const doc = this.document;
		if (doc instanceof Item) return doc;
		if (doc instanceof Actor) return undefined;
		return doc.item;
	}

	get itemData() {
		return this.item.system;
	}

	/** @type {ItemAction|undefined} */
	get action() {
		const doc = this.document;
		if (doc instanceof Item) return undefined;
		if (doc instanceof Actor) return undefined;
		return doc;
	}

	#mindless;
	get isMindless() {
		this.#mindless ??= this.actor?.system.abilities?.int?.value === null;
		return this.#mindless;
	}

	/**
	 * @param {User} user
	 * @param {Item|Actor} doc
	 */
	constructor(user, doc) {
		this.user = user;
		this.document = doc;
	}
}
