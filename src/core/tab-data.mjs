export class TabData {
	/** @type {Element} */
	tab;

	/**
	 * @param {Element} tab
	 */
	constructor(tab) {
		this.tab = tab;
	}
}
