function boundingWidth(b) {
	if (!b) return { l: 0, w: window.innerWidth, t: 0 };

	const bv = b.getBoundingClientRect();
	return { l: bv.left, w: bv.width, t: bv.top };
}

/**
 * @param {Element} parentEl
 * @param {Element} tooltip
 * @param {object} options
 * @param {boolean} [options.aligned]
 * @param {Element} [options.boundedBy] Bounding element. Window if nothing is specified
 */
export function setStaticToolTipListener(parentEl, tooltip, { aligned = true, boundedBy } = {}) {
	parentEl.addEventListener('pointerenter', function onHover() {
		const p = parentEl.getBoundingClientRect();
		const { l, w, t } = boundingWidth(boundedBy);
		tooltip.style.cssText += `--p-left:${p.left};--p-bottom:${p.bottom};--width:${tooltip.clientWidth};--p-right:${p.right};--height:${tooltip.clientHeight};--p-height:${p.height};--p-width:${p.width};--p-right:${p.right};--p-top:${Math.max(t, p.top)};`;
		if (aligned) tooltip.classList.toggle('right-aligned', p.left > l + Math.floor(w / 2));
	}, { passive: true });
}

/**
 * Hook parent
 *
 * @param {Element} el Parent
 * @param {object} options
 * @param {number} options.offsetX
 * @param {number} options.offsetY
 */
export function hookTooltip(el, { offsetX = 0, offsetY = 0 } = {}) {
	const tt = el.querySelector('.lil-tooltip');
	el.addEventListener('pointermove', ev => tt.style.cssText += `left: ${ev.clientX + 14 + offsetX}px; top: ${ev.clientY - 24 + offsetY}px;`, { passive: true });
}

/**
 * @param {Element} el
 * @param {object} options
 * @param {TooltipManager.TOOLTIP_DIRECTIONS} options.direction
 * @param {Function} options.fn - Content generating callback
 * @param {any} options.context - Context data for the content function
 * @param {string} options.css - Space separated CSS class strings
 */
export function hookCoreTooltip(el, { css, direction = TooltipManager.TOOLTIP_DIRECTIONS.UP, fn, context } = {}) {
	el.addEventListener('pointerover', async event => {
		const content = await fn(event, el, context);
		if (!content) return;

		const tt = { direction };

		if (typeof content === 'string') tt.text = content;
		else tt.content = content;

		if (css) tt.cssClass = css;

		game.tooltip.activate(el, tt);
	}, { passive: true });

	el.addEventListener('pointerout', () => game.tooltip.deactivate(), { passive: true });
}
