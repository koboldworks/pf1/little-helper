import { CFG } from '../config.mjs';

const ConditionHelp = {};

Hooks.once('setup', function conditions_i18n() {
	const C = CFG.console.colors;
	console.debug('%cLittle Helper%c 🦎 | i18n | Building condition help', C.main, C.unset);

	for (const k of pf1.registry.conditions.keys()) {
		ConditionHelp[k] = `LittleHelper.Conditions.${k}`;
	}

	CFG.i18n.conditions = ConditionHelp;
});
