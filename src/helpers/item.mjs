import { i18n } from '@root/utility/i18n.mjs';

/**
 * @param {string} type
 */
export function getItemMainTypeLabel(type) {
	return i18n.get(CONFIG.Item.typeLabels[type]);
}

/**
 * @param {Item} item
 * @param {string} type
 * @param {string} subType
 * @returns {string}
 */
export function getFullItemTypeLabel(item, type, subType) {
	const showIdentified = !item.showUnidentifiedData;

	const itemData = item.system;
	switch (type) {
		case 'equipment': {
			if (['armor', 'shield'].includes(subType)) {
				let subType2 = itemData.equipmentSubtype;
				if (!subType2) {
					if (subType === 'armor') subType2 ||= 'lightArmor';
					else if (subType === 'shield') subType2 ||= 'lightShield';
				}
				return pf1.config.equipmentTypes[subType]?.[subType2];
			}
			else {
				if (!showIdentified && subType !== 'clothing') return i18n.get(CONFIG.Item.typeLabels.equipment);

				const label = pf1.config.equipmentTypes[subType]?._label;
				const slot = item.system.slot || 'slotless';
				if (['wondrous', 'other'].includes(subType)) {
					const slabel = pf1.config.equipmentSlots[subType][slot];
					if (slabel) return `${label} (${slabel})`;
				}
				return label;
			}
		}
		case 'loot': {
			if (!showIdentified) return i18n.get(CONFIG.Item.typeLabels.loot);
			const label = pf1.config.lootTypes[subType];
			if (subType === 'ammo') {
				const subLabel = pf1.config.ammoTypes[itemData.extraType];
				return subLabel ? `${label} [${subLabel}]` : label;
			}
			return label;
		}
		case 'feat': {
			const ablType = pf1.config.abilityTypes[itemData.abilityType]?.short; // Ex, Su, Sp
			const label = pf1.config.featTypes[subType];
			if (ablType) return `${label} (${ablType})`;
			if (subType === 'misc') return `${i18n.get('PF1.Feature')} [${label}]`;
			return label;
		}
		case 'consumable':
			if (showIdentified)
				return `${i18n.get(CONFIG.Item.typeLabels.consumable)} [${pf1.config.consumableTypes[subType]}]`;
			else return i18n.get(CONFIG.Item.typeLabels.consumable);
		case 'class':
			return pf1.config.classTypes[subType];
		case 'race':
			return i18n.get(CONFIG.Item.typeLabels.race);
		case 'container':
			return i18n.get(CONFIG.Item.typeLabels.container);
		case 'weapon':
		case 'attack':
		case 'buff':
		case 'spell':
		default:
			return i18n.get(CONFIG.Item.typeLabels[type]);
	}
}

/**
 * @param {Item} item
 * @param {string} type
 * @param {string} subType
 * @returns {string}
 */
export function getItemSubTypeLabel(item, type, subType) {
	const itemData = item.system;
	switch (type) {
		case 'equipment': {
			const subType2 = itemData.equipmentSubtype;
			return pf1.config.equipmentTypes[subType][subType2];
		}
		case 'loot': {
			const label = pf1.config.lootTypes[subType];
			if (subType === 'ammo') {
				const subLabel = pf1.config.ammoTypes[itemData.extraType];
				return subLabel ? `${label} [${subLabel}]` : label;
			}
			return label;
		}
		case 'feat':
			return pf1.config.featTypes[subType];
		case 'consumable':
			return pf1.config.consumableTypes[subType];
		case 'class':
			return pf1.config.classTypes[subType];
		case 'buff':
			return i18n.get(pf1.config.buffTypes[subType]);
		case 'attack':
			return pf1.config.attackTypes[subType];
		case 'race':
		case 'container':
		case 'weapon':
		case 'spell':
			return null;
	}
}
