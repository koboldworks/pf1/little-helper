import { i18n } from '@root/utility/i18n.mjs';

export const healthLabels = {
	0: 'LittleHelper.HealthLevel.Healthy',
	1: 'LittleHelper.HealthLevel.Grazed',
	2: 'LittleHelper.HealthLevel.Wounded',
	3: 'LittleHelper.HealthLevel.Critical',
	dying: 'LittleHelper.HealthLevel.Dying',
	dead: 'LittleHelper.HealthLevel.Dead',
};

/**
 * Evaluates the numeric point at which character dies.
 *
 * @param {Actor} actor
 * @returns {number}
 */
export const evaluateDeathPoint = (actor) => {
	const ad = actor.system,
		isUnliving = ad.abilities.con.value === null,
		deathPoint = isUnliving ? 0 : -(ad.abilities[ad.attributes.hpAbility]?.total ?? 0);
	return deathPoint;
};

Hooks.once('i18nInit', () => {
	Object.entries(healthLabels)
		.forEach(([key, value]) => healthLabels[key] = i18n.get(value));
});
