import { CFG } from '@root/config.mjs';
import { maxPrecision, signNum } from '@root/common.mjs';
import { unflair, getVariables } from '@core/formula.mjs';
import { i18n } from '@root/utility/i18n.mjs';

const METATYPES = {
	normal: 'normal',
	nonCritical: 'nonCrit',
	criticalOnly: 'crit',
};

const baseDamageData = () => {
	return {
		enabled: true,
		formula: '',
		static: true,
		avg: 0,
		est: 0,
	};
};

class DamageInstance {
	label;
	formula;
	roll;
	type;
	metatype;
	enabled = true;
	conditional = false;
	verbose = true;

	damage = {};

	constructor(label, formula, roll = null, type = 'inherit', metatype = METATYPES.normal) {
		this.label = label;
		this.formula = formula;
		this.roll = roll;
		this.type = type;
		this.metatype = metatype;
	}
}

export class TheoryDialog extends FormApplication {
	_config = {
		size: undefined,
		damage: {
			custom: { formula: '', enabled: true },
		},
		attacks: [],
		clOffset: 0,
		haste: false,
		manyshot: false,
		full: false,
		action: null,
	};

	rollData;

	constructor(item, action) {
		super();

		this.item = item;
		this.action = action;

		// Register for item updates
		this.item.apps[this.appId] = this;

		this.setTitle();
		this.options.id = item.uuid + '-theory-dialog';
	}

	setTitle() {
		const item = this.item,
			action = this.action ?? item.defaultAction;

		if (action)
			this.options.title = `Theory on ${item.name}: ${action.name}`;
		else
			this.options.title = `Theory on ${item.name}`;
	}

	/**
	 * @override
	 */
	async close(opts) {
		delete this.item.apps[this.appId]; // Unreg
		return super.close(opts);
	}

	static get defaultOptions() {
		const _defaults = super.defaultOptions;
		return {
			..._defaults,
			title: 'Theory...',
			classes: [..._defaults.classes, 'koboldworks', 'theory'],
			resizable: true,
			submitOnClose: false,
			submitOnChange: true,
			closeOnSubmit: false,
			width: 'auto',
			height: 'auto',
			// tabs: [{ navSelector: 'nav.attacks', contentSelector: 'content.attacks', initial: 'attack.0' }]
		};
	}

	get template() {
		return `modules/${CFG.id}/template/item-theory.hbs`;
	}

	getBaseData() {
		const item = this.item;
		const action = this.action;
		const atkData = action ?? item.system;

		const held = action.held || item.system.held || '1h';

		const context = {
			isSpell: item.type === 'spell',
			isTwoHanded: held === '2h',
			isRanged: ['rwak', 'rsak', 'rcman'].includes(atkData.actionType),
			isOffhand: held === 'oh' || (!action.primaryAttack && item.system.attackType === 'natural'),

			options: {
				size: Object.values(pf1.config.actorSizes).reduce((sizes, v, index) => {
					sizes[index] = v;
					return sizes;
				}, {}),
			},

			size: 4,

			hasAttacks: true,

			rollData: action.getRollData(),
		};

		// Size
		context.size = this._config.size ?? context.rollData.size ?? 4;
		context.rollData.size = context.size;
		this._config.size = context.size;

		// Apply CL offset
		const clOffset = this._config.clOffset ?? 0;
		context.rollData.cl += clOffset;

		// Add attacks
		context.attacks = this.action.getAttacks({ full: true, resolve: true, bonuses: true }).reduce((arr, atk) => {
			arr.push(new DamageInstance(`BAB [${signNum(atk.bonus)}]`, `${atk.bonus}`));
			return arr;
		}, []);

		context.full = this._config.full;

		// Haste extra attack
		const hasteAttack = {
			label: 'Haste',
			mod: parseInt(context.attacks[0].formula),
			enabled: this._config.haste,
			damage: {},
		};
		context.attacks.splice(1, 0, hasteAttack);

		return context;
	}

	async baseDamageParts(item, action, rollData) {
		const atkDta = action ?? item.system;
		const dmg = atkDta.damage;

		const allDamage = [];

		const doPart = async (d, metatype) => {
			const roll = await RollPF.safeRoll(d.formula, rollData, undefined, undefined, { async: true });
			return [d, roll, metatype];
		};

		// TODO: Batch await
		const dmgParts = dmg.parts?.map(d => doPart(d, METATYPES.normal)) ?? [];
		const nonCritParts = dmg.nonCritParts?.map(d => doPart(d, METATYPES.nonCritical)) ?? [];

		const parts = await Promise.all([...dmgParts, ...nonCritParts]);

		parts.forEach(([d, roll, metatype]) => {
			const { formula, type } = d;
			allDamage.push({ formula, roll, type, metatype, enabled: true });
		});

		/*
		dmg.critParts.forEach(d => {
			const [formula, type] = d;
			const roll = RollPF.safeRoll(formula, rollData);
			allDamage.push({ formula, type,  metatype: METATYPES.nonCritical, enabled: true });
		});
		*/

		return allDamage;
	}

	async getDamage(data, rollData) {
		const item = this.item,
			action = this.action,
			itemData = item.system;

		const baseDamage = await this.baseDamageParts(item, action, rollData);
		const enhOverride = action.enh?.value ?? null;
		const enhBonus = enhOverride ?? itemData.enh ?? 0;

		// Ability to damage
		// console.log({ item });
		const actorData = item.actor?.system;
		if (actorData) {
			const abl = action.ability.damage;
			const mult = action.ability.damageMult;
			if (abl?.length && Number.isFinite(mult)) {
				const formula = `floor(@abilities.${abl}.mod * @action.ability.damageMult)`;
				const roll = RollPF.safeRollSync(formula, rollData);
				baseDamage.push(new DamageInstance(i18n.get(`PF1.Ability${abl.capitalize()}`), formula, roll, 'inherit', METATYPES.normal));
			}
		}

		if (enhBonus > 0) baseDamage.push(new DamageInstance('Enhancement', `${enhBonus}`, RollPF.safeRollSync(`${enhBonus}`), 'inherit', METATYPES.normal));

		const allDmg = [];
		const condDmg = {};
		itemData.conditionals?.forEach(cd => {
			cd.modifiers?.forEach(mod => {
				if (mod.target !== 'damage') return;

				let store;
				if (mod.subTarget === 'allDamage') {
					store = allDmg;
				}
				else {
					const re = /attack\.(?<attack>\d+)/.exec(mod.subTarget);
					if (!re) return;
					const attack = parseInt(re.groups.attack);
					if (!condDmg[attack]) condDmg[attack] = [];
					store = condDmg[attack];
				}
				const di = new DamageInstance(cd.name, mod.formula, null, mod.type, mod.critical);
				di.enabled = cd.default;
				store.push(di);
			});
		});

		const extendedDamage = [];
		for (const d of allDmg) {
			const roll = await RollPF.safeRoll(d.formula, rollData, undefined, undefined, { async: true });
			const di = new DamageInstance(d.label, d.formula, roll, d.type, d.metatype);
			di.conditional = true;
			di.enabled = d.enabled;
			extendedDamage.push(di);
		}

		const optionalDamage = [];
		// Power Attack, Deadly Aim, Piranha Strike
		// Off-hand = 1, Ranged = 2, Two-handed = 3, anything else is 2
		// TODO: Primary/secondary natural
		const powerAttackDamage = data.isOffhand ? 1 : data.isRanged ? 2 : data.isTwoHanded ? 3 : 2;
		const bab = rollData.attributes?.bab.total;
		if (bab !== undefined) {
			const powerAttackSteps = (bab + 4) / 4;
			const formula = `${powerAttackDamage} * floor((@attributes.bab.total + 4) / 4)`;
			const roll = RollPF.safeRollSync(formula, rollData);
			rollData.powerAttackDamage = roll.total;
			rollData.powerAttackPenalty = -powerAttackSteps;
			const di = new DamageInstance('Power Attack/Deadly Aim/Piranha Strike', formula, roll, METATYPES.normal);
			di.enabled = false;
			di.custom = false;
			di.verbose = true;
			di.powerAttack = true;
			optionalDamage.push(di);
		}

		return { baseDamage, optionalDamage, extendedDamage, allDmg, condDmg };
	}

	fillAttackData(atk, rollData, stats) {
		atk.error = atk.roll?.err ?? false;
		if (atk.error) atk.verbose = true;
		atk.originalFormula = atk.formula;
		const formula = atk.roll.formula || atk.formula;
		atk.strippedFormula = unflair(pf1.utils.formula.simplify(formula, rollData));
		atk.max = atk.roll.reroll({ maximize: true });
		atk.min = atk.roll.reroll({ minimize: true });
		const l_est = (atk.max.total - atk.min.total) / 2 + atk.min.total;
		const l_avg = (atk.max.total + atk.min.total) / 2;
		atk.avg = maxPrecision(l_avg, 2);
		atk.est = maxPrecision(l_est, 2);
		atk.static = atk.max.total === atk.min.total && atk.roll.isDeterministic;
		if (atk.base) atk.enabled = true;
		if (atk.enabled) {
			stats.avg += l_avg;
			stats.est += l_est;
			stats.min += atk.min.total;
			stats.max += atk.max.total;
		}

		atk.finalFormula = atk.roll.formula;

		atk.staticFormula = atk.originalFormula == atk.finalFormula;

		atk.variables = getVariables(atk.formula);
		if (atk.variables.length) {
			atk.variablesFormatted = atk.variables.map(v => `${v} = ${foundry.utils.getProperty(rollData, v)}`).join('\n');
			atk.verbose = true; // force verbosity
		}
	}

	// Custom formula
	async getCustom(rollData, stats) {
		const custom = this._config.damage.custom ?? { formula: '', enabled: true };
		custom.custom = true;

		custom.roll = await RollPF.safeRoll(custom.formula || '0', rollData, undefined, undefined, { async: true });
		if (custom.formula.length)
			custom.verbose = !!custom.variables;
		else {
			custom.static = true;
			custom.verbose = false;
		}

		return custom;
	}

	async getData() {
		const item = this.item,
			action = this.action,
			itemData = item.system;

		this.setTitle();

		const context = { ...super.getData(), ...this.getBaseData(), item, action };
		// console.log('getData:', data);

		context.actions = item.actions.reduce((actions, act) => {
			if (act.hasDamage) actions[act.id] = act.name;
			return actions;
		}, {});
		context.selectedAction = action.id;

		const isTwoHanded = context.isTwoHanded,
			isRanged = context.isRanged,
			isOffhand = context.isOffhand;

		// TODO: Buffs

		if (this._config.manyshot) {
			//
		}

		const isCritical = false;

		const damage = await this.getDamage(context, context.rollData);

		const stats = { avg: 0, est: 0, min: 0, max: 0 };

		const custom = await this.getCustom(context.rollData, stats);

		const all = [...damage.baseDamage, ...damage.extendedDamage, ...damage.optionalDamage, custom];

		// console.log({ config: this._config });
		// Fill data for all damage instances
		all.forEach((atk, index) => {
			const attacks = this._config.attacks;
			atk.enabled = attacks[0]?.damage?.[index]?.enabled ?? atk.enabled ?? false;
			atk.index = index;
			this.fillAttackData(atk, context.rollData, stats);
		});

		const total = { avg: maxPrecision(stats.avg, 2), est: maxPrecision(stats.est, 2), min: stats.min, max: stats.max, static: stats.min === stats.max };

		// Construct final data structure
		const derived = {
			damage: {
				base: damage.baseDamage,
				extended: damage.extendedDamage,
				optional: damage.optionalDamage,
				all,
				grouped: [
					{
						label: null,
						base: true,
						conditional: false,
						custom: false,
						items: damage.baseDamage,
					},
					{
						label: 'Conditional',
						base: false,
						conditional: true,
						custom: false,
						items: damage.extendedDamage,
					},
					{
						label: 'Optional',
						base: false,
						conditional: false,
						custom: false,
						items: damage.optionalDamage,
					},
					{
						label: 'Custom',
						base: false,
						conditional: false,
						custom: true,
						items: [custom],
					},
				],
				hasPerAttack: !foundry.utils.isEmpty(damage.condDmg),
				perAttack: damage.condDmg,
				custom,
				total,
			},
			attacks: this._config.attacks,
			config: this._config,
		};

		context.attacks.forEach(atk => {
			atk.damage = {
				groups: derived.damage.grouped,
				total: derived.damage.total,
			};
		});

		context.attack = context.attacks[0];

		const attackCount = context.attacks.length + (this._config.haste ? 1 : 0);
		// console.log(data.attackArray, attackCount);
		// console.log(data.attack);

		context.fullAttack = {
			est: maxPrecision(attackCount * context.attack.damage.total.est, 2),
			static: false,
			attackCount,
			min: maxPrecision(attackCount * context.attack.damage.total.min, 2),
			max: maxPrecision(attackCount * context.attack.damage.total.max, 2),
		};

		context.d = derived;

		// console.log('getData:', data);

		context.uuid = `${item.uuid}-theory`;

		return context;
	}

	_updateObject(event, formData) {
		const data = foundry.utils.expandObject(formData);
		// console.log('_updateObject:', { data });

		// Objects to arrays
		data.attacks = Object.values(data.attacks);
		data.attacks.forEach(atk => atk.damage);

		data.damage.custom = foundry.utils.mergeObject(baseDamageData(), data.damage.custom);
		data.damage.custom.enabled = data.attacks[0].damage.custom.enabled;

		foundry.utils.mergeObject(this._config, data);

		if (data.action)
			this.action = this.item.actions.get(data.action);

		this.render();
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = jq[0];

		const sliders = html.querySelectorAll('input[type="range"]');
		sliders.forEach(slider => {
			const label = slider.nextElementSibling;
			slider.addEventListener('input', () => label.textContent = `${slider.value}%`, { passive: true });
		});

		this.setPosition();
	}
}
