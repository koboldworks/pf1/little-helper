import { Ancillary } from '@root/ancillary.mjs';
import { Feature } from '@core/feature.mjs';
import { TheoryDialog } from './theory-dialog.mjs';
import { FAIcons } from '@core/icons.mjs';
import { addCoreTooltip } from '@root/common.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param {SharedData} shared
 */
function injectTooltip(sheet, [html], options, shared) {
	// const pv = sheet.__littleHelper?.unidentifiedView ?? false;
	const app = html.closest('.app[data-appid]');
	const button = app?.querySelector('header > .little-helper-theory-button');
	if (button) addCoreTooltip(button, 'Damage Theory');
}

/**
 * @param {ItemSheet} sheet
 * @param {Array<object>} buttons
 */
function theoryHeaderButton(sheet, buttons) {
	const doc = sheet.document ?? sheet.action;

	const item = doc.item ?? doc,
		action = doc instanceof Item ? doc.defaultAction : doc;

	if (!doc.hasDamage) return;

	const button = {
		label: '',
		icon: FAIcons.search,
		class: 'little-helper-theory-button',
		onclick: _ => new TheoryDialog(item, action).render(true),
	};

	buttons.unshift(button);
	// buttons.splice(buttons.length - 1, 0, button);
}

function enable() {
	Hooks.on('getItemSheetHeaderButtons', theoryHeaderButton);
	Hooks.on('getItemActionSheetHeaderButtons', theoryHeaderButton);
	Ancillary.register('item', injectTooltip);
	Ancillary.register('action', injectTooltip);
}

function disable() {
	Hooks.off('getItemSheetHeaderButtons', theoryHeaderButton);
	Hooks.off('getItemActionSheetHeaderButtons', theoryHeaderButton);
	Ancillary.unregister('item', injectTooltip);
	Ancillary.unregister('action', injectTooltip);
}

new Feature({ setting: 'item-theory', label: 'Theory', hint: 'Adds a dialog button to item sheet header with theoretical data about them.', category: 'items', enable, disable, stage: 'ready' });
