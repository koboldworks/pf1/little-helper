import { CFG } from './config.mjs';

import { i18n } from './utility/i18n.mjs';

import './common.mjs';
import '@core/settings.mjs';

import './data/_init.mjs';

import './ancillary.mjs';
import '@core/feature.mjs';

import '@core/feature-setup.mjs';

import './context/_init.mjs';
import './modules/_init.mjs';

import './info/_init.mjs';

import './config/_init.mjs';

import '@core/keybindings.mjs';

import './migration/migration.mjs';

// Load HBS templates
Hooks.once('setup', () => {
	loadTemplates([`modules/${CFG.id}/template/subsettings.hbs`]);
});

// i18n support
Hooks.once('setup', function i18nSetup() {
	// Hack to counter Foundry allowing dependencies to be disabled
	if (!game.modules.get('lib-wrapper')?.active)
		ui.notifications.error('Little Helper requires libWrapper module to be enabled to function correctly', { permanent: true });

	const C = CFG.console.colors;
	console.debug('%cLittle Helper%c 🦎 | i18n | Calling pre-translation hooks', C.main, C.unset);
	Hooks.callAll('little-helper.i18n', CFG.i18n);

	console.debug('%cLittle Helper%c 🦎 | i18n | Pre-translating', C.main, C.unset);
	Object.values(CFG.i18n)
		.forEach(category => {
			Object.entries(category).forEach(([key, value]) => {
				try {
					if (i18n.has(value)) {
						category[key] = i18n.get(value);
					}
					else {
						category[key] = value;
					}
					category[key] = category[key].replace(/\n/g, '<br>'); // Add line-breaks
				}
				catch (err) {
					console.error(category, key, value);
					delete category[key];
					throw err;
				}
			});
		});
});

Hooks.once('init', () => {
	game.modules.get(CFG.id).api = CFG.API;

	if (game.modules.get('quench')?.active) {
		// This import fails for unknown reasons
		Hooks.on('quenchReady', async function () {
			try {
				await import('./tests/unit-tests.mjs')
					.then(m => m.registerTests())
					.catch(err => console.error(err));
			}
			catch (err) {
				console.error(err);
			}
		});
	}
});
