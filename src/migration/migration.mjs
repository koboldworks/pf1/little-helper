import { CFG } from '@root/config.mjs';
import { migrateItemFlags } from '@root/config/item-config.mjs';

function limitPrecision(number, decimals = 2) {
	const mult = Math.pow(10, decimals);
	return Math.floor(number * mult) / mult;
}

// pf1PostReady is fired after system migration is done
Hooks.once('pf1PostReady', async () => {
	const activeGM = game.users.activeGM;
	if (activeGM) {
		if (!activeGM?.isSelf) return;
	}

	const migration = game.settings.get(CFG.id, 'migration');
	const mod = game.modules.get(CFG.id);

	const C = CFG.console.colors;
	if (migration === mod.version || foundry.utils.isNewerVersion(migration, mod.version)) return void console.debug('%cLittle Helper%c 🦎 | Migration | Not needed', C.main, C.unset);

	console.log(`%cLittle Helper%c 🦎 | Migration | ${migration} -> ${mod.version}`, C.main, C.unset);

	const actors = game.actors.filter(a => a.isOwner);
	let pct10 = Math.max(25, Math.floor(actors.length / 10));
	if (actors.length < 50) pct10 = 100;
	let i = 0, migrated = 0;
	for (const actor of actors) {
		i += 1;
		if (i % pct10 === 0) console.log('%cLittle Helper%c 🦎 | Migration |', C.main, C.unset, limitPrecision((i / actors.length) * 100, 1), '% |', i, 'out of', actors.length);
		for (const item of actor.items) {
			const rv = await migrateItemFlags(item);
			if (rv) migrated += 1;
		}
	}

	console.log('%cLittle Helper%c 🦎 | Migration | Complete', C.main, C.unset, '|', migrated, 'Items Adjusted');
	if (game.users.activeGM?.isSelf) game.settings.set(CFG.id, 'migration', mod.version);
});
