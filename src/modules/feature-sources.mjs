/**
 * Support https://gitlab.com/mkahvi/fvtt-micro-modules/-/tree/master/pf1-feature-sources
 * BUG: This is incredibly cumbersome.
 */

import { Feature } from '../core/feature.mjs';
import { Ancillary } from '../ancillary.mjs';

import { delSheetMarker, setSheetMarker } from '../common.mjs';

const marker = 'featureSources';
let markerId = 0;

const levelConflicting = {
	feat: ['level', 'class'],
	classFeat: ['feat'],
};

/**
 * @param {ActorSheetPF} sheet
 * @param {JQuery} html
 * @param _opts
 * @param {SharedData} shared
 */
async function featureSourceConflicts(sheet, [html], _opts, shared) {
	let cancel = false;
	const _markerId = markerId++;
	setSheetMarker(sheet, marker, _markerId, () => cancel = true);

	const actor = sheet.document;
	const feats = {};
	// Filter by subtype
	actor.itemTypes.feat?.forEach(i => {
		const subType = i.system.featType;
		if (feats[subType] === undefined) feats[subType] = { items: [], conflicts: [] };
		feats[subType].items.push(i);
	});

	// Build lists by type
	Object.entries(levelConflicting).forEach(e => {
		const [subType, conflicts] = e;
		const items = feats[subType]?.items;
		if (!(items?.length > 1)) return;

		const levels = {};
		items.forEach(i => {
			const type = i.flags?.world?.['source_type'];
			if (!conflicts.includes(type)) return;
			if (levels[type] === undefined) levels[type] = {};
			const level = i.flags?.world?.['source_level'];
			if (level === undefined) return;
			if (levels[type][level] === undefined) levels[type][level] = [];
			levels[type][level].push({ item: i, type, level });
		});
		feats[subType].levels = levels;
	});

	const majorconflicts = [];
	Object.entries(levelConflicting).forEach(e => {
		const [subType, conflicts] = e;
		if (!feats[subType]?.levels) return;
		Object.values(feats[subType].levels).forEach(vs => Object.values(vs).forEach(vsis => vsis.length > 1 && majorconflicts.push(vsis)));
	});

	if (cancel || majorconflicts.length === 0) {
		delSheetMarker(sheet, marker, _markerId);
		return;
	}

	//
	const conflictingItems = majorconflicts.flat().map(i => i.item);
	// console.log(conflictingItems);
	shared.tabs.feats?.tab
		?.querySelectorAll('.item[data-item-id]')
		?.forEach(el => {
			if (cancel) return;
			const id = el.dataset.itemId;
			const item = conflictingItems.find(i => i.id === id);
			if (!item) return;
			el.classList.add('lil-feature-source-conflict');
		});

	delSheetMarker(sheet, marker, _markerId);
}

function enable() {
	if (!game.modules.get('mkah-pf1-feature-sources')?.active) return;

	Ancillary.register('actor', featureSourceConflicts);
}

function disable() {
	Ancillary.unregister('actor', featureSourceConflicts);
}

new Feature({ setting: 'module-feature-sources', label: 'Feature Sources', hint: 'Attempts to display source conflicts on actor sheet. This is slow procedure and may impact sheet performance.', category: 'module', enable, disable, stage: 'ready', requirements: { module: 'mkah-pf1-feature-sources' } });
