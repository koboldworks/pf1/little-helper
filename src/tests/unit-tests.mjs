const MODULE_ID = 'koboldworks-pf1-little-helper';

export function registerTests() {
	try {
		const api = game.modules.get(MODULE_ID).api;

		/* global quench */
		quench.registerBatch(
			`${MODULE_ID}.formulas`,
			async function formulaResolution({ describe, it, expect, before, after } = {}) {
				describe('Formula Resolution', function resolveFormulas() {
					const rollData = {
						dex: 2,
						bab: 17,
						size: 4,
					};

					const formulas = {
						'3d6+(@dex)d6+2': '3d6+2d6+2',
						'@dex > 1 ? 5 : 2': '5',
						'@dex > 1 ? (2 < 1 ? 3 : 4) : 5': '4',
						'sizeRoll(1,12,@size)+1d8+6+2+1-2-6+2': '1d12+1d8+3',
						'-(1 + (floor(@bab / 4))) + 1': '-4',
						'@formulaicAttack * -5': '0',
						'2 <= 4 ? 1 : (floor(2 / 11 + 1)d6)': '1',
						'2 <= 4 ? 1 : floor(2 / 11 + 1)d6': '1',
						'4>=10 ? 0 : 4>=5 ? -2 : -4': '-4',
						'2 + 2 * 2 ** 2': '10',
						'2 + 2 ** 2 * 2': '10',
						'-5 + 1': '-4',
						'2 * 2 + -6 / 2': '1',
						'4d6-0+2': '4d6+2',
						'sizeRoll(1, 12)': '1d12',
						'sizeRoll(2, 6, 5) + 5 + 2': '3d6+7',
						'(floor(5/2))d6': '2d6',
						'max(1d6,4)': 'max(1d6,4)',
						'(--2 + 3)': '5', // Resulted in NaN before
						'1d8 + min(2,3)': '1d8+2',
						'sizeRoll(1, 8, @size) + 2 + 2': '1d8+4',
					};

					Object.entries(formulas).forEach(([formula, expected]) => {
						it(formula, function () {
							try {
								const rv = api.formula.simplify(formula, rollData, { debug: true });
								console.log('Formula Testing:', rv.formula, '=', expected, rv);
								expect(rv.formula).to.equal(expected);
							}
							catch (err) {
								console.warn('Failed to parse:', formula);
								throw err;
							}
						});
					});
				});
			},
			{ displayName: 'Koboldworks – Little Helper 🦎' },
		);
	}
	catch (err) {
		console.error(err);
	}
}
