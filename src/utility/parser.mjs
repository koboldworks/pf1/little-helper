// NOTES

// Match all @variables
/(@\b[\w.]+)\b/g;

// Match deepest parenthesis.
// Needs to be repeated after eliminating each parenthetical
/\([^()]+\)/g;
// or
/\B\(([^()]+)\)/g;

// Match functions
/\b(\w+)(\([^()]+\))/g;

// Match parenthesis and functions
// Needs to be repeated for nested parenthesis
/(\w+)?\(([^()]+)\)/g;
// $1 = empty for nonfunctions

// Test data used:
/*
(5+1)
10+floor(@classes.paladin.level/2)+@abilities.cha.mod
10 + floor(@classes.paladin.level / 2 ) + @abilities.cha.mod
10 + floor(@classes.paladin.level / 2 + (2+(@deep+1)/5)) + @abilities.cha.mod
(ab / (5 + 2))

5d10
(@classes.boob/2)d(@classes.d.level+2)

@classes.paladin.level-2
@_linked.classes.paladin.level-2
@_linked.classes.paladin.level/2
@_linked.classes.paladin.level*2
*/
